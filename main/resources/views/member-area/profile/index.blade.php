@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Members Update</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Membership</a></li>
                    <li class="breadcrumb-item active">Members Update</li>
                </ol>
            </div>

        </div>
    </div>
</div>

                    <div class="position-relative mx-n4 mt-n4">
                        <div class="profile-wid-bg profile-setting-img">
                            <img src="" class="profile-wid-img" alt="" onerror="this.onerror=null;this.src='<?= url('/') ?>/assets/images/profile-bg.jpg'">
                            <div class="overlay-content">
                                <div class="text-end p-3">
                                    <div class="p-0 ms-auto rounded-circle profile-photo-edit">
                                        <label for="profile-foreground-img-file-input" class="profile-photo-edit btn btn-light">
                                            <i class="ri-medal-line align-bottom me-1"></i> {{ $level }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xxl-3">
                            <div class="card mt-n5">
                                <div class="card-body p-4">
                                    <div class="text-center">
                                        <div class="profile-user position-relative d-inline-block mx-auto  mb-4">
                                            <img src="" class="rounded-circle avatar-xl img-thumbnail user-profile-image" alt="user-profile-image" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/icon_user.jpg'">
                                            <div class="avatar-xs p-0 rounded-circle profile-photo-edit">
                                                <label for="profile-img-file-input" class="profile-photo-edit avatar-xs">
                                                    <span class="avatar-title rounded-circle bg-light text-body">
                                                        <i class="ri-medal-line"></i>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <h5 class="fs-16 mb-1">{{ $members->first_name }} {{ $members->last_name }}</h5>
                                        <p class="text-muted mb-0">{{ $get_profesi->name }}</p>
                                    </div>
                                </div>
                            </div>
                            <!--end card-->
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-5">
                                        <div class="flex-grow-1">
                                            <h5 class="card-title mb-0">Complete Your Profile</h5>
                                        </div>
                                    </div>
                                    <div class="progress animated-progress custom-progress progress-label">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
                                            <div class="label">80%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card" style="display: none">
                                <div class="card-body">
                                    <div class="d-flex align-items-center mb-4">
                                        <div class="flex-grow-1">
                                            <h5 class="card-title mb-0">Social Media</h5>
                                        </div>
                                    </div>
                                    <div class="mb-3 d-flex">
                                        <div class="avatar-xs d-block flex-shrink-0 me-3">
                                            <span class="avatar-title rounded-circle fs-16 bg-dark text-light">
                                                <i class="ri-github-fill"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" id="github" placeholder="@username" value="{{ $members->github }}" autocomplete="off" name="github">
                                    </div>
                                    <div class="mb-3 d-flex">
                                        <div class="avatar-xs d-block flex-shrink-0 me-3">
                                            <span class="avatar-title rounded-circle fs-16 bg-primary">
                                                <i class="ri-facebook-fill"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" id="facebook" placeholder="@username" value="{{ $members->facebook }}" autocomplete="off" name="facebook">
                                    </div>
                                    <div class="mb-3 d-flex">
                                        <div class="avatar-xs d-block flex-shrink-0 me-3">
                                            <span class="avatar-title rounded-circle fs-16 bg-success">
                                                <i class="ri-instagram-fill"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" id="instagram" placeholder="@username" value="{{ $members->instagram }}" autocomplete="off" name="instagram">
                                    </div>
                                </div>
                            </div>
                            <!--end card-->
                        </div>
                        <!--end col-->
                        <div class="col-xxl-9">
                            <div class="card mt-xxl-n5">
                                <div class="card-header">
                                    <ul class="nav nav-tabs-custom rounded card-header-tabs border-bottom-0" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#personalDetails" role="tab">
                                                <i class="fas fa-home"></i> Personal Details
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#changePassword" role="tab">
                                                <i class="far fa-user"></i> Change Password
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body p-4">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="personalDetails" role="tabpanel">
                                              <form action="{{ url('pg_profile/update') }}" method="post" enctype="multipart/form-data">
                                               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                               <input type="hidden" name="code_id" value="{{ $members->uuid }}">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="firstnameInput" class="form-label">First Name</label>
                                                            <input type="text" class="form-control" id="firstnameInput" placeholder="Enter your firstname" name="first_name" autocomplete="off" value="{{ $members->first_name }}">
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="lastnameInput" class="form-label">Last Name</label>
                                                            <input type="text" class="form-control" id="lastnameInput" placeholder="Enter your lastname" name="last_name" autocomplete="off" value="{{ $members->last_name }}">
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="phonenumberInput" class="form-label">Phone Number</label>

                                                             <div class="input-group">
                                                              <span class="input-group-text" id="basic-addon1"><img src="{{ asset('assets/images/flags/id.svg') }}" alt="flag img" height="20" class="country-flagimg rounded"><span class="ms-2 country-codeno">+ 62</span></span>
                                                              <input type="text" class="form-control"  placeholder="Enter your phone number" name="phone" autocomplete="off" value="{{ $members->phone }}" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                                                          </div>
                                                           
                                                      </div>
                                                    </div>


                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="emailInput" class="form-label">Email Address</label>
                                                            <input type="email" class="form-control" id="emailInput" name="email" placeholder="Enter your email" autocomplete="off" value="{{ $members->email }}">
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="phonenumberInput" class="form-label">Join Date</label>
                                                            <input type="text" class="form-control" id="phonenumberInput" placeholder="" autocomplete="off" value="{{ date('d F Y', strtotime($members->join_date)) }}" readonly>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="countryInput" class="form-label">Profession</label>
                                                            <select class="js-example-basic-single form-control" name="profession_id" id="profession_id">
                                                              @foreach($profession as $val)
                                                              <option value="{{ $val->id }}" <?php if($val->id == $members->profession_id) echo 'selected="selected"';?>>{{ $val->name }}</option>
                                                              @endforeach
                                                          </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="mb-3">
                                                            <label for="countryInput" class="form-label">Country</label>
                                                            <select class=" form-control" name="country_id" id="country_id">
                                                              @foreach($country as $val)
                                                              <option value="{{ $val->id }}" <?php if($val->id == $members->country_id) echo 'selected="selected"';?>>{{ $val->name }}</option>
                                                              @endforeach
                                                          </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="mb-3">
                                                            <label for="" class="form-label">Province</label>
                                                            <select class="js-example-basic-single form-control" name="province_id" id="province_id" onchange="getprovince()">
                                                          </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="mb-3">
                                                            <label for="" class="form-label">City</label>
                                                            <select class="js-example-basic-single form-control" name="city_id" id="city_id">
                                                          </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-12">
                                                        <div class="mb-3 pb-2">
                                                            <label for="" class="form-label">Motto</label>
                                                            <textarea class="form-control" id="desc" placeholder="Enter your description" name="desc" rows="3">{!! $members->desc !!}</textarea>
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-12">
                                                        <div class="hstack gap-2 justify-content-end">
                                                            <button type="submit" class="btn btn-primary"><i class="ri-save-line align-bottom me-1"></i> Updates</button>
                                                            <a href="{{ url('membership/members') }}">
                                                            <button type="button" class="btn btn-danger"><i class="ri-close-line align-bottom me-1"></i> Cancel</button>
                                                          </a>
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                </div>
                                                <!--end row-->
                                            </form>
                                        </div>
                                        <!--end tab-pane-->
                                        <div class="tab-pane" id="changePassword" role="tabpanel">
                                           <form action="{{ url('pg_password/update') }}" method="post" enctype="multipart/form-data">
                                               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                               <input type="hidden" name="code_id" value="{{ $members->id }}">
                                                <div class="row g-2">
                                                   
                                                    <div class="col-lg-6">
                                                        <div>
                                                            <label for="" class="form-label">New Password *</label>
                                                            <input type="password" class="form-control" id="password1" name="password" placeholder="Enter new password" onkeyup="getPass()" autocomplete="off" required minlength="6">
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div>
                                                            <label for="" class="form-label">Confirm Password *</label>
                                                            <input type="password" name="password2" class="form-control" id="password2" placeholder="Confirm password" onkeyup="getPass2()" autocomplete="off" required minlength="6">
                                                        </div>
                                                         <div id="notif_pass"></div>
                                                    </div>
                                                   
                                                    <div class="col-lg-12">
                                                        <div class="text-end">
                                                            <button type="submit" id="btnSubmit" class="btn btn-success">Change Password</button>
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                </div>
                                                <!--end row-->
                                            </form>
                                            <div class="mt-4 mb-3 border-bottom pb-2">
                                                <h5 class="card-title">Login History</h5>
                                            </div>
                                            @foreach($logs_activity as $val)
                                            <div class="d-flex align-items-center mb-3">
                                                <div class="flex-shrink-0 avatar-sm">
                                                    <div class="avatar-title bg-light text-primary rounded-3 fs-18">
                                                        @if($val->type == 'mobile')
                                                        <i class="ri-smartphone-line"></i>
                                                        @else
                                                        <i class="ri-macbook-line"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="flex-grow-1 ms-3">
                                                    <h6>
                                                        {{ $val->device }} {{ $val->platform }} - {{ $val->type }}

                                                    </h6>
                                                    <p class="text-muted mb-0">
                                                    {{ $val->region }} {{ $val->country }}, {{ $val->city }} - {{ date('d M Y H:i', strtotime($val->created_at)) }}</p>
                                                </div>
                                                <div>
                                                    <a href="javascript:void(0);">{{ Carbon\Carbon::parse($val->created_at)->diffForHumans()}}</a>
                                                </div>
                                                
                                            </div>
                                            @endforeach

                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->

@section('page-script')
<script type="text/javascript">

  $( document ).ready(function() {
    getcountry();
});

  function getcountry(){
    $("#province_id").empty();
    var country = $("#country_id").val();
    $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ url("pg_wilayah_json")}}',
            data: 'id='+country + '&param=1',
            success: function (data) {
              $("#province_id").append(data);
              $("#province_id").val("{{ $members->province_id }}");

              getprovince();
         }
        });
  }

  function getprovince(){
    $("#city_id").empty();
    var province = $("#province_id").val();
    $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ url("pg_wilayah_json")}}',
            data: 'id='+province + '&param=2',
            success: function (data) {
              $("#city_id").append(data);
              $("#city_id").val("{{ $members->city_id }}");
         }
        });
  }

  function getPass(){
        $("#password2").val("");
        $("#notif_pass").html("");
        $("#btnSubmit").hide();
    }
    function getPass2(){
        var passsword1 = $("#password1").val();
        var passsword2 = $("#password2").val();
        if(passsword1 == passsword2){
            $("#notif_pass").html('<span style="font-size: 13px;  color: green;"><i class="ri-check-double-fill align-bottom me-1"></i> Password Sama</span>');
            $("#btnSubmit").show();
        }else{
           $("#notif_pass").html(' <span style="font-size: 13px;  color: red;"><i class=" ri-close-fill align-bottom me-1"></i> Password Tidak Sama</span>');
           $("#btnSubmit").hide();
       }
   }
</script>
@stop
@endsection