@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Form Ticket</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Helpdesk</a></li>
                    <li class="breadcrumb-item active">Form Ticket</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<form action="{{ url('pg_ticket/action') }}" method="post" enctype="multipart/form-data" novalidate>
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="mb-3">
                    <label class="form-label" for="project-title-input">Subject <code>*</code></label>
                    <input type="text" autocomplete="off" class="form-control" id="subject" name="subject" placeholder="Enter subject" required >
                </div>

                 <div class="mb-3">
                            <label for="choices-priority-input" class="form-label">Category <code>*</code></label>
                            <select class="form-select" data-choices data-choices-search-false id="category_id" name="category_id" required>
                                <option value="">Choose Category</option>
                                @foreach($category_ticket as $val)
                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                @endforeach
                            </select>
                        </div>

                <div class="mb-3">
                    <label class="form-label" for="project-thumbnail-img">Attachment <code>*</code> </label>
                    <input class="form-control" id="file" name="file"  type="file" accept="image/png, image/gif, image/jpeg" >
                </div>

                <div class="mb-3">
                    <label class="form-label">Description</label>
                    <textarea id="ckeditor-classic" name="desc" required></textarea>
                </div>

                  <div class="text-end mb-4">

            <a href="{{ url('pg_ticket') }}">
                <button type="button" class="btn btn-success"><i class="ri-arrow-left-line align-bottom me-1"></i> Back</button>
            </a>
                <button type="submit" class="btn btn-primary"><i class="ri-save-line align-bottom me-1"></i> Submit</button>
            <button type="reset" class="btn btn-danger"><i class="ri-close-line align-bottom me-1"></i> Cancel</button>
        </div>

            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->


        <!-- end card -->
      
    </div>
  </div>
</form>

@section('page-script')

@stop
@endsection