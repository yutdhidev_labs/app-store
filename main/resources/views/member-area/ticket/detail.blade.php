@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Ticket Detail</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Helpdesk</a></li>
                    <li class="breadcrumb-item active">Ticket Detail</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card mt-n4 mx-n4">
            <div class="bg-soft-warning">
                <div class="card-body pb-0 px-4">
                    <div class="row mb-3">
                        <div class="col-md">
                            <div class="row align-items-center g-3">
                                <div class="col-md-auto">
                                    <div class="avatar-md">
                                        <div class="avatar-title bg-white rounded-circle">
                                            <img src="{{ asset('dist/ticket.png') }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'" alt="" class="avatar-md">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div>
                                        <h4 class="fw-semibold" id="ticket-title">#{{ $ticket->ticket_code }}</h4>
                                        <div class="hstack gap-3 flex-wrap">
                                            <div class="text-muted"><i class="ri-building-line align-bottom me-1"></i><span id="ticket-client">{{ $ticket->first_name }} {{ $ticket->last_name }}</span></div>
                                            <div class="vr"></div>
                                            <div class="text-muted">Create Date : <span class="fw-medium " id="create-date">{{ date('d M Y', strtotime($ticket->ticket_date)) }} - {{ Carbon\Carbon::parse($ticket->ticket_date)->diffForHumans()}}</span></div>
                                            <div class="vr"></div>
                                            @if($ticket->ticket_status == 'Waiting')
                                            <div class="badge rounded-pill bg-success fs-12" id="ticket-priority">Waiting</div>
                                            @elseif($ticket->ticket_status == 'Inprogress')
                                            <div class="badge rounded-pill bg-primary fs-12" id="ticket-priority">Inprogress</div>
                                            @elseif($ticket->ticket_status == 'Close')
                                            <div class="badge rounded-pill bg-danger fs-12" id="ticket-priority">Close</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <ul class="nav nav-tabs-custom border-bottom-0" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active fw-semibold" data-bs-toggle="tab" href="#project-overview" role="tab">
                                Overview
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- end card body -->
            </div>
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="tab-content text-muted">
            <div class="tab-pane fade show active" id="project-overview" role="tabpanel">
                 @if ($message = Session::get('success'))
                 <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-check-double-line label-icon"></i>
                       {{ $message }}
                  </div>
              @elseif ($message = Session::get('error'))
                  <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-error-warning-line label-icon"></i>
                       {{ $message }}
                  </div>
              @endif
              
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="text-muted">
                                    <h6 class="mb-3 fw-semibold text-uppercase">Subject</h6>
                                    <p>{{ $ticket->subject }}</p>

                                    <h6 class="mb-3 fw-semibold text-uppercase">Category</h6>
                                    <p>{{ $ticket->category_name }}</p>

                                    <h6 class="mb-3 fw-semibold text-uppercase">Description</h6>
                                    {!! $ticket->desc !!}
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->

                        <div class="card">
                            <div class="card-header align-items-center d-flex">
                                <h4 class="card-title mb-0 flex-grow-1">Comments</h4>
                            </div><!-- end card header -->

                            <div class="card-body">
                                @if(count($ticket_detail) > 0)
                                  <?php  $height = '400px';  ?>
                                @else
                                  <?php  $height = '100px';  ?>
                                @endif
                                <div data-simplebar style="<?= $height ?>" class="px-3 mx-n3 mb-2">
                                    @if(count($ticket_detail) > 0)
                                    @foreach($ticket_detail as $val)
                                    <div class="d-flex" style="margin-bottom: 10px">
                                        <div class="flex-shrink-0">
                                            @if($val->roles_id == 4)
                                            <img src="{{ asset('dist/iconuser2.png') }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/iconuser2.png'" alt="" class="avatar-xs rounded-circle" />
                                            @else
                                            <img src="{{ asset('dist/iconcs.jpeg') }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/iconcs.jpeg'" alt="" class="avatar-xs rounded-circle" />
                                            @endif
                                        </div>
                                        <div class="flex-grow-1 ms-3">
                                            <h5 class="fs-13">
                                                {{ $val->first_name }} {{ $val->last_name }}
                                                @if($val->roles_id == 4)
                                                <span class="badge bg-soft-secondary text-secondary align-middle">Members</span>
                                                @else
                                                <span class="badge bg-soft-secondary text-secondary align-middle">Support Team</span>
                                                @endif

                                                <small class="text-muted ms-2">
                                                    {{ date('d M Y - H:i', strtotime($val->created_at)) }}
                                                </small>

                                            </h5>
                                            <p class="text-muted">
                                                {!! $val->desc !!}


                                            </p>
                                             @if(!empty($val->filename))
                                            <div class="pt-0">
                                                <div class="row g-3">
                                                    <div class="col-xxl-12 col-lg-12">
                                                        <div class="border rounded border-dashed p-2">
                                                            <div class="d-flex align-items-center">
                                                                <div class="flex-shrink-0 me-3">
                                                                    <div class="avatar-sm">
                                                                        <div class="avatar-title bg-light text-secondary rounded fs-24">
                                                                            <i class="ri-folder-zip-line"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-grow-1 overflow-hidden">
                                                                    <h5 class="fs-13 mb-1"><a href="#" class="text-body text-truncate d-block">{{ $val->filename }}</a></h5>
                                                                    <div>
                                                                        <?php
                                                                           $file = "dist/img/ticket/".$val->filename;

                                                                           if((is_file($file))&&(file_exists($file))){
                                                                             $size = filesize($file);
                                                                             $mod = 1024;
                                                                             $units = explode(' ','B KB MB GB TB PB');
                                                                             for ($i = 0; $size > $mod; $i++) {
                                                                              $size /= $mod;
                                                                            }
                                                                            echo round($size, 2) . ' ' . $units[$i];
                                                                          }else{
                                                                            echo '0 KB';
                                                                          }   

                                                                          ?>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-shrink-0 ms-2">
                                                                    <div class="d-flex gap-1">
                                                                        <a href="{{ asset('dist/img/ticket') }}/{{ $val->filename }}" target="_blank">
                                                                        <button type="button" class="btn btn-icon text-muted btn-sm fs-18">
                                                                            <i class="ri-eye-line"></i>
                                                                        </button>
                                                                        </a>

                                                                        <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="downloadURI('{{ $file }}', '{{ $val->filename }}');">
                                                                            <i class="ri-download-2-line"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif


                                                <small class="text-muted">
                                                    <i class="ri-time-line align-bottom me-1"></i>
                                                    {{ Carbon\Carbon::parse($val->created_at)->diffForHumans()}}
                                                </small>
                                        </div>
                                    </div>
                                    @endforeach

                                    @else
                                    Comment is empty.
                                    @endif

                                </div>
                                 @if($ticket->ticket_status == 'Inprogress')
                                <form class="mt-4" action="{{ url('pg_ticket_detail/action') }}" method="post" enctype="multipart/form-data">
                                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="row g-3">
                                        <div class="col-12">
                                            <label for="exampleFormControlTextarea1" class="form-label text-body">Leave a Comments</label>
                                            <textarea class="form-control bg-light border-light" id="exampleFormControlTextarea1" name="desc" rows="3" placeholder="Enter your comment..." required></textarea>
                                        </div>
                                        <div class="col-12 text-end">
                                           Attachment : <input type="file" name="file">
                                           <input type="hidden" name="ticket_id" value="{{ $ticket->id }}">

                                            <button type="submit" class="btn btn-success">
                                            Post Comments</a>
                                        </div>
                                    </div>
                                </form>
                                @endif
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->
                    </div>
                    
                </div>
                <!-- end row -->
            </div>
            
        </div>
    </div>
    <!-- end col -->
</div>

@section('page-script')
<script type="text/javascript">
    function downloadURI(uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.setAttribute("target", "_blank");
    link.click();
    link.remove();

    Swal.fire('Successfully', 'File berhasil diunduh !', "success");
  }
</script>
@stop
@endsection