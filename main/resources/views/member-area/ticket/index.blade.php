@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Ticket</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Helpdesk</a></li>
                    <li class="breadcrumb-item active">Ticket</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

              @if ($message = Session::get('success'))
                 <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-check-double-line label-icon"></i>
                       {{ $message }}
                  </div>
              @elseif ($message = Session::get('error'))
                  <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-error-warning-line label-icon"></i>
                       {{ $message }}
                  </div>
              @endif

                <div class="row">
                    <div class="col-md-10">
                      <h4 class="card-title">TICKET</h4>
                      <p class="card-title-desc">Helpdesk</p>
                  </div>
                  <div class="col-md-2">
                     <a href="{{ url('pg_ticket/add') }}">
                   <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" ><i class="ri-add-circle-line align-bottom me-1"></i> Tambah</button>
                 </a>
                   <button type="button" class="btn btn-info btn-sm waves-effect waves-light" onClick="window.location.href=window.location.href" title="Refresh"><i class="ri-refresh-line align-bottom me-1"></i> Reload </button>

               </div>
           </div>

          <table id="alternative-pagination" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
             <thead>
                <tr>
                    <th>Ticket Code</th>
                    <th>Members</th>
                    <th>Category</th>
                    <th>Subject</th>
                    <th>Date</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @php $no=1; @endphp
                @foreach($ticket as $val)
                <tr>
                   <td>
                    <a href="{{ url('pg_ticket_detail') }}/{{ $val->ticket_code }}/{{ $val->uuid }}" >
                    {{ $val->ticket_code }}  <i class="ri-eye-fill align-bottom me-1"></i> 
                  </a>
                  </td>
                   <td>{{ $val->first_name }} {{ $val->last_name }} </td>
                   <td>{{ $val->category_name }} </td>
                   <td>{{ $val->subject }} </td>
                   <td>{{ $val->ticket_date }} </td>
                   <td>
                    @if($val->ticket_status == 'Waiting')
                    <span class="badge badge-soft-success text-uppercase">Waiting</span>
                    @elseif($val->ticket_status == 'Inprogress')
                    <span class="badge badge-soft-primary text-uppercase">Inprogress</span>
                    @elseif($val->ticket_status == 'Close')
                    <span class="badge badge-soft-danger text-uppercase">Close</span>
                   @endif
                  </td>
            </tr>
            @php $no++; @endphp
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')

@stop
@endsection