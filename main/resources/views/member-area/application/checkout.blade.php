@extends('layouts.master_backend')
@section('content')
 <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0">Checkout</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                                        <li class="breadcrumb-item active">Checkout</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-xl-8">
                            <div class="card">
                                <div class="card-body checkout-tab">

                                        <div class="step-arrow-nav mt-n3 mx-n3 mb-3">

                                            <ul class="nav nav-pills nav-justified custom-nav" role="tablist">

                                                @if($check_orders->payment_status == 'Checkout')
                                                
                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link fs-15 p-3 active" id="pills-orders-tab" data-bs-toggle="pill" data-bs-target="#pills-orders" type="button" role="tab" aria-controls="pills-orders" aria-selected="false">
                                                        <i class="ri-bank-card-line fs-16 p-2 bg-soft-primary text-primary rounded-circle align-middle me-2"></i> Informasi Pembayaran
                                                    </button>
                                                </li>

                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link fs-15 p-3" id="confirm-orders-tab" data-bs-toggle="pill" data-bs-target="#confirm-orders" type="button" role="tab" aria-controls="pills-orders" aria-selected="false">
                                                        <i class="ri-bank-card-line fs-16 p-2 bg-soft-primary text-primary rounded-circle align-middle me-2"></i> Konfirmasi Pembayaran
                                                    </button>
                                                </li>

                                                @endif

                                                @if($check_orders->payment_status == 'Verify')

                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link fs-15 p-3" id="pills-finish2-tab" data-bs-toggle="pill" data-bs-target="#pills-finish2" type="button" role="tab" aria-controls="pills-finish2" aria-selected="false">
                                                        <i class="ri-checkbox-circle-line fs-16 p-2 bg-soft-primary text-primary rounded-circle align-middle me-2"></i> Verifikasi Pembayaran
                                                    </button>
                                                </li>
                                                @endif

                                                  @if($check_orders->payment_status == 'Done')

                                                <li class="nav-item" role="presentation">
                                                    <button class="nav-link fs-15 p-3" id="pills-finish-tab" data-bs-toggle="pill" data-bs-target="#pills-finish" type="button" role="tab" aria-controls="pills-finish" aria-selected="false">
                                                        <i class="ri-checkbox-circle-line fs-16 p-2 bg-soft-primary text-primary rounded-circle align-middle me-2"></i> Selesai
                                                    </button>
                                                </li>

                                                 @endif
                                            </ul>
                                        </div>

                                        <?php 
                                         if($check_orders->payment_status == 'Checkout'){
                                            $active1 = 'show active';
                                            $active2 = '';
                                            $active3 = '';
                                         }else if($check_orders->payment_status == 'Verify'){
                                            $active1 = '';
                                            $active2 = 'show active';
                                            $active3 = '';

                                         }else if($check_orders->payment_status == 'Done'){
                                            $active1 = '';
                                            $active2 = '';
                                            $active3 = 'show active';
                                         }
                                         ?>


                                        <div class="tab-content">

                                             <div class="tab-pane fade {{ $active1 }}" id="pills-orders" role="tabpanel">
                                                <div>
                                                    <h5 class="mb-1">Order ID #{{ $check_orders->invoice }} </h5>
                                                    <p class="text-muted mb-4">Status transaksi anda menunggu pembayaran </p>
                                                </div>


                                                  <div class="row g-4">
                                                    
                                                    <div class="col-lg-12 col-sm-12">
                                                            <div class="form-check card-radio">
                                                                <center>
                                                                    <h4>MENUNGGU PEMBAYARAN</h4>
                                                                    <span class=" text-wrap" id="timer" style="font-size:30px;  font-weight: bold"></span>

                                                                    <p>
                                                                        Mohon lakukan pembayaran sebelum  
                                                                        <span style="color:red; font-weight: bold">
                                                                            {{  date('d F Y H:i:s', strtotime($check_orders->expired_date)) }}
                                                                        </span>
                                                                    </p>
                                                            </center>
                                                            </div>
                                                    </div>
                                                  
                                                </div>

                                                <br>

                               <center> <h5>Transfer ke bank dibawah ini</h5> </center>
                                  <div class="row g-4">
                                <div class="col-xxl-6 col-lg-6">
                                    <div class="card card-body" style="text-align: center">
                                        <div class="mb-3">
                                            <center>
                                            <img src="{{ asset('dist/bca.png') }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'" alt="" class="img-fluid d-block" width="200" style="margin-bottom: 10px">
                                        </center>
                                        </div>
                                        <h4 class="card-title">Yutdhi Kristiyantho</h4>
                                        <p class="card-text text-muted" id="norek1">{{ env('REK_BCA')}}</p>
                                        <a href="javascript:void(0);" class="btn btn-success" onclick="getCopy(1)">
                                        <i class="ri-file-copy-2-fill"></i> Copy Nomor Rekening
                                        </a>
                                    </div>
                                </div>
                               <div class="col-xxl-6 col-lg-6">
                                    <div class="card card-body" style="text-align: center">
                                        <div class="mb-3" >
                                            <center>
                                            <img src="{{ asset('dist/bri.png') }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'" alt="" class="img-fluid d-block" width="130" style="margin-bottom: 10px">
                                        </center>
                                        </div>
                                        <h4 class="card-title">Yutdhi Kristiyantho</h4>
                                        <p class="card-text text-muted" id="norek2">{{ env('REK_BRI')}}</p>
                                        <a href="javascript:void(0);" class="btn btn-success" onclick="getCopy(2)">
                                        <i class=" ri-file-copy-2-fill"></i> Copy Nomor Rekening
                                        </a>
                                    </div>
                                </div>
                            </div>

                             <div class="card">
                               
                                  <div class="card-body collapse show" id="collapseexample1">
                                    <h6>Ketentuan : </h6>
                                    <div class="d-flex">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                           Setelah transfer isi formulir konfirmasi pembayaran
                                        </div>
                                    </div>
                                    <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Verifikasi dana paling lama 1x24 jam
                                        </div>
                                    </div>
                                    <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Pesanan akan otomatis dibatalkan jika tidak ada konfirmasi pembayaran dalam waktu 1x24 jam 
                                        </div>
                                    </div>
                                    </div>
                                    </div>


                                               
                                   <div class="card">
                               
                                       <div class="card-body collapse show" id="collapseexample1">
                                    <h6>Cara Pembayaran Bank Transfer via ATM : </h6>
                                    <div class="d-flex">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Masukkan kartu ATM
                                        </div>
                                    </div>
                                    <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Masukkan PIN ATM
                                        </div>
                                    </div>
                                     <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Pilih menu Transfer
                                        </div>
                                    </div>
                                     <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Anda bisa melihat kode bank di menu <b>“Daftar Kode Bank”</b> (Jika tranfer ke bank yang berbeda)
                                        </div>
                                    </div>
                                     <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Masukkan kode bank dan nomor rekening tujuan transfer
                                        </div>
                                    </div>
                                     <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Masukkan nominal yang akan ditransfer
                                        </div>
                                    </div>
                                     <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Tunggu proses hingga selesai dan bukti transfer muncul 
                                        </div>
                                    </div>
                                     <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Selesai
                                        </div>
                                    </div>
                                 </div>
                              </div>


                             <div class="card">
                               
                                  <div class="card-body collapse show" id="collapseexample1">
                                      <h6>Cara pembayaran via Digital Payment (OVO, GoPay, DANA, dll) : </h6>
                                    <div class="d-flex">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                           Masuk ke aplikasi digital payment yang kamu gunakan (OVO, GoPay, Dana, dll). 
                                        </div>
                                    </div>
                                    <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Pilih menu <b>"Bayar"</b> atau <b>"Transfer"</b>
                                        </div>
                                    </div>
                                    <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Pilih <b>"Transfer ke Rekening Bank"</b>
                                        </div>
                                    </div>
                                    <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                            Pilih bank tujuan, lalu masukkan nomor rekening tujuan & nominal transfer
                                        </div>
                                    </div>
                                    <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                           Cek kembali rincian transaksi, lalu klik <b>"Transfer"</b>
                                        </div>
                                    </div>
                                     <div class="d-flex mt-2">
                                        <div class="flex-shrink-0">
                                            <i class="ri-checkbox-circle-fill text-success"></i>
                                        </div>
                                        <div class="flex-grow-1 ms-2 text-muted">
                                          Masukkan PIN akun digital payment-mu, <b>transaksi berhasil!</b>
                                        </div>
                                    </div>
                                    </div>
                                    </div>



                                             <div class="d-flex align-items-start gap-3 mt-3">
                                                         <button type="button" class="btn btn-danger btn-label " data-previous="pills-orders-tab" onclick="cancelTrx('{{ $check_orders->uuid }}')"><i class="ri-close-fill label-icon align-middle fs-16 me-2"></i>Batalkan Transaksi</button>


                                                        <button type="button" class="btn btn-primary btn-label right ms-auto nexttab" data-nexttab="confirm-orders-tab">
                                                            <i class="ri-wallet-fill label-icon align-middle fs-16 ms-2"></i>Konfirmasi Pembayaran Sekarang
                                                        </button>
                                                    </div>
                                            </div>

                                            <div class="tab-pane fade" id="confirm-orders" role="tabpanel">
                                                <div>
                                                    <h5 class="mb-1">Konfirmasi Pembayaran</h5>
                                                    <p class="text-muted mb-4">Upload bukti pembayaran dengan mengisi form dibawah ini</p>
                                                </div>

                                                <div class="row g-4">
                                                    
                                                    <div class="col-lg-4 col-sm-6">
                                                        <div data-bs-toggle="collapse" data-bs-target="#ordersmethodCollapsex" aria-expanded="true" aria-controls="ordersmethodCollapse">
                                                            <div class="form-check card-radio">
                                                                <input id="ordersMethod02" name="ordersMethod" type="radio" class="form-check-input" checked>
                                                                <label class="form-check-label" for="ordersMethod02">
                                                                    <span class="fs-16 text-muted me-2"><i class="ri-bank-card-fill align-bottom"></i></span>
                                                                    <span class="fs-14 text-wrap">ATM / Bank Transfer</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  
                                                </div>


                                               
                                                <form action="{{ url('confirm/action') }}" method="post" enctype="multipart/form-data">
                                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                                <div class="collapse show" id="ordersmethodCollapse">
                                                    <div class="card p-4 border shadow-none mb-0 mt-4">
                                                        <div class="row gy-3">

                                                             
                                                            <input type="hidden" name="members_id"  value="{{ Session('members_id') }}">
                                                            <input type="hidden" name="orders_id"  value="{{ $check_orders->uuid }}">
                                                            <input type="hidden" name="inv"  value="{{ $check_orders->invoice }}">
                                                            <input type="hidden" name="price"  value="{{ $check_orders->price }}">
                                                            <input type="hidden" name="appcode"  value="{{ $application->appcode }}">
                                                            <input type="hidden" name="application_id"  value="{{ $application->uuid }}">

                                                            <div class="col-md-6">
                                                                <label for="cc-number" class="form-label">Nama Lengkap <code>*</code></label>
                                                                <input type="text" class="form-control" id="name" name="name" value="{{ Session('name') }}" readonly>
                                                            </div>

                                                             <div class="col-md-6">
                                                                <label for="cc-number" class="form-label">E-mail</label>
                                                                <input type="text" class="form-control" id="email" name="email" value="{{ Session('email') }}" readonly>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="form-label">Transfer Ke Rekening <code>*</code></label>
                                                                <select class="form-control" name="norek_tujuan" required>
                                                                    <option value="">Pilih Rekening</option>
                                                                    <option value="{{ env('REK_BCA')}}">{{ env('REK_BCA')}} - Yutdhi Kristiyantho (BCA) </option>
                                                                     <option value="{{ env('REK_BRi')}}">{{ env('REK_BRI')}} - Yutdhi Kristiyantho (BRI)</option>
                                                                </select>
                                                            </div>


                                                            <div class="col-md-6">
                                                                <label for="cc-number" class="form-label">Atas Nama Rekening <code>*</code></label>
                                                                <input type="text" class="form-control" id="rek_name" name="rek_name" required autocomplete="off">
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="form-label">Tanggal Pembayaran <code>*</code></label>
                                                                 <input type="text" class="form-control" data-provider="flatpickr" name="paid_date" data-date-format="d M, Y" value="{{ date('Y-m-d')}}" readonly required>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label class="form-label">Upload Bukti Pembayaran <code>*</code></label>
                                                                <input type="file" class="form-control" id="file" name="file" placeholder="" required>
                                                            </div>

                                                             <div class="col-md-12">
                                                                <label class="form-label">Keterangan <code>*</code></label>
                                                               <textarea class="form-control" id="desc" placeholder="Masukkan nomor invoice di keterangan" name="desc" rows="3" required></textarea>
                                                            </div>

                                                               <button type="submit" class="btn btn-primary  btn-label  ms-auto nexttab" >SUBMIT KONFIRMASI PEMBAYARAN</button>


                                                               
                                                        </div>
                                                    </div>
                                                    <div class="text-muted mt-2 fst-italic">
                                                        <i data-feather="lock" class="text-muted icon-xs"></i> Your transaction is secured 
                                                    </div>
                                                </div>
                                                 </form>

                                                <div class="d-flex align-items-start gap-3 mt-4">
                                                    <button type="button" class="btn btn-light btn-label previestab" data-previous="pills-orders-tab"><i class="ri-arrow-left-line label-icon align-middle fs-16 me-2"></i>Kembali ke informasi pembayaran</button>

                                                 
                                                </div>
                                            </div>

                                            <div class="tab-pane fade {{ $active2 }}" id="pills-finish2" role="tabpanel" aria-labelledby="pills-finish2-tab">
                                                <div class="text-center py-5">

                                                    <div class="mb-4">
                                                        <lord-icon src="https://cdn.lordicon.com/kbtmbyzy.json" trigger="loop" colors="primary:#0ab39c,secondary:#405189" style="width:120px;height:120px"></lord-icon>
                                                    </div>
                                                    <h5>Terima kasih ! Sudah konfirmasi pembayaran !</h5>
                                                    <p class="text-muted">Tim kami akan segera verifikasi pembayaran anda paling lambat 1x24 jam. Teirma kasih.</p>

                                                    <h3 class="fw-semibold">Order ID: <a href="javascript:void(0)" class="text-decoration-underline">{{ $check_orders->invoice }}</a></h3>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade {{ $active3 }}" id="pills-finish" role="tabpanel" aria-labelledby="pills-finish-tab">
                                                <div class="text-center py-5">

                                                    <div class="mb-4">
                                                        <lord-icon src="https://cdn.lordicon.com/lupuorrc.json" trigger="loop" colors="primary:#0ab39c,secondary:#405189" style="width:120px;height:120px"></lord-icon>
                                                    </div>
                                                    <h5>Terima kasih ! Pesanan Anda Selesai!</h5>
                                                    <p class="text-muted">Anda akan menerima email konfirmasi pesanan dengan rincian pesanan Anda.</p>

                                                    <h3 class="fw-semibold">Order ID: <a href="javascript:void(0)" class="text-decoration-underline">{{ $check_orders->invoice }}</a></h3>
                                                </div>
                                            </div>

                                             
                                            <!-- end tab pane -->
                                        </div>
                                        <!-- end tab content -->
                                </div>
                                <!-- end card body -->
                            </div>
                            <!-- end card -->
                        </div>
                        <!-- end col -->

                        <div class="col-xl-4">
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-flex">
                                        <div class="flex-grow-1">
                                            <h5 class="card-title mb-0">Detail Order</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive table-card">
                                        <table class="table table-borderless align-middle mb-0">
                                            <thead class="table-light text-muted">
                                                <tr>
                                                    <th style="width: 90px;" scope="col">Thumbnail</th>
                                                    <th scope="col">Aplikasi</th>
                                                    <th scope="col" class="text-end">Harga</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="avatar-md bg-light rounded p-1">
                                                            <img src="{{ asset('dist/app') }}/{{ $application->appcode }}/{{ $application->uuid }}//{{ $application->thumbnail }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'" alt="" class="img-fluid d-block">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <h6 class="fs-14"><a href="apps-ecommerce-product-details.html" class="text-dark">{{ $application->name }}</h6>
                                                        <p class="text-muted mb-0">{{ $application->category_name }}</p>
                                                    </td>
                                                    <td class="text-end">{{ number_format($check_orders->price,0,",",".") }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="fw-semibold" colspan="2">Sub Total :</td>
                                                    <td class="fw-semibold text-end">{{ number_format($check_orders->price,0,",",".") }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Discount 
                                                        @if(!empty($check_orders->voucher_code))
                                                        <span class="text-muted">({{ $check_orders-voucher_code}})</span> 
                                                        @endif
                                                    : </td>
                                                    <td class="text-end">{{ number_format($check_orders->price_discount,0,",",".") }}</td>
                                                </tr>
                                                <tr class="table-active">
                                                    <th colspan="2">Total:</th>
                                                    <td class="text-end">
                                                        <span class="fw-semibold">
                                                           {{ number_format($check_orders->price+$check_orders->price_discount,0,",",".") }}
                                                        </span>
                                                    </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>

                                        
                                    </div>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!-- end card -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->


@section('page-script')
<script type="text/javascript">
     $( document ).ready(function() {
    getStart();
});

    function getStart(){

        // var d1 = new Date();
        // d1.setMinutes(+ "360" + d1.getMinutes());
        var countDownDate = new Date('{{ $check_orders->expired_date }}').getTime();
        // Update the count down every 1 second
        var x = setInterval(function() {
          // Get today's date and time
          var now = new Date().getTime();
          // Find the distance between now and the count down date
          var distance = countDownDate - now;
          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          // Output the result in an element with id="demo"
          document.getElementById("timer").innerHTML =  hours + "h " + minutes + "m " + seconds + "s ";

          // If the count down is over, write some text 
          if (distance < 0) {
            clearInterval(x);
            document.getElementById("timer").innerHTML = "00:00:00";
          }
        }, 1000);
    
    }

    function getCopy(id){
        // get the container
          const element = document.querySelector('#norek'+id);
          // Create a fake `textarea` and set the contents to the text
          // you want to copy
          const storage = document.createElement('textarea');
          storage.value = element.innerHTML;
          element.appendChild(storage);

          // Copy the text in the fake `textarea` and remove the `textarea`
          storage.select();
          storage.setSelectionRange(0, 99999);
          document.execCommand('copy');
          element.removeChild(storage);

           Swal.fire('OK', 'Nomor Rekening berhasil di copy', "success");

}


function cancelTrx(id){
    Swal.fire({
        title: "Apakah anda yakin?",
        text: "Untuk membatalkan transaksi ini!",
        icon: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-primary w-xs me-2 mt-2",
        cancelButtonClass: "btn btn-danger w-xs mt-2",
        confirmButtonText: "Ya, saya yakin!",
        buttonsStyling: !1,
        showCloseButton: !0
   }).then(function (result) {
    if (result.value) {
         $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ url("transaction/delete")}}',
            enctype: "multipart/form-data",
            dataType : 'JSON',
            data: 'id='+id,
            success: function (data) {

             Swal.fire(data.title, data.msg, "success");
             setTimeout(() => {
                  window.location.reload();
              }, 2000);
         },
     });

    }
    })
}


</script>

@stop
@endsection