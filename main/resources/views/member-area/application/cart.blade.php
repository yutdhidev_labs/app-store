@extends('layouts.master_backend')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Daftar Pesanan</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                    <li class="breadcrumb-item active">Daftar Pesanan</li>
                </ol>
            </div>

        </div>
    </div>
</div>

 <div class="row mb-3">
                        <div class="col-xl-8">

                            <div class="card product">
                                <div class="card-body">
                                    <div class="row gy-3">
                                        <div class="col-sm-auto">
                                            <div class="avatar-lg bg-light rounded p-1">
                                                <img src="{{ asset('dist/app') }}/{{ $application->appcode }}/{{ $application->uuid }}//{{ $application->thumbnail }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'" alt="" class="img-fluid d-block">
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <h5 class="fs-14 text-truncate"><a href="ecommerce-product-detail.html" class="text-dark">{{ $application->name}}</a></h5>
                                            <ul class="list-inline text-muted">
                                                <li class="list-inline-item">Category : <span class="fw-medium">{{ $application->category_name}}</span></li>
                                            </ul>

                                            <div class="input-step">
                                                <button type="button" class="minus" disabled>–</button>
                                                <input type="number" class="product-quantity" value="1" min="0" max="1" readonly>
                                                <button type="button" class="plus" disabled>+</button>
                                            </div>
                                        </div>
                                        <div class="col-sm-auto">
                                            <div class="text-lg-end">
                                                <p class="text-muted mb-1">Harga</p>
                                                <h5 class="fs-14">Rp. <span id="ticket_price" class="product-price">{{ number_format($application->price,0,",",".") }}</span></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- card body -->
                                <div class="card-footer">
                                    <div class="row align-items-center gy-3">
                                        <div class="col-sm">
                                            <div class="d-flex flex-wrap my-n1">
                                                <div>
                                                    <a href="#" class="d-block text-body p-1 px-2"><i class="ri-star-fill text-muted align-bottom me-1"></i> Add Wishlist</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-auto">
                                            <div class="d-flex align-items-center gap-2 text-muted">
                                                <div>Total :</div>
                                                <h5 class="fs-14 mb-0">RP. <span class="product-line-price">{{ number_format($application->price,0,",",".") }}</span></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end card footer -->
                            </div>

                               <div class="alert border-dashed alert-danger" role="alert" style="display: none">
                                    <div class="d-flex align-items-center">
                                        <lord-icon src="https://cdn.lordicon.com/nkmsrxys.json" trigger="loop" colors="primary:#121331,secondary:#f06548" style="width:80px;height:80px"></lord-icon>
                                        <div class="ms-2">
                                            <h5 class="fs-14 text-danger fw-semibold"> Buying for a loved one?</h5>
                                            <p class="text-black mb-1">Gift wrap and personalised message on card, <br />Only for <span class="fw-semibold">$9.99</span> USD </p>
                                            <button type="button" class="btn ps-0 btn-sm btn-link text-danger text-uppercase">Add Gift Wrap</button>
                                        </div>
                                    </div>
                                </div>
                            <!-- end card -->


                            <div class="text-end mb-4">
                                <a href="javascript:void(0)" onclick="checkout()" class="btn btn-success btn-label right ms-auto"><i class="ri-arrow-right-line label-icon align-bottom fs-16 ms-2"></i> <i class="ri-shopping-cart-2-line align-bottom me-1"></i>  Checkout</a>
                            </div>
                        </div>
                        <!-- end col -->

                        <div class="col-xl-4">
                            <div class="sticky-side-div">
                                <div class="card">
                                    <div class="card-header border-bottom-dashed">
                                        <h5 class="card-title mb-0">Ringkasan Pesanan</h5>
                                    </div>
                                    <div class="card-header bg-soft-light border-bottom-dashed">
                                        <div class="text-center">
                                            <h6 class="mb-2">Have a <span class="fw-semibold">promo</span> code ?</h6>
                                        </div>
                                        <div class="hstack gap-3 px-3 mx-n3">
                                            <input class="form-control me-auto" type="text" placeholder="Enter coupon code" aria-label="Add Promo Code here...">
                                            <button type="button" class="btn btn-success w-xs">Apply</button>
                                        </div>
                                    </div>
                                    <div class="card-body pt-2">
                                        <div class="table-responsive">
                                            <table class="table table-borderless mb-0">
                                                <tbody>
                                                    <tr>
                                                        <td>Sub Total :</td>
                                                        <td class="text-end" id="cart-subtotal">Rp. {{ number_format($application->price,0,",",".") }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Discount <span class="text-muted"></span> : </td>
                                                        <td class="text-end" id="cart-discount">- Rp.  {{ number_format(0,0,",",".") }}</td>
                                                    </tr>
                                                    <tr class="table-active">
                                                        <th>Total :</th>
                                                        <td class="text-end">
                                                            <span class="fw-semibold" id="cart-total">
                                                                Rp. {{ number_format($application->price,0,",",".") }}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- end table-responsive -->
                                    </div>
                                </div>

                             
                            </div>
                            <!-- end stickey -->

                        </div>
                    </div>

                    <div id="checkout-cart" class="modal fade flip" tabindex="-1" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-body text-center p-5">
                                                    <div class="text-end">
                                                        <button type="button" class="btn-close text-end" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="mt-2">
                                                        <lord-icon src="https://cdn.lordicon.com/tdrtiskw.json" trigger="loop" colors="primary:#f7b84b,secondary:#405189" style="width:120px;height:120px"></lord-icon>
                                                        <h4 class="mb-3 mt-4">Apakah orderan anda sudah sesuai ?</h4>
                                                        <p class="text-muted fs-15 mb-4">
                                                        Silahkan melakukan proses checkout untuk melanjutkan pembayaran pesanan anda 
                                                        </p>
                                                        <div class="hstack gap-2 justify-content-center">
                                                          
                                                            <input type="hidden" id="appid" value="{{ $application->id }}">

                                                            <button class="btn btn-primary"  onclick="checkoutAction()"><i class=" ri-shopping-cart-2-line align-bottom me-1"></i> Checkout</button>
                                                            <button class="btn btn-soft-danger"  data-bs-dismiss="modal" ><i class="ri-close-circle-line align-bottom me-1"></i> Batalkan</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>


@section('page-script')
<script type="text/javascript">
    function checkout(){
        $("#checkout-cart").modal('show');
    }

    function checkoutAction() {
      var appid = $("#appid").val();

    $.ajax({
          type: 'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: '{{ url("pg_application/cart/action")}}',
        dataType : 'JSON',
          data: 'appid='+appid,
          success: function (data) {
            Swal.fire(data.title, data.msg, "success");
             setTimeout(() => {
                 window.location = data.url;
              }, 2000);
          },
      });
    }
</script>
@stop
@endsection