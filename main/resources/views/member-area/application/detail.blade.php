@extends('layouts.master_backend')
@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="card mt-n4 mx-n4">
      <div class="bg-soft-warning">
        <div class="card-body pb-0 px-4">
          <div class="row mb-3">
            <div class="col-md">
              <div class="row align-items-center g-3">
                <div class="col-md-auto">
                  <div class="avatar-md">
                    <div class="avatar-title bg-white rounded-circle">
                      <img src="{{ asset('dist/app') }}/{{ $application->appcode }}/{{ $application->uuid }}//{{ $application->thumbnail }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'" alt="" class="avatar-xs">
                    </div>
                  </div>
                </div>
                <div class="col-md">
                  <div>
                    <h4 class="fw-bold">{{ $application->name }}</h4>
                    <div class="hstack gap-3 flex-wrap">
                      <div><i class="ri-building-line align-bottom me-1"></i> {{ $application->category_name }}</div>
                      <div class="vr"></div>
                      <div>Tanggal dibuat : <span class="fw-medium">{{ date('d M Y', strtotime($application->created_at)) }}</span></div>
                      <div class="vr"></div>

                      <div>Terkahir diperbaharui : <span class="fw-medium">{{ date('d M Y', strtotime($application->updated_at)) }}</span></div>
                      <div class="badge rounded-pill bg-info fs-12">{{ $application->app_type }}</div>
                      <div class="badge rounded-pill bg-danger fs-12">{{ $application->level }}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-auto">
              <div class="hstack gap-1 flex-wrap">
                <button type="button" class="btn py-0 fs-16 favourite-btn active">
                  <i class="ri-star-fill"></i>
                </button>
                <button type="button" class="btn py-0 fs-16 text-body">
                  <i class="ri-share-line"></i>
                </button>
                <button type="button" class="btn py-0 fs-16 text-body">
                  <i class="ri-flag-line"></i>
                </button>
              </div>
            </div>
          </div>

          <ul class="nav nav-tabs-custom border-bottom-0" role="tablist">
            <li class="nav-item">
              <a class="nav-link active fw-semibold" data-bs-toggle="tab" href="#overview" role="tab">
                Overview
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-lg-12">
    <div class="tab-content text-muted">
      <div class="tab-pane fade show active" id="overview" role="tabpanel">
        <div class="row">
          <div class="col-xl-9 col-lg-8">
            <div class="card">
              <div class="card-body">
                <div class="text-muted">
                  <h6 class="mb-3 fw-semibold text-uppercase">Ringkasan Aplikasi</h6>
                  <div id="shortdesc">
                    {!! substr($application->desc,0,1000) !!} ...
                  </div>
                  <div id="fulldesc" style="display: none">{!! $application->desc !!}</div>
                  <div>
                    <button type="button" id="readmore" onclick="Readmore()" class="btn btn-link link-success p-0">Baca selengkapnya </button>
                  </div>


                  <div class="pt-3 border-top border-top-dashed mt-4">
                    <h6 class="mb-3 fw-semibold text-uppercase">File Source Code</h6>
                    <div class="row g-3">

                      <div class="col-xxl-11 col-lg-12">
                        <div class=" rounded p-2">
                          <div class="d-flex align-items-center">
                            <div class="flex-shrink-0 me-3">
                              <div class="avatar-sm">
                                <div class="avatar-title bg-light text-secondary rounded fs-24">
                                  <i class="ri-folder-zip-line"></i>
                                </div>
                              </div>
                            </div>
                            <div class="flex-grow-1 overflow-hidden">
                              <h5 class="fs-13 mb-1"><a href="#" class="text-body text-truncate d-block">{{ $application->name }}
                                <br>
                                <span style="color:#aaa">{{ $application->category_name }}</span>
                              </a></h5>
                              <div>

                               @if(!empty($check_orders))
                               @if($check_orders->payment_status == 'Done')

                               <?php
                               $file = "dist/app/".$application->appcode."/".$application->uuid."/".$application->filename."";

                               if((is_file($file))&&(file_exists($file))){
                                 $size = filesize($file);
                                 $mod = 1024;
                                 $units = explode(' ','B KB MB GB TB PB');
                                 for ($i = 0; $size > $mod; $i++) {
                                  $size /= $mod;
                                }
                                echo round($size, 2) . ' ' . $units[$i];
                              }else{
                                echo '0 KB';
                              }   

                              ?>
                              @endif
                              @endif


                              @if($application->app_type == 'Free')

                               <?php
                               $file = "dist/app/".$application->appcode."/".$application->uuid."/".$application->filename."";

                               if((is_file($file))&&(file_exists($file))){
                                 $size = filesize($file);
                                 $mod = 1024;
                                 $units = explode(' ','B KB MB GB TB PB');
                                 for ($i = 0; $size > $mod; $i++) {
                                  $size /= $mod;
                                }
                                echo round($size, 2) . ' ' . $units[$i];
                              }else{
                                echo '0 KB';
                              }   

                              ?>
                              @endif
                            </div>
                          </div>
                          <div class="flex-shrink-0 ms-2">
                            <div class="d-flex gap-1">

                             @if(!empty($check_orders))
                             @if($check_orders->payment_status == 'Done')


                             <?php 
                             $uri = asset('dist/app')."/".$application->appcode."/".$application->uuid."/".$application->filename."";
                             $filename = $application->filename;
                             ?>

                             <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="getStart();" style="cursor: pointer" id="btn_download">

                              <i class="ri-download-2-line"></i> 
                              <span style="font-size: 14px; margin-left: 5px">DOWNLOAD</span>
                            </button>

                            @elseif($check_orders->payment_status == 'Checkout')
                            <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="showpayment(2);" style="cursor: pointer" id="btn_download">

                             <i class="ri-download-2-line"></i> 
                             <span style="font-size: 14px; margin-left: 5px">DOWNLOAD</span>
                           </button>
                           @elseif($check_orders->payment_status == 'Verify')
                           <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="showpayment(3);" style="cursor: pointer" id="btn_download">

                             <i class="ri-download-2-line"></i> 
                             <span style="font-size: 14px; margin-left: 5px">DOWNLOAD</span>
                           </button>
                           @endif
                           @else

                           @if($application->app_type == 'Premium')
                           <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="showpayment(1);" style="cursor: pointer" id="btn_download">

                             <i class="ri-download-2-line"></i> 
                             <span style="font-size: 14px; margin-left: 5px">DOWNLOAD</span>
                           </button>
                          @elseif($application->app_type == 'Free')
                          <?php 
                             $uri = asset('dist/app')."/".$application->appcode."/".$application->uuid."/".$application->filename."";
                             $filename = $application->filename;
                             ?>

                             <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="getStart();" style="cursor: pointer" id="btn_download">

                              <i class="ri-download-2-line"></i> 
                              <span style="font-size: 14px; margin-left: 5px">DOWNLOAD</span>
                            </button>


                           @endif
                           @endif

                         </div>
                       </div>
                     </div>
                   </div>
                 </div>

               </div>
             </div>

             @if(!empty($check_orders))
             @if($check_orders->payment_status == 'Done')
             <div class="card bg-light overflow-hidden shadow-none" id="progress_bar" style="display: none">
              <div class="card-body">
                <p align="center">

                  Kami memberikan jeda waktu download selama 30 detik untuk download file. Hal tersebut dikarenakan banyaknya member yang melakukan download file dalam jumlah banyak sekaligus yang mengakibatkan sangat mengganggu aktifitas teman-teman yang lainnya disini.
                </p>
                <div class="d-flex">
                  <div class="flex-grow-1">
                    <h6 class="mb-0"><b class="text-secondary" id="count_percent"></b> Generate link in progress...</h6>
                  </div>
                  <div class="flex-shrink-0">
                    <h6 class="mb-0" id="timer"></h6>
                  </div>
                </div>
              </div>
              <div class="progress bg-soft-secondary rounded-0">
                <div class="progress-bar bg-secondary" role="progressbar" id="percent_progress" style="width: 0%"aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>


            <div class="card bg-light overflow-hidden shadow-none" id="btn_unduh" style="display: none">
              <div class="card-body">
                <p align="center">
                  Klik tombol dibawah ini untuk melanjutkan download file aplikasi
                </p>

                <center>
                  <button class="btn btn-primary" onclick="downloadURI('{{ $uri }}', '{{ $filename }}', '{{ $application->id }}');"> <i class="ri-download-2-line"></i>  DOWNLOAD NOW</button>
                </center>

              </div>

            </div>
            @endif
            @endif


             @if($application->app_type == 'Free')
             <div class="card bg-light overflow-hidden shadow-none" id="progress_bar" style="display: none">
              <div class="card-body">
                <p align="center">

                  Kami memberikan jeda waktu download selama 30 detik untuk download file. Hal tersebut dikarenakan banyaknya member yang melakukan download file dalam jumlah banyak sekaligus yang mengakibatkan sangat mengganggu aktifitas teman-teman yang lainnya disini.
                </p>
                <div class="d-flex">
                  <div class="flex-grow-1">
                    <h6 class="mb-0"><b class="text-secondary" id="count_percent"></b> Generate link in progress...</h6>
                  </div>
                  <div class="flex-shrink-0">
                    <h6 class="mb-0" id="timer"></h6>
                  </div>
                </div>
              </div>
              <div class="progress bg-soft-secondary rounded-0">
                <div class="progress-bar bg-secondary" role="progressbar" id="percent_progress" style="width: 0%"aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>


            <div class="card bg-light overflow-hidden shadow-none" id="btn_unduh" style="display: none">
              <div class="card-body">
                <p align="center">
                  Klik tombol dibawah ini untuk melanjutkan download file aplikasi
                </p>

                <center>
                  <button class="btn btn-primary" onclick="downloadURI('{{ $uri }}', '{{ $filename }}', '{{ $application->id }}');"> <i class="ri-download-2-line"></i>  DOWNLOAD NOW</button>
                </center>

              </div>

            </div>
            @endif

          </div>


        </div>
      </div>

      @if(!empty($check_orders))
        @if($check_orders->payment_status == 'Done')
        <div class="card">
          <div class="card-body">  
            <p align="center">
                    Untuk demo semu fitur aplikasi bisa di cek di bawah ini
                  </p>
            <div class="row">
              <div class="col-md-6">
                 <button class="btn btn-danger" style="width: 100%" onclick="showVideo()" target="_blank"><i class="ri-youtube-fill align-bottom me-1"></i> DEMO VIDEO </button>
              </div>
              <div class="col-md-6">
                   <button class="btn btn-success" style="width: 100%" target="_blank" onclick="showWeb()"><i class="ri-global-fill align-bottom me-1"></i> DEMO WEBSITE </button>
              </div>
            </div>

                <div class="modal " id="demo_video" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered modal-xl" role="document">


                    <div class="modal-content">
                      <div class="modal-header">
                        <h5>{{ $application->name }} | {{ $application->category_name }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> </button>
                      </div>

                      <div class="card">
                        <div class="card-body"> 
                        
                           <div class="ratio ratio-16x9">
                              <iframe src="https://www.youtube.com/embed/{{ $application->link_video }}" title="YouTube video" allowfullscreen></iframe>
                          </div>
                        </div>
                      </div> 


                         
                  </div>
                </div>
                </div>

          </div>
        </div>
        @endif
      @else
      <div class="card">
          <div class="card-body">  
            <p align="center">
                    Untuk demo semua fitur aplikasi bisa di cek di bawah ini
                  </p>
            <div class="row">
              <div class="col-md-6">
                 <a href="javascript:void(0)" onclick="showpayment(1)" class="btn btn-danger" style="width: 100%"><i class="ri-youtube-fill align-bottom me-1"></i> DEMO VIDEO </a>
              </div>
              <div class="col-md-6">
                   <a href="javascript:void(0)" onclick="showpayment(1)" class="btn btn-success" style="width: 100%"><i class="ri-global-fill align-bottom me-1"></i> DEMO WEBSITE </a>
              </div>
            </div>
          </div>
        </div>
      @endif


        <div class="card">
        <div class="card-body">
          <div class="d-flex position-relative">
            <img src="#" class="flex-shrink-0 me-3 avatar-xl img-thumbnail rounded rounded-circle" alt="..." onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/work.jpg'">
            <div>
              <h5 class="mt-0">Web Developer</h5>
              <p>Yutdhi Kristiyantho</p>

              <div class="d-flex flex-wrap gap-2 fs-16">
                <div class="badge fw-medium badge-soft-secondary">PHP</div>
                <div class="badge fw-medium badge-soft-secondary"> JavaScript</div>
                <div class="badge fw-medium badge-soft-secondary"> CSS</div>
                <div class="badge fw-medium badge-soft-secondary"> Bootstrap</div>
                <div class="badge fw-medium badge-soft-secondary"> Ajax & JQuery</div>
                <div class="badge fw-medium badge-soft-secondary"> Laravel</div>
                <div class="badge fw-medium badge-soft-secondary"> Codeigneter</div>
                <div class="badge fw-medium badge-soft-secondary"> Server</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-body">
          <div class="card-body p-4 border-top border-top-dashed">
            <h6 class="text-muted text-uppercase fw-semibold mb-4">Application Reviews</h6>
            <div class="swiper vertical-swiper" style="height: 242px;">
              <div class="swiper-wrapper">
                @if(count($testimoni) > 0)
                @foreach($testimoni as $val)
                <div class="swiper-slide">
                  <div class="card border border-dashed shadow-none">
                    <div class="card-body">
                      <div class="d-flex">
                        <div class="flex-shrink-0 avatar-sm">
                          <div class="avatar-title bg-light rounded">
                            <img src="" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/usericon.jpg'" alt="" height="30">
                          </div>
                        </div>
                        <div class="flex-grow-1 ms-3">
                          <div>
                            <p class="text-muted mb-1 fst-italic">" {{ $val->desc }} "</p>
                            <div class="fs-11 align-middle text-warning">
                              @for($i=0; $i<=$val->star; $i++)
                              <i class="ri-star-fill"></i>
                              @endfor
                            </div>
                          </div>
                          <div class="text-end mb-0 text-muted">
                            <cite title="">{{ $val->first_name }} {{ $val->last_name }}</cite>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                 @endforeach
                 @else
                   <div class="">
                  <div class="card border border-dashed shadow-none">
                    <div class="card-body">
                      <div class="d-flex">
                        <div class="flex-grow-1 ms-3">
                          <div>
                            <p class="text-muted mb-1 fst-italic">Review is empty</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                 @endif

              </div>

            </div>

            @if(!empty($check_orders))
            @if($check_orders->payment_status == 'Done')
            <button class="btn btn-primary" onclick="showreview()" style="width: 100%"><i class="ri-pencil-fill align-bottom me-1"></i> SUBMIT TESTIMONI </button>
            @endif
            @endif 

            @if($application->app_type == 'Free')
            <button class="btn btn-primary" onclick="showreview()" style="width: 100%"><i class="ri-pencil-fill align-bottom me-1"></i> SUBMIT TESTIMONI </button>
            @endif
          </div>
        </div>
      </div>

    </div>

    <div class="col-xl-3 col-lg-4">
      <div class="card">
        <div class="card-body">
          <div class="product-img-slider sticky-side-div">
            <div class="swiper product-thumbnail-slider p-2 rounded bg-light">
              <div class="swiper-wrapper">
                @if(count($application_gallery) > 0)
                @foreach($application_gallery as $galerry)
                <div class="swiper-slide">
                  <img src="{{ asset('dist/app') }}/{{ $galerry->appcode }}/{{ $galerry->uuid }}//{{ $galerry->thumbnail }}" alt="" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/background2.jpg'" class="img-fluid d-block" />
                </div>
                @endforeach
                @else
                <div class="swiper-slide">
                  <img src="#" alt="" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/background2.jpg'" class="img-fluid d-block" />
                </div>
                @endif
              </div>
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
            </div>
            <!-- end swiper thumbnail slide -->
            <div class="swiper product-nav-slider mt-2">
              <div class="swiper-wrapper">
                @if(count($application_gallery) > 0)
                @foreach($application_gallery as $galerry)
                <div class="swiper-slide">
                  <div class="nav-slide-item">
                    <img src="{{ asset('dist/app') }}/{{ $galerry->appcode }}/{{ $galerry->uuid }}//{{ $galerry->thumbnail }}" alt="" class="img-fluid d-block"  onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/background2.jpg'" />
                  </div>
                </div>
                @endforeach
                @else
                <div class="swiper-slide">
                  <div class="nav-slide-item">
                    <img src="#" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/background2.jpg'" alt="" class="img-fluid d-block" />
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>

      @if($application->app_type == 'Premium')
      @if(empty($check_orders))
      <div class="card">
        <div class="card-body">
          <div class="d-flex align-items-center">

            <div class="flex-grow-1 ms-3">
              <p class="text-uppercase fw-semibold fs-12 text-muted mb-1"> Harga</p>
              <div class="py-4">
                <h2><sup><small>Rp.</small></sup>{{ number_format($application->price,0,",",".") }} <span class="fs-13 text-muted"> / Full Source Code</span></h2>
                <button class="btn btn-primary" onclick="showpayment(1)" style="width: 100%"><i class="ri-shopping-cart-fill align-bottom me-1"></i> BELI SEKARANG </button>
              </div>



            </div>
          </div>
        </div>
      </div>
      @endif
      @endif

      @if(!empty($check_orders))
        @if($check_orders->payment_status == 'Done')
       <div class="card">
        <div class="card-body">
          <center>
          <h4 class="card-title mb-4" style="font-size: 20px">
            ORDER ID <br><b>#{{ $check_orders->invoice }} </b>
          </h4>

          <p>Detail Transaksi</p>

           <!-- <button class="btn btn-info" onclick="history_order()" style="width: 100%"><i class="ri-eye-fill align-bottom me-1"></i> DETAIL TRANSAKSI </button> -->

        </center>

          <div class="acitivity-timeline py-3">
            <div class="acitivity-item d-flex">
                <div class="avatar-xs acitivity-avatar">
                <div class="avatar-title bg-soft-success text-success rounded-circle">
                  <i class="ri-checkbox-circle-fill text-success"></i>
                </div>
              </div>
                <div class="flex-grow-1 ms-3">
                    <h6 class="mb-1">Checkout Pesanan</h6>
                    <p class="text-muted mb-2">
                    {{ date('d F Y - H:i', strtotime($check_orders->checkout_date)) }}</p>
                </div>
            </div>

            <div class="acitivity-item d-flex">
                 <div class="avatar-xs acitivity-avatar">
                <div class="avatar-title bg-soft-success text-success rounded-circle">
                  <i class="ri-checkbox-circle-fill text-success"></i>
                </div>
              </div>
                <div class="flex-grow-1 ms-3">
                     <h6 class="mb-1">Konfirmasi Pembayaran</h6>
                     <p class="text-muted mb-2">
                      {{ date('d F Y - H:i', strtotime($check_orders->paid_date)) }}</p>
                </div>
            </div>


            <div class="acitivity-item d-flex">
                 <div class="avatar-xs acitivity-avatar">
                <div class="avatar-title bg-soft-success text-success rounded-circle">
                  <i class="ri-checkbox-circle-fill text-success"></i>
                </div>
              </div>
                <div class="flex-grow-1 ms-3">
                     <h6 class="mb-1">Pesanan Selesai</h6>
                     <p class="text-muted mb-2">{{ date('d F Y - H:i', strtotime($check_orders->approved_date)) }}</p>
                </div>
            </div>


        </div>

       
        </div>
      </div>
      @endif
      @endif

      <div class="card">
        <div class="card-body">
          <h5 class="card-title mb-4">Label</h5>
          <div class="d-flex flex-wrap gap-2 fs-16">
            <?php 
            $myArray = explode(',', $application->label);
            foreach ($myArray as $key => $value) : 
              ?>
              <div class="badge fw-medium badge-soft-secondary">{{ $value }}</div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>


      <div class="card">
        <div class="card-body">
          <div>
            <h5 class="fs-14 mb-3">Ratings</h5>
          </div>
          <div class="row gy-4 gx-0">
            <div class="col-lg-12">
              <div>
                <div class="pb-3">
                  <div class="bg-light px-3 py-2 rounded-2 mb-2">
                    <div class="d-flex align-items-center">
                      <div class="flex-grow-1">
                        <div class="fs-16 align-middle text-warning">
                          <?php 
                          // if ( strpos( $rate, "." ) !== false ) {
                          //   $half = $rate;
                          // }else{
                          //   $half = '';
                          // }
                          ?>
                          @for($i=1; $i<=5; $i++)
                          @if($i <= $rate)
                          <i class="ri-star-fill"></i>
                          @else
                          <i class="ri-star-line"></i>
                          @endif
                          @endfor
                          <!-- <i class="ri-star-half-fill"></i> -->
                        </div>
                      </div>
                      <div class="flex-shrink-0">
                        <h6 class="mb-0">{{ round($rate) }} out of 5</h6>
                      </div>
                    </div>
                  </div>
                  <div class="text-center">
                    <div class="text-muted">Total <span class="fw-medium">{{ $total_testimoni }}</span> reviews
                    </div>
                  </div>
                </div>

                <?php 

                if($total_star_1 > 0){
                   $percent1 = ($total_star_1 / $total_testimoni)*100;
                }else{
                   $percent1 = 0;
                }

                if($total_star_2 > 0){
                   $percent2 = ($total_star_2 / $total_testimoni)*200;
                }else{
                   $percent2 = 0;
                }

                if($total_star_3 > 0){
                   $percent3 = ($total_star_3 / $total_testimoni)*300;
                }else{
                   $percent3 = 0;
                }

                if($total_star_4> 0){
                   $percent4= ($total_star_4/ $total_testimoni)*100;
                }else{
                   $percent4= 0;
                }

                if($total_star_5> 0){
                   $percent5= ($total_star_5/ $total_testimoni)*100;
                }else{
                   $percent5= 0;
                }
                 
                ?>

                <div class="mt-3">
                  <div class="row align-items-center g-2">
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0">5 star</h6>
                      </div>
                    </div>
                    <div class="col">
                      <div class="p-2">
                        <div class="progress animated-progress progress-sm">
                          <div class="progress-bar bg-success" role="progressbar" style="width: <?=$percent5.'%' ?>" aria-valuenow="50.16" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0 text-muted">{{ $total_star_5 }}</h6>
                      </div>
                    </div>
                  </div>
                  <!-- end row -->

                  <div class="row align-items-center g-2">
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0">4 star</h6>
                      </div>
                    </div>
                    <div class="col">
                      <div class="p-2">
                        <div class="progress animated-progress progress-sm">
                          <div class="progress-bar bg-success" role="progressbar" style="width: <?=$percent4.'%' ?>" aria-valuenow="19.32" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0 text-muted">{{ $total_star_4 }}</h6>
                      </div>
                    </div>
                  </div>
                  <!-- end row -->

                  <div class="row align-items-center g-2">
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0">3 star</h6>
                      </div>
                    </div>
                    <div class="col">
                      <div class="p-2">
                        <div class="progress animated-progress progress-sm">
                          <div class="progress-bar bg-success" role="progressbar" style="width: <?=$percent3.'%' ?>" aria-valuenow="18.12" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0 text-muted">{{ $total_star_3 }}</h6>
                      </div>
                    </div>
                  </div>
                  <!-- end row -->

                  <div class="row align-items-center g-2">
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0">2 star</h6>
                      </div>
                    </div>
                    <div class="col">
                      <div class="p-2">
                        <div class="progress animated-progress progress-sm">
                          <div class="progress-bar bg-warning" role="progressbar" style="width: <?=$percent2.'%' ?>" aria-valuenow="7.42" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    </div>

                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0 text-muted">{{ $total_star_2 }}</h6>
                      </div>
                    </div>
                  </div>
                  <!-- end row -->

                  <div class="row align-items-center g-2">
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0">1 star</h6>
                      </div>
                    </div>
                    <div class="col">
                      <div class="p-2">
                        <div class="progress animated-progress progress-sm">
                          <div class="progress-bar bg-danger" role="progressbar" style="width: <?=$percent1.'%' ?>" aria-valuenow="4.98" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-auto">
                      <div class="p-2">
                        <h6 class="mb-0 text-muted">{{ $total_star_1 }}</h6>
                      </div>
                    </div>
                  </div>
                  <!-- end row -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

       <button class="btn btn-danger" onclick="showreport()" style="width: 100%"><i class=" ri-bug-fill align-bottom me-1"></i> REPORT APP </button>


    </div>

  </div>
</div>
</div>


</div>
<!-- end tab pane -->
</div>
</div>
<!-- end col -->
</div>
<!-- end row -->


<!-- MODAL -->

<div class="modal " id="payment_info" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> </button>
      </div>

      <div class="modal-body text-center p-5">
       <lord-icon src="https://cdn.lordicon.com/slkvcfos.json" trigger="loop" colors="primary:#121331,secondary:#08a88a" style="width:120px;height:120px"></lord-icon>

       <div class="mt-4">
        <h4 class="mb-3">Beli Sekarang juga!</h4>
        <p class="text-muted mb-4"> Order untuk mendapatkan aplikasi terbaik kami</p>
        <div class="hstack gap-2 justify-content-center">
          <a href="{{ url('pg_application/cart') }}/{{ $application->uuid }}/{{ $application->appcode }}" class="btn btn-primary"><i class="ri-wallet-fill align-bottom me-1"></i> CHECKOUT</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<div class="modal " id="history_order" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> </button>
      </div>

      <div class="modal-body p-5">
        <div class="acitivity-timeline py-3">
            <div class="acitivity-item d-flex">
                <div class="avatar-xs acitivity-avatar">
                <div class="avatar-title bg-soft-{{ $members->avatar_color }} text-{{ $members->avatar_color }} rounded-circle">
                  @if(!empty($members->last_name))
                  {{ substr($members->first_name,0,1) }}{{ substr($members->last_name,0,1) }}
                  @else
                  {{ substr($members->first_name,0,1) }}
                  @endif
                </div>
              </div>
                <div class="flex-grow-1 ms-3">
                    <h6 class="mb-1">Oliver Phillips <span class="badge bg-soft-primary text-primary align-middle">New</span></h6>
                    <p class="text-muted mb-2">We talked about a project on linkedin.</p>
                    <small class="mb-0 text-muted">Today</small>
                </div>
            </div>
        </div>

    </div>
  </div>
</div>
</div>



<div class="modal " id="checkout_info" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> </button>
      </div>

      <div class="modal-body text-center p-5">
       <lord-icon src="https://cdn.lordicon.com/kbtmbyzy.json" trigger="loop" colors="primary:#121331,secondary:#08a88a" style="width:120px;height:120px"></lord-icon>

       <div class="mt-4">
        <h4 class="mb-3">Konfirmasi Pembayaran</h4>
        <p class="text-muted mb-4"> Status transaksi anda menunggu Konfirmasi pembayaran</p>
        <div class="hstack gap-2 justify-content-center">
          <a href="{{ url('pg_application/checkout') }}/{{ $application->uuid }}/{{ $application->appcode }}" class="btn btn-primary"><i class="ri-eye-fill align-bottom me-1"></i> LIHAT TRANSAKSI</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<div class="modal " id="verify_info" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> </button>
      </div>

      <div class="modal-body text-center p-5">
       <lord-icon src="https://cdn.lordicon.com/kbtmbyzy.json" trigger="loop" colors="primary:#121331,secondary:#08a88a" style="width:120px;height:120px"></lord-icon>

       <div class="mt-4">
        <h4 class="mb-3">Menunggu Verify pembayaran!</h4>
        <p class="text-muted mb-4"> Tim kami akan segera verifikasi pembayaran anda paling lambat 1x24 jam. Teirma kasih.</p>
        <div class="hstack gap-2 justify-content-center">
          <a href="{{ url('pg_application/checkout') }}/{{ $application->uuid }}/{{ $application->appcode }}" class="btn btn-primary"><i class="ri-eye-fill align-bottom me-1"></i> LIHAT TRANSAKSI</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div>




<div class="modal " id="demo_web" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"> </button>
      </div>

      <div class="modal-body text-center p-5">
       <lord-icon src="https://cdn.lordicon.com/kbtmbyzy.json" trigger="loop" colors="primary:#121331,secondary:#08a88a" style="width:120px;height:120px"></lord-icon>

       <div class="mt-4">
        <h4 class="mb-3">Warning</h4>
        <p class="text-muted mb-4"> Mohon maaf link belum tersedia.</p>
      </div>
    </div>
  </div>
</div>
</div>


<div class="modal fade" id="show_review" tabindex="-1" aria-labelledby="exampleModalgridLabel" aria-modal="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalgridLabel">FORM REVIEW</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" data-toggle="validator" id="form_data">
                    <div class="row g-3">
                        <div class="col-xxl-12">
                            <div>
                                <label for="firstName" class="form-label">Member Name</label>
                                <input type="text" class="form-control" id="fullname" value="{{ Session('name') }} " readonly required>
                            </div>
                        </div>
                        <div class="col-xxl-12">
                            <div>
                                <label for="emailInput" class="form-label">Application</label>
                                <input type="hidden" class="form-control" name="application_id" value="{{ $application->id }}">
                                <input type="text" class="form-control" name="application_name" value="{{ $application->name }}" readonly required>
                            </div>
                        </div>

                        <div class="col-xxl-12">
                            <div>
                                <label for="emailInput" class="form-label">Rate</label>
                                <select class="form-control" name="star" style="color:#f6b84b">
                                  <option value="5">5 &#9733; &#9733; &#9733; &#9733; &#9733;</option>
                                  <option value="4">4 &#9733; &#9733; &#9733; &#9733;</option>
                                  <option value="3">3 &#9733; &#9733; &#9733;</option>
                                  <option value="2">2 &#9733; &#9733;</option>
                                  <option value="1">1 &#9733;</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xxl-12">
                            <div>
                                <label for="passwordInput" class="form-label">Description</label>
                                <textarea class="form-control" name="desc" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="hstack gap-2 justify-content-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </form>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="show_report" tabindex="-1" aria-labelledby="exampleModalgridLabel" aria-modal="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalgridLabel">FORM REPORT</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" data-toggle="validator" id="form_data2">
                    <div class="row g-3">
                        <div class="col-xxl-12">
                            <div>
                                <label for="firstName" class="form-label">Member Name</label>
                                <input type="text" class="form-control" id="fullname" value="{{ Session('name') }} " readonly required>
                            </div>
                        </div>
                        <div class="col-xxl-12">
                            <div>
                                <label for="emailInput" class="form-label">Application</label>
                                <input type="hidden" class="form-control" name="application_id" value="{{ $application->id }}">
                                <input type="text" class="form-control" name="application_name" value="{{ $application->name }}" readonly required>
                            </div>
                        </div>

                        <div class="col-xxl-12">
                            <div>
                                <label for="emailInput" class="form-label">Category</label>
                                <select class="form-control" name="category" required>
                                  <option value="">Change Category</option>
                                  <option value="Error File Resources">Error File Resources</option>
                                  <option value="Bugs System">Bugs System</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xxl-12">
                            <div>
                                <label for="firstName" class="form-label">Subject</label>
                                <input type="text" class="form-control" name="subject" autocomplete="off" required>
                            </div>
                        </div>

                        <div class="col-xxl-12">
                            <div>
                                <label for="passwordInput" class="form-label">Description</label>
                                <textarea class="form-control" name="desc" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="hstack gap-2 justify-content-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </form>
            </div>
        </div>
    </div>
</div>


@section('page-script')
<script type="text/javascript">
  function Readmore(){
    $("#shortdesc").hide();
    $("#fulldesc").show();
    $("#readmore").hide();
  }

  function showReport(id){
    $("#modalreport"+id).modal('show');
  }

  function showreview(){
    $("#show_review").modal('show');
  }

  function showreport(){
    $("#show_report").modal('show');
  }

  function showVideo(){
    $("#demo_video").modal('show');
  }

  function showWeb(){
    var link = '{{ $application->link_web }}';
    if(!empty(link)){
       window.open(link, '_blank');
    }else{
      $("#demo_web").modal('show');
    }
    
  }
</script>


<script type="text/javascript">

  function downloadURI(uri, name, id) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.setAttribute("target", "_blank");
    link.click();
    link.remove();


    var file_id = id;

    $.ajax({
      type: 'POST',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '{{ url("hit_unduh")}}',
      data: 'file_id='+file_id+'&name='+name,
      success: function (data) {

      },
    });
  }
</script>

<script type="text/javascript">
  function getStart() {
    $("#btn_download").hide();
    $("#progress_bar").show();

    var d1 = new Date();
    d1.setSeconds(+ "30" + d1.getSeconds());
    var countDownDate = d1.getTime();
        // Update the count down every 1 second
        var x = setInterval(function() {
          // Get today's date and time
          var now = new Date().getTime();
          // Find the distance between now and the count down date
          var distance = countDownDate - now;
          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          // Output the result in an element with id="demo"
          document.getElementById("timer").innerHTML =  seconds + " detik lagi ";
          // If the count down is over, write some text 
          if (distance < 0) {
            clearInterval(x);

            $("#progress_bar").hide();
            $("#btn_unduh").fadeIn();

          }
        }, 1000);

        // 60 detik = 820

        var count=0;
        var ms = 530;
        var step = 5;
        var counter=setTimeout(timer, ms); //1000 will  run it every 1 second

        function timer()
        {
          count=count+1;
          if (count <= 100)
          {
            //Do code for showing the number of seconds here
             document.getElementById("count_percent").innerHTML=count +'%' ; // watch for spelling
             document.getElementById("percent_progress").style.width = count +'%';
             ms = ms - step;
             counter = setTimeout(timer, ms);

           }

         }


       }


       function showpayment(id){
        if(id == 1){
         $("#payment_info").modal('show');
       }else if(id == 2){
        $("#checkout_info").modal('show');
      }else if(id == 3){
        $("#verify_info").modal('show');

      }
    }

 
    
 $(function(){
        $("#form_data").submit(function(stay){
          stay.preventDefault(); 
           var formdata = $(this).serialize(); 
            $.ajax({
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url("testimoni/action")}}',
                enctype: "multipart/form-data",
                dataType : 'JSON',
                data: formdata,
                success: function (data) {
                    if(data.status == 'OK'){
                         Swal.fire(data.title, data.msg, "success");
                    }else{
                         Swal.fire(data.title, data.msg, "error");
                    }
                 setTimeout(() => {
                      window.location.reload();
                  }, 2000);
                },
            });
            stay.preventDefault(); 
        });
       });


 $(function(){
        $("#form_data2").submit(function(stay){
          stay.preventDefault(); 
           var formdata = $(this).serialize(); 
            $.ajax({
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url("report_testimoni/action")}}',
                enctype: "multipart/form-data",
                dataType : 'JSON',
                data: formdata,
                success: function (data) {
                    if(data.status == 'OK'){
                         Swal.fire(data.title, data.msg, "success");
                    }else{
                         Swal.fire(data.title, data.msg, "error");
                    }
                 setTimeout(() => {
                      window.location.reload();
                  }, 2000);
                },
            });
            stay.preventDefault(); 
        });
       });

 function history_order(){
         $("#history_order").modal('show');
      }


  </script>
  @stop
  @endsection