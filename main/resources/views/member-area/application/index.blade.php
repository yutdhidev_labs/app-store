@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Application</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                    <li class="breadcrumb-item active">Application</li>
                </ol>
            </div>

        </div>
    </div>
</div>

 <div class="row">
                        <div class="col-xl-3 col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-flex mb-3">
                                        <div class="flex-grow-1">
                                            <h5 class="fs-16">Filters</h5>
                                        </div>
                                    </div>

                                    <div class="filter-choices-input">
                                        <input class="form-control" data-choices data-choices-removeItem type="hidden" id="filter-choices-input" value="T-Shirts" />
                                    </div>
                                </div>

                                <div class="accordion accordion-flush filter-accordion">

                                    <div class="card-body border-bottom">
                                        <div>
                                            <p class="text-muted text-uppercase fs-12 fw-medium mb-2">Category</p>
                                            <ul class="list-unstyled mb-0 filter-list">
                                                @foreach($application_category as $val)
                                                <li>
                                                    <a href="javascript:void(0)" class="d-flex py-1 align-items-center">
                                                        <div class="flex-grow-1">
                                                            <h5 class="fs-13 mb-0 listname">{{ $val->name }}</h5>
                                                        </div>

                                                        <?php 
                                                        $cek = DB::table('application')->where('status_id',1)->where('category_id',$val->id)->count();
                                                        ?>
                                                        @if($cek > 0)
                                                        <div class="flex-shrink-0 ms-2">
                                                            <span class="badge bg-light text-muted">{{ $cek }}</span>
                                                        </div>
                                                        @endif
                                                    </a>
                                                </li>
                                                @endforeach


                                                
                                            </ul>
                                        </div>
                                    </div>
<!-- 
                                    <div class="card-body border-bottom">
                                        <p class="text-muted text-uppercase fs-12 fw-medium mb-4">Price</p>

                                        <div id="product-price-range"></div>
                                        <div class="formCost d-flex gap-2 align-items-center mt-3">
                                            <input class="form-control form-control-sm" type="text" id="minCost" value="0" /> <span class="fw-semibold text-muted">to</span> <input class="form-control form-control-sm" type="text" id="maxCost" value="5000000" />
                                        </div>
                                    </div> -->


                                </div>
                            </div>
                            <!-- end card -->
                        </div>
                        <!-- end col -->

                        <div class="col-xl-9 col-lg-8">
                            <div>
                                <div class="card">
                                    <div class="card-header border-0">
                                        <div class="row g-4">
                                            <div class="col-sm-auto">
                                                <div>

                                                    <a href="javascript:void(0)" onClick="window.location.href=window.location.href" class="btn btn-primary" id="addproduct-btn"><i class="ri-refresh-line align-bottom me-1"></i> Reload</a>
                                                </div>
                                            </div>
                                            <div class="col-sm">
                                                <div class="d-flex justify-content-sm-end">
                                                    <div class="search-box ms-2">
                                                        <input type="text" class="form-control" id="searchProductList" placeholder="Search Products...">
                                                        <i class="ri-search-line search-icon"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-header">
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <ul class="nav nav-tabs-custom card-header-tabs border-bottom-0" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active fw-semibold" data-bs-toggle="tab" href="#productnav-all" role="tab">
                                                            All <span class="badge badge-soft-danger align-middle rounded-pill ms-1">{{ $application }}</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link fw-semibold" data-bs-toggle="tab" href="#productnav-published" role="tab">
                                                            Free <span class="badge badge-soft-danger align-middle rounded-pill ms-1">{{ count($application_free) }}</span>
                                                        </a>
                                                    </li>
                                                    
                                                </ul>
                                            </div>
                                            <div class="col-auto">
                                                <div id="selection-element">
                                                    <div class="my-n1 d-flex align-items-center text-muted">
                                                        Select <div id="select-content" class="text-body fw-semibold px-1"></div> Result <button type="button" class="btn btn-link link-danger p-0 ms-3" data-bs-toggle="modal" data-bs-target="#removeItemModal">Remove</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end card header -->
                                    <div class="card-body">

                                        <div class="tab-content text-muted">
                                            <div class="tab-pane active" id="productnav-all" role="tabpanel">
                                                <div id="table-product-list-all" class="table-card gridjs-border-none"></div>
                                            </div>
                                            <!-- end tab pane -->

                                            <div class="tab-pane" id="productnav-published" role="tabpanel">
                                                <div id="table-product-list-published" class="table-card gridjs-border-none"></div>
                                            </div>
                                            <!-- end tab pane -->

                                        </div>
                                        <!-- end tab content -->

                                    </div>
                                    <!-- end card body -->
                                </div>
                                <!-- end card -->
                            </div>
                        </div>
                        <!-- end col -->
                    </div>

@section('page-script')

    <script src="{{ asset('assets/js/application.js') }}"></script>
@stop
@endsection
