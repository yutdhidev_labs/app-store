@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Dashboard</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card overflow-hidden">
            <div class="card-body bg-marketplace d-flex">
                <div class="flex-grow-1">
                    <h4 class="fs-18 lh-base mb-0">Selamat {{ $salam }}, <span class="text-success">{{ Session::get('name') }} </span> </h4>
                    <p class="mb-0 mt-2 pt-1 text-muted">Here's what's happening with your store today.</p>
                    <div class="d-flex gap-3 mt-4">
                        <a href="" class="btn btn-success" style="font-size: 16px">
                            <i class="ri-medal-fill align-bottom me-1" ></i>
                        Members Reguler</a>
                    </div>
                </div>
                <img src="{{ asset('assets/images/bg-d.png') }}" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</div>
   

@section('page-script')

@stop
@endsection
