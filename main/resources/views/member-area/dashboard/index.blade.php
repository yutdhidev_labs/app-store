@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Dashboard</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card overflow-hidden">
            <div class="card-body bg-marketplace d-flex">
                <div class="flex-grow-1">
                    <h4 class="fs-18 lh-base mb-0">{{ $salam }}, <span class="text-success">{{ Session::get('name') }} </span> </h4>
                    <p class="mb-0 mt-2 pt-1 text-muted">Here's what's happening with your store today.</p>
                    <div class="d-flex gap-3 mt-4">
                        <a href="" class="btn btn-success" style="font-size: 16px">
                            <i class="ri-medal-fill align-bottom me-1" ></i>
                            Members {{ $members->status_membership }}</a>
                        </div>
                    </div>
                    <img src="{{ asset('assets/images/bg-d.png') }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    @if($members->status_membership == 'Reguler')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show mb-xl-0" role="alert">
                        <i class="ri-error-warning-line label-icon"></i><strong>Informasi Akun</strong>
                        Upgrade sekarang juga akun membership anda menjadi >> <a href="{{ url('pg_upgrade_account') }}">Members Premium</a>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
            </div>

        </div>


        <div class="row justify-content-center mt-0">
            <div class="col-lg-5">
                <div class="text-center mb-4 pb-2">
                    <h4 class="fw-semibold fs-22">Pilih paket membership Anda</h4>
                    <p class="text-muted mb-4 fs-15">Harga terjangkau dan banyak fitur yang didapatkan untuk Anda.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card pricing-box text-center">
                    <div class="row g-0">
                        <div class="col-lg-6">
                            <div class="card-body h-100">
                                <div>
                                    <h5 class="mb-1">Reguler</h5>
                                    <p class="text-muted">Paket Reguler</p>
                                </div>

                                <div class="py-4">
                                    <h2><sup><small>Rp.</small></sup>0 <span class="fs-13 text-muted"> / Lifetime</span></h2>
                                </div>

                                <div class="text-center plan-btn mt-2">
                                    <a href="{{ url('register') }}" class="btn btn-success w-sm waves-effect waves-light">Join Members</a>
                                </div>

                            </div>
                        </div>
                        <!--end col-->
                        <div class="col-lg-6">
                            <div class="card-body border-start mt-4 mt-lg-0">
                                <div class="card-header bg-light">
                                    <h5 class="fs-15 mb-0">Fitur Reguler :</h5>
                                </div>
                                <div class="card-body pb-0">
                                    <ul class="list-unstyled vstack gap-3 mb-0">
                                        <li>Account: <span class="text-success fw-semibold">Unlimited</span></li>
                                        <li>Aplikasi Gratis: <span class="text-success fw-semibold">Terbatas</span></li>
                                        <li>Support: <span class="text-success fw-semibold">Tidak</span></li>
                                        <li>Diskon Pembelian: <span class="text-success fw-semibold">Tidak</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </div>
            </div>
            <!--end row-->

            <div class="col-lg-6">
                <div class="card pricing-box ribbon-box ribbon-fill text-center">
                    <div class="ribbon ribbon-primary">BEST</div>
                    <div class="row g-0">
                        <div class="col-lg-6">
                            <div class="card-body h-100">
                                <div>
                                    <h5 class="mb-1">Premium</h5>
                                    <p class="text-muted">Paket Premium</p>
                                </div>

                                <div class="py-4">
                                    <h2><sup><small>Rp.</small></sup>{{ number_format($premium->price,0,",",".") }}<span class="fs-13 text-muted">/ Lifetime</span></h2>
                                </div>

                                <div class="text-center plan-btn mt-2">
                                    <a href="{{ url('register') }}" class="btn btn-success w-sm waves-effect waves-light">Join Members</a>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                        <div class="col-lg-6">
                            <div class="card-body border-start mt-4 mt-lg-0">
                                <div class="card-header bg-light">
                                    <h5 class="fs-15 mb-0">Fitur Premium :</h5>
                                </div>
                                <div class="card-body pb-0">
                                    <ul class="list-unstyled vstack gap-3 mb-0">
                                        <li>Account: <span class="text-success fw-semibold">Unlimited</span></li>
                                        <li>Aplikasi Gratis: <span class="text-success fw-semibold">Unlimited</span></li>
                                        <li>Support: <span class="text-success fw-semibold">Ya</span></li>
                                        <li>Diskon Pembelian: <span class="text-success fw-semibold">Ya</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
                </div>
            </div>
            <!--end row-->
        </div>
        @endif

        @section('page-script')

        @stop
        @endsection
