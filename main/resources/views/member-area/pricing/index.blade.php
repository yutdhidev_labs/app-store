@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Pricing</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                    <li class="breadcrumb-item active">Pricing</li>
                </ol>
            </div>

        </div>
    </div>
</div>


<div class="row justify-content-center mt-5">
    <div class="col-lg-5">
        <div class="text-center mb-4 pb-2">
            <h4 class="fw-semibold fs-22">Choose the plan that's right for you</h4>
            <p class="text-muted mb-4 fs-15">Simple pricing. No hidden fees. Advanced features for you business.</p>
        </div>
    </div>
    <!--end col-->
</div>

 <div class="row">
                        <div class="col-lg-6">
                            <div class="card pricing-box text-center">
                                <div class="row g-0">
                                    <div class="col-lg-6">
                                        <div class="card-body h-100">
                                            <div>
                                                <h5 class="mb-1">Basic</h5>
                                                <p class="text-muted">Basic plans</p>
                                            </div>

                                            <div class="py-4">
                                                <h2><sup><small>Rp.</small></sup>0 <span class="fs-13 text-muted"> / Lifetime</span></h2>
                                            </div>

                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-6">
                                        <div class="card-body border-start mt-4 mt-lg-0">
                                            <div class="card-header bg-light">
                                                <h5 class="fs-15 mb-0">Plan Features:</h5>
                                            </div>
                                            <div class="card-body pb-0">
                                                <ul class="list-unstyled vstack gap-3 mb-0">
                                                    <li>Account: <span class="text-success fw-semibold">Unlimited</span></li>
                                                    <li>Free Application: <span class="text-success fw-semibold">1</span></li>
                                                    <li>Support: <span class="text-success fw-semibold">No</span></li>
                                                    <li>Discount Orders: <span class="text-success fw-semibold">No</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                        </div>
                        <!--end row-->

                        <div class="col-lg-6">
                            <div class="card pricing-box ribbon-box ribbon-fill text-center">
                                <div class="ribbon ribbon-primary">BEST</div>
                                <div class="row g-0">
                                    <div class="col-lg-6">
                                        <div class="card-body h-100">
                                            <div>
                                                <h5 class="mb-1">Pro Premium</h5>
                                                <p class="text-muted">Pro premium plans</p>
                                            </div>

                                            <div class="py-4">
                                                <h2><sup><small>Rp.</small></sup>300.000 <span class="fs-13 text-muted">/ Lifetime</span></h2>
                                            </div>

                                            <div class="text-center plan-btn mt-2">
                                                <a href="javascript:void(0);" class="btn btn-success w-sm waves-effect waves-light">Upgrade Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-6">
                                        <div class="card-body border-start mt-4 mt-lg-0">
                                            <div class="card-header bg-light">
                                                <h5 class="fs-15 mb-0">Plan Features:</h5>
                                            </div>
                                            <div class="card-body pb-0">
                                                <ul class="list-unstyled vstack gap-3 mb-0">
                                                    <li>Account: <span class="text-success fw-semibold">Unlimited</span></li>
                                                    <li>Free Application: <span class="text-success fw-semibold">Unlimited</span></li>
                                                    <li>Support: <span class="text-success fw-semibold">Yes</span></li>
                                                    <li>Discount Orders: <span class="text-success fw-semibold">Yes</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </div>
                        </div>
                        <!--end row-->
                       </div>


@section('page-script')

@stop
@endsection