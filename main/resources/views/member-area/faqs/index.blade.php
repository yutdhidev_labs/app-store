@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Dashboard</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row">
                        <div class="col-lg-12">
                            <div class="card rounded-0 bg-soft-success mx-n4 mt-n4 border-top">
                                <div class="px-4">
                                    <div class="row">
                                        <div class="col-xxl-5 align-self-center">
                                            <div class="py-4">
                                                <h4 class="display-6 coming-soon-text">Frequently asked questions</h4>
                                                <p class="text-success fs-15 mt-3">Jika Anda tidak dapat menemukan jawaban atas pertanyaan Anda di FAQ kami, Anda selalu dapat menghubungi kami. Kami akan segera menjawab Anda!</p>
                                                <div class="hstack flex-wrap gap-2">
                                                    <button type="button" class="btn btn-primary btn-label rounded-pill"><i class="ri-mail-line label-icon align-middle rounded-pill fs-16 me-2"></i> Contact Us</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xxl-3 ms-auto">
                                            <div class="mb-n5 pb-1 faq-img d-none d-xxl-block">
                                                <img src="assets/images/faq-img.png" alt="" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!-- end card -->

                            <div class="row justify-content-evenly">
                                <div class="col-lg-6">
                                    <div class="mt-3">
                                        <div class="d-flex align-items-center mb-2">
                                            <div class="flex-shrink-0 me-1">
                                                <i class="ri-question-line fs-24 align-middle text-success me-1"></i>
                                            </div>
                                            <div class="flex-grow-1">
                                                <h5 class="fs-16 mb-0 fw-semibold">General Questions</h5>
                                            </div>
                                        </div>

                                        <div class="accordion accordion-border-box" id="genques-accordion">
                                           
                                            @foreach($general as $val)
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="genques-headingTwo{{$val->id}}">
                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#genques-collapseTwo{{ $val->id}}" aria-expanded="false" aria-controls="genques-collapseTwo{{ $val->id}}">
                                                        {{ $val->name }}
                                                    </button>
                                                </h2>
                                                <div id="genques-collapseTwo{{ $val->id}}" class="accordion-collapse collapse" aria-labelledby="genques-headingTwo{{$val->id}}" data-bs-parent="#genques-accordion">
                                                    <div class="accordion-body">
                                                         {{ $val->desc }}
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            
                                        </div>
                                        <!--end accordion-->
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="mt-3">
                                        <div class="d-flex align-items-center mb-2">
                                            <div class="flex-shrink-0 me-1">
                                                <i class="ri-user-settings-line fs-24 align-middle text-success me-1"></i>
                                            </div>
                                            <div class="flex-grow-1">
                                                <h5 class="fs-16 mb-0 fw-semibold">Payment Information</h5>
                                            </div>
                                        </div>

                                        <div class="accordion accordion-border-box" id="manageaccount-accordion">
                                             @foreach($payment as $val)
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="manageaccount-headingOne">
                                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#manageaccount-collapseOne{{$val->id}}" aria-expanded="false" aria-controls="manageaccount-collapseOne{{$val->id}}">
                                                       {{ $val->name }}
                                                    </button>
                                                </h2>
                                                <div id="manageaccount-collapseOne{{$val->id}}" class="accordion-collapse collapse" aria-labelledby="manageaccount-headingOne" data-bs-parent="#manageaccount-accordion">
                                                    <div class="accordion-body">
                                                        {{ $val->desc }}
                                                    </div>
                                                </div>
                                            </div>

                                            @endforeach
                                        </div>
                                        <!--end accordion-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->.
                    </div>
@section('page-script')

@stop
@endsection
