@extends('layouts.master_backend')
@section('content')

<div class="row">
  <div class="col-12">
    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
      <h4 class="mb-sm-0 font-size-18">Profile</h4>

      <div class="page-title-right">
        <ol class="breadcrumb m-0">
          <li class="">Home</li>
          <li class="">&nbsp; > &nbsp; </li>
          <li class="breadcrumb-item active"><a href="{{ url('profile') }}">Profile</a></li>
        </ol>
      </div>

    </div>
  </div>
</div>

 @if ($message = Session::get('success'))
                      <div class="alert alert-success alert-dismissible alert_notif">
                          <i class="icon fa fa-check"></i> {{ $message }}
                      </div>
                    @elseif ($message = Session::get('failed'))
                      <div class="alert alert-danger alert-dismissible alert_notif">
                        <i class="icon fa fa-info"></i> {{ $message }}
                      </div>
                     @elseif ($message = Session::get('warning'))
                      <div class="alert alert-warning alert-dismissible alert_notif">
                           <i class="icon fa fa-info"></i> {{ $message }}
                        </div>
                     @endif

   <div class="row">
                            <div class="col-xl-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4">Informasi Data Diri</h4>

                                        <p class="text-muted mb-4">Hi Saya {{ $members->name }}</p>
                                        <div class="table-responsive">
                                            <table class="table table-nowrap mb-0">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Nama :</th>
                                                        <td>{{ $members->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">No WhatsApp :</th>
                                                        <td>{{ $members->telp }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">E-mail :</th>
                                                        <td>{{ $members->email }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Jenis Kelamin :</th>
                                                        <td>{{ $members->jenis_kelamin }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Username :</th>
                                                        <td>{{ $members->username }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- end card -->

                            </div>         
                            
                            <div class="col-xl-8">

                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Form Update Profile</h4>
                                <p class="card-title-desc">Informasi Biodata</p>

                                 <form action="{{ url('change_profile/action') }}" method="POST" enctype="multipart/form-data">
                                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                 <input type="hidden" value="{{ $members->id }}" name="members_id">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-3">
                                                          <label >No Telp / No WhatsApp*</label>
                                                          <input type="text" class="form-control" id="telp" name="telp" placeholder="" autocomplete="off" required value="{{ $members->telp }}">
                                                        </div>
                                                </div>
                                                 <div class="col-md-6">
                                                <div class="form-group mb-3">
                                                      <label >Nama Lengkap*</label>
                                                      <input type="text" class="form-control" id="name" name="name" placeholder="" autocomplete="off" required value="{{ $members->name }}">
                                                 </div>
                                                </div>
                                            </div>

                                             <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                          <label >Email*</label>
                                                          <input type="email" class="form-control" id="email" name="email" placeholder="" autocomplete="off" required value="{{ $members->email }}">
                                                        </div>
                                                </div>
                                                 <div class="col-md-6">
                                                <div class="" id="">
                                                            <label>Jenis Kelamin *</label>
                                                            <select class="form-control " name="jenis_kelamin" id="jenis_kelamin" style="width: 100%;" required>
                                                              <option value="">Pilih</option>
                                                              <option value="Laki-Laki" <?php if($members->jenis_kelamin == 'Laki-Laki') echo 'selected="selected"';?>>Laki-Laki</option>
                                                              <option value="Perempuan" <?php if($members->jenis_kelamin == 'Perempuan') echo 'selected="selected"';?>>Perempuan</option>
                                                            </select>
                                                          </div>
                                                </div>
                                            </div>

                                                <div class="row">
                                                <div class="col-md-12">
                                                      <div class="form-group mb-3">
                                                      <label >Alamat*</label>
                                                      <input type="text" class="form-control" id="alamat" name="alamat" placeholder="" autocomplete="off" required value="{{ $members->alamat }}">
                                                 </div>
                                                </div>
                                            </div>

                                    <button type="submit" class="btn btn-primary mr-1 waves-effect waves-light">
                                        <i class="fa fa-save"></i> Update Data</button>
                                </form>

                            </div>
                        </div>
                               

                            </div>
                        </div>

@section('page-script')
<script type="text/javascript">
  $( document ).ready(function() {
    $("#tablelist").datatable();
});
</script>
@stop
@endsection
