@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Ubah Password</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('change_password') }}">Ubah Password</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">


                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible alert_notif">
                  <i class="icon fa fa-check"></i> {{ $message }}
              </div>
              @elseif ($message = Session::get('failed'))
              <div class="alert alert-danger alert-dismissible alert_notif">
                <i class="icon fa fa-info"></i> {{ $message }}
            </div>
            @elseif ($message = Session::get('warning'))
            <div class="alert alert-warning alert-dismissible alert_notif">
              <i class="icon fa fa-info"></i> {{ $message }}
          </div>
          @endif

          <h4 class="card-title">Form Update Password</h4>
          <p class="card-title-desc">Buat Password yang mudah di ingat dan Gunakanlah Kombinasi Angka dan Huruf meningkatkan keamanan</p>

          <form action="{{ url('change_password/action') }}" method="POST" enctype="multipart/form-data">
           <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
           <input type="hidden" value="{{ session('members_id') }}" name="id_user">
           <div class="row">

            <div class="col-sm-12">

                <div class="form-group">
                    <label for="productdesc">Masukkan Password Baru *</label>
                    <input type="password" class="form-control" id="password1" name="password" placeholder="Password" onkeyup="getPass()" autocomplete="off" required>
                </div>
                <br>

                <div class="form-group">
                    <label for="productdesc">Masukkan Password Baru *</label>
                    <input type="password" class="form-control" id="password2" name="password2" placeholder="Ulangi Password" onkeyup="getPass2()" autocomplete="off" required>
                    <div id="notif_pass"></div>
                </div>

            </div>
        </div>

        <br>

        <button type="submit" class="btn btn-success mr-1 waves-effect waves-light">Update Password</button>
    </form>

</div>
</div>
</div>
</div>
@section('page-script')
<script type="text/javascript">
    function getPass(){
        $("#password2").val("");
        $("#notif_pass").html("");
        $("#btnSubmit").hide();
    }
    function getPass2(){
        var passsword1 = $("#password1").val();
        var passsword2 = $("#password2").val();
        if(passsword1 == passsword2){
            $("#notif_pass").html('<span style="font-size: 13px;  color: green;"><i class="fa fa-check"></i> Password Sama</span>');
            $("#btnSubmit").show();
        }else{
           $("#notif_pass").html(' <span style="font-size: 13px;  color: red;"><i class="fa fa-remove"></i> Password Tidak Sama</span>');
           $("#btnSubmit").hide();
       }
   }
</script>
@stop
@endsection
