@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Abouts Us</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Abouts Us</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

    <form class="form p-t-20" method="POST" data-toggle="validator" id="form_data">
<div class="row" id="form_input">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Abouts</h4>
                        <hr>

                         <div class="row">
                            <div class="col-md-6">
                                 <div class="mb-3">
                                        <label for="formrow-firstname-input" class="form-label">Name</label>
                                        <input type="text" class="form-control" autocomplete="off" name="name" id="name" value="{{ $abouts->name }}">
                                    </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="formrow-firstname-input" class="form-label">Tagline</label>
                                    <input type="text" class="form-control" autocomplete="off" name="tagline" id="tagline" value="{{ $abouts->tagline }}">
                                </div>
                            </div>
                        </div>

                       

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="formrow-firstname-input" class="form-label">Short Description</label>
                                   <textarea class="form-control" cols="10" rows="5" name="short_desc">{{ $abouts->short_desc }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="formrow-firstname-input" class="form-label">Description</label>
                                   <textarea class="form-control" cols="10" rows="5" name="long_desc">{{ $abouts->long_desc }}</textarea>
                                </div>
                            </div>
                        </div>

                            <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="ri-save-2-line"></i> Simpan Data</button>
                                </div>
                            </div>
                </div>
            </div>
        </div>
</div>
</form>

@section('page-script')

<script type="text/javascript">


     $(function(){
        $("#form_data").submit(function(stay){
          stay.preventDefault(); 
           var formdata = $(this).serialize(); 
           // var fileData  = $('input[name=file]').val();
            $.ajax({
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url("abouts/action")}}',
                enctype: "multipart/form-data",
                dataType : 'JSON',
                data: formdata,
                success: function (data) {
                    if(data.status == 'OK'){
                         Swal.fire(data.title, data.msg, "success");
                    }else{
                         Swal.fire(data.title, data.msg, "error");
                    }
                        setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                },
            });
            stay.preventDefault(); 
        });
       });
    

</script>
@stop
@endsection