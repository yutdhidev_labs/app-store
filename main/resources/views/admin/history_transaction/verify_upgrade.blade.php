@extends('layouts.master_backend')
@section('content')


<div class="row">
  <div class="col-12">
    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
      <h4 class="mb-sm-0">Verify Transaction</h4>

      <div class="page-title-right">
        <ol class="breadcrumb m-0">
          <li class="breadcrumb-item"><a href="javascript: void(0);">Master Data</a></li>
          <li class="breadcrumb-item active">Verify Transaction</li>
        </ol>
      </div>

    </div>
  </div>
</div>

<div class="row">
  @php $no=1; @endphp
  @if(count($transaction) > 0)
  @foreach($transaction as $val)
  <div class="col-xxl-4 col-sm-6 project-card">
    <div class="card card-height-100">
      <div class="card-body">
        <div class="d-flex flex-column h-100">
          <div class="d-flex">
            <div class="flex-grow-1">
              <p class="text-muted mb-4"> {{ Carbon\Carbon::parse($val->created_at)->diffForHumans()}}</p>
            </div>
            <div class="flex-shrink-0">
              <div class="d-flex gap-1 align-items-center">
                <button type="button" class="btn avatar-xs mt-n1 p-0 favourite-btn">
                  <span class="avatar-title bg-transparent fs-15">
                    <i class="ri-star-fill"></i>
                  </span>
                </button>
                <div class="dropdown">
                  <div class="dropdown-menu dropdown-menu-end">
                    <a class="dropdown-item" href="javascript:void(0)" onclick="info_detail('{{ $val->invoice }}')"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="d-flex mb-2">
            <div class="flex-shrink-0 me-3">
              <div class="avatar-md bg-light rounded p-1">
                                                            <img src="{{ asset('dist/level.png') }}" alt="" class="img-fluid d-block">
                                                        </div>
            </div>
            <div class="flex-grow-1">
              <h5 class="mb-1 fs-15"><a href="javascript:void(0)" onclick="info_detail('{{ $val->invoice }}')" class="text-dark">Upgrade Membership</a></h5>
              <p class="text-muted text-truncate-two-lines mb-3">{{ $val->first_name }} {{ $val->last_name }}</p>
            </div>
          </div>
          <div class="mt-auto">
            <div class="d-flex mb-2">
              <div class="flex-grow-1">
                <div>
                  @if($val->norek_tujuan == '2990 611 249')
                    BCA <br>
                  @else
                    BRI
                  @endif

                  {{ $val->norek_tujuan }}</div>
              </div>
              <div class="flex-shrink-0">
                <div>Rp. {{ number_format($val->price,0,",",".") }}</div>
              </div>
            </div>
           
          </div>
        </div>

      </div>
      <!-- end card body -->
      <div class="card-footer bg-transparent border-top-dashed py-2">
        <div class="d-flex align-items-center">
          <div class="flex-grow-1">
           
          </div>
          <div class="flex-shrink-0">
            <div class="text-muted">
              <i class="ri-calendar-event-fill me-1 align-bottom"></i>  {{ date('d M Y H:i', strtotime($val->created_at )) }}
            </div>
          </div>

        </div>

      </div>
      <!-- end card footer -->
    </div>
    <!-- end card -->
  </div>
  @endforeach
</div>


  @foreach($transaction as $val)
<div id="info_detail{{ $val->invoice}}" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                  <div class="modal-content border-0 overflow-hidden">
                    <div class="modal-body login-modal" style="padding: 30px">
                      <h5 class="text-white fs-20">Detail Transaction</h5>
                      <p class="text-white-50 mb-0">#{{ $val->invoice }} - {{ $val->first_name }} {{ $val->last_name }} - {{ $val->members_id }}</p>
                    </div>
                    <div class="modal-body">

                      <center><h5 class="fs-15">
                         @if($val->payment_status == 'Checkout')
                      <span class="badge badge-soft-primary text-uppercase">Checkout</span>
                    @elseif($val->payment_status == 'Verify')
                      <span class="badge badge-soft-info text-uppercase">Verify Payment</span>
                    @elseif($val->payment_status == 'Done')
                      <span class="badge badge-soft-success text-uppercase">Done</span>
                    @elseif($val->payment_status == 'Expired')
                      <span class="badge badge-soft-danger text-uppercase">Expired</span>
                    @endif
                      </h5></center>
                      <div class="table-responsive">
                        <table class="table table-bordered table-nowrap mb-0">
                              
                              <tr>
                                  <th class="text-nowrap" scope="row">Application</th>
                                  <td>Upgrade Membership</td>
                              </tr>
                              <tr>
                                  <th class="text-nowrap" scope="row">Category</th>
                                  <td>Premium</td>
                              </tr>
                              <tr>
                                  <th class="text-nowrap" scope="row">Price</th>
                                  <td>Rp. {{ number_format($val->price,0,",",".") }}</td>
                              </tr>
                              <tr>
                                  <th class="text-nowrap" scope="row">a/n Rekening</th>
                                  <td>{{ $val->rek_name }}</td>
                              </tr>
                        </table>
                    </div>

                    <br>

                     <?php 
                        $bukti = DB::table('confirm_payment')->where('invoice_id',$val->uuid)->where('members_id',$val->members_id)->orderBy('id','DESC')->first();

                        ?>

                        <img src="{{ asset('dist/payment/confirm') }}/{{ $val->filename }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'" alt="" class="img-fluid p-1">
                 </div>
                 <div class="modal-footer">
                    <button type="button" class="btn btn-primary " onclick="getConfirm('{{ $val->invoice }}','{{ $val->invoice_id }}','{{ $val->members_id }}')"><i class="ri-check-double-fill align-bottom me-1"></i> VERIFY NOW</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">CLOSE</button>
                </div>


               </div>
             </div>
           </div>
@endforeach
@else

  <div class="card">
  <div class="card-body">
<div class="text-center py-5">

        <div class="mb-4">
          <lord-icon src="https://cdn.lordicon.com/msoeawqm.json" trigger="loop" colors="primary:#405189,secondary:#0ab39c" style="width: 120px;height: 120px"></lord-icon>
        </div>
        <h5>Daftar Pembayaran transaction</h5>
        <p class="text-muted">Mohon maaf ! Daftar konfirmasi pembayaran belum tersedia.</p>
    </div>
    </div>
    </div>


@endif






@section('page-script')
<script type="text/javascript">
 function showdetail(id){
  $("#modaldetail"+id).modal('show');
}

function info_detail(id){
  $("#info_detail"+id).modal('show');
}

function downloadURI(uri, name, id) {
  var link = document.createElement("a");
  link.download = name;
  link.href = uri;
  link.setAttribute("target", "_blank");
  link.click();
  link.remove();
}


  function getConfirm(inv, orderid,members_id) {
    
    $.ajax({
      type: 'POST',
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      url: '{{ url("verify/action")}}',
       dataType : 'JSON',
      data: 'inv='+inv+'&orderid='+orderid+'&members_id='+members_id,
      success: function (data) {
        if(data.status == 'OK'){
               Swal.fire(data.title, data.msg, "success");
          }else{
               Swal.fire(data.title, data.msg, "error");
          }
       setTimeout(() => {
            window.location.reload();
        }, 2000);
      },
    });
  }
</script>
@stop
@endsection