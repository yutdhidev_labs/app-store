@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">History Transaction</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Master Data</a></li>
                    <li class="breadcrumb-item active">History Transaction</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

              @if ($message = Session::get('success'))
                 <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-check-double-line label-icon"></i>
                       {{ $message }}
                  </div>
              @elseif ($message = Session::get('error'))
                  <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-error-warning-line label-icon"></i>
                       {{ $message }}
                  </div>
              @endif

                <div class="row">
                    <div class="col-md-10">
                      <h4 class="card-title">TRANSACTION</h4>
                      <p class="card-title-desc">History List</p>
                  </div>
                  <div class="col-md-2">
                   <button type="button" class="btn btn-info btn-sm waves-effect waves-light" onClick="window.location.href=window.location.href" title="Refresh"><i class="ri-refresh-line align-bottom me-1"></i> Reload </button>

               </div>
           </div>

          <table id="alternative-pagination" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
             <thead>
                <tr>
                    <th>Invoice</th>
                    <th>Members</th>
                    <th>Application</th>
                    <th>Price</th>
                    <th>Checkout</th>
                    <th>Paid</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @php $no=1; @endphp
                @foreach($transaction as $val)
                <tr>
                   <td>
                    <a href="javascript:void(0)" onclick="showdetail('{{ $val->invoice }}')"> 
                    {{ $val->invoice }}
                  </a>
                  </td>
                   <td>{{ $val->first_name }} {{ $val->last_name }}</td>
                   <td>{{ $val->application_name }}</td>
                   <td align="right">Rp. {{ number_format($val->price,0,",",".") }}</td>
                   <td>
                     {{ date('d M Y H:i', strtotime($val->checkout_date )) }}
                   </td>
                   <td>
                     {{ date('d M Y H:i', strtotime($val->paid_date )) }}
                   </td>
                   <td>
                    @if($val->payment_status == 'Checkout')
                      <span class="badge badge-soft-primary text-uppercase">Checkout</span>
                    @elseif($val->payment_status == 'Verify')
                      <span class="badge badge-soft-info text-uppercase">Verify Payment</span>
                    @elseif($val->payment_status == 'Done')
                      <span class="badge badge-soft-success text-uppercase">Done</span>
                    @elseif($val->payment_status == 'Expired')
                      <span class="badge badge-soft-danger text-uppercase">Expired</span>
                    @endif

                    <div id="modaldetail{{ $val->invoice}}" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                  <div class="modal-content border-0 overflow-hidden">
                    <div class="modal-body login-modal" style="padding: 30px">
                      <h5 class="text-white fs-20">Detail Transaction</h5>
                      <p class="text-white-50 mb-0">#{{ $val->invoice }} - {{ $val->first_name }} {{ $val->last_name }} - {{ $val->members_id }}</p>
                    </div>
                    <div class="modal-body">

                      <center><h5 class="fs-15">
                         @if($val->payment_status == 'Checkout')
                      <span class="badge badge-soft-primary text-uppercase">Checkout</span>
                    @elseif($val->payment_status == 'Verify')
                      <span class="badge badge-soft-info text-uppercase">Verify Payment</span>
                    @elseif($val->payment_status == 'Done')
                      <span class="badge badge-soft-success text-uppercase">Done</span>
                    @elseif($val->payment_status == 'Expired')
                      <span class="badge badge-soft-danger text-uppercase">Expired</span>
                    @endif
                      </h5></center>
                      <h6 class="fs-15">Application</h6>
                      <div class="table-responsive">
                        <table class="table table-bordered table-nowrap mb-0">
                              
                              <tr>
                                  <th class="text-nowrap" scope="row">Name</th>
                                  <td>{{ $val->application_name }}</td>
                              </tr>
                              <tr>
                                  <th class="text-nowrap" scope="row">Category</th>
                                  <td>{{ $val->category_name }}</td>
                              </tr>
                              <tr>
                                  <th class="text-nowrap" scope="row">Price</th>
                                  <td>Rp. {{ number_format($val->price,0,",",".") }}</td>
                              </tr>
                        </table>
                    </div>

                    <br>

                     <?php 
                        $bukti = DB::table('confirm_payment')->where('invoice_id',$val->uuid)->where('members_id',$val->members_id)->orderBy('id','DESC')->first();

                        ?>


                      <h6 class="fs-15">History</h6>
                     <div class="acitivity-timeline py-3">
                          <div class="acitivity-item d-flex">
                              <div class="avatar-xs acitivity-avatar">
                              <div class="avatar-title bg-soft-success text-success rounded-circle">
                                <i class="ri-checkbox-circle-fill text-success"></i>
                              </div>
                            </div>
                              <div class="flex-grow-1 ms-3">
                                  <h6 class="mb-1">Checkout Pesanan</h6>
                                  <p class="text-muted mb-2">
                                  {{ date('d F Y - H:i', strtotime($val->checkout_date)) }}</p>
                              </div>
                          </div>



                          <div class="acitivity-item d-flex">
                               <div class="avatar-xs acitivity-avatar">
                              <div class="avatar-title bg-soft-success text-success rounded-circle">
                                <i class="ri-checkbox-circle-fill text-success"></i>
                              </div>
                            </div>
                              <div class="flex-grow-1 ms-3">
                                   <h6 class="mb-1">Konfirmasi Pembayaran</h6>
                                   <p class="text-muted mb-2">
                                    @if($val->payment_status == 'Verify')
                                    <b>Transfer ke {{ $bukti->norek_tujuan }}</b> <br>
                                    {{ date('d F Y - H:i', strtotime($val->paid_date)) }}
                                    @else
                                    -
                                    @endif
                                  </p>
                              </div>
                          </div>

                           @if($val->payment_status == 'Expired')
                           <div class="acitivity-item d-flex">
                               <div class="avatar-xs acitivity-avatar">
                              <div class="avatar-title bg-soft-success text-success rounded-circle">
                                <i class="ri-checkbox-circle-fill text-success"></i>
                              </div>
                            </div>
                              <div class="flex-grow-1 ms-3">
                                   <h6 class="mb-1">Pesanan Expired</h6>
                                   <p class="text-muted mb-2">
                                    {{ date('d F Y - H:i', strtotime($val->expired_date)) }}
                                  </p>
                              </div>
                          </div>
                           @endif


                          <div class="acitivity-item d-flex">
                               <div class="avatar-xs acitivity-avatar">
                              <div class="avatar-title bg-soft-success text-success rounded-circle">
                                <i class="ri-checkbox-circle-fill text-success"></i>
                              </div>
                            </div>
                              <div class="flex-grow-1 ms-3">
                                   <h6 class="mb-1">Pesanan Selesai</h6>
                                   <p class="text-muted mb-2">
                                    @if($val->payment_status == 'Done')
                                    {{ date('d F Y - H:i', strtotime($val->approved_date)) }}
                                    @else
                                    -
                                    @endif
                                  </p>
                              </div>
                          </div>


                      </div>


                      <div class="pt-3 border-top border-top-dashed mt-0">
                        <h6 class="mb-3 fw-semibold text-uppercase">Bukti Pembayaran</h6>
                        <div class="row g-3">

                          <div class="col-xxl-12 col-lg-12">
                            <div class="border rounded border-dashed p-2">
                              <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 me-3">
                                  <div class="avatar-sm">
                                    
                                    <div class="avatar-title bg-light text-primary rounded fs-24">
                                      <i class="ri-file-text-fill"></i>
                                    </div>
                                  </div>
                                </div>
                                 
                                     @if(!empty($bukti))
                                <div class="flex-grow-1 overflow-hidden">


                                  <h5 class="fs-13 mb-1"><a href="#" class="text-body text-truncate d-block">Rekening a/n {{ $bukti->rek_name}} </a></h5>
                                  <div>
                                    <?php
                                    $file = "dist/payment/confirm/".$bukti->filename;

                                    if((is_file($file))&&(file_exists($file))){
                                     $size = filesize($file);
                                     $mod = 1024;
                                     $units = explode(' ','B KB MB GB TB PB');
                                     for ($i = 0; $size > $mod; $i++) {
                                      $size /= $mod;
                                    }
                                    echo round($size, 2) . ' ' . $units[$i];
                                  }else{
                                    echo '0 KB';
                                  }   

                                  ?>
                                </div>
                              </div>
                              <div class="flex-shrink-0 ms-2">
                                <div class="d-flex gap-1">

                                 <?php 
                                 $uri_confirm = asset('dist/payment/confirm')."/".$bukti->filename;
                                 $filename = $bukti->filename;
                                 ?>


                                 <a href="{{ $uri_confirm }}" target="_blank">
                                   <button type="button" class="btn btn-icon text-muted btn-sm fs-18" style="cursor: pointer"><i class="ri-eye-line"></i></button>
                                 </a>

                                 <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="downloadURI('{{ $uri_confirm }}', '{{ $filename }}', '{{ $val->id }}');" style="cursor: pointer"><i class="ri-download-2-line"></i></button>


                               </div>
                             </div>
                             @endif
                           </div>
                         </div>
                       </div>

                     </div>
                   </div>
                 </div>


               </div>
             </div>
           </div>
                    </td>
                </td>
            </tr>
            @php $no++; @endphp
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')
<script type="text/javascript">
   function showdetail(id){
    $("#modaldetail"+id).modal('show');
  }

   function downloadURI(uri, name, id) {
      var link = document.createElement("a");
      link.download = name;
      link.href = uri;
      link.setAttribute("target", "_blank");
      link.click();
      link.remove();
    }
</script>
@stop
@endsection