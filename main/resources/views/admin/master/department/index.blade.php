@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Department</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Department</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row" id="form_input" style="display: none">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Form Tambah & Update Data</h4>
                <h6 class="card-title">Masukkan Informasi Detail dibawah ini</h6>

                <div class="alert alert-warning  mb-4" style="margin-top: 10px; margin-bottom: 10px; display: none" id="alert_edit">
                    <i class="icon fa fa-info"></i> 
                    <strong>Ubah Data dibawah ini</strong> Untuk Mengupdate Data !
                </div>

                 <form class="form p-t-20" method="POST" data-toggle="validator" id="form_data">
                    <input type="hidden" name="id_department" id="id_department">
                    <div class="mb-3">
                        <label for="formrow-firstname-input" class="form-label">Nama</label>
                        <input type="text" class="form-control" autocomplete="off" name="name" id="name" aria-label="" aria-describedby="basic-addon1" required>
                    </div>

                    <div class="mb-3">
                        <label for="formrow-firstname-input" class="form-label">Deskripsi</label>
                            <textarea class="form-control" id="desc" name="desc" rows="3" required=""></textarea>
                    </div>
                  <div>
                    <button type="submit" class="btn btn-success w-md"><i class="fa fa-save"></i> Simpan Data</button>
                    <div class="btn btn-danger w-md" onclick="showData()"><i class="fa fa-window-close"></i> Batalkan</div>
                </div>
            </form>
        </div>
        <!-- end card body -->
    </div>
    <!-- end card -->
</div>
<!-- end col -->
</div>

<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-10">
                          <h4 class="card-title">Department</h4>
                            <p class="card-title-desc">Master Data</p>
                    </div>
                    <div class="col-md-2">
                         <button type="button" class="btn btn-sm btn-info" onclick="showForm()"><i class="fa fa-plus-circle"></i> Tambah</button>
                        <button type="button" class="btn btn-sm btn-warning" onClick="window.location.href=window.location.href" title="Refresh"><i class=" fas fa-sync-alt"></i> Reload </button>

                    </div>
                </div>
                
                <table id="datatable" class="table table-bordered dt-responsive  nowrap w-100">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Deskripsi</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $no=1; @endphp
                        @foreach($department as $val)
                        <tr>
                            <td width="5%">{{ $no }}</td>
                            <td width="40%">{{ $val->name }}</td>
                            <td width="45%">{{ $val->desc }}</td>
                            <td width="10%">
                                <button class="btn btn-primary btn-sm" onclick='editData("{{ $val->id }}","{{ $val->name }}","{{ $val->desc}}")'><i class="fa fa-edit"></i> Edit Data</button>
                                <button class="btn btn-danger btn-sm" onclick='deleteData("{{ $val->id }}")'><i class="fa fa-trash"></i> Hapus Data</button>
                            </td>
                        </tr>
                        @php $no++; @endphp
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')

<script type="text/javascript">

  function showForm(){
    $("#form_input").fadeIn();
    $("#list_data").hide();
  }

  function showData(){
    $("#form_input").hide();
    $("#list_data").fadeIn();
  }

     $(function(){
        $("#form_data").submit(function(stay){
          stay.preventDefault(); 
           var formdata = $(this).serialize(); 
           // var fileData  = $('input[name=file]').val();
            $.ajax({
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url("department/action")}}',
                enctype: "multipart/form-data",
                dataType : 'JSON',
                data: formdata,
                success: function (data) {
                    if(data.status == 'OK'){
                         Swal.fire(data.title, data.msg, "success");
                    }else{
                         Swal.fire(data.title, data.msg, "error");
                    }
                  window.location.reload();
                },
            });
            stay.preventDefault(); 
        });
       });
    function deleteData(id){

         Swal.fire({
            title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
          }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{ url("department/delete")}}',
                    enctype: "multipart/form-data",
                    dataType : 'JSON',
                    data: 'id='+id,
                    success: function (data) {

                       Swal.fire(data.title, data.msg, "success");
                       window.location.reload();
                    },
                });
                stay.preventDefault(); 
            }
        });
    }

   function editData(id,name,desc) {

      showForm();

      $("#id_department").val(id);
      $("#name").val(name);
      $("#desc").val(desc);
      $("#alert_edit").fadeIn();
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

</script>
@stop
@endsection