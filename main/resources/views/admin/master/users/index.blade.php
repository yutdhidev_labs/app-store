@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Users</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Users</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row h-100">
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="avatar-sm flex-shrink-0">
                        <span class="avatar-title bg-light text-primary rounded-circle fs-3">
                            <i class="ri-account-circle-line align-middle"></i>
                        </span>
                    </div>
                    <div class="flex-grow-1 ms-3">
                        <p class="text-uppercase fw-semibold fs-12 text-muted mb-1"> Total All</p>
                        <h4 class=" mb-0"><span class="counter-value" data-target="{{ count($members) }}">0</span></h4>
                    </div>
                    <div class="flex-shrink-0 align-self-end">
                        <span class="badge badge-soft-success"><i class="ri-arrow-up-s-fill align-middle me-1"></i>6.24 %<span> </span></span>
                    </div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="avatar-sm flex-shrink-0">
                        <span class="avatar-title bg-light text-primary rounded-circle fs-3">
                            <i class=" ri-shield-user-fill align-middle"></i>
                        </span>
                    </div>
                    <div class="flex-grow-1 ms-3">
                        <p class="text-uppercase fw-semibold fs-12 text-muted mb-1"> Administrator</p>
                        <h4 class=" mb-0"><span class="counter-value" data-target="{{ $total_admin }}">0</span></h4>
                    </div>
                    <div class="flex-shrink-0 align-self-end">
                        <span class="badge badge-soft-success"><i class="ri-arrow-up-s-fill align-middle me-1"></i>6.24 %<span> </span></span>
                    </div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->

    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="avatar-sm flex-shrink-0">
                        <span class="avatar-title bg-light text-primary rounded-circle fs-3">
                            <i class="ri-customer-service-2-fill align-middle"></i>
                        </span>
                    </div>
                    <div class="flex-grow-1 ms-3">
                        <p class="text-uppercase fw-semibold fs-12 text-muted mb-1"> CS Agent</p>
                        <h4 class=" mb-0"><span class="counter-value" data-target="{{ $total_cs }}">0</span></h4>
                    </div>
                    <div class="flex-shrink-0 align-self-end">
                        <span class="badge badge-soft-success"><i class="ri-arrow-up-s-fill align-middle me-1"></i>3.67 %<span> </span></span>
                    </div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="avatar-sm flex-shrink-0">
                        <span class="avatar-title bg-light text-primary rounded-circle fs-3">
                            <i class="ri-contacts-line align-middle"></i>
                        </span>
                    </div>
                    <div class="flex-grow-1 ms-3">
                        <p class="text-uppercase fw-semibold fs-12 text-muted mb-1">Membership</p>
                        <h4 class=" mb-0"><span class="counter-value" data-target="{{ $total_members }}">0</span></h4>
                    </div>
                    <div class="flex-shrink-0 align-self-end">
                        <span class="badge badge-soft-danger"><i class="ri-arrow-down-s-fill align-middle me-1"></i>4.80 %<span> </span></span>
                    </div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
</div><!-- end row -->


<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-8">
                          <h4 class="card-title">Users</h4>
                    </div>
                    <div class="col-md-4" style="text-align: right;">
                        
                        <button type="button" class="btn btn-info btn-sm waves-effect waves-light" onClick="window.location.href=window.location.href" title="Refresh"><i class="ri-refresh-line align-bottom me-1"></i> Reload </button>

                    </div>
                </div>

                <br>
                
                <table id="alternative-pagination" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Fullname</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $no=1; @endphp
                        @foreach($members as $val)
                        <tr>
                            <td width="5%" align="center">{{ $no }}.</td>
                            <td width="">{{ $val->first_name }} {{ $val->last_name }}</td>
                            <td width="">{{ $val->phone }}</td>
                            <td width="">{{ $val->email }}</td>
                            <td width="">{{ $val->nama_roles }}</td>
                            <td width="" align="center">
                                @if($val->status_id == 1)
                                 <span class="badge badge-soft-success text-uppercase">Active</span>
                                 @elseif($val->status_id == 0)
                                 <span class="badge badge-soft-danger text-uppercase">Non-Active</span>
                                 @endif
                            </td>
                            <td width="22%" align="center">

                                <button type="button" class="btn btn-primary btn-sm" onclick='resetPassword("{{ $val->uuid }}")'>
                                     <i class="ri-key-fill"></i> Reset Password
                                </button>

                                @if($val->status_id == 1)

                                 <button type="button" class="btn btn-warning btn-sm" onclick='blockAccess("{{ $val->uuid }}","1")'>
                                       <i class="ri-lock-2-fill"></i> Suspend
                                </button>
                                @else

                                 <button type="button" class="btn btn-danger btn-sm" onclick='blockAccess("{{ $val->uuid }}","2")'>
                                       <i class="ri-lock-unlock-fill"></i> Un-Suspend
                                </button>
                                @endif
                            </td>
                        </tr>
                        @php $no++; @endphp
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')

<script type="text/javascript">

    function deleteData(id){

         Swal.fire({
            title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
          }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{ url("users/delete")}}',
                    enctype: "multipart/form-data",
                    dataType : 'JSON',
                    data: 'id='+id,
                    success: function (data) {

                       Swal.fire(data.title, data.msg, "success");
                       window.location.reload();
                    },
                });
                stay.preventDefault(); 
            }
        });
    }

</script>
@stop
@endsection