<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <style type="text/css">
    	body{
    		font-family: Arial, Helvetica, sans-serif;
    	}

    </style>

    <style>
            /** Define the margins of your page **/
            @page {
                margin: 100px 25px;
            }

            header {
                font-size:12px;
                position: fixed;
                top: -80px;
                left: 0px;
                right: 0px;
                height: 0px;
                text-align: left;
            }

            footer {
                border-top: 1px solid;
                font-size:12px;
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 10px; 
                text-align: left;
                line-height: 35px;
            }
        </style>
</head>
<body>

        <header>
           <span style="font-weight:bold; font-size: 14px">{{ env("COMPANY_NAME")}}</span> <br>
           {{ env("COMPANY_TAGLINE")}} <br>
           {{ env("COMPANY_ADDRESS")}} <br>
           {{ env("COMPANY_CITY")}} <br>
           {{ env("COMPANY_PHONE")}} 
        </header>

        <footer>
            Copyright &copy; <?php echo date("Y");?> YXLabs Creative
        </footer>

      <main>   
            <center>
            	<label style="font-weight: bold; font-size: 20px">{{ $header_label }}</label> 
            </center>

            <table id="datatable" border="1" cellpadding="2" style="font-size: 12px; width: 100%; margin-top: 20px; border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th style="background-color: #000; color:#fff">No</th>
                            <th style="background-color: #000; color:#fff">Nama</th>
                            <th style="background-color: #000; color:#fff">Email</th>
                            <th style="background-color: #000; color:#fff">Gender</th>
                            <th style="background-color: #000; color:#fff">Jabatan</th>
                        </tr>
                    </thead>

                    <tbody id="load_data">

                        @php $no=1; @endphp
                        @foreach($members as $val)
                        <tr>
                            <td width="5%" align="center">{{ $no }}</td>
                            <td width="">{{ $val->name }}</td>
                            <td width="">{{ $val->email }}</td>
                            <td width="">{{ $val->jenis_kelamin }}</td>
                            <td width="">{{ $val->position }}</td>
                        </tr>
                        @php $no++; @endphp
                        @endforeach
                        
                    </tbody>
                </table>
        </main>

</body>
</html>