@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">FAQs</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Master Data</a></li>
                    <li class="breadcrumb-item active">FAQs</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row" id="form_input" style="display: none">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">Form Tambah & Update Data</h4>
        </div><!-- end card header -->
        <div class="card-body">
            <div class="alert alert-warning alert-dismissible alert-label-icon rounded-label fade show" role="alert" style="margin-top: 10px; margin-bottom: 10px; display: none" id="alert_edit">
                <i class="ri-error-warning-line label-icon"></i>
                <strong>Ubah Data dibawah ini</strong> Untuk Mengupdate Data !
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>


             <form method="POST" data-toggle="validator" id="form_data">
               <input type="hidden" name="id_faqs" id="id_faqs">
              <div class="mb-3">
                <label for="formrow-firstname-input" class="form-label">Name <code>*</code></label>
                <input type="text" class="form-control" autocomplete="off" name="name" id="name" aria-label="" aria-describedby="basic-addon1" required>
            </div>

            <div class="mb-3">
                <label for="formrow-firstname-input" class="form-label">Description <code>*</code></label>
                <textarea class="form-control" cols="10" rows="5" name="desc" id="desc" required></textarea>
            </div>
            <div>
                <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="ri-save-2-line align-bottom me-1"></i> Simpan Data</button>
                <div class="btn btn-danger waves-effect waves-light" onclick="showData()"><i class="ri-close-circle-line align-bottom me-1"></i> Batalkan</div>
            </div>
        </form>
    </div>
</div>
</div>
</div>

<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-10">
                      <h4 class="card-title">Frequently Asked Questions</h4>
                      <p class="card-title-desc">Master Data</p>
                  </div>
                  <div class="col-md-2">
                   <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" onclick="showForm()"><i class="ri-add-circle-line align-bottom me-1"></i> Tambah</button>
                   <button type="button" class="btn btn-info btn-sm waves-effect waves-light" onClick="window.location.href=window.location.href" title="Refresh"><i class="ri-refresh-line align-bottom me-1"></i> Reload </button>

               </div>
           </div>

          <table id="alternative-pagination" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
             <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php $no=1; @endphp
                @foreach($faqs as $val)
                <tr>
                   <td width="25%">
                    {{ $val->name }}
                </td>
                <td width="25%">{{ $val->desc }}</td>
                <td width="10%" align="center">

                    <button type="button" class="btn btn-primary btn-icon waves-effect waves-light" onclick='editData("{{ $val->id }}","{{ $val->name }}","{{ $val->desc }}")' title="Edit Data"><i class="ri-quill-pen-line"></i></button>

                    <button type="button" class="btn btn-danger btn-icon waves-effect waves-light" onclick='deleteData("{{ $val->id }}")' title="Hapus Data"><i class="ri-delete-bin-5-line"></i></button>
                </td>
            </tr>
            @php $no++; @endphp
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')

<script type="text/javascript">

  function showForm(){
    $("#form_input").fadeIn();
    $("#list_data").hide();
}

function showData(){
    $("#form_input").hide();
    $("#list_data").fadeIn();
}

 $(function(){
        $("#form_data").submit(function(stay){
          stay.preventDefault(); 
           var formdata = $(this).serialize(); 
           // var fileData  = $('input[name=file]').val();
            $.ajax({
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url("faqs/action")}}',
                enctype: "multipart/form-data",
                dataType : 'JSON',
                data: formdata,
                success: function (data) {
                    if(data.status == 'OK'){
                         Swal.fire(data.title, data.msg, "success");
                    }else{
                         Swal.fire(data.title, data.msg, "error");
                    }
                  setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                },
            });
            stay.preventDefault(); 
        });
       });

function deleteData(id){
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-primary w-xs me-2 mt-2",
        cancelButtonClass: "btn btn-danger w-xs mt-2",
        confirmButtonText: "Yes, delete it!",
        buttonsStyling: !1,
        showCloseButton: !0
   }).then(function (result) {
    if (result.value) {
         $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ url("faqs/delete")}}',
            enctype: "multipart/form-data",
            dataType : 'JSON',
            data: 'id='+id,
            success: function (data) {

             Swal.fire(data.title, data.msg, "success");
             setTimeout(() => {
                  window.location.reload();
              }, 2000);
         },
     });

    }
    })
}

function editData(id,name, desc) {

  showForm();
  $("#id_faqs").val(id);
  $("#name").val(name);
  $("#desc").html(desc);
  $("#alert_edit").fadeIn();
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

</script>
@stop
@endsection