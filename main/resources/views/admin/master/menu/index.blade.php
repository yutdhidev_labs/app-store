@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Menu</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Menu</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>


<div class="row" id="list_data">
    <div class="col-8">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-10">
                          <h4 class="card-title">Menu</h4>
                            <p class="card-title-desc">Master Data</p>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-sm btn-warning" onClick="window.location.href=window.location.href" title="Refresh"><i class=" fas fa-sync-alt"></i> Reload </button>

                    </div>
                </div>
                
                <table id="" class="table table-bordered dt-responsive  nowrap w-100">
                    <tr>
                        <th>Nama</th>
                        <th>Status</th>
                        <th>Function</th>
                        <th>Action</th>
                    </tr>
                   
                    <tbody>
                        <?php
                                $titleMenu = DB::table('menu')
                                    ->where('status_active',1)
                                    ->where('current',1)
                                    ->get();
                        ?>

                            @foreach($titleMenu as $val)
                               <tr>
                                    <td>
                                        <b>{{ $val->display_name }}</b>
                                    </td>
                                    <td width="15%">Main Menu</td>
                                    <td>{{ $val->url_func }}</td>
                                    <td width="14%" align="center">
                                        <button class="btn btn-sm btn-success">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" onClick="deleteData('{{ $val->id }}')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>

                               <?php
                                $parent = DB::table('menu')
                                            ->where('status_active',1)
                                            ->where('parent_id',$val->id)
                                            ->get();
                                ?>

                                 @foreach($parent as $val2)

                               <?php
                                  $cek_child = DB::table('menu')
                                              ->where('status_active',1)
                                              ->where('parent_id',$val2->id)
                                              ->count();
                                  ?>

                                  @if($cek_child > 0)
                                    <tr>
                                        <td style="padding-left: 30px">
                                            <span><i class="fa fa-angle-down"></i> {{ $val2->display_name }}</span>
                                        </td>
                                        <td>Parent</td>
                                        <td>{{ $val2->url_func }}</td>
                                        <td width="14%" align="center">
                                            <button class="btn btn-sm btn-success" disabled>
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button class="btn btn-sm btn-danger" disabled>
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>

                                          <?php
                                              $child = DB::table('menu')
                                                          ->where('status_active',1)
                                                          ->where('parent_id',$val2->id)
                                                          ->get();
                                              ?>
                                               @foreach($child as $val3)
                                                <tr>
                                                    <td style="padding-left: 60px">
                                                        <i class="fa fa-angle-right"></i> {{ $val3->display_name }}
                                                    </td>
                                                    <td>Child</td>
                                                    <td>{{ $val3->url_func }}</td>
                                                    <td width="14%" align="center">
                                                        <button class="btn btn-sm btn-success">
                                                            <i class="fa fa-edit"></i>
                                                        </button>
                                                        <button class="btn btn-sm btn-danger" onClick="deleteData('{{ $val3->id }}')">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                       
                                  @else
                                    <tr>
                                       <td style="padding-left: 30px">
                                        <span><i class="fa fa-angle-right"></i> {{ $val2->display_name }}</span>
                                   </td>
                                   <td>Child</td>
                                    <td>{{ $val2->url_func }}</td>
                                    <td width="14%" align="center">
                                        <button class="btn btn-sm btn-success">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button class="btn btn-sm btn-danger" onClick="deleteData('{{ $val2->id }}')">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                               </tr>
                                  @endif
                                 
                                @endforeach
                            @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->

    <div class="col-4">
        <div class="card">
            <div class="card-body">

               <div class="alert alert-warning  mb-4" style="margin-top: 10px; margin-bottom: 10px; display: none" id="alert_edit">
                    <i class="icon fa fa-info"></i> 
                    <strong>Ubah Data dibawah ini</strong> Untuk Mengupdate Data !
                </div>

                 <form class="form p-t-20" method="POST" data-toggle="validator" id="form_data">
                    <input type="hidden" name="id_menu" id="id_menu">

                    <h4>Form Create & Update Menu</h4>

                    <div class="mb-3">
                        <label for="formrow-firstname-input" class="form-label">Type Menu</label>
                        <br>
                        <select class="form-control select2" style="width: 100%" required name="type_menu" id="type_menu" onchange="getType()">
                            <option value="">Pilih Type Menu</option>
                            <option value="main">Main Menu</option>
                            <option value="parent">Parent</option>
                            <option value="child">Child</option>
                        </select>
                    </div>


                    <div class="mb-3" id="form_display_name" style="display: none">
                        <label for="formrow-firstname-input" class="form-label">Nama Menu</label>
                        <br>
                        <input type="text" class="form-control" name="display_name" id="display_name" autocomplete="off">
                    </div>

                    <div class="mb-3" id="form_fa_icon" style="display: none">
                        <label for="formrow-firstname-input" class="form-label">Font Icon</label>
                        <br>
                        <input type="text" class="form-control" name="fa_icon" id="fa_icon" autocomplete="off">
                    </div>

                    <div class="mb-3" id="form_url_func" style="display: none">
                        <label for="formrow-firstname-input" class="form-label">URL Func</label>
                        <br>
                        <input type="text" class="form-control" name="url_func" id="url_func" autocomplete="off">
                    </div>


                    <div class="mb-3" id="form_main_menu" style="display: none">
                        <label for="formrow-firstname-input" class="form-label">Main Menu</label>
                        <br>
                        <select class="form-control select2" style="width: 100%" name="main_menu" id="">
                            @foreach($main_menu as $val)
                             <option value="{{ $val->id }}">{{ $val->display_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-3" id="form_parent" style="display: none">
                        <label for="formrow-firstname-input" class="form-label">Parent</label>
                        <br> 
                        <select class="form-control select2" style="width: 100%" name="parent" id="parent">
                            <option value="0">No Parent</option>
                            @foreach($parent_menu as $val_parent)
                             <option value="{{ $val_parent->id }}">{{ $val_parent->display_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-3" id="form_child" style="display: none">
                        <label for="formrow-firstname-input" class="form-label">Child</label>
                        <br>
                        <select class="form-control select2" style="width: 100%" name="child" id="child">
                            @foreach($child as $val)
                             <option value="{{ $val->id }}">{{ $val->display_name }}</option>
                            @endforeach
                        </select>
                    </div>
                  <div>
                    <button type="submit" class="btn btn-success w-md"><i class="fa fa-save"></i> Simpan Data</button>
                    <div class="btn btn-danger w-md" onclick="showData()"><i class="fa fa-window-close"></i> Batalkan</div>
                </div>
            </form>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')

<script type="text/javascript">

    function getType(){
        var type_menu = $("#type_menu").val();

        $("#form_main_menu").hide();
        $("#form_parent").hide();
        $("#form_child").hide();
        $("#form_display_name").hide();
        $("#form_fa_icon").hide();
        $("#form_url_func").hide();

        if(type_menu == 'main'){
              $("#form_display_name").show();
        }else if(type_menu == 'parent'){
            $("#form_fa_icon").show();
            $("#form_display_name").show();
            $("#form_main_menu").show();
        }else if(type_menu == 'child'){
            $("#form_url_func").show();
            $("#form_fa_icon").show();
            $("#form_display_name").show();
            $("#form_main_menu").show();
            $("#form_parent").show();
        }
    }


     $(function(){
        $("#form_data").submit(function(stay){
          stay.preventDefault(); 
           var formdata = $(this).serialize(); 
           // var fileData  = $('input[name=file]').val();
            $.ajax({
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url("menu/action")}}',
                enctype: "multipart/form-data",
                dataType : 'JSON',
                data: formdata,
                success: function (data) {
                    if(data.status == 'OK'){
                         Swal.fire(data.title, data.msg, "success");
                    }else{
                         Swal.fire(data.title, data.msg, "error");
                    }
                  window.location.reload();
                },
            });
            stay.preventDefault(); 
        });
       });
    function deleteData(id){

         Swal.fire({
            title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
          }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{ url("menu/delete")}}',
                    enctype: "multipart/form-data",
                    dataType : 'JSON',
                    data: 'id='+id,
                    success: function (data) {

                       Swal.fire(data.title, data.msg, "success");
                       window.location.reload();
                    },
                });
                stay.preventDefault(); 
            }
        });
    }

   function editData(id,name,desc) {

      showForm();

      $("#id_menu").val(id);
      $("#name").val(name);
      $("#desc").val(desc);
      $("#alert_edit").fadeIn();
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

</script>
@stop
@endsection