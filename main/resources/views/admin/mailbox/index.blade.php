@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Mailbox</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('mailbox') }}">Mailbox</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>


                    <div class="email-wrapper d-lg-flex gap-1 mx-n4 mt-n4 p-1">
                        <div class="email-menu-sidebar">
                            <div class="p-4 d-flex flex-column h-100">
                                <div class="pb-4 border-bottom border-bottom-dashed">
                                    <button type="button" class="btn btn-danger w-100" data-bs-toggle="modal" data-bs-target="#composemodal"><i data-feather="plus-circle" class="icon-xs me-1 icon-dual-light"></i> Compose</button>
                                </div>

                                <div class="mx-n4 px-4 email-menu-sidebar-scroll" data-simplebar>
                                    <div class="mail-list mt-3">
                                        <a href="#" class="active"><i class="ri-mail-fill me-3 align-middle fw-medium"></i> <span class="mail-list-link">All</span> <span class="badge badge-soft-success ms-auto  ">5</span></a>
                                        <a href="#">
                                            <i class="ri-inbox-archive-fill me-3 align-middle fw-medium"></i>
                                            <span class="mail-list-link">Inbox</span>
                                            <span class="badge badge-soft-success ms-auto  ">5</span>
                                        </a>
                                        <a href="#">
                                            <i class="ri-send-plane-2-fill me-3 align-middle fw-medium"></i>
                                            <span class="mail-list-link">Sent</span>
                                        </a>
                                        <a href="#">
                                            <i class="ri-delete-bin-5-fill me-3 align-middle fw-medium"></i>
                                            <span class="mail-list-link">Trash</span>
                                        </a>
                                        <a href="#">
                                            <i class="ri-star-fill me-3 align-middle fw-medium"></i>
                                            <span class="mail-list-link">Starred</span>
                                        </a>

                                    </div>
                                </div>

                                <div class="mt-auto">
                                    <h5 class="fs-13">1.75 GB of 10 GB used</h5>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end email-menu-sidebar -->

                        <div class="email-content">
                            <div class="p-4 pb-0">
                                <div class="border-bottom border-bottom-dashed">
                                    <div class="row mt-n2 mb-3 mb-sm-0">
                                        <div class="col col-sm-auto order-1 d-block d-lg-none">
                                            <button type="button" class="btn btn-soft-success btn-icon btn-sm fs-16 email-menu-btn">
                                                <i class="ri-menu-2-fill align-bottom"></i>
                                            </button>
                                        </div>
                                        <div class="col-sm order-3 order-sm-2">
                                            <div class="hstack gap-sm-1 align-items-center flex-wrap email-topbar-link">
                                                <div class="form-check fs-14 m-0">
                                                    <input class="form-check-input" type="checkbox" value="" id="checkall">
                                                    <label class="form-check-label" for="checkall"></label>
                                                </div>
                                                <div id="email-topbar-actions">
                                                    <div class="hstack gap-sm-1 align-items-center flex-wrap">
                                                        <button type="button" class="btn btn-ghost-secondary btn-icon btn-sm fs-16" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="Starred">
                                                            <i class="ri-star-fill align-bottom"></i>
                                                        </button>
                                                        <div data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="Trash">
                                                            <button type="button" class="btn btn-ghost-secondary btn-icon btn-sm fs-16" data-bs-toggle="modal" data-bs-target="#removeItemModal">
                                                                <i class="ri-delete-bin-5-fill align-bottom"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="dropdown">
                                                    <button class="btn btn-ghost-secondary btn-icon btn-sm fs-16" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="ri-more-2-fill align-bottom"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="#" id="mark-all-read">Mark all as Read</a>
                                                    </div>
                                                </div>
                                                <div class="alert alert-warning alert-dismissible unreadConversations-alert px-4 fade show " id="unreadConversations" role="alert">
                                                    No Unread Conversations
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-auto order-2 order-sm-3">
                                            <div class="d-flex gap-sm-1 email-topbar-link">
                                                <button type="button" class="btn btn-ghost-secondary btn-icon btn-sm fs-16">
                                                    <i class="ri-refresh-line align-bottom"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row align-items-end mt-3">
                                        <div class="col">
                                            <div id="mail-filter-navlist">
                                                <ul class="nav nav-tabs nav-tabs-custom nav-success gap-1 text-center border-bottom-0" role="tablist">
                                                    <li class="nav-item">
                                                        <button class="nav-link fw-semibold active" id="pills-primary-tab" data-bs-toggle="pill" data-bs-target="#pills-primary" type="button" role="tab" aria-controls="pills-primary" aria-selected="true">
                                                            <i class="ri-inbox-fill align-bottom d-inline-block"></i>
                                                            <span class="ms-1 d-none d-sm-inline-block">Primary</span>
                                                        </button>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                        <div class="col-auto">
                                            <div class="text-muted mb-2">1-50 of 154</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="pills-primary" role="tabpanel" aria-labelledby="pills-primary-tab">
                                        <div class="message-list-content mx-n4 px-4 message-list-scroll" data-simplebar>
                                            <div id="elmLoader">
                                                <div class="spinner-border text-primary avatar-sm" role="status">
                                                    <span class="visually-hidden">Loading...</span>
                                                </div>
                                            </div>
                                            <ul class="message-list" id="mail-list"></ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-social" role="tabpanel" aria-labelledby="pills-social-tab">
                                        <div class="message-list-content mx-n4 px-4 message-list-scroll" data-simplebar>
                                            <ul class="message-list" id="social-mail-list"></ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-promotions" role="tabpanel" aria-labelledby="pills-promotions-tab">
                                        <div class="message-list-content mx-n4 px-4 message-list-scroll" data-simplebar>
                                            <ul class="message-list" id="promotions-mail-list"></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end email-content -->

                        <div class="email-detail-content">
                            <div class="p-4 d-flex flex-column h-100">
                                <div class="pb-4 border-bottom border-bottom-dashed">
                                    <div class="row">
                                        <div class="col">
                                            <div class="">
                                                    <button type="button" class="btn btn-soft-danger btn-icon btn-sm fs-16 close-btn-email" id="close-btn-email">
                                                    <i class="ri-close-fill align-bottom"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <div class="hstack gap-sm-1 align-items-center flex-wrap email-topbar-link">
                                                <button type="button" class="btn btn-ghost-secondary btn-icon btn-sm fs-16 favourite-btn active">
                                                    <i class="ri-star-fill align-bottom"></i>
                                                </button>
                                                <button class="btn btn-ghost-secondary btn-icon btn-sm fs-16">
                                                    <i class="ri-printer-fill align-bottom"></i>
                                                </button>
                                                    <button class="btn btn-ghost-secondary btn-icon btn-sm fs-16 remove-mail" data-remove-id=""  data-bs-toggle="modal" data-bs-target="#removeItemModal">
                                                    <i class="ri-delete-bin-5-fill align-bottom"></i>
                                                </button>
                                                <div class="dropdown">
                                                    <button class="btn btn-ghost-secondary btn-icon btn-sm fs-16" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="ri-more-2-fill align-bottom"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="#">Mark as Unread</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mx-n4 px-4 email-detail-content-scroll" data-simplebar>
                                    <div class="mt-4 mb-3">
                                        <h5 class="fw-bold email-subject-title">New updates for Skote Theme</h5>
                                    </div>

                                    <div class="accordion accordion-flush">
                                        <div class="accordion-item border-dashed left">
                                            <div class="accordion-header">
                                                <a role="button" class="btn w-100 text-start px-0 bg-transparent shadow-none collapsed" data-bs-toggle="collapse" href="#email-collapseOne" aria-expanded="true" aria-controls="email-collapseOne">
                                                    <div class="d-flex align-items-center text-muted">
                                                        <div class="flex-shrink-0 avatar-xs me-3">
                                                            <img src="assets/images/users/avatar-3.jpg" alt="" class="img-fluid rounded-circle">
                                                        </div>
                                                        <div class="flex-grow-1 overflow-hidden">
                                                            <h5 class="fs-14 text-truncate email-user-name mb-0">Yudhi</h5>
                                                            <div class="text-truncate fs-12">to: me</div>
                                                        </div>
                                                        <div class="flex-shrink-0 align-self-start">
                                                            <div class="text-muted fs-12">09 Jan 2022, 11:12 AM</div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div id="email-collapseOne" class="accordion-collapse collapse">
                                                <div class="accordion-body text-body px-0">
                                                    <div>
                                                        <p>Hi,</p>
                                                        <p>Praesentddd dui ex, dapibus eget mauris ut, finibus vestibulum enim. Quisque arcu leo, facilisis in fringilla id, luctus in tortor.</p>
                                                        <p>Sed elementum turpis eu lorem interdum, sed porttitor eros commodo. Nam eu venenatis tortor, id lacinia diam. Sed aliquam in dui et porta. Sed bibendum orci non tincidunt ultrices.</p>
                                                        <p>Sincerly,</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end accordion-item -->

                                        <div class="accordion-item border-dashed right">
                                            <div class="accordion-header">
                                                <a role="button" class="btn w-100 text-start px-0 bg-transparent shadow-none collapsed" data-bs-toggle="collapse" href="#email-collapseTwo" aria-expanded="true" aria-controls="email-collapseTwo">
                                                    <div class="d-flex align-items-center text-muted">
                                                        <div class="flex-shrink-0 avatar-xs me-3">
                                                            <img src="assets/images/users/avatar-1.jpg" alt="" class="img-fluid rounded-circle">
                                                        </div>
                                                        <div class="flex-grow-1 overflow-hidden">
                                                            <h5 class="fs-14 text-truncate email-user-name-right mb-0">Zahran</h5>
                                                            <div class="text-truncate fs-12">to: jackdavis@email.com</div>
                                                        </div>
                                                        <div class="flex-shrink-0 align-self-start">
                                                            <div class="text-muted fs-12">09 Jan 2022, 02:15 PM</div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                            <div id="email-collapseTwo" class="accordion-collapse collapse">
                                                <div class="accordion-body text-body px-0">
                                                    <div>
                                                        <p>Hi,</p>
                                                        <p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual.</p>
                                                        <p>Thank you</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end accordion-item -->

                                      
                                    </div>
                                    <!-- end accordion -->
                                </div>
                                <div class="mt-auto">
                                    <form class="mt-2">
                                        <div>
                                            <label for="exampleFormControlTextarea1" class="form-label">Reply :</label>
                                            <textarea class="form-control border-bottom-0 rounded-top rounded-0 border" id="exampleFormControlTextarea1" rows="3" placeholder="Enter message"></textarea>
                                            <div class="bg-light px-2 py-1 rouned-bottom border">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="btn-group" role="group">
                                                            <button type="button" class="btn btn-sm py-0 fs-15 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Bold"><i class="ri-bold align-bottom"></i></button>
                                                            <button type="button" class="btn btn-sm py-0 fs-15 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Italic"><i class="ri-italic align-bottom"></i></button>
                                                            <button type="button" class="btn btn-sm py-0 fs-15 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Link"><i class="ri-link align-bottom"></i></button>
                                                            <button type="button" class="btn btn-sm py-0 fs-15 btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Image"><i class="ri-image-2-line align-bottom"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-sm btn-success"><i class="ri-send-plane-2-fill align-bottom"></i></button>
                                                            <button type="button" class="btn btn-sm btn-success dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                                                <span class="visually-hidden">Toggle Dropdown</span>
                                                            </button>
                                                            <ul class="dropdown-menu dropdown-menu-end">
                                                                <li><a class="dropdown-item" href="#"><i class="ri-timer-line text-muted me-1 align-bottom"></i> Schedule Send</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- end email-detail-content -->
                    </div>
                    <!-- end email wrapper -->


@section('page-script')

</script>
@stop
@endsection
