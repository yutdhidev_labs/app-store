@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Contact Us</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Contact Us</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

    <form class="form p-t-20" method="POST" data-toggle="validator" id="form_data">
<div class="row" id="form_input">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Company Detail</h4>
                        <hr>

                       

                        <div class="row">
                            <div class="col-md-8">
                                  <div class="mb-3">
                                    <label for="formrow-firstname-input" class="form-label">Company Name</label>
                                    <input type="text" class="form-control" autocomplete="off" name="name" id="name" value="{{ $contacts->name }}">
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="mb-3">
                                        <label for="formrow-firstname-input" class="form-label">Phone</label>
                                        <input type="text" class="form-control" autocomplete="off" name="phone" id="phone" value="{{ $contacts->phone }}">
                                    </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6">
                                  <div class="mb-3">
                                        <label for="formrow-firstname-input" class="form-label">Building</label>
                                        <input type="text" class="form-control" autocomplete="off" name="building" id="building" value="{{ $contacts->building }}">
                                    </div>
                            </div>
                             <div class="col-md-6">
                                  <div class="mb-3">
                                        <label for="formrow-firstname-input" class="form-label">Address</label>
                                        <input type="text" class="form-control" autocomplete="off" name="address" id="address" value="{{ $contacts->address }}">
                                    </div>
                            </div>
                        </div>

                            <div class="row">
                            <div class="col-md-4">
                                  <div class="mb-3">
                                        <label for="formrow-firstname-input" class="form-label">City</label>
                                        <input type="text" class="form-control" autocomplete="off" name="city" id="city" value="{{ $contacts->city }}">
                                    </div>
                            </div>
                             <div class="col-md-4">
                                 <div class="mb-3">
                                    <label for="formrow-firstname-input" class="form-label">Country</label>
                                    <input type="text" class="form-control" autocomplete="off" name="country" id="country" value="{{ $contacts->country }}">
                                </div>
                            </div>
                             <div class="col-md-4">
                                 <div class="mb-3">
                                    <label for="formrow-firstname-input" class="form-label">Postal Code</label>
                                    <input type="text" class="form-control" autocomplete="off" name="postal_code" id="postal_code" value="{{ $contacts->postal_code }}">
                                </div>
                            </div>
                        </div>
                            
                         
                </div>
            </div>
        </div>

  <div class="col-xl-6">
        <div class="card" style="height: 330px">
            <div class="card-body">
                <h4 class="card-title">Social Media</h4>
                <hr>

                    <div class="input-group mb-3">
                        <span class="input-group-text" ><i class="mdi mdi-instagram"></i></span>
                        <input type="text" class="form-control" name="instagram" value="{{ $contacts->instagram }}" >
                    </div>

                    <div class="input-group mb-3">
                        <span class="input-group-text" ><i class="mdi mdi-facebook"></i></span>
                        <input type="text" class="form-control" name="facebook"  value="{{ $contacts->facebook }}" >
                    </div>

                    <div class="input-group mb-3">
                        <span class="input-group-text" ><i class="mdi mdi-youtube"></i></span>
                        <input type="text" class="form-control" name="youtube"  value="{{ $contacts->youtube }}" >
                    </div>
        </div>
    </div>
</div>
</div>

<div class="row">
<div class="col-md-12">
                    <center>
                        <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="ri-save-2-line"></i> Simpan Data</button>
                                </div>
                    </center>
            
    </div>
</div>
</form>

@section('page-script')

<script type="text/javascript">


     $(function(){
        $("#form_data").submit(function(stay){
          stay.preventDefault(); 
           var formdata = $(this).serialize(); 
           // var fileData  = $('input[name=file]').val();
            $.ajax({
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url("contacts/action")}}',
                enctype: "multipart/form-data",
                dataType : 'JSON',
                data: formdata,
                success: function (data) {
                    if(data.status == 'OK'){
                         Swal.fire(data.title, data.msg, "success");
                    }else{
                         Swal.fire(data.title, data.msg, "error");
                    }
                   setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                },
            });
            stay.preventDefault(); 
        });
       });
    

</script>
@stop
@endsection