@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Abouts Us</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Abouts Us</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
     <table id="datatable" class="table table-bordered dt-responsive   w-100">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Keterangan</th>
                            <th>Tanggal</th>
                            <th>User</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $no=1; @endphp
                        @foreach($logs as $val)
                        <tr>
                            <td width="5%">{{ $no }}</td>
                            <td width="60%">
                                {!! $val->desc !!} <br>

                                <span style="font-size: 12px">
                                    <i class="bx bx-time-five"></i> 
                                    {{ Carbon\Carbon::parse($val->created_at)->diffForHumans()}}</span>
                            </td>
                            <td width="20%">{{ date('Y-m-d H:i:s', strtotime($val->created_at)); }}</td>
                            <td width="10%">{{ $val->name }}</td>
                        </tr>
                        @php $no++; @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            </div>
            </div>

@section('page-script')

@stop
@endsection