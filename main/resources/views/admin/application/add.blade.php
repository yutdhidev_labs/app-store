@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Application</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Application</a></li>
                    <li class="breadcrumb-item active">Tambah Data</li>
                </ol>
            </div>

        </div>
    </div>
</div>
 <form action="{{ url('application/action') }}" method="post" enctype="multipart/form-data">
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <div class="mb-3">
                    <label class="form-label" for="project-title-input">Title <code>*</code></label>
                    <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Enter application title" required>
                </div>

                <div class="mb-3">
                    <label class="form-label" for="project-thumbnail-img">Thumbnail </label>
                    <input class="form-control" id="file2" name="file2"  type="file" accept="image/png, image/gif, image/jpeg" >
                </div>

                <div class="mb-3">
                    <label class="form-label">Description</label>
                    <textarea id="ckeditor-classic" name="desc" ></textarea>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="mb-3 mb-lg-0">
                            <label for="choices-priority-input" class="form-label">Level <code>*</code></label>
                            <select class="form-select" data-choices data-choices-search-false id="level" name="level" required>
                                <option value="High" selected>High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="mb-3 mb-lg-0">
                            <label for="choices-status-input" class="form-label" >App Type <code>*</code></label>
                            <select class="form-select" data-choices data-choices-search-false id="app_type" name="app_type" required>
                                <option value="Free" selected>Free</option>
                                <option value="Premium">Premium</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div>
                            <label for="datepicker-deadline-input" class="form-label">Price</label>
                            <input type="text" autocomplete="off" class="form-control" id="price" name="price" placeholder="Price" data-provider="flatpickr" value="0">
                        </div>
                    </div>
                </div>

                  <br>

                 <div class="mb-3">
                    <label class="form-label" for="project-thumbnail-img">File Aplikasi (.zip) </label>
                    <input class="form-control" id="file" name="file"  type="file" >
                </div>
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->

        <div class="text-end mb-4">
            
        <a href="{{ url('application') }}">
            <button type="button" class="btn btn-success"><i class="ri-arrow-left-line align-bottom me-1"></i> Back</button>
        </a>
           <a href="{{ url('application/app') }}">
            <button type="submit" class="btn btn-primary"><i class="ri-save-line align-bottom me-1"></i> Create Application</button>
        </a>
        <button type="reset" class="btn btn-danger"><i class="ri-close-line align-bottom me-1"></i> Cancel</button>
    </div>
</div>
<!-- end col -->
<div class="col-lg-4">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Status</h5>
        </div>
        <div class="card-body">
            <div>
                <label for="choices-privacy-status-input" class="form-label">Status <code>*</code></label>
                <select class="form-select" data-choices data-choices-search-false id="status_publish" name="status_publish" required>
                    <option value="0" selected>Draft</option>
                    <option value="1">Publish</option>
                </select>
            </div>
        </div>
        <!-- end card body -->
    </div>
    <!-- end card -->

    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Tags</h5>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <label for="choices-categories-input" class="form-label">Categories <code>*</code></label>
                <select class="form-select" data-choices data-choices-search-false id="category_id" name="category_id" required>
                    @foreach($application_category as $val)
                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                    @endforeach
                </select>
            </div>

            <div>
                <label for="choices-text-input" class="form-label">Label</label>
                <input class="form-control" id="choices-text-input" data-choices data-choices-limit="Required Limit" placeholder="" type="text" autocomplete="off" value="PHP, MYSQL, HTML, LARAVEL"  id="label" name="label"/>
            </div>
        </div>
        <!-- end card body -->
    </div>
    <!-- end card -->


    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Link</h5>
        </div>
        <div class="card-body">
           <div>
            <label for="choices-text-input" class="form-label">Demo Video</label>
            <input class="form-control" id="choices-text-input" data-choices data-choices-limit="Required Limit" placeholder="" type="text" autocomplete="off" value="#"  id="link_video" name="link_video"/>
        </div>

        <div>
            <label for="choices-text-input" class="form-label">Demo Web</label>
            <input class="form-control" id="choices-text-input" data-choices data-choices-limit="Required Limit" placeholder="" type="text" autocomplete="off" value="#"  id="link_web" name="link_web"/>
        </div>
    </div>
    <!-- end card body -->
</div>
<!-- end card -->


</div>
<!-- end col -->
</div>
</form>


@section('page-script')

@stop
@endsection