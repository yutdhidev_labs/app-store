@extends('layouts.master_backend')
@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="card mt-n4 mx-n4">
      <div class="bg-soft-warning">
        <div class="card-body pb-0 px-4">
          <div class="row mb-3">
            <div class="col-md">
              <div class="row align-items-center g-3">
                <div class="col-md-auto">
                  <div class="avatar-md">
                    <div class="avatar-title bg-white rounded-circle">
                      <img src="{{ asset('dist/app') }}/{{ $application->appcode }}/{{ $application->uuid }}//{{ $application->thumbnail }}" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'" alt="" class="avatar-xs">
                    </div>
                  </div>
                </div>
                <div class="col-md">
                  <div>
                    <h4 class="fw-bold">{{ $application->name }}</h4>
                    <div class="hstack gap-3 flex-wrap">
                      <div><i class="ri-building-line align-bottom me-1"></i> {{ $application->category_name }}</div>
                      <div class="vr"></div>
                      <div>Create Date : <span class="fw-medium">{{ date('d M Y', strtotime($application->created_at)) }}</span></div>
                      <div class="vr"></div>

                      <div>Last Update : <span class="fw-medium">{{ date('d M Y', strtotime($application->updated_at)) }}</span></div>
                      <div class="badge rounded-pill bg-info fs-12">{{ $application->app_type }}</div>
                      <div class="badge rounded-pill bg-danger fs-12">{{ $application->level }}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-auto">
              <div class="hstack gap-1 flex-wrap">
                <button type="button" class="btn py-0 fs-16 favourite-btn active">
                  <i class="ri-star-fill"></i>
                </button>
                <button type="button" class="btn py-0 fs-16 text-body">
                  <i class="ri-share-line"></i>
                </button>
                <button type="button" class="btn py-0 fs-16 text-body">
                  <i class="ri-flag-line"></i>
                </button>
              </div>
            </div>
          </div>

          <ul class="nav nav-tabs-custom border-bottom-0" role="tablist">
            <li class="nav-item">
              <a class="nav-link active fw-semibold" data-bs-toggle="tab" href="#overview" role="tab">
                Overview
              </a>
            </li>
           <!--  <li class="nav-item" >
              <a class="nav-link fw-semibold" data-bs-toggle="tab" href="#documents" role="tab">
                Documents
              </a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link fw-semibold" data-bs-toggle="tab" href="#activities" role="tab">
                Activities
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link fw-semibold" data-bs-toggle="tab" href="#payment" role="tab">
                Payment
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link fw-semibold" data-bs-toggle="tab" href="#download" role="tab">
                Download
              </a>
            </li>
          </ul>
        </div>
        <!-- end card body -->
      </div>
    </div>
    <!-- end card -->
  </div>
  <!-- end col -->
</div>
<!-- end row -->
<div class="row">
  <div class="col-lg-12">
    <div class="tab-content text-muted">
      <div class="tab-pane fade show active" id="overview" role="tabpanel">
        <div class="row">
          <div class="col-xl-9 col-lg-8">
            <div class="card">
              <div class="card-body">
                <div class="text-muted">
                  <h6 class="mb-3 fw-semibold text-uppercase">Summary</h6>
                  <div id="shortdesc">
                    {!! substr($application->desc,0,1000) !!} ...
                  </div>
                  <div id="fulldesc" style="display: none">{!! $application->desc !!}</div>
                  <div>
                    <button type="button" id="readmore" onclick="Readmore()" class="btn btn-link link-success p-0">Read more</button>
                  </div>

                  <div class="pt-3 border-top border-top-dashed mt-4">
                    <h6 class="mb-3 fw-semibold text-uppercase">Resources</h6>
                    <div class="row g-3">

                      <div class="col-xxl-12 col-lg-12">
                        <div class="border rounded border-dashed p-2">
                          <div class="d-flex align-items-center">
                            <div class="flex-shrink-0 me-3">
                              <div class="avatar-sm">
                                <div class="avatar-title bg-light text-secondary rounded fs-24">
                                  <i class="ri-folder-zip-line"></i>
                                </div>
                              </div>
                            </div>
                            <div class="flex-grow-1 overflow-hidden">
                              <h5 class="fs-13 mb-1"><a href="#" class="text-body text-truncate d-block">{{ $application->name }}</a></h5>
                              <div>
                                <?php
                                $file = "dist/app/".$application->appcode."/".$application->uuid."/".$application->filename."";

                                if((is_file($file))&&(file_exists($file))){
                                 $size = filesize($file);
                                 $mod = 1024;
                                 $units = explode(' ','B KB MB GB TB PB');
                                 for ($i = 0; $size > $mod; $i++) {
                                  $size /= $mod;
                                }
                                echo round($size, 2) . ' ' . $units[$i];
                              }else{
                                echo '0 KB';
                              }   

                              ?>
                            </div>
                          </div>
                          <div class="flex-shrink-0 ms-2">
                            <div class="d-flex gap-1">

                             <?php 
                             $uri = asset('dist/app')."/".$application->appcode."/".$application->uuid."/".$application->filename."";
                             $filename = $application->filename;
                             ?>

                             <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="downloadURI('{{ $uri }}', '{{ $filename }}', '{{ $application->id }}');" style="cursor: pointer"><i class="ri-download-2-line"></i></button>

                           </div>
                         </div>
                       </div>
                     </div>
                   </div>

                 </div>
                 <!-- end row -->
               </div>
             </div>
           </div>
           <!-- end card body -->
         </div>
         <!-- end card -->

      </div>
      <!-- ene col -->
      <div class="col-xl-3 col-lg-4">

        <div class="card">
          <div class="card-body">
              <div class="d-flex align-items-center">
                  <div class="avatar-sm flex-shrink-0">
                      <span class="avatar-title bg-light text-primary rounded-circle fs-3">
                          <i class="ri-money-dollar-circle-fill align-middle"></i>
                      </span>
                  </div>
                  <div class="flex-grow-1 ms-3">
                      <p class="text-uppercase fw-semibold fs-12 text-muted mb-1"> Price</p>
                      <h4 class=" mb-0">Rp. <span class="counter-value" data-target='{{ number_format($application->price,0,",",".") }}'>{{ number_format($application->price,0,",",".") }},-</span></h4>
                  </div>
              </div>
          </div><!-- end card body -->
      </div>


        <div class="card">
          <div class="card-body">
            <h5 class="card-title mb-4">Label</h5>
            <div class="d-flex flex-wrap gap-2 fs-16">
              <?php 
              $myArray = explode(',', $application->label);
              foreach ($myArray as $key => $value) : 
                ?>
                <div class="badge fw-medium badge-soft-secondary">{{ $value }}</div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header align-items-center d-flex border-bottom-dashed">
            <h4 class="card-title mb-0 flex-grow-1">Members</h4>
            <div class="flex-shrink-0">
              <button type="button" class="btn btn-soft-danger btn-sm" data-bs-toggle="modal" data-bs-target="#inviteMembersModal"><i class="ri-share-line me-1 align-bottom"></i> Invite Member</button>
            </div>
          </div>

          <div class="card-body">
            <div data-simplebar style="height: 235px;" class="mx-n3 px-3">
              <div class="vstack gap-3">

               @foreach($members as $val)
               <div class="d-flex align-items-center">
                <div class="avatar-xs flex-shrink-0 me-3">
                  <div class="avatar-title bg-soft-{{ $val->avatar_color }} text-{{ $val->avatar_color }} rounded-circle">
                    @if(!empty($val->last_name))
                    {{ substr($val->first_name,0,1) }}{{ substr($val->last_name,0,1) }}
                    @else
                    {{ substr($val->first_name,0,1) }}
                    @endif
                  </div>
                </div>
                <div class="flex-grow-1">
                  <h5 class="fs-13 mb-0"><a href="#" class="text-body d-block">{{ $val->first_name }} {{ $val->last_name }}</a></h5>
                </div>
              </div>
              @endforeach
            </div>
            <!-- end list -->
          </div>
        </div>
        <!-- end card body -->
      </div>
      <!-- end card -->

      <!-- end card -->
    </div>
    <!-- end col -->
  </div>
  <!-- end row -->
</div>
<!-- end tab pane -->
<div class="tab-pane fade" id="documents" role="tabpanel">
  <div class="card">
    <div class="card-body">
      <div class="d-flex align-items-center mb-4">
        <h5 class="card-title flex-grow-1">Documents</h5>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="table-responsive table-card">
            <table class="table table-borderless align-middle mb-0">
              <thead class="table-light">
                <tr>
                  <th scope="col">File Name</th>
                  <th scope="col">Type</th>
                  <th scope="col">Size</th>
                  <th scope="col">Upload Date</th>
                  <th scope="col" style="width: 120px;">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div class="d-flex align-items-center">
                      <div class="avatar-sm">
                        <div class="avatar-title bg-light text-secondary rounded fs-24">
                          <i class="ri-folder-zip-line"></i>
                        </div>
                      </div>
                      <div class="ms-3 flex-grow-1">
                        <h5 class="fs-14 mb-0"><a href="javascript:void(0)" class="text-dark">Artboard-documents.zip</a></h5>
                      </div>
                    </div>
                  </td>
                  <td>Zip File</td>
                  <td>4.57 MB</td>
                  <td>12 Dec 2021</td>
                  <td>
                    <div class="dropdown">
                      <a href="javascript:void(0);" class="btn btn-soft-secondary btn-sm btn-icon" data-bs-toggle="dropdown" aria-expanded="true">
                        <i class="ri-more-fill"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-eye-fill me-2 align-bottom text-muted"></i>View</a></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-download-2-fill me-2 align-bottom text-muted"></i>Download</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-delete-bin-5-fill me-2 align-bottom text-muted"></i>Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="d-flex align-items-center">
                      <div class="avatar-sm">
                        <div class="avatar-title bg-light text-danger rounded fs-24">
                          <i class="ri-file-pdf-fill"></i>
                        </div>
                      </div>
                      <div class="ms-3 flex-grow-1">
                        <h5 class="fs-14 mb-0"><a href="javascript:void(0);" class="text-dark">Bank Management System</a></h5>
                      </div>
                    </div>
                  </td>
                  <td>PDF File</td>
                  <td>8.89 MB</td>
                  <td>24 Nov 2021</td>
                  <td>
                    <div class="dropdown">
                      <a href="javascript:void(0);" class="btn btn-soft-secondary btn-sm btn-icon" data-bs-toggle="dropdown" aria-expanded="true">
                        <i class="ri-more-fill"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-eye-fill me-2 align-bottom text-muted"></i>View</a></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-download-2-fill me-2 align-bottom text-muted"></i>Download</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-delete-bin-5-fill me-2 align-bottom text-muted"></i>Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="d-flex align-items-center">
                      <div class="avatar-sm">
                        <div class="avatar-title bg-light text-secondary rounded fs-24">
                          <i class="ri-video-line"></i>
                        </div>
                      </div>
                      <div class="ms-3 flex-grow-1">
                        <h5 class="fs-14 mb-0"><a href="javascript:void(0);" class="text-dark">Tour-video.mp4</a></h5>
                      </div>
                    </div>
                  </td>
                  <td>MP4 File</td>
                  <td>14.62 MB</td>
                  <td>19 Nov 2021</td>
                  <td>
                    <div class="dropdown">
                      <a href="javascript:void(0);" class="btn btn-soft-secondary btn-sm btn-icon" data-bs-toggle="dropdown" aria-expanded="true">
                        <i class="ri-more-fill"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-eye-fill me-2 align-bottom text-muted"></i>View</a></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-download-2-fill me-2 align-bottom text-muted"></i>Download</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-delete-bin-5-fill me-2 align-bottom text-muted"></i>Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="d-flex align-items-center">
                      <div class="avatar-sm">
                        <div class="avatar-title bg-light text-success rounded fs-24">
                          <i class="ri-file-excel-fill"></i>
                        </div>
                      </div>
                      <div class="ms-3 flex-grow-1">
                        <h5 class="fs-14 mb-0"><a href="javascript:void(0);" class="text-dark">Account-statement.xsl</a></h5>
                      </div>
                    </div>
                  </td>
                  <td>XSL File</td>
                  <td>2.38 KB</td>
                  <td>14 Nov 2021</td>
                  <td>
                    <div class="dropdown">
                      <a href="javascript:void(0);" class="btn btn-soft-secondary btn-sm btn-icon" data-bs-toggle="dropdown" aria-expanded="true">
                        <i class="ri-more-fill"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-eye-fill me-2 align-bottom text-muted"></i>View</a></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-download-2-fill me-2 align-bottom text-muted"></i>Download</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-delete-bin-5-fill me-2 align-bottom text-muted"></i>Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="d-flex align-items-center">
                      <div class="avatar-sm">
                        <div class="avatar-title bg-light text-warning rounded fs-24">
                          <i class="ri-folder-fill"></i>
                        </div>
                      </div>
                      <div class="ms-3 flex-grow-1">
                        <h5 class="fs-14 mb-0"><a href="javascript:void(0);" class="text-dark">Project Screenshots Collection</a></h5>
                      </div>
                    </div>
                  </td>
                  <td>Floder File</td>
                  <td>87.24 MB</td>
                  <td>08 Nov 2021</td>
                  <td>
                    <div class="dropdown">
                      <a href="javascript:void(0);" class="btn btn-soft-secondary btn-sm btn-icon" data-bs-toggle="dropdown" aria-expanded="true">
                        <i class="ri-more-fill"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-eye-fill me-2 align-bottom text-muted"></i>View</a></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-download-2-fill me-2 align-bottom text-muted"></i>Download</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-delete-bin-5-fill me-2 align-bottom text-muted"></i>Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="d-flex align-items-center">
                      <div class="avatar-sm">
                        <div class="avatar-title bg-light text-danger rounded fs-24">
                          <i class="ri-image-2-fill"></i>
                        </div>
                      </div>
                      <div class="ms-3 flex-grow-1">
                        <h5 class="fs-14 mb-0"><a href="javascript:void(0);" class="text-dark">Velzon-logo.png</a></h5>
                      </div>
                    </div>
                  </td>
                  <td>PNG File</td>
                  <td>879 KB</td>
                  <td>02 Nov 2021</td>
                  <td>
                    <div class="dropdown">
                      <a href="javascript:void(0);" class="btn btn-soft-secondary btn-sm btn-icon" data-bs-toggle="dropdown" aria-expanded="true">
                        <i class="ri-more-fill"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-eye-fill me-2 align-bottom text-muted"></i>View</a></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-download-2-fill me-2 align-bottom text-muted"></i>Download</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="javascript:void(0);"><i class="ri-delete-bin-5-fill me-2 align-bottom text-muted"></i>Delete</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="text-center mt-3">
            <a href="javascript:void(0);" class="text-success "><i class="mdi mdi-loading mdi-spin fs-20 align-middle me-2"></i> Load more </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end tab pane -->
<div class="tab-pane fade" id="activities" role="tabpanel">
  <div class="card">
    <div class="card-body">
      <h5 class="card-title">Activities</h5>
      <div class="acitivity-timeline py-3">
        @foreach($logs_activity as $val)
         <div class="acitivity-item py-3 d-flex">
          <div class="flex-shrink-0">

            <?php 
            $file = "dist/img/members/".$val->avatar;
            ?>

            @if((is_file($file))&&(file_exists($file)))
              <img src="{{ asset('dist/app') }}/{{ $val->avatar }}" alt="" class="avatar-xs rounded-circle acitivity-avatar" />
            @else
              <div class="avatar-xs acitivity-avatar">
                <div class="avatar-title bg-soft-{{ $val->avatar_color }} text-{{ $val->avatar_color }} rounded-circle">
                  @if(!empty($val->last_name))
                  {{ substr($val->first_name,0,1) }}{{ substr($val->last_name,0,1) }}
                  @else
                  {{ substr($val->first_name,0,1) }}
                  @endif
                </div>
              </div>
            @endif
          </div>
          <div class="flex-grow-1 ms-3">
            <h6 class="mb-1">{{ $val->first_name }} {{ $val->last_name }}</h6>
            <p class="text-muted mb-2">
              @if($val->type == 'application')
              {!! $val->desc !!}
              @elseif($val->type == 'report')
              {!! $val->desc !!} <a href="javascript:void(0);" onClick="showReport('{{ $val->report_id }}')" class="link-warning text-decoration-underline">Reports</a>


              <div id="modalreport{{ $val->report_id}}" class="modal fade" tabindex="-1" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                  <div class="modal-content border-0 overflow-hidden">
                    <div class="modal-body login-modal" style="padding: 30px">
                      <h5 class="text-white fs-20">Report Members</h5>
                      <p class="text-white-50 mb-4">{{ $val->category }} 
                        <br>
                        {{ date('d M Y H:i', strtotime($val->created_at)) }} - {{ Carbon\Carbon::parse($val->created_at)->diffForHumans()}}
                      </p>
                    </div>
                    <div class="modal-body p-5">
                      <h5 class="fs-15">  Hi, Administrator</h5>

                      <h6 class="fs-15">Report Title</h6>
                      {!! $val->subject !!}

                      <hr>

                      <h6 class="fs-15">Description</h6>
                      {!! $val->desc !!}

                      <hr>


                      <div class="pt-3 border-top border-top-dashed mt-4">
                        <h6 class="mb-3 fw-semibold text-uppercase">Resources</h6>
                        <div class="row g-3">

                          <div class="col-xxl-12 col-lg-12">
                            <div class="border rounded border-dashed p-2">
                              <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 me-3">
                                  <div class="avatar-sm">
                                    
                                      <?php 

                                      $info = new SplFileInfo($val->report_filename);
                                         $format = $info->getExtension();
                                         if($format == 'jpg' ||$format == 'jpeg' || $format == 'png' || $format == 'svg'){
                                           $icon = 'ri-image-2-fill';
                                           $color = 'text-primary';
                                        }else if($format == 'doc'){
                                             $icon = 'ri-file-word-fill';
                                              $color = 'text-info';
                                        }else if($format == 'xlsx'){
                                            $icon = 'ri-file-excel-fill';
                                             $color = 'text-success';
                                        }else if($format == 'pdf'){
                                             $icon = 'ri-file-pdf-line';
                                              $color = 'text-danger';
                                        }else if($format == 'zip'){
                                            $icon = 'ri-folder-zip-line';
                                             $color = 'text-secondary';
                                        }else if($format == 'mp3' || $format == 'wav'){
                                            $icon = 'ri-file-music-fill';
                                             $color = 'text-info';
                                        }else if($format == 'mp4'){
                                             $icon = 'ri-video-line';
                                              $color = 'text-info';
                                        }else{
                                            $icon = 'ri-file-text-fill';
                                             $color = 'text-secondary';
                                         }
                                         ?>
                                    <div class="avatar-title bg-light {{ $color }} rounded fs-24">
                                      <i class="{{ $icon }}"></i>
                                    </div>
                                  </div>
                                </div>
                                <div class="flex-grow-1 overflow-hidden">
                                  <h5 class="fs-13 mb-1"><a href="#" class="text-body text-truncate d-block">{{ $val->report_filename }} </a></h5>
                                  <div>
                                    <?php
                                    $file = "dist/report/".$val->report_filename;

                                    if((is_file($file))&&(file_exists($file))){
                                     $size = filesize($file);
                                     $mod = 1024;
                                     $units = explode(' ','B KB MB GB TB PB');
                                     for ($i = 0; $size > $mod; $i++) {
                                      $size /= $mod;
                                    }
                                    echo round($size, 2) . ' ' . $units[$i];
                                  }else{
                                    echo '0 KB';
                                  }   

                                  ?>
                                </div>
                              </div>
                              <div class="flex-shrink-0 ms-2">
                                <div class="d-flex gap-1">

                                 <?php 
                                 $uri_report = asset('dist/report')."/".$val->report_filename;
                                 $report_filename = $val->report_filename;
                                 ?>

                                 <a href="{{ $uri_report }}" target="_blank">
                                   <button type="button" class="btn btn-icon text-muted btn-sm fs-18" style="cursor: pointer"><i class="ri-eye-line"></i></button>
                                 </a>

                                 <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="downloadURI('{{ $uri_report }}', '{{ $report_filename }}', '{{ $val->id }}');" style="cursor: pointer"><i class="ri-download-2-line"></i></button>


                               </div>
                             </div>
                           </div>
                         </div>
                       </div>

                     </div>
                   </div>
                 </div>


               </div>
             </div>
           </div>
         @endif
         </div>

       <small class="mb-0 text-muted">
        {{ date('d M Y', strtotime($val->created_at)) }} 
        <br>
        {{ Carbon\Carbon::parse($val->created_at)->diffForHumans()}}
      </small>
    </div>
  @endforeach
  </div>
</div>
</div>
</div>
</div>

<div class="tab-pane fade" id="payment" role="tabpanel">
  <div class="row g-4 mb-3">
    <div class="col-sm">

    </div>
    <div class="col-sm-auto">
      <div>
        <button type="button" class="btn btn-info" onClick="window.location.href=window.location.href">
          <i class="ri-refresh-line align-bottom me-1"></i> Reload</button>
        </div>
      </div>
    </div>


    <div class="row" id="list_data">
      <div class="col-12">
        <div class="card">
          <div class="card-body">

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
             <i class="ri-check-double-line label-icon"></i>
             {{ $message }}
           </div>
           @elseif ($message = Session::get('error'))
           <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
             <i class="ri-error-warning-line label-icon"></i>
             {{ $message }}
           </div>
           @endif

           <div class="row">
            <div class="col-md-12">
              <h4 class="card-title">Payment</h4>
              <p class="card-title-desc">Transaction</p>
            </div>

          </div>

          <table id="buttons-datatables" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
           <thead>
            <tr>
              <th>Id</th>
              <th>Type</th>
              <th>Invoice</th>
              <th>Members</th>
              <th>Price</th>
              <th>Status</th>
              <th>Payment Date</th>
            </tr>
          </thead>
          <tbody>
            @php $no=1; @endphp
            @foreach($orders as $val)
            <tr>
             <td width="5%" align="center">{{ $val->id }}</td>
             <td>{{ $val->payment_type }}</td>
             <td>{{ $val->invoice }}</td>
             <td>{{ $val->first_name }} {{ $val->last_name }}</td>
             <td>{{ $val->price }}</td>
             <td>{{ $val->status_payment }}</td>
             <td>{{ date('d F Y H:i', strtotime($val->payment_date)) }}</td>
           </tr>
           @php $no++; @endphp
           @endforeach
         </tbody>
       </table>
     </div>
   </div>
 </div> 
</div> 

</div>


<div class="tab-pane fade" id="download" role="tabpanel">
  <div class="row g-4 mb-3">
    <div class="col-sm">

    </div>
    <div class="col-sm-auto">
      <div>
        <button type="button" class="btn btn-info" onClick="window.location.href=window.location.href">
          <i class="ri-refresh-line align-bottom me-1"></i> Reload</button>
        </div>
      </div>
    </div>


    <div class="row" id="list_data">
      <div class="col-12">
        <div class="card">
          <div class="card-body">

            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
             <i class="ri-check-double-line label-icon"></i>
             {{ $message }}
           </div>
           @elseif ($message = Session::get('error'))
           <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
             <i class="ri-error-warning-line label-icon"></i>
             {{ $message }}
           </div>
           @endif

           <div class="row">
            <div class="col-md-12">
              <h4 class="card-title">Download Resources </h4>
              <p class="card-title-desc">Transaction</p>
            </div>

          </div>

          <table id="alternative-pagination" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
           <thead>
            <tr>
              <th>Id</th>
              <th>Members</th>
              <th>Download Date</th>
            </tr>
          </thead>
          <tbody>
            @php $no=1; @endphp
            @foreach($history_download as $val)
            <tr>
             <td width="5%" align="center">{{ $val->id }}</td>
             <td>{{ $val->first_name }} {{ $val->last_name }}</td>
             <td width="15%">{{ date('d F Y H:i', strtotime($val->download_date)) }}</td>
           </tr>
           @php $no++; @endphp
           @endforeach
         </tbody>
       </table>
     </div>
   </div>
 </div> 
</div> 





</div>
<!-- end tab pane -->
</div>
</div>
<!-- end col -->
</div>
<!-- end row -->

@section('page-script')
<script type="text/javascript">
  function Readmore(){
    $("#shortdesc").hide();
    $("#fulldesc").show();
    $("#readmore").hide();
  }

  function showReport(id){
    $("#modalreport"+id).modal('show');
  }
</script>

<script type="text/javascript">
   
    function downloadURI(uri, name, id) {
      var link = document.createElement("a");
      link.download = name;
      link.href = uri;
      link.setAttribute("target", "_blank");
      link.click();
      link.remove();


    var file_id = id;

    $.ajax({
          type: 'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: '{{ url("hit_unduh")}}',
          data: 'file_id='+file_id+'&name='+name,
          success: function (data) {

          },
      });
    }
 </script>
@stop
@endsection