@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Application</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Application</a></li>
                    <li class="breadcrumb-item active">Tambah Data</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<form action="{{ url('application/update') }}" method="post" enctype="multipart/form-data">
  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <input type="hidden" name="code_id" value="{{ $application->uuid }}">
  <div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <div class="mb-3">
                    <label class="form-label" for="project-title-input">Title <code>*</code></label>
                    <input type="text" autocomplete="off" class="form-control" id="name" name="name" placeholder="Enter application title" required value="{{ $application->name }}">
                </div>

                <div class="mb-3">
                    <label class="form-label" for="project-thumbnail-img">Thumbnail <code>*</code> </label>
                    <input class="form-control" id="file2" name="file2"  type="file" accept="image/png, image/gif, image/jpeg" >
                </div>

                <div class="mb-3">
                    <label class="form-label">Description</label>
                    <textarea id="ckeditor-classic" name="desc" >{{ $application->desc }}</textarea>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="mb-3 mb-lg-0">
                            <label for="choices-priority-input" class="form-label">Level <code>*</code></label>
                            <select class="form-select" data-choices data-choices-search-false id="level" name="level" required>
                                <option value="High" <?php if('High' == $application->level) echo 'selected="selected"';?>>High</option>
                                <option value="Medium" <?php if('Medium' == $application->level) echo 'selected="selected"';?>>Medium</option>
                                <option value="Low" <?php if('Low' == $application->level) echo 'selected="selected"';?>>Low</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="mb-3 mb-lg-0">
                            <label for="choices-status-input" class="form-label" >App Type <code>*</code></label>
                            <select class="form-select" data-choices data-choices-search-false id="app_type" name="app_type" required>
                                <option value="Free" <?php if('Free' == $application->app_type) echo 'selected="selected"';?>>Free</option>
                                <option value="Premium" <?php if('Premium' == $application->app_type) echo 'selected="selected"';?>>Premium</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div>
                            <label for="datepicker-deadline-input" class="form-label">Price</label>
                            <input type="text" autocomplete="off" class="form-control" id="price" name="price" placeholder="Price" data-provider="flatpickr" value="{{ $application->price }}">
                        </div>
                    </div>
                </div>
                <br>

                <div class="mb-3">
                    <label class="form-label" for="project-thumbnail-img">File Aplikasi (.zip)  </label>
                    <input class="form-control" id="file" name="file"  type="file" >
                </div>

                <div class="pt-3 border-top border-top-dashed mt-4">
                    <h6 class="mb-3 fw-semibold text-uppercase">Resources</h6>
                    <div class="row g-3">

                        <div class="col-xxl-12 col-lg-12">
                            <div class="border rounded border-dashed p-2">
                                <div class="d-flex align-items-center">
                                    <div class="flex-shrink-0 me-3">
                                        <div class="avatar-sm">
                                            <div class="avatar-title bg-light text-secondary rounded fs-24">
                                                <i class="ri-folder-zip-line"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 overflow-hidden">
                                        <h5 class="fs-13 mb-1"><a href="#" class="text-body text-truncate d-block">{{ $application->name }}</a></h5>
                                        <div>
                                            <?php
                                            $file = "dist/app/".$application->appcode."/".$application->uuid."/".$application->filename."";

                                            if((is_file($file))&&(file_exists($file))){
                                                   $size = filesize($file);
                                                    $mod = 1024;
                                                        $units = explode(' ','B KB MB GB TB PB');
                                                        for ($i = 0; $size > $mod; $i++) {
                                                            $size /= $mod;
                                                        }
                                                     echo round($size, 2) . ' ' . $units[$i];
                                              }else{
                                                    echo '0 KB';
                                              }   
                                           
                                            ?>
                                        </div>
                                    </div>
                                    <div class="flex-shrink-0 ms-2">
                                        <div class="d-flex gap-1">

                                             <?php 
                                                    $uri = asset('dist/app')."/".$application->appcode."/".$application->uuid."/".$application->filename."";
                                                    $filename = $application->filename;
                                             ?>

                                            <button type="button" class="btn btn-icon text-muted btn-sm fs-18" onclick="downloadURI('{{ $uri }}', '{{ $filename }}', '{{ $application->id }}');" style="cursor: pointer"><i class="ri-download-2-line"></i></button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->
                </div>

                <br>


                <div class="row">
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label class="form-label" for="project-title-input">Demo Video <code>*</code></label>
                            <input type="text" autocomplete="off" class="form-control" id="link_video" name="link_video" placeholder="Enter link demo video" required value="{{ $application->link_video }}">
                        </div>

                    </div>
                    <div class="col-lg-6">
                        <div class="mb-3">
                            <label class="form-label" for="project-title-input">Demo Web <code>*</code></label>
                            <input type="text" autocomplete="off" class="form-control" id="link_web" name="link_web" placeholder="Enter link demo web" required value="{{ $application->link_web }}">
                        </div>

                    </div>
                </div>

            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->


        <!-- end card -->
        <div class="text-end mb-4">

            <a href="{{ url('application') }}">
                <button type="button" class="btn btn-success"><i class="ri-arrow-left-line align-bottom me-1"></i> Back</button>
            </a>
            <a href="{{ url('application/app') }}">
                <button type="submit" class="btn btn-primary"><i class="ri-save-line align-bottom me-1"></i> Update Application</button>
            </a>
            <button type="reset" class="btn btn-danger"><i class="ri-close-line align-bottom me-1"></i> Cancel</button>
        </div>
    </div>
    <!-- end col -->
    <div class="col-lg-4">

       <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Thumbnail</h5>
        </div>
        <div class="card-body">

            <figure class="figure mb-0">
                <img src="{{ asset('dist/app') }}/{{ $application->appcode }}/{{ $application->uuid }}//{{ $application->thumbnail }}" class="figure-img img-fluid rounded" alt="..." onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/no-image.jpg'">
                <figcaption class="figure-caption text-end">{{ $application->name }}</figcaption>
            </figure>
        </div>
        <!-- end card body -->
    </div>

    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Status</h5>
        </div>
        <div class="card-body">
            <div>
                <label for="choices-privacy-status-input" class="form-label">Status <code>*</code></label>
                <select class="form-select" data-choices data-choices-search-false id="status_publish" name="status_publish" required>
                    <option value="0" <?php if('0' == $application->status_publish) echo 'selected="selected"';?>>Draft</option>
                    <option value="1" <?php if('1' == $application->status_publish) echo 'selected="selected"';?>>Publish</option>
                </select>
            </div>
        </div>
        <!-- end card body -->
    </div>
    <!-- end card -->

    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">Tags</h5>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <label for="choices-categories-input" class="form-label">Categories <code>*</code></label>
                <select class="form-select" data-choices data-choices-search-false id="category_id" name="category_id" required>
                    @foreach($application_category as $val)
                    <option value="{{ $val->id }}" <?php if($val->id == $application->category_id) echo 'selected="selected"';?>>{{ $val->name }}</option>
                    @endforeach
                </select>
            </div>

            <div>
                <label for="choices-text-input" class="form-label">Label</label>
                <input class="form-control" id="choices-text-input" data-choices data-choices-limit="Required Limit" placeholder="" type="text" autocomplete="off"  id="label" name="label" value="{{ $application->label }}">
            </div>
        </div>
        <!-- end card body -->
    </div>

    <!-- end card body -->
</div>
<!-- end card -->


</div>
<!-- end col -->
</div>
</form>


@section('page-script')
<script type="text/javascript">
   
    function downloadURI(uri, name, id) {
      var link = document.createElement("a");
      link.download = name;
      link.href = uri;
      link.setAttribute("target", "_blank");
      link.click();
      link.remove();


    var file_id = id;

    $.ajax({
          type: 'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          url: '{{ url("hit_unduh")}}',
          data: 'file_id='+file_id+'&name='+name,
          success: function (data) {

          },
      });
    }
 </script>
@stop
@endsection