@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Application</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Master Data</a></li>
                    <li class="breadcrumb-item active">Application</li>
                </ol>
            </div>

        </div>
    </div>
</div>


<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

              @if ($message = Session::get('success'))
                 <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-check-double-line label-icon"></i>
                       {{ $message }}
                  </div>
              @elseif ($message = Session::get('error'))
                  <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-error-warning-line label-icon"></i>
                       {{ $message }}
                  </div>
              @endif

                <div class="row">
                    <div class="col-md-10">
                      <h4 class="card-title">Application List</h4>
                      <p class="card-title-desc">Free</p>
                  </div>
                  <div class="col-md-2">
                    <a href="{{ url('application/add') }}">
                   <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" ><i class="ri-add-circle-line align-bottom me-1"></i> Tambah</button>
                 </a>
                   <button type="button" class="btn btn-info btn-sm waves-effect waves-light" onClick="window.location.href=window.location.href" title="Refresh"><i class="ri-refresh-line align-bottom me-1"></i> Reload </button>

               </div>
           </div>

          <table id="alternative-pagination" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
             <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>View</th>
                    <th>Unduh</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php $no=1; @endphp
                @foreach($app_free as $val)
                <tr>
                   <td width="2%" align="center">{{ $no }}</td>
                   <td width="20%">{{ $val->name }} <br> 
                    <span class="badge badge-soft-success text-uppercase">{{ $val->appcode }} </span>
                  </td>
                   <td width="10%">{{ $val->category_name }}</td>
                   <td width="5%"><span class="badge badge-soft-info text-uppercase">{{ $val->app_type }}</span></td>
                   <td width="5%" align="center">{{ $val->hit_view }}</td>
                   <td width="10%" align="center">{{ $val->hit_download }}</td>
                   <td width="5%" align="center">
                     @if($val->status_publish == 1)
                     <span class="badge badge-soft-success text-uppercase">Publish</span>
                     @elseif($val->status_publish == 0)
                     <span class="badge badge-soft-danger text-uppercase">Draft</span>
                     @endif
                   </td>
                <td width="3%" align="center">

                   <div class="dropdown">
                            <a href="javascript:void(0);" class="btn btn-soft-secondary btn-sm btn-icon" data-bs-toggle="dropdown" aria-expanded="true">
                              <i class="ri-more-fill"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end">
                              <li><a class="dropdown-item" href="{{ url('application/show') }}/{{ $val->uuid }}"><i class="ri-eye-fill me-2 align-bottom text-muted"></i>View</a></li>
                              <li><a class="dropdown-item" href="{{ url('application/edit') }}/{{ $val->uuid }}"><i class="ri-pencil-fill me-2 align-bottom text-muted"></i>Update</a></li>
                              <li class="dropdown-divider"></li>
                              <li><a class="dropdown-item" href="javascript:void(0);"  onclick='deleteData("{{ $val->id }}'><i class="ri-delete-bin-5-fill me-2 align-bottom text-muted"></i>Delete</a></li>
                            </ul>
                          </div>
                </td>
            </tr>
            @php $no++; @endphp
            @endforeach
        </tbody>
    </table>

   </div>
</div>
</div>
</div>

 
<div class="row" id="list_data2">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

       <div class="row">
                    <div class="col-md-10">
                      <h4 class="card-title">Application List</h4>
                      <p class="card-title-desc">Premium</p>
                  </div>
                  <div class="col-md-2">
                    <a href="{{ url('application/add') }}">
                   <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" ><i class="ri-add-circle-line align-bottom me-1"></i> Tambah</button>
                 </a>
                   <button type="button" class="btn btn-info btn-sm waves-effect waves-light" onClick="window.location.href=window.location.href" title="Refresh"><i class="ri-refresh-line align-bottom me-1"></i> Reload </button>

               </div>
           </div>

     <table id="buttons-datatables" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
             <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Type</th>
                    <th>Price</th>
                    <th>View</th>
                    <th>Unduh</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php $num=1; @endphp
                @foreach($app_premium as $val)
                <tr>
                    <td width="2%" align="center">{{ $num }}</td>
                   <td width="20%">{{ $val->name }} <br> 
                    <span class="badge badge-soft-success text-uppercase">{{ $val->appcode }} </span>
                  </td>
                   <td width="10%">{{ $val->category_name }}</td>
                   <td width="5%"><span class="badge badge-soft-info text-uppercase">{{ $val->app_type }}</span></td>
                   <td width="10%" align="right">Rp. {{ number_format($val->price,0,",",".") }}</td>
                   <td width="5%" align="center">{{ $val->hit_view }}</td>
                   <td width="10%" align="center">{{ $val->hit_download }}</td>
                   <td width="5%" align="center">
                     @if($val->status_publish == 1)
                     <span class="badge badge-soft-success text-uppercase">Publish</span>
                     @elseif($val->status_publish == 0)
                     <span class="badge badge-soft-danger text-uppercase">Draft</span>
                     @endif
                   </td>
                <td width="3%" align="center">

                   <div class="dropdown">
                            <a href="javascript:void(0);" class="btn btn-soft-secondary btn-sm btn-icon" data-bs-toggle="dropdown" aria-expanded="true">
                              <i class="ri-more-fill"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end">
                              <li><a class="dropdown-item" href="{{ url('application/show') }}/{{ $val->uuid }}"><i class="ri-eye-fill me-2 align-bottom text-muted"></i>View</a></li>
                              <li><a class="dropdown-item" href="{{ url('application/edit') }}/{{ $val->uuid }}"><i class="ri-pencil-fill me-2 align-bottom text-muted"></i>Update</a></li>
                              <li class="dropdown-divider"></li>
                              <li><a class="dropdown-item" href="javascript:void(0);"  onclick='deleteData("{{ $val->id }}'><i class="ri-delete-bin-5-fill me-2 align-bottom text-muted"></i>Delete</a></li>
                            </ul>
                          </div>
                </td>
            </tr>
            @php $num++; @endphp
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')

<script type="text/javascript">

function deleteData(id){
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-primary w-xs me-2 mt-2",
        cancelButtonClass: "btn btn-danger w-xs mt-2",
        confirmButtonText: "Yes, delete it!",
        buttonsStyling: !1,
        showCloseButton: !0
   }).then(function (result) {
    if (result.value) {
         $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ url("membership/members/delete")}}',
            enctype: "multipart/form-data",
            dataType : 'JSON',
            data: 'id='+id,
            success: function (data) {

             Swal.fire(data.title, data.msg, "success");
             setTimeout(() => {
                  window.location.reload();
              }, 2000);
         },
     });

    }
    })
}

</script>
@stop
@endsection