@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Members Add</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Membership</a></li>
                    <li class="breadcrumb-item active">Members Add</li>
                </ol>
            </div>

        </div>
    </div>
</div>

                    <div class="position-relative mx-n4 mt-n4">
                        <div class="profile-wid-bg profile-setting-img">
                            <img src="" class="profile-wid-img" alt="" onerror="this.onerror=null;this.src='<?= url('/') ?>/assets/images/profile-bg.jpg'">
                            <div class="overlay-content">
                                <div class="text-end p-3">
                                    <div class="p-0 ms-auto rounded-circle profile-photo-edit">
                                        <label for="profile-foreground-img-file-input" class="profile-photo-edit btn btn-light">
                                            <i class="ri-medal-line align-bottom me-1"></i> {{ $level }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-xxl-3">
                            <div class="card mt-n5">
                                <div class="card-body p-4">
                                    <div class="text-center">
                                        <div class="profile-user position-relative d-inline-block mx-auto  mb-4">
                                            <img src="" class="rounded-circle avatar-xl img-thumbnail user-profile-image" alt="user-profile-image" onerror="this.onerror=null;this.src='<?= url('/') ?>/dist/icon_user.jpg'">
                                            <div class="avatar-xs p-0 rounded-circle profile-photo-edit">
                                                <label for="profile-img-file-input" class="profile-photo-edit avatar-xs">
                                                    <span class="avatar-title rounded-circle bg-light text-body">
                                                        <i class="ri-medal-line"></i>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <h5 class="fs-16 mb-1" id="fullname"></h5>
                                        <p class="text-muted mb-0">Membership</p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!--end col-->
                        <div class="col-xxl-9">
                            <div class="card mt-xxl-n5">
                                <div class="card-header">
                                    <ul class="nav nav-tabs-custom rounded card-header-tabs border-bottom-0" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#personalDetails" role="tab">
                                                <i class="fas fa-home"></i> Personal Details
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="card-body p-4">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="personalDetails" role="tabpanel">
                                              <form action="{{ url('membership/members/add') }}" method="post" enctype="multipart/form-data">
                                               <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="firstnameInput" class="form-label">First Name</label>
                                                            <input type="text" class="form-control" id="firstnameInput" placeholder="Enter your firstname" name="first_name" >
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="lastnameInput" class="form-label">Last Name</label>
                                                            <input type="text" class="form-control" id="lastnameInput" placeholder="Enter your lastname" name="last_name" >
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="phonenumberInput" class="form-label">Phone Number</label>

                                                             <div class="input-group">
                                                              <span class="input-group-text" id="basic-addon1"><img src="{{ asset('assets/images/flags/id.svg') }}" alt="flag img" height="20" class="country-flagimg rounded"><span class="ms-2 country-codeno">+ 62</span></span>
                                                              <input type="text" class="form-control"  placeholder="Enter your phone number" name="phone" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                                                          </div>
                                                           
                                                      </div>
                                                    </div>


                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="emailInput" class="form-label">Email Address</label>
                                                            <input type="email" class="form-control" id="emailInput" name="email" placeholder="Enter your email" >
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="phonenumberInput" class="form-label">Join Date</label>
                                                            <input type="text" class="form-control" id="phonenumberInput" placeholder="" value="{{ date('Y-m-d') }}" readonly>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="countryInput" class="form-label">Profession</label>
                                                            <select class="js-example-basic-single form-control" name="profession_id" id="profession_id">
                                                              @foreach($profession as $val)
                                                              <option value="{{ $val->id }}" >{{ $val->name }}</option>
                                                              @endforeach
                                                          </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="mb-3">
                                                            <label for="countryInput" class="form-label">Country</label>
                                                            <select class=" form-control" name="country_id" id="country_id">
                                                              @foreach($country as $val)
                                                              <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                              @endforeach
                                                          </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="mb-3">
                                                            <label for="" class="form-label">Province</label>
                                                            <select class="js-example-basic-single form-control" name="province_id" id="province_id" onchange="getprovince()">
                                                          </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <div class="mb-3">
                                                            <label for="" class="form-label">City</label>
                                                            <select class="js-example-basic-single form-control" name="city_id" id="city_id">
                                                          </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-12">
                                                        <div class="mb-3 pb-2">
                                                            <label for="" class="form-label">Description</label>
                                                            <textarea class="form-control" id="desc" placeholder="Enter your description" name="desc" rows="3"></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="emailInput" class="form-label">Password </label>
                                                           <input type="password" class="form-control" id="password1" name="password" placeholder="Enter new password" onkeyup="getPass()" autocomplete="off" required minlength="6">
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-lg-6">
                                                        <div class="mb-3">
                                                            <label for="phonenumberInput" class="form-label">Confirm Password</label>
                                                            <input type="password" name="password2" class="form-control" id="password2" placeholder="Confirm password" onkeyup="getPass2()" autocomplete="off" required minlength="6">

                                                            <span id="notif_pass"></span>
                                                        </div>
                                                    </div>

                                                    

                                                    <div class="col-lg-12">
                                                        <div class="hstack gap-2 justify-content-end">

                                                            <a href="{{ url('membership/members') }}">
                                                            <button type="submit" class="btn btn-primary"><i class="ri-save-line align-bottom me-1"></i> Create Account</button>
                                                            </a>
                                                            <button type="reset" class="btn btn-danger"><i class="ri-close-line align-bottom me-1"></i> Cancel</button>
                                                        </div>
                                                    </div>
                                                    <!--end col-->

                                                   
                                                </div>
                                            </form>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->

@section('page-script')
<script type="text/javascript">

  $( document ).ready(function() {
    getcountry();
});

  function getcountry(){
    $("#province_id").empty();
    var country = $("#country_id").val();
    $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ url("membership/wilayah_json")}}',
            data: 'id='+country + '&param=1',
            success: function (data) {
              $("#province_id").append(data);

              getprovince();
         }
        });
  }

  function getprovince(){
    $("#city_id").empty();
    var province = $("#province_id").val();
    $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ url("membership/wilayah_json")}}',
            data: 'id='+province + '&param=2',
            success: function (data) {
              $("#city_id").append(data);
         }
        });
  }

  function getPass(){
        $("#password2").val("");
        $("#notif_pass").html("");
        $("#btnSubmit").hide();
    }
    function getPass2(){
        var passsword1 = $("#password1").val();
        var passsword2 = $("#password2").val();
        if(passsword1 == passsword2){
            $("#notif_pass").html('<span style="font-size: 13px;  color: green;"><i class="ri-check-double-fill align-bottom me-1"></i> Password Sama</span>');
            $("#btnSubmit").show();
        }else{
           $("#notif_pass").html(' <span style="font-size: 13px;  color: red;"><i class=" ri-close-fill align-bottom me-1"></i> Password Tidak Sama</span>');
           $("#btnSubmit").hide();
       }
   }
</script>
@stop
@endsection