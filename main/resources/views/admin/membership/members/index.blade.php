@extends('layouts.master_backend')
@section('content')


<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">Members</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Master Data</a></li>
                    <li class="breadcrumb-item active">Members</li>
                </ol>
            </div>

        </div>
    </div>
</div>


<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

              @if ($message = Session::get('success'))
                 <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-check-double-line label-icon"></i>
                       {{ $message }}
                  </div>
              @elseif ($message = Session::get('error'))
                  <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                       <i class="ri-error-warning-line label-icon"></i>
                       {{ $message }}
                  </div>
              @endif

                <div class="row">
                    <div class="col-md-10">
                      <h4 class="card-title">Members</h4>
                      <p class="card-title-desc">Master Data</p>
                  </div>
                  <div class="col-md-2">
                    <a href="{{ url('membership/members/add') }}">
                   <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" ><i class="ri-add-circle-line align-bottom me-1"></i> Tambah</button>
                 </a>
                   <button type="button" class="btn btn-info btn-sm waves-effect waves-light" onClick="window.location.href=window.location.href" title="Refresh"><i class="ri-refresh-line align-bottom me-1"></i> Reload </button>

               </div>
           </div>

          <table id="alternative-pagination" class="table  dt-responsive align-middle table-hover table-bordered" style="width:100%">
             <thead>
                <tr>
                    <th>Id</th>
                    <th>Fullname</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Join Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php $no=1; @endphp
                @foreach($members as $val)
                <tr>
                   <td width="5%" align="center">{{ $val->id }}</td>
                   <td width="20%">{{ $val->first_name }} {{ $val->last_name }}</td>
                   <td width="20%">{{ $val->email }}</td>
                   <td width="10%">{{ $val->phone }}</td>
                   <td width="10%">{{ $val->join_date }}</td>
                   <td width="5%" align="center">
                     @if($val->status_id == 1)
                     <span class="badge badge-soft-success text-uppercase">Active</span>
                     @elseif($val->status_id == 0)
                     <span class="badge badge-soft-danger text-uppercase">Non-Active</span>
                     @endif
                   </td>
                <td width="10%" align="center">

                  <a href="{{ url('membership/members/edit') }}/{{ $val->uuid }}"  class="text-primary d-inline-block edit-item-btn">
                          <i class="ri-pencil-fill fs-16"></i>
                        </a>

                    <a class="text-danger d-inline-block remove-item-btn" href="javascript:void(0)" onclick='deleteData("{{ $val->id }}")'>
                          <i class="ri-delete-bin-5-fill fs-16"></i>
                        </a>
                </td>
            </tr>
            @php $no++; @endphp
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')

<script type="text/javascript">

function deleteData(id){
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn btn-primary w-xs me-2 mt-2",
        cancelButtonClass: "btn btn-danger w-xs mt-2",
        confirmButtonText: "Yes, delete it!",
        buttonsStyling: !1,
        showCloseButton: !0
   }).then(function (result) {
    if (result.value) {
         $.ajax({
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: '{{ url("membership/members/delete")}}',
            enctype: "multipart/form-data",
            dataType : 'JSON',
            data: 'id='+id,
            success: function (data) {

             Swal.fire(data.title, data.msg, "success");
             setTimeout(() => {
                  window.location.reload();
              }, 2000);
         },
     });

    }
    })
}

</script>
@stop
@endsection