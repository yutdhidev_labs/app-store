<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Login | Sistem Informasi Rumah Stunting</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/img/logo/logo.png') }}">
    <!-- Bootstrap Css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('assets/css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="account-pages my-5 pt-sm-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="bg-soft-primary">
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-primary">
                                         <img src="{{ asset('assets/kemensos.jpg') }}" alt="" class="img-fluid">
                                    </div>
                                </div>
                                <div class="col-5 align-self-end">
                                   <!--  <img src="{{ asset('assets/kemensos.jpg') }}" alt="" class="img-fluid"> -->
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0"> 
                           <!--  <div>
                                <a href="{{url('/')}}">
                                    <div class="avatar-md profile-user-wid mb-4">
                                        <span class="avatar-title rounded-circle bg-light">
                                            <img src="{{ asset('assets/img/logo/logo.png') }}" alt="" class="rounded-circle" height="34">
                                        </span>
                                    </div>
                                </a>
                            </div>
 -->
                            <div class="p-2">
                             @if ($message = Session::get('success'))
                             <div class="alert alert-success alert-dismissible alert_notif">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="icon fa fa-check"></i> {{ $message }}
                            </div>
                            @elseif ($message = Session::get('error'))
                            <div class="alert alert-danger alert-dismissible alert_notif">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <i class="icon fa fa-info"></i> {{ $message }}
                            </div>
                            @endif
                            <form action="{{ url('login/access') }}" method="post">
                              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                              <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="Enter username">
                            </div>
                            <div class="form-group">
                                <label for="userpassword">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                            </div>
                            <div class="mt-3">
                                <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Log In</button>
                            </div>
                            <div class="mt-4 text-center">
                                <a href="{{ url('forgot')}}" class="text-muted"><i class="mdi mdi-lock mr-1"></i> Forgot your password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <div>
                    <p>© <?= date('Y') ?> Sistem Informasi Rumah Stunting</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- JAVASCRIPT -->
<script src="{{ asset('assets/libs/jquery/jquery.min.js')  }}"></script>
<script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js')  }}"></script>
<script src="{{ asset('assets/libs/metismenu/metisMenu.min.js')  }}"></script>
<script src="{{ asset('assets/libs/simplebar/simplebar.min.js')  }}"></script>
<script src="{{ asset('assets/libs/node-waves/waves.min.js')  }}"></script>
<script src="{{ asset('assets/js/app.js')  }}"></script>
</body>
</html>
