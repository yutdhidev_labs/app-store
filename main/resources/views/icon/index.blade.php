@extends('layouts.master_backend')
@section('content')

<div class="row">
	<div class="col-12">
		<div class="page-title-box d-sm-flex align-items-center justify-content-between">
			<h4 class="mb-sm-0">Icon</h4>

			<div class="page-title-right">
				<ol class="breadcrumb m-0">
					<li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
					<li class="breadcrumb-item active">Icon</li>
				</ol>
			</div>

		</div>
	</div>
</div>

<div class="row">
	@foreach($icon_animation as $val)
	<div class="col-lg-2">
		<div class="card" style="height: 250px">
			<div class="card-header align-items-center d-flex">
				<div class="flex-shrink-0">
					<div class="form-check form-switch form-switch-right form-switch-md">
						<label for="outline-button" class="form-label text-muted">Show Code</label>
						<input class="form-check-input code-switcher" type="checkbox" id="outline-button">
					</div>
				</div>
			</div><!-- end card header -->
			<div class="card-body">
				<div class="live-preview" style="text-align: center">
					{!! $val->sourcecode !!}

					<a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="getCopy('{{ $val->id }}')" style="width: 100%; text-align: bottom">{{ $val->name }}</a>
				</div>
				<div class="d-none code-view">
					<pre class="language-markup" style="height: 100px;"><code>{{ $val->sourcecode }}</code></pre>
				</div>
			</div>
		</div>
	</div>
	@endforeach
</div>


@section('page-script')
<script type="text/javascript">
	function getCopy(id){

		var copyText = document.getElementById("myText"+id).innerHTML;

  // Select the text field
  copyText.select();
  copyText.setSelectionRange(0, 99999); // For mobile devices

  // Copy the text inside the text field
  navigator.clipboard.writeText(copyText.value);
  
  // Alert the copied text
  alert(copyText.value);

}
</script>
@stop
@endsection
