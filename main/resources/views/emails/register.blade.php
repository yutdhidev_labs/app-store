<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none" data-preloader="disable">

<head>

	<meta charset="utf-8" />
	<title>Register | {{ env('APP_NAME')}}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta content="Application Premium" name="description" />
	<meta content="{{ env('APP_NAME')}}" name="author" />
	<!-- App favicon -->
	<link rel="shortcut icon" href="{{ asset('dist/favicon.png') }}">

	<!-- jsvectormap css -->
	<link href="{{ asset('assets/libs/jsvectormap/css/jsvectormap.min.css') }}" rel="stylesheet" type="text/css" />

	<!--Swiper slider css-->
	<link href="{{ asset('assets/libs/swiper/swiper-bundle.min.css') }}" rel="stylesheet" type="text/css" />

	<!-- Layout config Js -->
	<script src="{{ asset('assets/js/layout.js') }}"></script>
	<!-- Bootstrap Css -->
	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Icons Css -->
	<link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- App Css-->
	<link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- custom Css-->
	<link href="{{ asset('assets/css/custom.min.css') }}" rel="stylesheet" type="text/css" />

</head>

<body>
	<div class="row">

		<div class="col-12">
			<table class="body-wrap" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: transparent; margin: 0;">
				<tr style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
					<td style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"></td>
					<td class="container" width="600" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top">
						<div class="content" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
							<table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope itemtype="http://schema.org/ConfirmAction" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; margin: 0; border: none;">
								<tr style="font-family: 'Roboto', sans-serif; font-size: 14px; margin: 0;">
									<td class="content-wrap" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; color: #495057; font-size: 14px; vertical-align: top; margin: 0;padding: 30px; box-shadow: 0 3px 15px rgba(30,32,37,.06); ;border-radius: 7px; background-color: #fff;" valign="top">
										<meta itemprop="name" content="Confirm Email" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" />
										<table width="100%" cellpadding="0" cellspacing="0" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
											<tr style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;" valign="top">
													<div style="margin-bottom: 15px;">
														<img src="{{ asset('dist/logo.png') }}" alt="" height="60">
													</div>
												</td>
											</tr>
											<tr style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 16px; line-height: 1.5; font-weight: 500; vertical-align: top; margin: 0; padding: 0 0 10px;" valign="top">
													Hi, {{ $name }}
												</td>
											</tr>
											<tr style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" style="font-family: 'Roboto', sans-serif; color: #878a99; box-sizing: border-box; line-height: 1.5; font-size: 15px; vertical-align: top; margin: 0; padding: 0 0 10px;" valign="top">
													Selamat datang dan bergabung di {{ env('APP_NAME') }}, Akun membership anda telah aktif. Yuk explore aplikasi di {{ env('APP_NAME') }} !
												</td>
											</tr>
											
											<tr style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
												<td class="content-block" itemprop="handler" itemscope itemtype="http://schema.org/HttpActionHandler" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 24px;" valign="top">
													<a href="{{ url('login') }}" itemprop="url" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: .8125rem;font-weight: 400; color: #FFF; text-decoration: none; text-align: center; cursor: pointer; display: inline-block; border-radius: .25rem; text-transform: capitalize; background-color: #0ab39c; margin: 0; border-color: #0ab39c; border-style: solid; border-width: 1px; padding: .5rem .9rem;" onMouseOver="this.style.background='#099885'" onMouseOut="this.style.background='#0ab39c'">Login </a>
												</td>
											</tr>

											<tr style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; margin: 0; border-top: 1px solid #e9ebec;">
												<td class="content-block" style="font-family: 'Roboto', sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0; padding-top: 15px" valign="top">
													<div style="display: flex; align-items: center;">
														<img src="assets/images/users/avatar-3.jpg" alt="" height="35" width="35" style="border-radius: 50px;">
														<div style="margin-left: 8px;">
															<span style="font-weight: 600;">Admin</span>
															<p style="font-size: 13px; margin-bottom: 0px; margin-top: 3px; color: #878a99;">{{ env('APP_NAME') }}</p>
														</div>
													</div>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<div style="text-align: center; margin: 0px auto; margin-top:30px">

								<p style="font-family: 'Roboto', sans-serif; font-size: 14px;color: #98a6ad; margin: 0px;">2022 {{ env('APP_NAME')}} - {{ env('APP_NAME_CREATED')}}</p>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<!-- end table -->
		</div>
		<!--end col-->
	</div><!-- end row -->


	<!-- JAVASCRIPT -->
	<script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
	<script src="{{ asset('assets/libs/node-waves/waves.min.js') }}"></script>
	<script src="{{ asset('assets/libs/feather-icons/feather.min.js') }}"></script>
	<script src="{{ asset('assets/js/pages/plugins/lord-icon-2.1.0.js') }}"></script>
	<script src="{{ asset('assets/js/plugins.js') }}"></script>

	<!-- apexcharts -->
	<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>

	<!-- Vector map-->
	<script src="{{ asset('assets/libs/jsvectormap/js/jsvectormap.min.js') }}"></script>
	<script src="{{ asset('assets/libs/jsvectormap/maps/world-merc.js') }}"></script>

	<!--Swiper slider js-->
	<script src="{{ asset('assets/libs/swiper/swiper-bundle.min.js') }}"></script>

	<!-- Dashboard init -->
	<script src="{{ asset('assets/js/pages/dashboard-ecommerce.init.js') }}"></script>

	<!-- App js -->
	<script src="{{ asset('assets/js/app.js') }}"></script>

</body>

</html>