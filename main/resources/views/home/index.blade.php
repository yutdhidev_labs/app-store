<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none" data-preloader="disable">

<head>

	<meta charset="utf-8" />
	<title>{{ env('APP_NAME')}}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta content="Application Premium" name="description" />
	<meta content="{{ env('APP_NAME')}}" name="author" />
	<!-- App favicon -->
	<link rel="shortcut icon" href="{{ asset('dist/favicon.png') }}">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

	<!--Swiper slider css-->
	<link href="{{ asset('assets/libs/swiper/swiper-bundle.min.css') }}" rel="stylesheet" type="text/css" />

	<!-- Layout config Js -->
	<script src="{{ asset('assets/js/layout.js') }}"></script>
	<!-- Bootstrap Css -->
	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- Icons Css -->
	<link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- App Css-->
	<link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- custom Css-->
	<link href="{{ asset('assets/css/custom.min.css') }}" rel="stylesheet" type="text/css" />

     <link href="{{ asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

</head>

<body data-bs-spy="scroll" data-bs-target="#navbar-example">

	<!-- Begin page -->
	<div class="layout-wrapper landing">
		<nav class="navbar navbar-expand-lg navbar-landing fixed-top" id="navbar">
			<div class="container">
				<a class="navbar-brand" href="index.html">
					<img src="{{ asset('dist/logo-header.png') }}" class="card-logo card-logo-dark" alt="logo dark" width="100">
					<img src="{{ asset('dist/logo-header.png') }}" class="card-logo card-logo-light" alt="logo light" width="100">
				</a>
				<button class="navbar-toggler py-0 fs-20 text-body" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<i class="mdi mdi-menu"></i>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mx-auto mt-2 mt-lg-0" id="navbar-example">
						<li class="nav-item">
							<a class="nav-link active" href="{{ url('/')}}">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#services">Services</a>
						</li>

						<li class="nav-item">
							<a class="nav-link" href="#plans">Membership</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#reviews">Testimoni</a>
						</li>
						
						<li class="nav-item">
							<a class="nav-link" href="#contact">Hubungi Kami</a>
						</li>
					</ul>

					<div class="">
						@if(empty(Session::get('members_id')))
						<a href="{{ url('login')}}" class="btn btn-link fw-medium text-decoration-none text-dark">Login</a>
						<a href="{{ url('register')}}" class="btn btn-primary">Register</a>
						@else

							@if(Session::get('roles_id') != 4)
							<a href="{{ url('dashboard')}}" class="btn btn-primary"><i class=" ri-user-3-line  align-bottom me-1"></i> Panel Admin | {{ Session::get('name') }}</a>
							@elseif(Session::get('roles_id') == 4)
							<a href="{{ url('pg_dashboard')}}" class="btn btn-primary"><i class=" ri-user-3-line  align-bottom me-1"></i> {{ Session::get('name') }}</a>
							@endif

							<a href="{{ url('logout')}}" class="btn btn-danger">
								<i class=" ri-logout-box-r-line align-bottom me-1"></i>
							LOGOUT</a>

						@endif
					</div>
				</div>

			</div>
		</nav>
		<!-- end navbar -->
		<div class="vertical-overlay" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent.show"></div>

		<!-- start hero section -->
		<section class="section pb-0 hero-section" id="hero">
			<div class="bg-overlay bg-overlay-pattern"></div>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8 col-sm-10">
						<div class="text-center mt-lg-5 pt-5">
							<h1 class="display-6 fw-semibold mb-3 lh-base">Portal Kumpulan Aplikasi Bisnis dan<span class="text-success"> Sistem Informasi terlangkap</span></h1>
							<p class="lead text-muted lh-base">{{ env('APP_NAME')}} menyediakan berbagai sistem aplikasi terbaik untuk menunjang kebutuhan personal maupun bisnis anda  </p>

							<div class="d-flex gap-2 justify-content-center mt-4">
								<a href="#services" class="btn btn-primary">Get Started <i class="ri-arrow-right-line align-middle ms-1"></i></a>
								<!-- <a href="#contact" class="btn btn-danger">Request Aplikasi <i class=" ri-wechat-2-line align-middle ms-1"></i></a> -->
							</div>
						</div>

						<div class="mt-4 mt-sm-5 pt-sm-5 mb-sm-n5 demo-carousel">
							<div class="demo-img-patten-top d-none d-sm-block">
								<img src="assets/images/landing/img-pattern.png" class="d-block img-fluid" alt="...">
							</div>
							<div class="demo-img-patten-bottom d-none d-sm-block">
								<img src="assets/images/landing/img-pattern.png" class="d-block img-fluid" alt="...">
							</div>
							<div class="carousel slide carousel-fade" data-bs-ride="carousel">
								<div class="carousel-inner shadow-lg p-2 bg-white rounded">
									@php $no=1; @endphp
									@foreach($sliders as $val)
									<?php 
									if($no == 1){
										$act = 'active';
									}else{
										$act = '';
									}
									?>
									<div class="carousel-item {{ $act }}" data-bs-interval="2000">
										<img src="{{ asset('dist/img/sliders') }}/{{ $val->filename }}"" class="d-block w-100" alt="...">
									</div>
									@php $no++; @endphp
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- end row -->
			</div>
			<!-- end container -->
			<div class="position-absolute start-0 end-0 bottom-0 hero-shape-svg">
				<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1440 120">
					<g mask="url(&quot;#SvgjsMask1003&quot;)" fill="none">
						<path d="M 0,118 C 288,98.6 1152,40.4 1440,21L1440 140L0 140z">
						</path>
					</g>
				</svg>
			</div>
			<!-- end shape -->
		</section>
		<!-- end hero section -->


		<!-- start services -->
		<section class="section" id="services">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<div class="text-center mb-5">
							<h1 class="mb-3 ff-secondary fw-semibold lh-base">Fitur membership {{ env('APP_NAME') }}</h1>
							<p class="text-muted">Berbagai fitur yang didapatkan sebagai membership {{ env('APP_NAME') }}</p>
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->

				<div class="row g-3">
					@foreach($services as $val)
					<div class="col-lg-4">
						<div class="d-flex p-3">
							<div class="flex-shrink-0 me-3">
								<div class="avatar-sm icon-effect">
									<div class="avatar-title bg-transparent text-success rounded-circle">
										<i class="{{ $val->icon }} fs-36"></i>
									</div>
								</div>
							</div>
							<div class="flex-grow-1">
								<h5 class="fs-18">{{ $val->name }}</h5>
								<p class="text-muted my-3 ff-secondary">{{ $val->desc }}</p>
							</div>
						</div>
					</div>
					@endforeach

				</div>
				<!-- end row -->
			</div>
			<!-- end container -->
		</section>
		<!-- end services -->


		<!-- start plan -->
		<section class="section bg-light" id="plans">
			<div class="bg-overlay bg-overlay-pattern"></div>
			<div class="container">

				<div class="row justify-content-center mt-0">
					<div class="col-lg-5">
						<div class="text-center mb-4 pb-2">
							<h4 class="fw-semibold fs-22">Pilih paket membership Anda</h4>
							<p class="text-muted mb-4 fs-15">Harga terjangkau dan banyak fitur yang didapatkan untuk Anda.</p>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="card pricing-box text-center">
							<div class="row g-0">
								<div class="col-lg-6">
									<div class="card-body h-100">
										<div>
											<h5 class="mb-1">Reguler</h5>
											<p class="text-muted">Paket Reguler</p>
										</div>

										<div class="py-4">
											<h2><sup><small>Rp.</small></sup>0 <span class="fs-13 text-muted"> / Lifetime</span></h2>
										</div>

										<div class="text-center plan-btn mt-2">
											<a href="{{ url('register') }}" class="btn btn-success w-sm waves-effect waves-light">Join Members</a>
										</div>

									</div>
								</div>
								<!--end col-->
								<div class="col-lg-6">
									<div class="card-body border-start mt-4 mt-lg-0">
										<div class="card-header bg-light">
											<h5 class="fs-15 mb-0">Fitur Reguler :</h5>
										</div>
										<div class="card-body pb-0">
											<ul class="list-unstyled vstack gap-3 mb-0">
												<li>Account: <span class="text-success fw-semibold">Unlimited</span></li>
												<li>Aplikasi Gratis: <span class="text-success fw-semibold">Terbatas</span></li>
												<li>Support: <span class="text-success fw-semibold">Tidak</span></li>
												<li>Diskon Pembelian: <span class="text-success fw-semibold">Tidak</span></li>
											</ul>
										</div>
									</div>
								</div>
								<!--end col-->
							</div>
							<!--end row-->
						</div>
					</div>
					<!--end row-->

					<div class="col-lg-6">
						<div class="card pricing-box ribbon-box ribbon-fill text-center">
							<div class="ribbon ribbon-primary">BEST</div>
							<div class="row g-0">
								<div class="col-lg-6">
									<div class="card-body h-100">
										<div>
											<h5 class="mb-1">Premium</h5>
											<p class="text-muted">Paket Premium</p>
										</div>

										<div class="py-4">
			                                    <h2><sup><small>Rp.</small></sup>{{ number_format($premium->price,0,",",".") }}<span class="fs-13 text-muted">/ Lifetime</span></h2>
			                                </div>


										<div class="text-center plan-btn mt-2">
											<a href="{{ url('register') }}" class="btn btn-success w-sm waves-effect waves-light">Join Members</a>
										</div>
									</div>
								</div>
								<!--end col-->
								<div class="col-lg-6">
									<div class="card-body border-start mt-4 mt-lg-0">
										<div class="card-header bg-light">
											<h5 class="fs-15 mb-0">Fitur Premium :</h5>
										</div>
										<div class="card-body pb-0">
											<ul class="list-unstyled vstack gap-3 mb-0">
												<li>Account: <span class="text-success fw-semibold">Unlimited</span></li>
												<li>Aplikasi Gratis: <span class="text-success fw-semibold">Unlimited</span></li>
												<li>Support: <span class="text-success fw-semibold">Ya</span></li>
												<li>Diskon Pembelian: <span class="text-success fw-semibold">Ya</span></li>
											</ul>
										</div>
									</div>
								</div>
								<!--end col-->
							</div>
							<!--end row-->
						</div>
					</div>
					<!--end row-->
				</div>

			</div>
			<!-- end container -->
		</section>
		<!-- end plan -->

		<!-- start review -->
		<section class="section bg-primary" id="reviews">
			<div class="bg-overlay bg-overlay-pattern"></div>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-10">
						<div class="text-center">
							<div>
								<i class="ri-double-quotes-l text-success display-3"></i>
							</div>
							<h4 class="text-white mb-5"><span class="text-success">19k</span>+ Satisfied clients</h4>

							<!-- Swiper -->
							<div class="swiper client-review-swiper rounded" dir="ltr">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<div class="row justify-content-center">
											<div class="col-10">
												<div class="text-white-50">
													<p class="fs-20 ff-secondary mb-4">" I am givng 5 stars. Theme is great and everyone one stuff everything in theme. Future request should not affect current state of theme. "</p>

													<div>
														<h5 class="text-white">gregoriusus</h5>
														<p>- Skote User</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- end slide -->
									<div class="swiper-slide">
										<div class="row justify-content-center">
											<div class="col-10">
												<div class="text-white-50">
													<p class="fs-20 ff-secondary mb-4">" Awesome support. Had few issues while setting up because of my device, the support team helped me fix them up in a day. Everything looks clean and good. Highly recommended! "</p>

													<div>
														<h5 class="text-white">GeekyGreenOwl</h5>
														<p>- Skote User</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- end slide -->
									<div class="swiper-slide">
										<div class="row justify-content-center">
											<div class="col-10">
												<div class="text-white-50">
													<p class="fs-20 ff-secondary mb-4">" Amazing template, Redux store and components is nicely designed. It's a great start point for an admin based project. Clean Code and good documentation. Template is completely in React and absolutely no usage of jQuery "</p>

													<div>
														<h5 class="text-white">sreeks456</h5>
														<p>- Veltrix User</p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- end slide -->
								</div>
								<div class="swiper-button-next bg-white rounded-circle"></div>
								<div class="swiper-button-prev bg-white rounded-circle"></div>
								<div class="swiper-pagination position-relative mt-2"></div>
							</div>
							<!-- end slider -->
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->
			</div>
			<!-- end container -->
		</section>
		<!-- end review -->

		<!-- start counter -->
		<section class="py-5 position-relative bg-light">
			<div class="container">
				<div class="row text-center gy-4">
					<div class="col-lg-4 col-4">
						<div>
							<h2 class="mb-2"><span class="counter-value" data-target="{{ $project_done }}">0</span>+</h2>
							<div class="text-muted">Projects Completed</div>
						</div>
					</div>
					<!-- end col -->

					<div class="col-lg-4 col-4">
						<div>
							<h2 class="mb-2"><span class="counter-value" data-target="{{ $members_count }}">0</span>+</h2>
							<div class="text-muted">Members</div>
						</div>
					</div>
					<!-- end col -->

					<div class="col-lg-4 col-4">
						<div>
							<h2 class="mb-2"><span class="counter-value" data-target="{{ $app_count }}">0</span>+</h2>
							<div class="text-muted">Application</div>
						</div>
					</div>
				</div>
				<!-- end row -->
			</div>
			<!-- end container -->
		</section>
		<!-- end counter -->


		<!-- start contact -->
		<section class="section" id="contact">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<div class="text-center mb-5">
							<h3 class="mb-3 fw-semibold">Kontak Kami</h3>
							<p class="text-muted mb-4 ff-secondary">Untuk Informasi lebih lanjut terkait kritik, saran dan request aplikasi maupun ingin berkerja sama untuk membangun bisnis anda silahkan hubungi kami di dnegan mengisi form dibawah ini</p>
						</div>
					</div>
				</div>
				<!-- end row -->

				<div class="row gy-4">
					
					<div class="col-lg-12">
						<div>
							 <form method="POST" data-toggle="validator" id="form_data" id="form-login">
								<div class="row">
									<div class="col-lg-4">
										<div class="mb-4">
											<label for="name" class="form-label fs-13">Nama Lengkap <code>*</code></label>
											<input name="name" id="name" type="text" class="form-control bg-light border-light" placeholder="Masukkan Nama Lengkap" autocomplete="off" required>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="mb-4">
											<label for="name" class="form-label fs-13">Telepon / No WhatsApp <code>*</code></label>
											<input name="phone" id="phone" type="text" class="form-control bg-light border-light" placeholder="Masukkan No WhatsApp " required>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="mb-4">
											<label for="email" class="form-label fs-13">Alamat Email <code>*</code></label>
											<input name="email" id="email" type="email" class="form-control bg-light border-light" placeholder="Masukkan alamat email" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="mb-4">
											<label for="subject" class="form-label fs-13">Subjek <code>*</code></label>
											<input type="text" class="form-control bg-light border-light" id="subject" name="subject" placeholder="Masukkan subjek pesan" autocomplete="off" required />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="mb-3">
											<label for="comments" class="form-label fs-13">Pesan <code>*</code></label>
											<textarea name="desc" id="desc" rows="3" class="form-control bg-light border-light" placeholder="Ketik pesan disini ..." autocomplete="off" required></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 text-end">
										<button type="submit" id="submit" name="send" class="submitBnt btn btn-primary"><i class="ri-send-plane-fill align-bottom me-1"></i> Kirim Pesan</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- end row -->
			</div>
			<!-- end container -->
		</section>
		<!-- end contact -->

		<!-- start cta -->
		<section class="py-5 bg-primary position-relative">
			<div class="bg-overlay bg-overlay-pattern opacity-50"></div>
			<div class="container">
				<div class="row align-items-center gy-4">
					<div class="col-sm">
						<div>
							<h4 class="text-white mb-0 fw-semibold">Butuh Bantuan, Hubungi Kami sekarang !</h4>
						</div>
					</div>
					<!-- end col -->
					<div class="col-sm-auto">
						<div>
							<a href="#contact" class="btn bg-gradient btn-success"><i class="ri-customer-service-2-fill align-middle me-1"></i> Hubungi Kami</a>
						</div>
					</div>
					<!-- end col -->
				</div>
				<!-- end row -->
			</div>
			<!-- end container -->
		</section>
		<!-- end cta -->

		<!-- Start footer -->
		<footer class="custom-footer bg-dark py-5 position-relative">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 mt-6">
						<div>
							<div>
								<img src="{{ asset('dist/logo-header-white.png') }}" alt="logo light" height="70">
							</div>
							<div class="mt-4 fs-13">
								<p>Application Store Premium</p>
								<p class="ff-secondary"> Kumpulan Sistem informasi dan Aplikasi Berbasis Web terbaik </p>
							</div>
						</div>
					</div>

					<div class="col-lg-6 ms-lg-auto">
						<div class="row">
							<div class="col-sm-6 mt-6">
								<h5 class="text-white mb-0">Layanan</h5>
								<div class="text-muted mt-3">
									<ul class="list-unstyled ff-secondary footer-list">
										<li><a href="#services">Services</a></li>
										<li><a href="#plans">Membership</a></li>
										<li><a href="#reviews">Testimoni</a></li>
										<li><a href="{{ url('register') }}">Register</a></li>
									</ul>
								</div>
							</div>
							
							<div class="col-sm-6 mt-6">
								<h5 class="text-white mb-0">Hubungi Kami</h5>
								<div class="text-muted mt-3">
									<ul class="list-unstyled ff-secondary footer-list">
										<li>
											<a href="#contact"> 
												<i class="ri-customer-service-2-fill"></i> Kontak Kami
											</a>
										</li>

										<li>
											<a href="http://facebook.com"> 
												<i class="ri-facebook-fill"></i> Facebook
											</a>
										</li>

										<li>
											<a href="http://instagram.com"> 
												<i class="ri-instagram-fill"></i> Instagram
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="row text-center text-sm-start align-items-center mt-5">
					<div class="col-sm-6">

						<div>
							<p class="copy-rights mb-0">
								<script> document.write(new Date().getFullYear()) </script> © {{ env('APP_NAME')}} - All rights reserved.
							</p>
						</div>
					</div>
					
				</div>
			</div>
		</footer>
		<!-- end footer -->


		<!--start back-to-top-->
		<button onclick="topFunction()" class="btn btn-danger btn-icon landing-back-top" id="back-to-top">
			<i class="ri-arrow-up-line"></i>
		</button>
		<!--end back-to-top-->

	</div>
	<!-- end layout wrapper -->


	<!-- JAVASCRIPT -->
	  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
	<script src="{{ asset('assets/libs/node-waves/waves.min.js') }}"></script>
	<script src="{{ asset('assets/libs/feather-icons/feather.min.js') }}"></script>
	<script src="{{ asset('assets/js/pages/plugins/lord-icon-2.1.0.js') }}"></script>
	<script src="{{ asset('assets/js/plugins.js') }}"></script>

	<!--Swiper slider js-->
	<script src="{{ asset('assets/libs/swiper/swiper-bundle.min.js') }}"></script>

	<!-- landing init -->
	<script src="{{ asset('assets/js/pages/landing.init.js') }}"></script>
	<script type="text/javascript">
		
 $(function(){
        $("#form_data").submit(function(stay){
          stay.preventDefault(); 
           var formdata = $(this).serialize(); 
           // var fileData  = $('input[name=file]').val();
            $.ajax({
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: '{{ url("pg_contacts/action")}}',
                dataType : 'JSON',
                data: formdata,
                success: function (data) {
                    if(data.status == 'OK'){
                         Swal.fire(data.title, data.msg, "success");
                    }else{
                         Swal.fire(data.title, data.msg, "error");
                    }
                  setTimeout(() => {
                            window.location.reload();
                        }, 2000);
                },
            });
            stay.preventDefault(); 
        });
       });
	</script>
</body>

</html>