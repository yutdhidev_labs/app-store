@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Tiket</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Tiket</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row" id="form_input" style="display: none">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-4">Buat Tiket Baru</h4>
                                          <form action="{{ url('ticket/action') }}" method="post" enctype="multipart/form-data">
                                      <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="id_client" id="id_client">
                                            <div class="row mb-4">
                                                <label for="projectname" class="col-form-label col-lg-2">Subject</label>
                                                <div class="col-lg-10">
                                                    <input id="subject" name="subject" type="text" class="form-control" placeholder="Judul Tiket">
                                                </div>
                                            </div>

                                             <div class="row mb-4">
                                                <label for="projectdesc" class="col-form-label col-lg-2">Category</label>
                                                <div class="col-lg-10">
                                                     <select class="form-control" style="width: 100%" required name="category_id" id="category_id">
                                                        <option value="">=== Pilih Data ===</option>
                                                        @foreach($category as $val)
                                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="row mb-4">
                                                <label for="desc" class="col-form-label col-lg-2">Description</label>
                                                <div class="col-lg-10">
                                                    <textarea class="form-control" name="desc" id="desc" rows="3" placeholder="Deskripsi..."></textarea>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <label for="projectdesc" class="col-form-label col-lg-2">Report By</label>
                                                <div class="col-lg-10">
                                                     <input id="report_by_name" name="report_by_name" type="text" class="form-control" value="{{ Session::get('department_name') }}" readonly>
                                                     <input id="report_by" name="report_by" type="hidden" class="form-control" value="{{ Session::get('department_id') }}">
                                                </div>
                                            </div>

                                             <div class="row mb-4">
                                                <label for="projectdesc" class="col-form-label col-lg-2">Assign By</label>
                                                <div class="col-lg-10">
                                                     <select class="form-control" style="width: 100%" required name="assign_by" id="assign_by">
                                                        <option value="">=== Pilih Data ===</option>
                                                        @foreach($department as $val)
                                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <label class="col-form-label col-lg-2">Ticket Date</label>
                                                <div class="col-lg-10">
                                                    
                                                        <input type="text" class="form-control" name="ticket_date" value="{{ date('Y-m-d')}}" readonly>
                                                </div>
                                            </div>

                                            <div class="row mb-4">
                                                <label for="projectbudget" class="col-form-label col-lg-2">Attached Files</label>
                                                <div class="col-lg-10">
                                                    <input id="file" name="file" type="file" class="form-control">
                                                </div>
                                            </div>
                                      
                                        <div class="row justify-content-end">
                                            <div class="col-lg-10">
                                                <button type="submit" class="btn btn-primary">Create Ticket</button>
                                            </div>
                                        </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
</div>
<!-- end col -->
<div class="row">
     
    <div class="col-lg-3">
        <div class="card mini-stats-wid">
            <div class="card-body">
                
                <div class="d-flex flex-wrap">
                    <div class="me-3">
                        <p class="text-muted mb-2" >Total Tiket</p>
                        <h5 class="mb-0">{{ $total_all }}</h5>
                    </div>
                </div>

            </div>
        </div>
    </div>


     <div class="col-lg-2">
        <div class="card mini-stats-wid">
            <div class="card-body">
                
                <div class="d-flex flex-wrap">
                    <div class="me-3">
                        <p class="text-muted mb-2">Waiting</p>
                        <h5 class="mb-0">{{ $total_waiting }}</h5>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-lg-2">
        <div class="card mini-stats-wid">
            <div class="card-body">
                
                <div class="d-flex flex-wrap">
                    <div class="me-3">
                        <p class="text-muted mb-2">Progress</p>
                        <h5 class="mb-0" style="color:#50a5f1">{{ $total_progress }}</h5>
                    </div>
                </div>

            </div>
        </div>
    </div>

     <div class="col-lg-2">
        <div class="card mini-stats-wid">
            <div class="card-body">
                
                <div class="d-flex flex-wrap">
                    <div class="me-3">
                        <p class="text-muted mb-2">Pending / Hold</p>
                        <h5 class="mb-0" style="color:red">{{ $total_hold }}</h5>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="card mini-stats-wid">
            <div class="card-body">
                
                <div class="d-flex flex-wrap">
                    <div class="me-3">
                        <p class="text-muted mb-2">Close</p>
                        <h5 class="mb-0" style="color:green">{{ $total_close }}</h5>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
</div>

<div class="row" id="list_data">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-10">
                          <h4 class="card-title">Tiket</h4>
                            <p class="card-title-desc">Master Data</p>
                    </div>
                    <div class="col-md-2">

                        @if(Session::get('roles_id') == 1)
                         <button type="button" class="btn btn-sm btn-info" onclick="showForm()"><i class="fa fa-plus-circle"></i> Tambah</button>
                        @endif
                        <button type="button" class="btn btn-sm btn-warning" onClick="window.location.href=window.location.href" title="Refresh"><i class=" fas fa-sync-alt"></i> Reload </button>

                    </div>
                </div>
                
               <table id="datatable" class="table table-bordered dt-responsive  w-100" >
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Subject</th>
                            <th>Report By</th>
                            <th>Assign By</th>
                            <th>Teknisi</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @php $no=1; @endphp
                        @foreach($ticket as $val)
                        <tr>
                            <td width="5%" align="center">{{ $no }}</td>
                            <td width="30%">
                                {{ $val->subject }} <br>
                                <span _ngcontent-nfd-c229="" style="color:#fff" class="badge rounded-pill bg-success">{{ $val->category_name }}</span>
                                
                            </td>
                            <td width="15%">{{ $val->report_by_name }}</td>
                            <td width="15%">{{ $val->assign_by_name }}</td>
                            <td width="10%">{{ $val->teknisi_name }}</td>
                            <td width="10%">
                                @if($val->ticket_status == 'Waiting')
                                <button type="button" class="btn btn-warning waves-effect btn-label btn-sm waves-light"><i class="bx bx-loader bx-spin label-icon "></i> Waiting</button>
                                @elseif($val->ticket_status == 'Progress')
                                <button type="button" class="btn btn-light waves-effect btn-label btn-sm waves-light"><i class="bx bx-hourglass bx-spin label-icon "></i> Progress</button>
                                @elseif($val->ticket_status == 'Pending')
                                <button type="button" class="btn btn-secondary waves-effect btn-label btn-sm waves-light"><i class="bx bx-hourglass bx-spin label-icon "></i> Pending</button>
                                @elseif($val->ticket_status == 'Close')
                                <button type="button" class="btn btn-success waves-effect btn-label btn-sm waves-light"><i class="bx bx-check-double  label-icon"></i> Close</button>
                                @endif

                            </td>
                            <td width="25%" align="left">
                                <a href="{{ url('detail') }}/{{ $val->uuid }}">
                                <button class="btn btn-info btn-sm" style="margin-bottom: 5px"><i class="fa fa-eye"></i> Detail </button> 
                             </a>

                                @if(Session::get('roles_id') == 1)
                                    @if($val->ticket_status == 'Waiting')
                                        <button class="btn btn-danger btn-sm" onclick='deleteData("{{ $val->id }}")'><i class="fa fa-trash"></i> Hapus </button> 
                                    @endif
                                @endif


                                @if(Session::get('roles_id') != 1)
                                    @if($val->ticket_status == 'Waiting')

                                    <button class="btn btn-primary btn-sm" onclick='OpenData("{{ $val->id }}")'><i class="bx bx-check"></i> Open Tiket</button>

                                    @elseif($val->ticket_status == 'Progress' || $val->ticket_status == 'Pending')
                                     @if($val->teknisi_id == Session::get('members_id') || Session::get('roles_id') == 2)

                                     @if($val->ticket_status == 'Progress')

                                     <button class="btn btn-secondary btn-sm" onclick='PendingData("{{ $val->id }}")' style="margin-bottom: 5px"><i class="bx bx-save"></i> Pending</button>

                                     <br>
                                     @endif

                                       <button class="btn btn-danger btn-sm" onclick='CloseData("{{ $val->id }}")'><i class="bx bx-check-double"></i> Close Now</button>
                                     @endif

                                    @elseif($val->ticket_status == 'Close')
                                      
                                    @endif
                                 @endif


                               
                               
                            </td>
                        </tr>
                        @php $no++; @endphp
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->


@section('page-script')

<script type="text/javascript">

  function showForm(){
    $("#form_input").fadeIn();
    $("#list_data").hide();
  }

  function showData(){
    $("#form_input").hide();
    $("#list_data").fadeIn();
  }

    function deleteData(id){

         Swal.fire({
            title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
          }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{ url("ticket/delete")}}',
                    enctype: "multipart/form-data",
                    dataType : 'JSON',
                    data: 'id='+id,
                    success: function (data) {

                       Swal.fire(data.title, data.msg, "success");
                       window.location.reload();
                    },
                });
                stay.preventDefault(); 
            }
        });
    }

    function OpenData(id){

         Swal.fire({
            title: "Are you sure?",
                text: "You will not be able to open ticket!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, open ticket it!",
                closeOnConfirm: false
          }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{ url("ticket/open")}}',
                    enctype: "multipart/form-data",
                    dataType : 'JSON',
                    data: 'id='+id,
                    success: function (data) {

                       Swal.fire(data.title, data.msg, "success");
                       window.location.reload();
                    },
                });
                stay.preventDefault(); 
            }
        });
    }

    function CloseData(id){

         Swal.fire({
            title: "Are you sure?",
                text: "You will not be able to close ticket!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, close ticket it!",
                closeOnConfirm: false
          }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{ url("ticket/close")}}',
                    enctype: "multipart/form-data",
                    dataType : 'JSON',
                    data: 'id='+id,
                    success: function (data) {

                       Swal.fire(data.title, data.msg, "success");
                       window.location.reload();
                    },
                });
                stay.preventDefault(); 
            }
        });
    }


    function PendingData(id){

         Swal.fire({
            title: "Are you sure?",
                text: "You will not be able to Pending / Hold ticket!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Pending / Hold ticket it!",
                closeOnConfirm: false
          }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: '{{ url("ticket/pending")}}',
                    enctype: "multipart/form-data",
                    dataType : 'JSON',
                    data: 'id='+id,
                    success: function (data) {

                       Swal.fire(data.title, data.msg, "success");
                       window.location.reload();
                    },
                });
                stay.preventDefault(); 
            }
        });
    }

</script>
@stop
@endsection