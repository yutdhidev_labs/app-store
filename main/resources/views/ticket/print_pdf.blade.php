<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <style type="text/css">
    	body{
    		font-family: Arial, Helvetica, sans-serif;
    	}

    </style>

    <style>
            /** Define the margins of your page **/
            @page {
                margin: 100px 25px;
            }

            header {
                font-size:12px;
                position: fixed;
                top: -80px;
                left: 0px;
                right: 0px;
                height: 0px;
                text-align: left;
            }

            footer {
                border-top: 1px solid;
                font-size:12px;
                position: fixed; 
                bottom: -60px; 
                left: 0px; 
                right: 0px;
                height: 10px; 
                text-align: left;
                line-height: 35px;
            }
        </style>
</head>
<body>

        <header>
           <span style="font-weight:bold; font-size: 14px">{{ $abouts->name }}</span> 
        </header>

        <footer>
            Copyright &copy; <?php echo date("Y");?> Helpdesk Ticketing System
        </footer>

      <main>   
            <center>
            	<label style="font-weight: bold; font-size: 20px">{{ $header_label }}</label> 
            </center>

            <table id="datatable" border="1" cellpadding="2" style="font-size: 12px; width: 100%; margin-top: 20px; border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th style="background-color: #000; color:#fff">No</th>
                            <th style="background-color: #000; color:#fff">Kode Tiket</th>
                            <th style="background-color: #000; color:#fff">Tanggal</th>
                            <th style="background-color: #000; color:#fff">Subject</th>
                            <th style="background-color: #000; color:#fff">Kategori</th>
                            <th style="background-color: #000; color:#fff">Report By</th>
                            <th style="background-color: #000; color:#fff">Report Department</th>
                            <th style="background-color: #000; color:#fff">Assign Department</th>
                            <th style="background-color: #000; color:#fff">Teknisi</th>
                            <th style="background-color: #000; color:#fff">Status</th>
                        </tr>
                    </thead>

                    <tbody id="load_data">

                        @php $no=1; @endphp
                        @foreach($ticket as $val)
                        <tr>
                            <td width="5%" align="center">{{ $no }}</td>
                            <td width="" align="center">{{ $val->ticket_code }}</td>
                            <td width="">{{ $val->ticket_date }}</td>
                            <td width="">{{ $val->subject }}</td>
                            <td width="">{{ $val->category_name }}</td>
                            <td width="">{{ $val->name }}</td>
                            <td width="">{{ $val->report_by_name }}</td>
                            <td width="">{{ $val->assign_by_name }}</td>
                            <td width="">{{ $val->teknisi_name }}</td>
                            <td width="" align="center">{{ $val->ticket_status }}</td>
                        </tr>
                        @php $no++; @endphp
                        @endforeach
                        
                    </tbody>
                </table>
        </main>

</body>
</html>