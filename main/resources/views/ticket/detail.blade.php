@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Tiket Detail</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Tiket Detail</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <div class="d-flex">
                   
                    <div class="flex-grow-1 overflow-hidden">
                        <h5 class="text-truncate font-size-15">{{ $ticket->subject }} ( {{ $ticket->ticket_code }} )</h5>
                        <p class="text-muted">{{ $ticket->category_name }}</p>
                    </div>
                </div>

                <h5 class="font-size-15 mt-4">Ticket Details :</h5>

                <p class="text-muted">{{ $ticket->desc }}</p>

                 <h5 class="font-size-15 mt-4">Status Ticket</h5>
                                    @if($ticket->ticket_status == 'Pending')
                                        <button type="button" class="btn btn-warning waves-effect btn-label btn-sm waves-light"><i class="bx bx-loader bx-spin label-icon "></i> Pending</button>
                                        @elseif($ticket->ticket_status == 'Progress')
                                        <button type="button" class="btn btn-light waves-effect btn-label btn-sm waves-light"><i class="bx bx-hourglass bx-spin label-icon "></i> Progress</button>
                                        @elseif($ticket->ticket_status == 'Close')
                                        <button type="button" class="btn btn-success waves-effect btn-label btn-sm waves-light"><i class="bx bx-check-double  label-icon"></i> Close</button>
                                        @endif


                <div class="row task-dates">
                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> Create Ticket</h5>
                            <p class="text-muted mb-0">{{ date('Y-m-d', strtotime($ticket->ticket_date)) }}</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> Open Ticket</h5>
                            <p class="text-muted mb-0">
                                @if(!empty($ticket->open_date))
                                {{ date('Y-m-d', strtotime($ticket->open_date)) }}
                                @else
                                -
                                @endif
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-6">
                        <div class="mt-4">
                            <h5 class="font-size-14"><i class="bx bx-calendar me-1 text-primary"></i> Close Ticket</h5>
                            <p class="text-muted mb-0">
                                @if(!empty($ticket->open_date))
                                {{ date('Y-m-d', strtotime($ticket->close_date)) }}
                                @else
                                -
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->

    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
               

                <div class="table-responsive">
                    <table class="table align-middle table-nowrap">
                        <tbody>
                            <tr>
                                <td colspan="2">Report By</td>
                            </tr>
                            <tr>
                                <td style="width: 50px;">
                                    <img src="{{ asset('assets/user.jpg') }}" class="rounded-circle avatar-xs" alt=""></td>
                                <td>
                                    <h5 class="font-size-14 m-0">
                                        <a href="javascript: void(0);" class="text-dark">{{ $ticket->name }}</a> <br>
                                        <span style="color:#74788d">
                                            <?php 
                                            $department_user = DB::table('department')->where('status_id',1)->where('id',$ticket->department_user)->first();
                                            echo $department_user->name;
                                            ?>
                                        </span>
                                    </h5>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>


                <div class="table-responsive">
                    <table class="table align-middle table-nowrap">
                        <tbody>
                             <tr>
                                <td colspan="2">Teknisi</td>
                            </tr>
                            <tr>
                                <td style="width: 50px;">
                                    <img src="{{ asset('assets/user.jpg') }}" class="rounded-circle avatar-xs" alt=""></td>
                                <td>
                                    <h5 class="font-size-14 m-0">
                                        @if(!empty($ticket->teknisi_name))
                                             <a href="javascript: void(0);" class="text-dark">{{ $ticket->teknisi_name }}</a> <br>
                                        <span style="color:#74788d">
                                            <?php 
                                            $department_user = DB::table('department')->where('status_id',1)->where('id',$ticket->department_teknisi)->first();
                                            echo $department_user->name;
                                            ?>
                                        </span>
                                        @else
                                            -
                                        @endif
                                    </h5>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <!-- end col -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                 @if($ticket->ticket_status == 'Progress')
                 <button type="button" class="btn btn-sm btn-info" onclick="showForm()"><i class="fa fa-plus-circle"></i> Tambah Komentar</button>
                 @endif

<div class="row" id="form_input" style="display: none">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title mb-4">Form Komentar</h4>
                  <form action="{{ url('ticket_detail/action') }}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id_ticket" id="id_ticket" value="{{ $ticket->id }}">
                <input type="hidden" name="ticket_uuid" id="ticket_uuid" value="{{ $ticket->uuid }}">

                    <div class="row mb-4">
                        <label for="desc" class="col-form-label col-lg-2">Description</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" name="desc" id="desc" rows="3" placeholder="Deskripsi..." required></textarea>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <label for="projectbudget" class="col-form-label col-lg-2">Attached Files</label>
                        <div class="col-lg-10">
                            <input id="file" name="file" type="file" class="form-control">
                        </div>
                    </div>
              
                <div class="row justify-content-end">
                    <div class="col-lg-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-danger" onclick="cancelData()">Batalkan</button>
                    </div>
                </div>

                </form>

            </div>
        </div>
    </div>
</div>

        <hr>


                <h5 class="mb-3">Activity History</h5>
                <ul class="verti-timeline list-unstyled">
                    @foreach($ticket_detail as $val)
                    <li class="event-list">
                        <div class="event-timeline-dot">
                            <i class="bx bx-right-arrow-circle"></i>
                        </div>
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <div>
                                    <h6 class="font-size-14 mb-1">{{ $val->name }}</h6>
                                    <p class="text-muted">{{ $val->department_name }}</p>
                                    
                                    <p class="text-muted mb-1">
                                      {{ date('d F Y H:i', strtotime($val->created_at)) }} 
                                    </p> 

                                    <p class="text-muted mb-2">
                                       {{ $val->desc }}
                                    </p>

                                    <?php 
                                        $info = new SplFileInfo($val->filename);
                                         $format = $info->getExtension();
                                         if($format == 'jpg' ||$format == 'jpeg' || $format == 'png' || $format == 'svg'){
                                             $icon = 'mdi-image';
                                             $color_file = 'text-success';
                                        }else if($format == 'doc'){
                                             $icon = 'mdi-file-word';
                                              $color_file = 'text-primary';
                                        }else if($format == 'xlsx'){
                                             $icon = 'mdi-file-excel';
                                              $color_file = 'text-success';
                                        }else if($format == 'pdf'){
                                             $icon = 'mdi-file-pdf';
                                              $color_file = 'text-danger';
                                        }else if($format == 'zip'){
                                             $icon = 'mdi-folder-zip-outline';
                                              $color_file = 'text-primary';
                                        }else if($format == 'mp3' || $format == 'wav'){
                                             $icon = 'mdi-file-music';
                                              $color_file = 'text-primary';
                                        }else if($format == 'mp4'){
                                              $icon = 'mdi-file-video';
                                              $color_file = 'text-primary';
                                        }else{
                                            $icon = 'mdi-file-document';
                                              $color_file = 'text-primary';
                                         }

                                          $uri = asset('dist/img/ticket')."/".$val->filename."";
                                          $filename = $val->filename;
                                        ?>

                                    <div class="card border shadow-none mb-2">
                                            <a href="javascript: void(0);" onclick="downloadURI('{{ $uri }}', '{{ $filename }}', '{{ $val->id }}');" class="text-body">
                                                <div class="p-2">
                                                    <div class="d-flex">
                                                        <div class="avatar-xs align-self-center me-2">
                                                            <div class="avatar-title rounded bg-transparent {{ $color_file }} font-size-20">
                                                                <i class="mdi {{ $icon}}"></i>
                                                            </div>
                                                        </div>
    
                                                        <div class="overflow-hidden me-auto">
                                                            <h5 class="font-size-13 text-truncate mb-1">{{ $val->filename }}</h5>
                                                            <p class="text-muted text-truncate mb-0">
                                                             <small>Size : 3.25 MB</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                    <p class="text-muted mb-0">
                                       <i>{{ Carbon\Carbon::parse($val->created_at)->diffForHumans()}}</i>
                                    </p>
                
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    
                </ul>
            </div>
        </div>
    </div>
    </div>


@section('page-script')
<script type="text/javascript">
    function showForm(){
    $("#form_input").fadeIn();
  }


  function cancelData(){
    $("#form_input").hide();
  }
</script>
@stop
@endsection