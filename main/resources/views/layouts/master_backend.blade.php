<!doctype html>  
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none" data-preloader="disable">

<head>

    <meta charset="utf-8" />
    <title>Dashboard | {{ env('APP_NAME')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Application Premium" name="description" />
    <meta content="{{ env('APP_NAME')}}" name="author" />
    
  <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('dist/favicon.png') }}">

     <link rel="stylesheet" href="{{ asset('assets/libs/@simonwep/pickr/themes/classic.min.css') }}" /> <!-- 'classic' theme -->
    <link rel="stylesheet" href="{{ asset('assets/libs/@simonwep/pickr/themes/monolith.min.css') }}" /> <!-- 'monolith' theme -->
    <link rel="stylesheet" href="{{ asset('assets/libs/@simonwep/pickr/themes/nano.min.css') }}" /> <!-- 'nano' theme -->


    <link href="{{ asset('assets/libs/swiper/swiper-bundle.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- jsvectormap css -->
    <link href="{{ asset('assets/libs/jsvectormap/css/jsvectormap.min.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

       <!-- nouisliderribute css -->
    <link rel="stylesheet" href="{{ asset('assets/libs/nouislider/nouislider.min.css') }}">

    <!-- gridjs css -->
    <link rel="stylesheet" href="{{ asset('assets/libs/gridjs/theme/mermaid.min.css') }}">

    <!--Swiper slider css-->
    <link href="{{ asset('assets/libs/swiper/swiper-bundle.min.css') }}" rel="stylesheet" type="text/css" />
      <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


    <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />


      <!-- Layout config Js -->
    <script src="{{ asset('assets/js/layout.js') }}"></script>
    <!-- Bootstrap Css -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- custom Css-->
    <link href="{{ asset('assets/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

    <link href="{{ asset('assets/libs/dropzone/dropzone.css') }}" rel="stylesheet" type="text/css" />

</head>

<body>

    <!-- Begin page -->
    <div id="layout-wrapper">

        <header id="page-topbar">
          <div class="layout-width">
              <div class="navbar-header">
                  <div class="d-flex">
                      <!-- LOGO -->
                      <div class="navbar-brand-box horizontal-logo">
                          <a href="javascript:void(0)" class="logo logo-dark">
                              <span class="logo-sm">
                                  <img src="{{ asset('dist/logo-white.png') }}" alt="" height="22">
                              </span>
                              <span class="logo-lg">
                                  <img src="{{ asset('dist/logo-header-white.png') }}" alt="" height="17">
                              </span>
                          </a>

                          <a href="javascript:void(0)" class="logo logo-light">
                              <span class="logo-sm">
                                  <img src="{{ asset('dist/logo-white.png') }}" alt="" height="22">
                              </span>
                              <span class="logo-lg">
                                  <img src="{{ asset('dist/logo-header-white.png') }}" alt="" height="17">
                              </span>
                          </a>
                      </div>

                      <button type="button" class="btn btn-sm px-3 fs-16 header-item vertical-menu-btn topnav-hamburger" id="topnav-hamburger-icon">
                          <span class="hamburger-icon">
                              <span></span>
                              <span></span>
                              <span></span>
                          </span>
                      </button>

                      
                  </div>

                  <div class="d-flex align-items-center">

                      <!-- <div class="dropdown topbar-head-dropdown ms-1 header-item">
                          <button type="button" class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle" id="page-header-cart-dropdown" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false">
                              <i class='bx bx-shopping-bag fs-22'></i>
                              <span class="position-absolute topbar-badge cartitem-badge fs-10 translate-middle badge rounded-pill bg-info">5</span>
                          </button>
                          <div class="dropdown-menu dropdown-menu-xl dropdown-menu-end p-0 dropdown-menu-cart" aria-labelledby="page-header-cart-dropdown">
                              <div class="p-3 border-top-0 border-start-0 border-end-0 border-dashed border">
                                  <div class="row align-items-center">
                                      <div class="col">
                                          <h6 class="m-0 fs-16 fw-semibold"> My Cart</h6>
                                      </div>
                                      <div class="col-auto">
                                          <span class="badge badge-soft-warning fs-13"><span class="cartitem-badge">7</span>
                                              items</span>
                                      </div>
                                  </div>
                              </div>
                              <div data-simplebar style="max-height: 300px;">
                                  <div class="p-2">
                                      <div class="text-center empty-cart" id="empty-cart">
                                          <div class="avatar-md mx-auto my-3">
                                              <div class="avatar-title bg-soft-info text-info fs-36 rounded-circle">
                                                  <i class='bx bx-cart'></i>
                                              </div>
                                          </div>
                                          <h5 class="mb-3">Your Cart is Empty!</h5>
                                          <a href="#" class="btn btn-success w-md mb-3">Order Now</a>
                                      </div>
                                      @for($i=0; $i<3; $i++)
                                      <div class="d-block dropdown-item dropdown-item-cart text-wrap px-3 py-2">
                                          <div class="d-flex align-items-center">
                                              <img src="{{ asset('assets/images/products/img-1.png') }}" class="me-3 rounded-circle avatar-sm p-2 bg-light" alt="user-pic">
                                              <div class="flex-1">
                                                  <h6 class="mt-0 mb-1 fs-14">
                                                      <a href="#" class="text-reset">
                                                        Sistem Penunjang Keputusan Weight Product
                                                      </a>
                                                  </h6>
                                                  <p class="mb-0 fs-12 text-muted">
                                                      Laravel, PHP, Algoritma
                                                  </p>
                                              </div>
                                              <div class="px-2">
                                                  <h5 class="m-0 fw-normal">Rp.<span class="cart-item-price">500.000</span></h5>
                                              </div>
                                              <div class="ps-2">
                                                  <button type="button" class="btn btn-icon btn-sm btn-ghost-secondary remove-item-btn"><i class="ri-close-fill fs-16"></i></button>
                                              </div>
                                          </div>
                                      </div>
                                      @endfor

                                  </div>
                              </div>
                              <div class="p-3 border-bottom-0 border-start-0 border-end-0 border-dashed border" id="checkout-elem">
                                  <div class="d-flex justify-content-between align-items-center pb-3">
                                      <h5 class="m-0 text-muted">Total:</h5>
                                      <div class="px-2">
                                          <h5 class="m-0" id="cart-item-total-all">Rp. 500.000</h5>
                                      </div>
                                  </div>

                                  <a href="#" class="btn btn-success text-center w-100">
                                      Checkout
                                  </a>
                              </div>
                          </div>
                      </div> -->

                      <div class="ms-1 header-item d-none d-sm-flex">
                          <button type="button" class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle" data-toggle="fullscreen">
                              <i class='bx bx-fullscreen fs-22'></i>
                          </button>
                      </div>

                      <div class="ms-1 header-item d-none d-sm-flex">
                          <button type="button" class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle light-dark-mode">
                              <i class='bx bx-moon fs-22'></i>
                          </button>
                      </div>

                     <!--  <div class="dropdown topbar-head-dropdown ms-1 header-item" id="notificationDropdown">
                          <button type="button" class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle" id="page-header-notifications-dropdown" data-bs-toggle="dropdown" data-bs-auto-close="outside" aria-haspopup="true" aria-expanded="false">
                              <i class='bx bx-bell fs-22'></i>
                              <span class="position-absolute topbar-badge fs-10 translate-middle badge rounded-pill bg-danger">3<span class="visually-hidden">unread messages</span></span>
                          </button>
                          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0" aria-labelledby="page-header-notifications-dropdown">

                              <div class="dropdown-head bg-primary bg-pattern rounded-top">
                                  <div class="p-3">
                                      <div class="row align-items-center">
                                          <div class="col">
                                              <h6 class="m-0 fs-16 fw-semibold text-white"> Notifications </h6>
                                          </div>
                                          <div class="col-auto dropdown-tabs">
                                              <span class="badge badge-soft-light fs-13"> 4 New</span>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="px-2 pt-2">
                                      <ul class="nav nav-tabs dropdown-tabs nav-tabs-custom" data-dropdown-tabs="true" id="notificationItemsTab" role="tablist">
                                          <li class="nav-item waves-effect waves-light">
                                              <a class="nav-link active" data-bs-toggle="tab" href="#all-noti-tab" role="tab" aria-selected="true">
                                                  Notification (4)
                                              </a>
                                          </li>
                                          <li class="nav-item waves-effect waves-light">
                                              <a class="nav-link" data-bs-toggle="tab" href="#messages-tab" role="tab" aria-selected="false">
                                                  Messages
                                              </a>
                                          </li>
                                      </ul>
                                  </div>

                              </div>

                              <div class="tab-content position-relative" id="notificationItemsTabContent">
                                  <div class="tab-pane fade show active py-2 ps-2" id="all-noti-tab" role="tabpanel">
                                      <div data-simplebar style="max-height: 300px;" class="pe-2">
                                        @for($i=1; $i<5; $i++)
                                          <div class="text-reset notification-item d-block dropdown-item position-relative">
                                              <div class="d-flex">
                                                  <div class="avatar-xs me-3">
                                                      <span class="avatar-title bg-soft-info text-info rounded-circle fs-16">
                                                          <i class="bx bx-badge-check"></i>
                                                      </span>
                                                  </div>
                                                  <div class="flex-1">
                                                      <a href="#!" class="stretched-link">
                                                          <h6 class="mt-0 mb-2 lh-base">Your <b>Elite</b> author Graphic
                                                              Optimization <span class="text-secondary">reward</span> is
                                                              ready!
                                                          </h6>
                                                      </a>
                                                      <p class="mb-0 fs-11 fw-medium text-uppercase text-muted">
                                                          <span><i class="mdi mdi-clock-outline"></i> Just 30 sec ago</span>
                                                      </p>
                                                  </div>
                                                  <div class="px-2 fs-15">
                                                      <div class="form-check notification-check">
                                                          <input class="form-check-input" type="checkbox" value="" id="all-notification-check01">
                                                          <label class="form-check-label" for="all-notification-check01"></label>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          @endfor

                                          <div class="my-3 text-center view-all">
                                              <button type="button" class="btn btn-soft-success waves-effect waves-light">View
                                                  All Notifications <i class="ri-arrow-right-line align-middle"></i></button>
                                          </div>
                                      </div>

                                  </div>

                                  <div class="tab-pane fade py-2 ps-2" id="messages-tab" role="tabpanel" aria-labelledby="messages-tab">
                                      <div data-simplebar style="max-height: 300px;" class="pe-2">
                                         @for($i=1; $i<5; $i++)
                                          <div class="text-reset notification-item d-block dropdown-item">
                                              <div class="d-flex">
                                                  <img src="{{ asset('assets/images/users/avatar-3.jpg') }}" class="me-3 rounded-circle avatar-xs" alt="user-pic">
                                                  <div class="flex-1">
                                                      <a href="#!" class="stretched-link">
                                                          <h6 class="mt-0 mb-1 fs-13 fw-semibold">James Lemire</h6>
                                                      </a>
                                                      <div class="fs-13 text-muted">
                                                          <p class="mb-1">We talked about a project on linkedin.</p>
                                                      </div>
                                                      <p class="mb-0 fs-11 fw-medium text-uppercase text-muted">
                                                          <span><i class="mdi mdi-clock-outline"></i> 30 min ago</span>
                                                      </p>
                                                  </div>
                                                  <div class="px-2 fs-15">
                                                      <div class="form-check notification-check">
                                                          <input class="form-check-input" type="checkbox" value="" id="messages-notification-check01">
                                                          <label class="form-check-label" for="messages-notification-check01"></label>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          @endfor

                                          <div class="my-3 text-center view-all">
                                              <button type="button" class="btn btn-soft-success waves-effect waves-light">View
                                                  All Messages <i class="ri-arrow-right-line align-middle"></i></button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div> -->

                      <div class="dropdown ms-sm-3 header-item topbar-user">
                          <button type="button" class="btn" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="d-flex align-items-center">
                                  <img class="rounded-circle header-profile-user" src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="Header Avatar">
                                  <span class="text-start ms-xl-2">
                                      <span class="d-none d-xl-inline-block ms-1 fw-medium user-name-text">{{ Session('name') }}</span>
                                      <span class="d-none d-xl-block ms-1 fs-12 text-muted user-name-sub-text">{{ Session('roles_name') }}</span>
                                  </span>
                              </span>
                          </button>
                          <div class="dropdown-menu dropdown-menu-end">
                              <!-- item-->
                              <h6 class="dropdown-header">Welcome Anna!</h6>
                              <a class="dropdown-item" href="{{ url('pg_profile')}}"><i class="mdi mdi-account-circle text-muted fs-16 align-middle me-1"></i> <span class="align-middle">Profile</span></a>
                              <a class="dropdown-item" href="{{ url('pg_faqs')}}"><i class="mdi mdi-lifebuoy text-muted fs-16 align-middle me-1"></i> <span class="align-middle">Helpdesk</span></a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="{{ url('pg_profile')}}"><i class="mdi mdi-lock text-muted fs-16 align-middle me-1"></i> <span class="align-middle">Change Password</span></a>
                              <a class="dropdown-item" href="{{ url('logout')}}"><i class="mdi mdi-logout text-muted fs-16 align-middle me-1"></i> <span class="align-middle" data-key="t-logout">Logout</span></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </header>

        <!-- ========== App Menu ========== -->
        <div class="app-menu navbar-menu">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <!-- Dark Logo-->
                <a href="javascript:void(0)" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{ asset('dist/icon-white.png') }}" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ asset('dist/logo-header-white.png') }}" alt="" height="50">
                    </span>
                </a>
                <!-- Light Logo-->
                <a href="javascript:void(0)" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{ asset('dist/icon-white.png') }}" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="{{ asset('dist/logo-header-white.png') }}" alt="" height="50">
                    </span>
                </a>
                <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
                    <i class="ri-record-circle-line"></i>
                </button>
            </div>

            <div id="scrollbar">
                <div class="container-fluid">

                    <div id="two-column-menu">
                    </div>

                    @if(Session::get('roles_id') == 1 || Session::get('roles_id') == 2)
                    <ul class="navbar-nav" id="navbar-nav">
                        <li class="menu-title"><span data-key="t-menu">Main Menu</span></li>
                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('dashboard') }}">
                                <i class="ri-dashboard-2-line"></i> <span data-key="t-widgets">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link menu-link" href="{{ url('mailbox') }}">
                                <i class="ri-dashboard-2-line"></i> <span data-key="t-widgets">Mailbox</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link menu-link" href="#sidebarDashboards" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                                <i class="ri-dashboard-2-line"></i> <span data-key="t-dashboards">Report</span>
                            </a>
                            <div class="collapse menu-dropdown" id="sidebarDashboards">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="dashboard-analytics.html" class="nav-link" data-key="t-analytics"> Membership </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="dashboard-analytics.html" class="nav-link" data-key="t-analytics"> Application </a>
                                    </li>
                                </ul>
                            </div>
                        </li> 

                         <li class="menu-title"><span data-key="t-menu">Master</span></li>
                          <li class="nav-item">
                            <a class="nav-link menu-link" href="#masterdata" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="masterdata">
                                <i class="ri-apps-2-line"></i> <span data-key="t-apps">Master Data</span>
                            </a>
                            <div class="collapse menu-dropdown" id="masterdata">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="{{ url('sliders') }}" class="nav-link" data-key="t-list"> Sliders </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('services') }}" class="nav-link" data-key="t-list"> Services </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('client') }}" class="nav-link" data-key="t-list"> Client </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('faqs') }}" class="nav-link" data-key="t-user"> FAQs </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('gallery') }}" class="nav-link" data-key="t-user"> Gallery </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('portfolio') }}" class="nav-link" data-key="t-user"> Portfolio </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('terms-conditions') }}" class="nav-link" data-key="t-user"> Terms and Conditions </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('abouts') }}" class="nav-link" data-key="t-user"> About Us </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('contacts') }}" class="nav-link" data-key="t-user"> Contact Us </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="menu-title"><span data-key="t-menu">Modul</span></li>
                        <li class="nav-item">
                            <a class="nav-link menu-link" href="#membership" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="membership">
                                <i class="ri-apps-2-line"></i> <span data-key="t-apps">Membership</span>
                            </a>
                            <div class="collapse menu-dropdown" id="membership">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="{{ url('membership/level') }}" class="nav-link" data-key="t-list"> Level </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('membership/members') }}" class="nav-link" data-key="t-user"> Members </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link menu-link" href="#application" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="application">
                                <i class="ri-apps-2-line"></i> <span data-key="t-apps">Application</span>
                            </a>
                            <div class="collapse menu-dropdown" id="application">
                                <ul class="nav nav-sm flex-column">

                                     <li class="nav-item">
                                        <a href="{{ url('application_category') }}" class="nav-link" data-key="t-user"> Category </a>
                                    </li>
                                    
                                    <li class="nav-item">
                                        <a href="{{ url('application') }}" class="nav-link" data-key="t-user">List </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link menu-link" href="#users" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="users">
                                <i class="ri-apps-2-line"></i> <span data-key="t-apps">Users</span>
                            </a>
                            <div class="collapse menu-dropdown" id="users">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="{{ url('roles') }}" class="nav-link" data-key="t-list"> Roles </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('roles_access') }}" class="nav-link" data-key="t-user"> Roles Access </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('users') }}" class="nav-link" data-key="t-user"> Users </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                         <li class="menu-title"><span data-key="t-menu">Transaction</span></li>


                       
                        <li class="nav-item">
                            <a class="nav-link menu-link" href="#verify" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="users">
                                <i class="ri-apps-2-line"></i> <span data-key="t-apps">Verify Payment</span>
                            </a>
                            <div class="collapse menu-dropdown" id="verify">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="{{ url('verify_payment') }}" class="nav-link" data-key="t-list"> Application </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('verify_payment_upgrade') }}" class="nav-link" data-key="t-user"> Upgrade Membership </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link menu-link" href="#history" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="users">
                                <i class="ri-apps-2-line"></i> <span data-key="t-apps">History Transaction</span>
                            </a>
                            <div class="collapse menu-dropdown" id="history">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="{{ url('transaction') }}" class="nav-link" data-key="t-list"> Application </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('transaction_upgrade') }}" class="nav-link" data-key="t-user"> Upgrade Membership </a>
                                    </li>
                                </ul>
                            </div>
                        </li>




                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('logout') }}">
                                <i class="ri-logout-box-r-line"></i> <span data-key="t-widgets">Logout</span>
                            </a>
                        </li>

                    </ul>

                    @elseif(Session::get('roles_id') == 4)
                    <ul class="navbar-nav" id="navbar-nav">
                        <li class="menu-title"><span data-key="t-menu">Main Menu</span></li>
                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('pg_dashboard') }}">
                                <i class="ri-dashboard-2-line"></i> <span data-key="t-widgets">Dashboard</span>
                            </a>
                        </li>
                       
                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('pg_application') }}">
                                <i class="ri-apps-2-line"></i> <span data-key="t-widgets">Application</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('pg_myapp') }}">
                                <i class="ri-apps-line"></i> <span data-key="t-widgets">My App</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('pg_profile') }}">
                                <i class="ri-user-line"></i> <span data-key="t-widgets">Profile</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('pg_faqs') }}">
                                <i class="ri-questionnaire-line"></i> <span data-key="t-widgets">FAQs</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('pg_ticket') }}">
                                <i class="ri-ticket-2-line"></i> <span data-key="t-widgets">Ticket</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link menu-link active" href="{{ url('logout') }}">
                                <i class="ri-logout-box-r-line"></i> <span data-key="t-widgets">Logout</span>
                            </a>
                        </li>



                    </ul>
                    @endif
                </div>
                <!-- Sidebar -->
            </div>

            <div class="sidebar-background"></div>
        </div>
        <!-- Left Sidebar End -->
        <!-- Vertical Overlay-->
        <div class="vertical-overlay"></div>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                   @yield('content')

                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <script>document.write(new Date().getFullYear())</script> © {{ env('APP_NAME')}}.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-end d-none d-sm-block">
                                Develop by {{ env('APP_NAME_CREATED')}}
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->



    <!--start back-to-top-->
    <button onclick="topFunction()" class="btn btn-danger btn-icon" id="back-to-top">
        <i class="ri-arrow-up-line"></i>
    </button>
    <!--end back-to-top-->

    <!--preloader-->
    <div id="preloader">
        <div id="status">
            <div class="spinner-border text-primary avatar-sm" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
    </div>

    
    <!-- JAVASCRIPT -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ asset('assets/libs/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/plugins/lord-icon-2.1.0.js') }}"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{ asset('assets/js/pages/select2.init.js') }}"></script>


    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ asset('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/sweetalerts.init.js') }}"></script>

    <script src="{{ asset('assets/libs/list.js/list.min.js') }}"></script>
    <script src="{{ asset('assets/libs/list.pagination.js/list.pagination.min.js') }}"></script>

     <!-- ckeditor -->
    <script src="{{ asset('assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js') }}"></script>

    <!-- dropzone js -->
    <script src="{{ asset('assets/libs/dropzone/dropzone-min.js') }}"></script>


    <script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jsvectormap/js/jsvectormap.min.js') }}"></script>
    <script src="{{ asset('assets/libs/jsvectormap/maps/world-merc.js') }}"></script>
    <script src="{{ asset('assets/libs/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/dashboard-ecommerce.init.js') }}"></script>
    <script src="{{ asset('assets/libs/@simonwep/pickr/pickr.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form-pickers.init.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>

    <!-- <script src="{{ asset('assets/js/plugins.js') }}"></script> -->

    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/toastify-js'></script>
    <script type='text/javascript' src="{{ asset('assets/libs/choices.js/public/assets/scripts/choices.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/project-create.init.js') }}"></script>

     <!-- nouisliderribute js -->
    <script src="{{ asset('assets/libs/nouislider/nouislider.min.js') }}"></script>
    <script src="{{ asset('assets/libs/wnumb/wNumb.min.js') }}"></script>

    <!-- gridjs js -->
    <script src="{{ asset('assets/libs/gridjs/gridjs.umd.js') }}"></script>
    <script src="https://unpkg.com/gridjs/plugins/selection/dist/selection.umd.js"></script>
    <!-- ecommerce product list -->
    <script src="{{ asset('assets/js/pages/ecommerce-product-checkout.init.js') }}"></script>
    <script src="{{ asset('assets/js/pages/form-input-spin.init.js') }}"></script>
    <script src="{{ asset('assets/js/pages/ecommerce-cart.init.js') }}"></script>
    <script src="{{ asset('assets/libs/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/pages/ecommerce-product-details.init.js') }}"></script>

    <script src="{{ asset('assets/js/pages/timeline.init.js') }}"></script>

    <script src="{{ asset('assets/js/mailbox.init.js') }}"></script>



    <!-- <script src="{{ asset('assets/js/members.js') }}"></script> -->

@yield('page-script')
</body>

</html>