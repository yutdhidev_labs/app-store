@extends('layouts.master_backend')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18">Report</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="">Home</li>
                    <li class="">&nbsp; > &nbsp; </li>
                    <li class="breadcrumb-item active"><a href="{{ url('dashboard') }}">Report</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>


<div class="row">
<div class="col-md-12">
      <div class="card">
            <div class="card-body">
<div class="row">
    <div class="col-3">
       <div class="mb-3">
            <label for="validationCustom01" class="form-label">Start Date</label>
            <input type="text" class="form-control" id="start_date" value="{{ date('Y-m-01')}}">
        </div>
    </div>
    <div class="col-3">
       <div class="mb-3">
            <label for="validationCustom01" class="form-label">End Date</label>
            <input type="text" class="form-control" id="end_date" value="{{ date('Y-m-t')}}">
        </div>
    </div>
    <div class="col-3">
       <div class="mb-3">
            <label for="validationCustom01" class="form-label">Status</label>
             <select class="form-control" style="width: 100%" required name="ticket_status" id="ticket_status">
            <option value="All">All</option>
            <option value="Open">Open</option>
            <option value="Close">Close</option>
        </select>
        </div>
    </div>
    <div class="col-3">
       <div class="mb-3">
            <label for="validationCustom01" class="form-label">Action</label> <br>
           <button class="btn btn-success" onclick="getExcel()">EXCEL</button>
           <button class="btn btn-danger" onclick="getPdf()">PDF</button>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>


@section('page-script')
<script type="text/javascript">
    function getPdf(){
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        var ticket_status = $("#ticket_status").val();
        var base_url    = '{{ url("report_pdf") }}/'+start_date+'/'+end_date+'/'+ticket_status;
        window.open(base_url,"_blank");
    }

    function getExcel(){
        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        var ticket_status = $("#ticket_status").val();
        var base_url    = '{{ url("report_excel") }}/'+start_date+'/'+end_date+'/'+ticket_status;
        window.open(base_url,"_blank");
    }
</script>
@stop
@endsection