@extends('errors::minimal')

@section('title', __('Halaman tidak ditemukan !'))
@section('code', '404')
@section('message', __('Halaman tidak ditemukan !'))
