<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Members;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use DB;
use Jenssegers\Agent\Facades\Agent;
use Date;
use Carbon\Carbon;
use Stevebauman\Location\Facades\Location;

class LoginController extends Controller
{

    public function testmail(){
                    $data = [
                        'name'                  => 'Yudhi',
                        'email'                 => 'yxlcreative@gmail.com',
                    ];

                    return view('emails.register')->with($data);

            //         Mail::send('emails.layout_mail', $data, function ($mail) use($data)
            //         {
                        // $email_send = $data['email'];
                        // $name_send  = $data['name'];
                        // $subject    = 'Testing';
                        // $mail->to($email_send, $name_send);
                        // $mail->subject($subject);
            //         });
    }

       function deviceip($members_id){
           
            $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                } else if (isset($_SERVER['HTTP_FORWARDED'])) {
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                } else if (isset($_SERVER['REMOTE_ADDR'])) {
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                } else {
                    $ipaddress = 'UNKNOWN';
                }

           $device = Agent::device();
           $browser = Agent::browser();
           $version = Agent::version($browser);
           $platform = Agent::platform();
           $version_platform = Agent::version($platform);

           $type = '';
            if (Agent::isDesktop()) {
                $type = 'desktop';
            } elseif (Agent::isTablet()) {
                $type = 'tablet';
            } elseif (Agent::isMobile()) {
                $type = 'mobile';
            }

            $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                } else if (isset($_SERVER['HTTP_FORWARDED'])) {
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                } else if (isset($_SERVER['REMOTE_ADDR'])) {
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                } else {
                    $ipaddress = 'UNKNOWN';
                }
                // $json     = file_get_contents("http://ipinfo.io/168.192.0.1/geo");
                $json     = file_get_contents("http://ipinfo.io/$ipaddress/geo");
                $json     = json_decode($json, true);
               if(!empty($json['country'])){
                $country  = $json['country'];
                $region   = $json['region'];
                $city     = $json['city'];
                $loc     = $json['loc'];
               }else{
                $country  = '';
                $region   = '';
                $city     = '';
                $loc     = '';
               }
                

            DB::table('device_info')->insert([
                    'type'              => $type,
                    'device'            => $device,
                    'browser'           => $browser,
                    'browser_version'   => $version,
                    'platform'          => $platform,
                    'platform_version'  => $version_platform,
                    'ip_address'        => $ipaddress,
                    'members_id'        => $members_id,
                    'country'           => $country,
                    'region'            => $region,
                    'city'              => $city,
                    'loc'              => $loc,
                ]);

            // return $browser.' '.$version.' '.$device.' '.$platform;
        }


    public function index()
    {
        return view('login.login');
    }

     public function loginPost(Request $request){
        $username = $request->username;
        $password = $request->password;
        $count = Users::where('username',$username)->where('status_id',1)->count();
        if($count > 0){
             $data      = Users::where('username',$username)->first();
             $members   = DB::table('members')->where('id',$data->members_id)->first();
             $roles   = DB::table('roles')->where('id',$data->roles_id)->first();
             $member_id = $members->id;
             $fullname  = $members->first_name.' '.$members->last_name;

             if($data->roles_id == 4){
                 $redirect  = 'pg_dashboard';
             }else{
                 $redirect  = 'dashboard';
             }
            
               
            if(Hash::check($password,$data->password)){

                Session::put('email',$members->email);
                Session::put('members_id',$member_id);
                Session::put('roles_id',$data->roles_id);
                Session::put('roles_name',$roles->name);
                Session::put('name',$data->fullname);
                Session::put('login',TRUE);

                // $this->deviceip($members->uuid);

                return redirect($redirect)->with(['success' => 'Selamat Anda berhasil masuk !']);
            }else{
                return redirect('login')->with(['error' => 'Akses Salah /Akun tidak Aktif !']);
            }
        }else{
            return redirect('login')->with(['error' => 'Akses Salah / Akun tidak Aktif !']);
        }
    }

    public function register()
    {
        $data['profession'] = DB::table('profession')
                              ->where('status_id',1)
                              ->orderBy('name','asc')->get();
        return view('login.register')->with($data);
    }


     public function registerPost(Request $request){

        $count = Members::where('email',$request->email)->count();
        if($count > 0){
             return redirect('register')->with(['error' => 'Registrasi Gagal, Email Sudah Terdaftar!']);
        }else{

            $rep         = str_replace("+","",$request->phone);
            $rep0        = str_replace(" ","",$rep);
            $rep1        = str_replace("*","",$rep0);
            $rep2        = str_replace("-","",$rep1);
            $no_whatsapp = str_replace("±","",$rep2);

            $dice = ['danger','primary','info','success','warning','secondary'];
            $throw = array_rand($dice);

            $data                   =  new Members();
            $data->uuid             = Str::uuid()->toString();
            $data->first_name       = str_replace("'","",strtoupper($request->first_name));
            $data->last_name        = str_replace("'","",strtoupper($request->last_name));
            $data->email            = $request->email;
            $data->profession_id    = $request->profession_id;
            $data->phone            = $no_whatsapp;
            $data->avatar_color     = $dice[$throw];
            $data->join_date        = date('Y-m-d');
            $data->status_membership = 'Basic';
            $data->save();

            DB::table('users')->insert([
                'uuid'       => Str::uuid()->toString(),
                'username'   => $request->email,
                'password'   => bcrypt($request->password),
                'fullname'   => $data->first_name.' '.$data->last_name,
                'members_id' => $data->id,
                'roles_id'   => 4,
            ]);

            Session::put('email',$request->email);
            Session::put('members_id',$data->id);
            Session::put('roles_id',4);
            Session::put('name',$data->first_name.' '.$data->last_name);
            Session::put('login',TRUE);

            return redirect('register')->with(['success' => 'Registrasi Berhasil, Login untuk masuk App!']);

            // return redirect('pg_dashboard')->with(['success' => 'Registrasi Berhasil !']);
        }
    }

    public function forgot()
    {
        return view('login.forgot');
    }

    public function forgotPost(Request $request){
         $check  = Members::where('email',$request->email)->where('status_id','1')->count();
         if($check > 0){
            $user  = Members::where('email',$request->email)->where('status_id','1')->first();

            $name       = $user->name;
            $email      = $user->email;
            
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTU';
            $pin        = mt_rand(100000, 999999).$characters[rand(0, strlen($characters) - 1)];
            $string     = str_shuffle($pin);

            $data = [
                'name'     => $name,
                'email'    => $email,
                'password' => $string,
                'base_url' => url('/'),
            ];
            Mail::send('emails.reset_password', $data, function ($mail) use($data)
            {
              $email_send = $data['email'];
              $name_send  = $data['name'];
              $subject    = 'Reset Password Berhasil';
              $mail->to($email_send, $name_send);
              $mail->subject($subject);
            });


            DB::table('users')->where('members_id',$user->id)->update([
                'password'      => bcrypt($string),

            ]);

                return redirect('forgot')->with(['success' => 'Password baru berhasil dikirim ke email, Silahkan cek email anda !']);
         }else{
            return redirect('forgot')->with(['error' => 'Email Belum Terdaftar atau Akun tidak aktif']);
         }
    }

    
    public function logout(){
          $members   = DB::table('members')->where('id',Session::get('members_id'))->first();
         DB::table('logs')->insert([
                    'name'   => 'Login',
                    'desc'   => 'Anda berhasil <code>Logout</code>',
                    'created_by'   => Session::get('members_id'),
                ]);
        Session::flush();
         return redirect('/')->with(['success' => 'Anda telah berhasil logout !']);
    }
}
