<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Members;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
class MembersController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){

		$data['members'] = DB::table('members')->where('status_id',1)->get();
		return view('admin.membership.members.index')->with($data);
	}  

	function members_json(){
		
		$members = DB::table('members')
					->select(
						'id',
						'first_name as customer_name',
						'email',
						'phone',
						'join_date as date',
						'status_id'
					)
					->where('status_id',1)
					->get();
		echo json_encode($members);

	} 

	public function add(){
		$data['level'] = 'Reguler';
		$data['country'] = DB::table('country')->where('status_id',1)->get();
		$data['profession'] = DB::table('profession')->where('status_id',1)->get();
		return view('admin.membership.members.add')->with($data);
	} 


	public function edit($uuid){

		$data['level'] = 'Premium';
		$data['country'] = DB::table('country')->where('status_id',1)->get();
		$data['profession'] = DB::table('profession')->where('status_id',1)->get();
		$members = DB::table('members')->where('status_id',1)->where('uuid',$uuid)->first();
		$data['members'] = $members;
		$data['get_profesi'] = DB::table('profession')->where('id',$members->profession_id)->first();
		$data['logs_activity'] = DB::table('logs')->where('created_by',$members->id)->whereIn('name',['Login', 'Logout'])->limit(10)->orderBy('id','desc')->get();
		return view('admin.membership.members.edit')->with($data);
	}  

	public function wilayah_json(Request $request){

		if($request->param == 1){
			$data['result'] = DB::table('province')->where('status_id',1)->where('country_id',$request->id)->orderby('name','ASC')->get();
		}else if($request->param == 2){
			$data['result'] = DB::table('city')->where('status_id',1)->where('province_id',$request->id)->orderby('name','ASC')->get();
		}
		return view('admin.membership.members.wilayah_json')->with($data);
	}

	public function add_action(Request $request){
		$file                = $request->file('file');

		$data                	=  new Application();
		$data->uuid          	= Str::uuid()->toString();
		$data->first_name 		= $request->first_name;
		$data->last_name 		= $request->last_name;
		$data->phone 			= $request->phone;
		$data->email 			= $request->email;
		$data->profession_id 	= $request->profession_id;
		$data->country_id 		= $request->country_id;
		$data->province_id 		= $request->province_id;
		$data->city_id 			= $request->city_id;
		$data->desc 			= $request->desc;

		if(!empty($request->file)){
			$nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
			$tujuan_upload = 'dist/img/members';
			$file->move($tujuan_upload,$nama_file);
			$data->filename =  $nama_file;
		}
		$data->save();


            DB::table('users')->insert([
                'uuid'       => Str::uuid()->toString(),
                'username'   => $request->email,
                'password'   => bcrypt($request->password),
                'fullname'   => str_replace("'","",strtoupper($request->first_name.' '.$request->last_name)),
                'members_id' => $data->id,
                'roles_id'   => 4,
            ]);

		return redirect('membership/members')->with(['success' => 'Account members berhasil di create']);
	}

	public function edit_action(Request $request){

		$file                = $request->file('file');

		DB::table('members')->where('uuid',$request->code_id)->update([
			'first_name'  	  => $request->first_name,
			'last_name'       => $request->last_name,
			'phone'   	      => $request->phone,
			'email'   	  	  => $request->email,
			'profession_id'   => $request->profession_id,
			'country_id'      => $request->country_id,
			'province_id'     => $request->province_id,
			'city_id'         => $request->city_id,
			'desc'     		  => $request->desc,
		]);

		$members = DB::table('members')->where('uuid',$request->code_id)->first();

		DB::table('users')->where('members_id',$members->id)->update([
			'fullname'  	  => $request->first_name.' '.$request->last_name,
			'username'   	  	  => $request->email,
		]);

		if(!empty($request->file)){
			$nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
			$tujuan_upload = 'dist/img/members';
			$file->move($tujuan_upload,$nama_file);
			DB::table('members')->where('uuid',$request->code_id)->update([
				'filename'      => $nama_file,
			]);
		}
		return redirect('membership/members')->with(['success' => 'Data Berhasil Diperbaharui']);
		
	}


	public function delete(Request $request){
		DB::table('members')->where('uuid',$request->id)->update([
			'status_id'      => 0,
		]);
		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}

	public function contacts_action(Request $request){

            DB::table('mailbox')->insert([
                'uuid'       => Str::uuid()->toString(),
                'name'   	 => $request->name,
                'phone'   	 => $request->phone,
                'email'      => $request->email,
                'subject'    => $request->subject,
                'desc'       => $request->desc,
            ]);

		return redirect('/')->with(['success' => 'Pesan anda berhasil dikirim !']);
	}
}

