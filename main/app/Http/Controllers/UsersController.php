<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use PDF;
use App\Models\Members;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Exports\ExportsUsers;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
class UsersController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['roles'] = DB::table('roles')->where('status_id',1)->get();
		$data['members'] = DB::table('members')
							->select(
								'members.*',
								'users.username',
								'users.roles_id',
								'roles.name as nama_roles'

								)
							->leftJoin('users', 'users.members_id', '=', 'members.id')
							->leftJoin('roles', 'roles.id', '=', 'users.roles_id')
							->where('members.status_id',1)
							->where('users.status_id',1)
							->get();

		$data['total_admin'] = DB::table('members')
							->leftJoin('users', 'users.members_id', '=', 'members.id')
							->leftJoin('roles', 'roles.id', '=', 'users.roles_id')
							->where('roles.id',2)
							->where('members.status_id',1)
							->where('users.status_id',1)
							->count();

		$data['total_cs'] = DB::table('members')
							->leftJoin('users', 'users.members_id', '=', 'members.id')
							->leftJoin('roles', 'roles.id', '=', 'users.roles_id')
							->where('roles.id',3)
							->where('members.status_id',1)
							->where('users.status_id',1)
							->count();

		$data['total_members'] = DB::table('members')
							->leftJoin('users', 'users.members_id', '=', 'members.id')
							->leftJoin('roles', 'roles.id', '=', 'users.roles_id')
							->where('roles.id',4)
							->where('members.status_id',1)
							->where('users.status_id',1)
							->count();

		return view('admin.master.users.index')->with($data);
	}     
	public function action(Request $request){

		if(!empty($request->id_users)){
			DB::table('members')->where('id',$request->id_users)->update([
				'name'   		=> str_replace("'","",strtoupper($request->name)),
				'email' 		=> $request->email,
				'position'	 	=> $request->position,
				'phone'	 	=> $request->phone,
				'alamat'	 	=> $request->alamat,
				'jenis_kelamin' => $request->jenis_kelamin,
				'department_id' => $request->department_id,
			]);

			DB::table('users')->where('members_id',$request->id_users)->update([
				'fullname'     => str_replace("'","",strtoupper($request->name)),
				'username'     => $request->email,
				'roles_id'     => $request->roles_id,
			]);

			if(!empty($request->password)){
				DB::table('users')->where('members_id',$request->id_users)->update([
					'password'   => bcrypt($request->password),
				]);
			}
			
			$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Diperbaharui !'
				);
		}else{
			$check = DB::table('members')->where('email',$request->email)->count();
			if($check > 0){
				return redirect('users')->with(['failed' => 'Data Email Sudah Ada ! Data Gagal di Simpan.']);
			}else{

				$data                =  new Members();
				$data->uuid          = Str::uuid()->toString();
				$data->name          = str_replace("'","",strtoupper($request->name));
				$data->email         = $request->email;
				$data->position     = $request->position;
				$data->jenis_kelamin = $request->jenis_kelamin;
				$data->phone 		= $request->phone;
				$data->alamat 		= $request->alamat;
				$data->department_id 		= $request->department_id;
				$data->save();

				DB::table('users')->insert([
					'uuid'       => Str::uuid()->toString(),
					'username'   => $request->email,
					'password'   => bcrypt($request->password),
					'fullname' 	 => str_replace("'","",strtoupper($request->name)),
					'members_id' => $data->id,
					'roles_id' 	 => $request->roles_id,
				]);

				$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Disimpan !'
				);
			}
		}
			echo json_encode($result);
	}


	public function delete(Request $request){
		DB::table('members')->where('id',$request->id)->update([
				'status_id'   		=> 0,
			]);

		DB::table('users')->where('members_id',$request->id)->update([
				'status_id'   		=> 0,
			]);

		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}

	public function unduh_excel() 
    {
            $data = array(
                        'jenis'      => 'test',   
                        );

            return Excel::download(new ExportsUsers($data), 'Rekap Data Users.xlsx');
    }

	public function print_pdf(){
		$filename 				= 'Rekap Data Users';
		$data['title'] 			= 'Daftar Users';
		$data['header_label'] 	= 'Rekap Data Users';
		$data['members'] = DB::table('members')
							->select(
								'members.*',
								'users.username',
								'users.roles_id'
								)
							->leftJoin('users', 'users.members_id', '=', 'members.id')
							->where('members.status_id',1)
							->where('users.status_id',1)
							->get();
		$pdf   = PDF::loadview('admin.master.users.print_pdf',$data)->setPaper('A4','potrait');
		return $pdf->stream($filename.'.pdf', array("Attachment" => false));
	}
}

