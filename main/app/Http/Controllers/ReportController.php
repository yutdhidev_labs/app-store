<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use PDF;
use App\Models\Exports\ExportsTiket;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
class ReportController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['category'] = DB::table('category')->where('status_id',1)->get();
		return view('report.index')->with($data);
	} 

	public function report_excel($start_date,$end_date,$ticket_status){
		 $data = array(
                        'start_date'      => $start_date,   
                        'end_date'      => $end_date,   
                        'ticket_status'      => $ticket_status,   
                        );

		 return Excel::download(new ExportsTiket($data), 'Report Tiket.xlsx');
	}     


	public function report_pdf($start_date,$end_date,$ticket_status){
		$filename 				= 'Rekap Data Tiket';
		$data['title'] 			= 'Daftar Tiket';
		$data['header_label'] 	= 'Rekap Data Tiket';

		$query = DB::table('ticket')
                            ->select(
                                'ticket.ticket_code',
                                'ticket.ticket_date',
                                'ticket.subject',
                                'category.name as category_name',
                                'members.name',
                                'a.name as report_by_name',
                                'b.name as assign_by_name',
                                'c.name as teknisi_name',
                                'ticket.ticket_status',
                                )
                            ->leftJoin('members', 'members.id', '=', 'ticket.created_by')
                            ->leftJoin('category', 'category.id', '=', 'ticket.category_id')
                            ->leftJoin('department as a', 'a.id', '=', 'ticket.report_by')
                            ->leftJoin('department as b', 'b.id', '=', 'ticket.assign_by')
                            ->leftJoin('members as c', 'c.id', '=', 'ticket.teknisi_id')
                            ->where('ticket.status_id',1)
                            ->whereBetween('ticket.ticket_date', [$start_date, $end_date]);

            if($ticket_status != 'All'){
              $getdata = $query->where('ticket.ticket_status',$ticket_status);
            }
            
            $getdata = $query->get();

            $data['ticket'] = $getdata;
            $data['abouts'] = DB::table('abouts')->where('id',1)->first();

		$pdf   = PDF::loadview('ticket.print_pdf',$data)->setPaper('A4','potrait');
		return $pdf->stream($filename.'.pdf', array("Attachment" => false));
	}
	



}

