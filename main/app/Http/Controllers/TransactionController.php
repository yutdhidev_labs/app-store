<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\transaction;
use DB;
use Illuminate\Support\Facades\Session;
class TransactionController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['transaction'] = DB::table('orders')
						->select(
								'orders.*',
								'members.first_name',
								'members.last_name',
								'members.phone',
								'members.email',
								 'application.name as application_name',
								 'application_category.name as category_name'
								)
						->leftJoin('members', 'members.id', '=', 'orders.members_id')
						->leftJoin('application', 'application.id', '=', 'orders.application_id')
						->leftJoin('application_category', 'application_category.id', '=', 'application.category_id')
						->where('orders.status_id',1)
						->where('orders.orders_type','App')
						->get();
		return view('admin.history_transaction.index')->with($data);
	}     

	public function upgrade(){
		$data['transaction'] = DB::table('orders')
						->select(
								'orders.*',
								'members.first_name',
								'members.last_name',
								'members.phone',
								'members.email'
								)
						->leftJoin('members', 'members.id', '=', 'orders.members_id')
						->where('orders.status_id',1)
						->where('orders.orders_type','Upgrade')
						->get();
		return view('admin.history_transaction.upgrade')->with($data);
	}     

	public function verifiy(){
		$data['transaction'] = DB::table('confirm_payment')
						->select(
								'orders.*',
								'confirm_payment.invoice_id',
								'confirm_payment.rek_name',
								'confirm_payment.norek_tujuan',
								'confirm_payment.filename',
								'confirm_payment.id as confirm_payment_id',
								'members.first_name',
								'members.last_name',
								'members.phone',
								'members.email',
								 'application.name as application_name',
								 'application.appcode as appcode',
								 'application.uuid as uuid_app',
								 'application.thumbnail as thumbnail',
								 'application_category.name as category_name'
								)
						->leftJoin('orders', 'confirm_payment.invoice', '=', 'orders.invoice')
						->leftJoin('members', 'members.id', '=', 'orders.members_id')
						->leftJoin('application', 'application.id', '=', 'orders.application_id')
						->leftJoin('application_category', 'application_category.id', '=', 'application.category_id')
						->where('orders.payment_status','Verify')
						->where('orders.orders_type','App')
						->where('orders.status_id',1)
						->where('confirm_payment.status_id',1)
						->get();
		return view('admin.history_transaction.verify')->with($data);
	} 

	 public function verify_action(Request $request){
        DB::table('confirm_payment')->where('invoice',$request->inv)->where('invoice_id',$request->orderid)->update([
            'status_confirm'       => 1,
            ]);

        DB::table('orders')->where('invoice',$request->inv)->where('uuid',$request->orderid)->update([
            'payment_status'       => 'Done',
            'approved_date'  => date('Y-m-d H:i:s'),
            ]);

        if(!empty($request->members_id)){
        	DB::table('members')->where('id',$request->members_id)->update([
            	'status_membership'       => 'Premium',
            ]);

        }

       
        $result = array(
                    'status' => 'OK',
                    'title'  => 'Berhasil',
                    'msg'    => 'Verifikasi pembayaran berhasil !'
                );

        echo json_encode($result);
    }   


    public function verify_upgrade(){
		$data['transaction'] = DB::table('confirm_payment')
						->select(
								'orders.*',
								'confirm_payment.invoice_id',
								'confirm_payment.rek_name',
								'confirm_payment.norek_tujuan',
								'confirm_payment.filename',
								'confirm_payment.id as confirm_payment_id',
								'members.first_name',
								'members.last_name',
								'members.phone',
								'members.email'
								)
						->leftJoin('orders', 'confirm_payment.invoice', '=', 'orders.invoice')
						->leftJoin('members', 'members.id', '=', 'orders.members_id')
						->where('orders.payment_status','Verify')
						->where('orders.orders_type','Upgrade')
						->where('orders.status_id',1)
						->where('confirm_payment.status_id',1)
						->get();
		return view('admin.history_transaction.verify_upgrade')->with($data);
	}  
}

