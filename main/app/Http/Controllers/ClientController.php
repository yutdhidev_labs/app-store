<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Client;
use DB;
use Illuminate\Support\Facades\Session;
class ClientController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['client'] = DB::table('client')->where('status_id',1)->get();
		return view('admin.master.client.index')->with($data);
	}     
	public function action(Request $request){

		$file                = $request->file('file');

		if(!empty($request->id_client)){
			DB::table('client')->where('id',$request->id_client)->update([
				'name'   => $request->name,
				'desc'   => $request->desc,
			]);

			if(!empty($request->file)){
				$nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
				$tujuan_upload = 'dist/img/client';
				$file->move($tujuan_upload,$nama_file);
				DB::table('client')->where('id',$request->id_client)->update([
					'filename'      => $nama_file,
				]);
			}
			return redirect('client')->with(['success' => 'Data Berhasil Diperbaharui']);
		}else{
			
			$data                =  new Client();
			$data->name          = $request->name;
			$data->desc          = $request->desc;
			if(!empty($request->file)){
				$nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
				$tujuan_upload = 'dist/img/client';
				$file->move($tujuan_upload,$nama_file);
				$data->filename =  $nama_file;
			}
			$data->save();
			
			return redirect('client')->with(['success' => 'Data Berhasil Disimpan']);
			
		}
	}


	public function delete(Request $request){
		DB::table('client')->where('id',$request->id)->update([
			'status_id'      => 0,
		]);
		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}
}

