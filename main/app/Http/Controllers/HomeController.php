<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;


class HomeController extends Controller
{

	public function index(){
		$data['sliders'] = DB::table('sliders')->where('status_id',1)->get();
		$data['services'] = DB::table('services')->where('status_id',1)->get();
		$data['abouts'] = DB::table('abouts')->where('id',1)->first();
		$data['contacts'] = DB::table('contacts')->where('id',1)->first();
		$data['premium'] = DB::table('level')->where('status_id',1)->where('id','2')->first();
		$data['project_done'] = 57;
		$data['members_count'] = 120;
		$data['app_count'] = 30;
		return view('home.index')->with($data);
	}

	public function contacts_action(Request $request){

            DB::table('mailbox')->insert([
                'uuid'       => Str::uuid()->toString(),
                'name'   	 => $request->name,
                'phone'   	 => $request->phone,
                'email'      => $request->email,
                'subject'    => $request->subject,
                'desc'       => $request->desc,
            ]);

            $result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Pesan anda berhasil dikirim !'
				);
		echo json_encode($result);
	}

}

