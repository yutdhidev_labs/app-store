<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Comfirmpayment;
use App\Models\Orders;
use App\Models\Ticket;
use App\Models\Ticketdetail;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
class MemberareaController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {
            if(empty(Session::get('members_id')))
            {
                return redirect('logout');
            }else{
                if(Session::get('roles_id') != 4)
                {
                    return redirect('logout');
                }else{
                    return $next($request);
                }
            }
        });
	}

    public function icon(){
          $data['icon_animation'] = DB::table('icon_animation')->where('status_id',1)->get();
        return view('icon.index')->with($data);;
    }
	
	public function dashboard(){

		date_default_timezone_set("Asia/Jakarta");
			$jam=date("G");
			if($jam>=0&&$jam<=11){
				$salam="PAGI";
			}else if($jam>=12&&$jam<=15){
				$salam="SIANG";
			}else if($jam>=16&&$jam<=18){
				$salam="SORE";
			}else if($jam>=19&&$jam<=23){
				$salam="MALAM";
			}

			$data['salam'] = 'SELAMAT '.$salam;
              $data['premium'] = DB::table('level')->where('status_id',1)->where('id','2')->first();
              $data['members'] = DB::table('members')->where('status_id',1)->where('id',Session('members_id'))->first();
		return view('member-area.dashboard.index')->with($data);
	}

    public function application(){
        $members = DB::table('members')->where('members.id',Session::get('members_id'))->where('status_id',1)->first();

          $data['application_category'] = DB::table('application_category')->where('status_id',1)->get();
          $data['application'] = DB::table('application')
                                ->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
                                ->where('application.status_id',1)
                                ->where('application.status_publish',1)
                                ->where('application.app_type','Premium')
                                ->count();

        if($members->status_membership == 'Reguler'){
         $limit = 1;
        }else if($members->status_membership == 'Premium'){
         $limit = 5000;
        }

        $data['application_free'] = DB::table('application')
                                ->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
                                ->where('application.status_id',1)
                                ->where('application.app_type','Free')
                                ->where('application.status_publish',1)
                                ->limit($limit)
                                ->get();

        return view('member-area.application.index')->with($data);
    }


    public function application_detail($uuid){

        $application = DB::table('application')->where('uuid',$uuid)->first();
        DB::table('application')->where('id',$application->id)->update([
                        'hit_view'          => $application->hit_view + 1,
                ]);

          $data['application_category'] = DB::table('application_category')->where('status_id',1)->get();
          $data['testimoni'] = DB::table('testimoni')
                                ->select(
                                'testimoni.*',
                                'members.first_name',
                                'members.last_name'
                                )
                                ->leftJoin('members', 'testimoni.members_id', '=', 'members.id')
                                ->where('testimoni.application_id',$application->id)
                                ->where('testimoni.status_id',1)
                                ->where('testimoni.status_publish',1)
                                ->get();

         $data['application_gallery'] = DB::table('application_gallery')
                        ->select(
                                'application_gallery.*',
                                'application.uuid',
                                'application.appcode'
                                )
                        ->leftJoin('application', 'application_gallery.application_id', '=', 'application.id')
                        ->where('application.status_id',1)
                        ->where('application_gallery.status_id',1)
                        ->where('application_gallery.application_id',$application->id)
                        ->get();

        $application = DB::table('application')
                        ->select(
                                'application.*',
                                'application_category.name as category_name'

                                )
                        ->leftJoin('application_category', 'application_category.id', '=', 'application.category_id')
                        ->where('application.status_id',1)
                        ->where('application.uuid',$uuid)->first();
        $data['application'] = $application;

        $data['orders'] = DB::table('orders')
                        ->select(
                                'orders.*',
                                'members.first_name',
                                'members.last_name',
                                'members.phone',
                                'members.email'
                                )
                        ->leftJoin('members', 'members.id', '=', 'orders.members_id')
                        ->where('orders.status_id',1)
                        ->where('orders.expiry',0)
                        ->where('orders.application_id',$application->id)
                        ->where('orders.members_id',Session::get('members_id'))
                        ->get();

        $data['check_orders'] = DB::table('orders')
                        ->select(
                                'orders.*',
                                )
                        ->leftJoin('members', 'members.id', '=', 'orders.members_id')
                        ->where('orders.status_id',1)
                        ->where('orders.expiry',0)
                        ->where('orders.application_id',$application->id)
                        ->where('orders.members_id',Session::get('members_id'))
                        ->first();

        $data['history_download'] = DB::table('history_download')
                        ->select(
                                'history_download.*',
                                'members.first_name',
                                'members.last_name',
                                'members.phone',
                                'members.email'
                                )
                        ->leftJoin('members', 'members.id', '=', 'history_download.members_id')
                        ->where('history_download.status_id',1)
                        ->where('history_download.application_id',$application->id)
                        ->where('history_download.members_id',Session::get('members_id'))
                        ->get();


        $data['logs_activity'] = DB::table('logs_activity')
                        ->select(
                                'logs_activity.*',
                                'members.first_name',
                                'members.last_name',
                                'members.avatar',
                                'members.avatar_color',
                                'members.phone',
                                'members.email',
                                'report.report_code',
                                'report.subject',
                                'report.category',
                                'report.desc as report_desc',
                                'report.filename as report_filename'
                                )
                        ->leftJoin('members', 'members.id', '=', 'logs_activity.members_id')
                        ->leftJoin('report', 'report.id', '=', 'logs_activity.report_id')
                        ->where('logs_activity.status_id',1)
                        ->where('logs_activity.application_id',$application->id)
                        ->where('logs_activity.members_id',Session::get('members_id'))
                        ->orderBy('logs_activity.id','desc')
                        ->get();

        $data['members'] = DB::table('members')
                        ->select(
                                'members.first_name',
                                'members.last_name',
                                'members.avatar_color',
                                'members.phone',
                                'members.email'
                                )
                        ->where('members.id',Session::get('members_id'))
                        ->first();


           $sum_start       = DB::table('testimoni')->where('status_id',1)->where('application_id',$application->id)->sum('star');
            $total_testimoni     = DB::table('testimoni')->where('status_id',1)->where('application_id',$application->id)->count();
            if($total_testimoni > 0){
                $rate           = $sum_start/$total_testimoni;
            }else{
                $rate = 0;
            }

            $data['total_star_1']  = DB::table('testimoni')->where('status_id',1)
                                ->where('application_id',$application->id)->where('star',1)->count(); 
            $data['total_star_2']  = DB::table('testimoni')->where('status_id',1)
                                ->where('application_id',$application->id)->where('star',2)->count(); 
            $data['total_star_3']  = DB::table('testimoni')->where('status_id',1)
                                ->where('application_id',$application->id)->where('star',3)->count(); 
            $data['total_star_4']  = DB::table('testimoni')->where('status_id',1)
                                ->where('application_id',$application->id)->where('star',4)->count(); 
            $data['total_star_5']  = DB::table('testimoni')->where('status_id',1)
                                ->where('application_id',$application->id)->where('star',5)->count();

            $data['rate'] = $rate;
            $data['total_testimoni'] = $total_testimoni;

        return view('member-area.application.detail')->with($data);
    }

    public function cart($uuid,$appcode){

        $application = DB::table('application')
                        ->select(
                                'application.*',
                                'application_category.name as category_name'

                                )
                        ->leftJoin('application_category', 'application_category.id', '=', 'application.category_id')
                        ->where('application.status_id',1)
                        ->where('application.uuid',$uuid)
                        ->where('application.appcode',$appcode)
                        ->first();

            $data['application'] = $application;

        $check_orders = DB::table('orders')
                            ->select(
                                    'orders.*',
                                    )
                            ->leftJoin('members', 'members.id', '=', 'orders.members_id')
                            ->where('orders.status_id',1)
                            ->where('orders.expiry',0)
                            ->where('orders.application_id',$application->id)
                            ->where('orders.members_id',Session::get('members_id'))
                            ->first();

        if(empty($check_orders)){
            
              $data['check_orders'] = $check_orders;

            $data['members'] = DB::table('members')->where('id',Session::get('members_id'))->where('status_id',1)->first();
            return view('member-area.application.cart')->with($data);
        }else{
             return redirect('pg_application/checkout/'.$uuid.'/'.$appcode);
        }
        
    }

     public function cart_action(Request $request){

         $application = DB::table('application')
                        ->select(
                                'application.*',
                                'application_category.name as category_name'

                                )
                        ->leftJoin('application_category', 'application_category.id', '=', 'application.category_id')
                        ->where('application.status_id',1)
                        ->where('application.id',$request->appid)->first();

        $characters = '0123456789';
        $pin        = mt_rand(100000, 999999).$characters[rand(0, strlen($characters) - 1)];
        $string     = 'INV'.str_shuffle($pin);
        $uuid = Str::uuid()->toString();


        DB::table('orders')->insert([
            'uuid'              => $uuid,
            'invoice'           => $string,
            'application_id'    => $request->appid,
            'members_id'        => Session::get('members_id'),
            'checkout_date'     => date('Y-m-d H:i:s'),
            'expired_date'      => date('Y-m-d H:i:s', strtotime('6 hour')),
            'price'             => $application->price,
            'desc'              => '',
            'payment_status'    => 'Checkout',
            'orders_type'       => 'App',
        ]);
       
        $paramdata = array(
            'title' => 'OK',
            'msg' => 'Item berhasil di checkout !',
            'url' => url('pg_application/checkout').'/'.$application->uuid.'/'.$application->appcode,
        );


         return json_encode($paramdata);
    }

    public function checkout($uuid){

        $application = DB::table('application')
                        ->select(
                                'application.*',
                                'application_category.name as category_name'

                                )
                        ->leftJoin('application_category', 'application_category.id', '=', 'application.category_id')
                        ->where('application.status_id',1)
                        ->where('application.uuid',$uuid)->first();

        $data['application'] = $application;
          $check_orders = DB::table('orders')
                        ->select(
                                'orders.*',
                                )
                        ->leftJoin('members', 'members.id', '=', 'orders.members_id')
                        ->where('orders.status_id',1)
                        ->where('orders.expiry',0)
                        ->where('orders.application_id',$application->id)
                        ->where('orders.members_id',Session::get('members_id'))
                        ->first();
        $data['check_orders'] = $check_orders;
        $data['members'] = DB::table('members')->where('id',Session::get('members_id'))->where('status_id',1)->first();
        if(!empty($check_orders)){
            return view('member-area.application.checkout')->with($data);
        }else{
             return redirect('pg_application/'.$application->uuid);
        }
    }

     public function confirm_action(Request $request){
        $file                = $request->file('file');


        if(!empty($request->members_id)){
            $data                =  new Comfirmpayment();
            $data->uuid          = Str::uuid()->toString();
            $data->invoice_id    = $request->orders_id;
            $data->invoice       = $request->inv;
            $data->name          = $request->name;
            $data->email         = $request->email;
            $data->norek_tujuan  = $request->norek_tujuan;
            $data->rek_name      = $request->rek_name;
            $data->price         = $request->price;
            $data->paid_date     = $request->paid_date;
            $data->desc          = $request->desc;
            $data->members_id    = $request->members_id;
            if(!empty($request->file)){
                $nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
                $tujuan_upload = 'dist/payment/confirm';
                $file->move($tujuan_upload,$nama_file);
                $data->filename =  $nama_file;
            }
            $data->save();

             DB::table('orders')->where('invoice',$request->inv)->where('uuid',$request->orders_id)->update([
                    'payment_status'     => 'Verify',
                    'paid_date'          => $request->paid_date,
            ]);
            
            return redirect('pg_application/checkout/'.$request->application_id.'/'.$request->appcode)->with(['success' => 'Konfirmasi pembayaran berhasil']);
        }else{
            
            return redirect('pg_application/checkout/'.$request->application_id.'/'.$request->appcode)->with(['failed' => 'Konfirmasi pembayaran gagal']);
            
        }
    }




    public function application_json(){

        $application = DB::table('application')
                                ->select(
                                'application.uuid as id',
                                'application.id as application_id',
                                'application.appcode',
                                'application.price',
                                'application.hit_view',
                                'application.hit_download',
                                'application.created_at',
                                'application.thumbnail',
                                'application.name',
                                'application_category.name as category'

                                )
                                ->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
                                ->where('application.status_id',1)
                                ->where('application.status_publish',1)
                                ->where('application.app_type','Premium')
                                ->orderBy('application.id','desc')
                                ->get();

        $result = [];
          foreach ($application as $element) {


           $sum_start       = DB::table('testimoni')->where('status_id',1)->where('application_id',$element->application_id)->sum('star');
            $total_star     = DB::table('testimoni')->where('status_id',1)->where('application_id',$element->application_id)->count();
            if($total_star > 0){
                $rate           = $sum_start/$total_star;
            }else{
                $rate = 0;
            }

                  $result[] = array(
                    'product' => array(
                        'img'       => "dist/app/".$element->appcode."/".$element->id."/".$element->thumbnail,
                        'title'     => $element->name,
                        'category'  => $element->category,
                    ),
                    'price'         => number_format($element->price,0,",","."),
                    'hit'      => $element->hit_view,
                    'download'  => $element->hit_download,
                    'rating'        => $rate,
                    'published' => array(
                        'publishDate' => date('d M Y', strtotime($element->created_at)),
                    ),
                    'id'        => $element->id,
                    'link_view'  => url('pg_application').'/'.$element->id,
                  );

          }

          $members = DB::table('members')->where('members.id',Session::get('members_id'))->where('status_id',1)->first();

        if($members->status_membership == 'Reguler'){
         $limit = 1;
        }else if($members->status_membership == 'Premium'){
         $limit = 5000;
        }


          $application_free = DB::table('application')
                                ->select(
                                'application.uuid as id',
                                'application.id as application_id',
                                'application.appcode',
                                'application.price',
                                'application.hit_view',
                                'application.hit_download',
                                'application.created_at',
                                'application.thumbnail',
                                'application.name',
                                'application_category.name as category'

                                )
                                ->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
                                ->where('application.status_id',1)
                                ->where('application.app_type','Free')
                                ->where('application.status_publish',1)
                                ->limit($limit)
                                ->orderBy('application.id','desc')
                                ->get();

          $result_free = [];
          foreach ($application_free as $element) {


           $sum_start2       = DB::table('testimoni')->where('status_id',1)->where('application_id',$element->application_id)->sum('star');
            $total_star2     = DB::table('testimoni')->where('status_id',1)->where('application_id',$element->application_id)->count();
            if($total_star2 > 0){
                $rate2           = $sum_start2/$total_star2;
            }else{
                $rate2 = 0;
            }

                  $result_free[] = array(
                    'product' => array(
                        'img'       => "dist/app/".$element->appcode."/".$element->id."/".$element->thumbnail,
                        'title'     => $element->name,
                        'category'  => $element->category,
                    ),
                    'price'         => number_format($element->price,0,",","."),
                    'hit'      => $element->hit_view,
                    'download'  => $element->hit_download,
                    'rating'        => $rate2,
                    'published' => array(
                        'publishDate' => date('d M Y', strtotime($element->created_at)),
                    ),
                    'id'        => $element->id,
                    'link_view'  => url('pg_application').'/'.$element->id,
                  );

          }

          $paramdata = array(
            'all' => $result,
            'free' => $result_free,
        );


         return json_encode($paramdata);
    }

     public function myapp(){
          $data['application_category'] = DB::table('application_category')->where('status_id',1)->get();
          $data['application'] = DB::table('orders')
                                ->leftJoin('application', 'orders.application_id', '=', 'application.id')
                                ->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
                                ->where('application.status_id',1)
                                ->where('application.status_publish',1)
                                ->where('application.app_type','Premium')
                                ->where('orders.expiry',0)
                                ->where('orders.status_id',1)
                                ->where('orders.payment_status','Done')
                                ->count();


           $members = DB::table('members')->where('members.id',Session::get('members_id'))->where('status_id',1)->first();

        if($members->status_membership == 'Reguler'){
         $limit = 1;
        }else if($members->status_membership == 'Premium'){
         $limit = 5000;
        }

        $data['application_free'] = DB::table('application')
                                ->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
                                ->where('application.status_id',1)
                                ->where('application.app_type','Free')
                                ->where('application.status_publish',1)
                                ->limit($limit)
                                ->get();

        return view('member-area.application.myapp')->with($data);
    }


    public function myapp_json(){

        $application = DB::table('orders')
                                ->select(
                                'application.uuid as id',
                                'application.id as application_id',
                                'application.appcode',
                                'application.price',
                                'application.hit_view',
                                'application.hit_download',
                                'application.created_at',
                                'application.thumbnail',
                                'application.name',
                                'application_category.name as category'

                                )
                                ->leftJoin('application', 'orders.application_id', '=', 'application.id')
                                ->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
                                ->where('application.status_id',1)
                                ->where('application.status_publish',1)
                                ->where('orders.expiry',0)
                                ->where('orders.status_id',1)
                                ->where('orders.payment_status','Done')
                                ->where('application.app_type','Premium')
                                ->orderBy('application.id','desc')
                                ->get();

        $result = [];
          foreach ($application as $element) {


           $sum_start       = DB::table('testimoni')->where('status_id',1)->where('application_id',$element->application_id)->sum('star');
            $total_star     = DB::table('testimoni')->where('status_id',1)->where('application_id',$element->application_id)->count();
            if($total_star > 0){
                $rate           = $sum_start/$total_star;
            }else{
                $rate = 0;
            }

                  $result[] = array(
                    'product' => array(
                        'img'       => "dist/app/".$element->appcode."/".$element->id."/".$element->thumbnail,
                        'title'     => $element->name,
                        'category'  => $element->category,
                    ),
                    'price'         => number_format($element->price,0,",","."),
                    'hit'      => $element->hit_view,
                    'download'  => $element->hit_download,
                    'rating'        => $rate,
                    'published' => array(
                        'publishDate' => date('d M Y', strtotime($element->created_at)),
                    ),
                    'id'        => $element->id,
                    'link_view'  => url('pg_application').'/'.$element->id,
                  );

          }

        $members = DB::table('members')->where('members.id',Session::get('members_id'))->where('status_id',1)->first();

        if($members->status_membership == 'Reguler'){
         $limit = 1;
        }else if($members->status_membership == 'Premium'){
         $limit = 5000;
        }


          $application_free = DB::table('application')
                                ->select(
                                'application.uuid as id',
                                'application.appcode',
                                'application.price',
                                'application.hit_view',
                                'application.hit_download',
                                'application.created_at',
                                'application.thumbnail',
                                'application.name',
                                'application_category.name as category'

                                )
                                ->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
                                ->where('application.status_id',1)
                                ->where('application.app_type','Free')
                                ->where('application.status_publish',1)
                                ->limit($limit)
                                ->orderBy('application.id','desc')
                                ->get();

          $result_free = [];
          foreach ($application_free as $element) {
                  $result_free[] = array(
                    'product' => array(
                        'img'       => "dist/app/".$element->appcode."/".$element->id."/".$element->thumbnail,
                        'title'     => $element->name,
                        'category'  => $element->category,
                    ),
                    'price'         => number_format($element->price,0,",","."),
                    'hit'      => $element->hit_view,
                    'download'  => $element->hit_download,
                    'rating'        => 5,
                    'published' => array(
                        'publishDate' => date('d M Y', strtotime($element->created_at)),
                    ),
                    'id'        => $element->id,
                    'link_view'  => url('pg_application').'/'.$element->id,
                  );

          }

          $paramdata = array(
            'all' => $result,
            'free' => $result_free,
        );


         return json_encode($paramdata);
    }


    public function pricing(){

        $data['application_category'] = DB::table('application_category')->where('status_id',1)->get();
      
        return view('member-area.pricing.index')->with($data);
    }

    public function faqs(){

        $data['general'] = DB::table('faqs')->where('category','General')->where('status_id',1)->get();
        $data['payment'] = DB::table('faqs')->where('category','Payment')->where('status_id',1)->get();
      
        return view('member-area.faqs.index')->with($data);
    }

    public function update_password(Request $request){
            DB::table('users')->where('members_id',$request->code_id)->update([
            'password'          => bcrypt($request->password),
            ]);
            return redirect('pg_profile')->with(['success' => 'Password Berhasil di perbaharui']);
    }

    public function profile(){

        $data['level'] = 'Premium';
        $data['country'] = DB::table('country')->where('status_id',1)->get();
        $data['profession'] = DB::table('profession')->where('status_id',1)->get();
        $members = DB::table('members')->where('status_id',1)->where('id',Session::get('members_id'))->first();
        $data['members'] = $members;
        $data['get_profesi'] = DB::table('profession')->where('id',$members->profession_id)->first();
        $data['logs_activity'] = DB::table('device_info')->where('members_id',$members->uuid)->limit(5)->orderBy('id','desc')->get();

        return view('member-area.profile.index')->with($data);
    }  
public function update_profile(Request $request){

        $file                = $request->file('file');

        DB::table('members')->where('uuid',$request->code_id)->update([
            'first_name'      => $request->first_name,
            'last_name'       => $request->last_name,
            'phone'           => $request->phone,
            'email'           => $request->email,
            'profession_id'   => $request->profession_id,
            'country_id'      => $request->country_id,
            'province_id'     => $request->province_id,
            'city_id'         => $request->city_id,
            'desc'            => $request->desc,
        ]);

        if(!empty($request->file)){
            $nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
            $tujuan_upload = 'dist/img/members';
            $file->move($tujuan_upload,$nama_file);
            DB::table('members')->where('uuid',$request->code_id)->update([
                'filename'      => $nama_file,
            ]);
        }
        return redirect('pg_profile')->with(['success' => 'Data Berhasil Diperbaharui']);
        
    }

    public function wilayah_json(Request $request){

        if($request->param == 1){
            $data['result'] = DB::table('province')->where('status_id',1)->where('country_id',$request->id)->orderby('name','ASC')->get();
        }else if($request->param == 2){
            $data['result'] = DB::table('city')->where('status_id',1)->where('province_id',$request->id)->orderby('name','ASC')->get();
        }
        return view('admin.membership.members.wilayah_json')->with($data);
    }

    public function form_review(Request $request){

        DB::table('testimoni')->insert([
            'members_id'      => Session('members_id'),
            'application_id'  => $request->application_id,
            'star'            => $request->star,
            'desc'            => $request->desc,
        ]);

        $result = array(
                    'status' => 'OK',
                    'title'  => 'Berhasil',
                    'msg'    => 'Data review Berhasil Disimpan !'
                );

        echo json_encode($result);
    }

     public function form_report(Request $request){

        DB::table('report')->insert([
            'members_id'      => Session('members_id'),
            'application_id'  => $request->application_id,
            'category'  => $request->category,
            'subject'  => $request->subject,
            'desc'            => $request->desc,
        ]);

        $result = array(
                    'status' => 'OK',
                    'title'  => 'Berhasil',
                    'msg'    => 'Data report Berhasil Disimpan !'
                );

        echo json_encode($result);
    }

     public function transaction_delete(Request $request){
        DB::table('orders')->where('uuid',$request->id)->update([
            'expiry'       => 1,
            'cancel_date'  => date('Y-m-d H:i:s'),
            ]);

        $result = array(
                    'status' => 'OK',
                    'title'  => 'Berhasil',
                    'msg'    => 'Data transaksi Berhasil dibatalkan !'
                );

        echo json_encode($result);
    }

    public function ticket(){

        $data['ticket'] = DB::table('ticket')
                ->select(
                    'ticket.*',
                    'members.first_name',
                    'members.last_name',
                    'members.email',
                    'members.phone',
                    'category_ticket.name as category_name',
                    'c.first_name as cs_first_name',
                    'c.last_name as cs_last_name'
                    )
                ->leftJoin('members', 'members.id', '=', 'ticket.members_id')
                ->leftJoin('category_ticket', 'category_ticket.id', '=', 'ticket.category_id')
                ->leftJoin('members as c', 'c.id', '=', 'ticket.cs_id')
                ->where('ticket.members_id',Session::get('members_id'))
                ->where('ticket.status_id',1)
                ->get();

        return view('member-area.ticket.index')->with($data);
    }   

    public function ticket_add(){

        $data['category_ticket'] = DB::table('category_ticket')->where('status_id',1)->get();

        return view('member-area.ticket.add')->with($data);
    }    

    public function ticket_action(Request $request){
        $file                = $request->file('file');

        $characters = '0123456789';
        $pin        = mt_rand(0001, 9999).$characters[rand(0, strlen($characters) - 1)];
        $string     = 'TIC-'.str_shuffle($pin);

            
        $data                =  new Ticket();
        $data->uuid          = Str::uuid()->toString();
        $data->ticket_code   = $string;
        $data->subject       = $request->subject;
        $data->desc          = $request->desc;
        $data->ticket_date   = date('Y-m-d');
        $data->category_id   = $request->category_id;
        $data->ticket_status   = 'Waiting';
        $data->members_id    = Session::get('members_id');

        if(!empty($request->file)){
            $nama_file     = $file->getClientOriginalName();
            $tujuan_upload = 'dist/img/ticket';
            $file->move($tujuan_upload,$nama_file);
            $data->filename =  $nama_file;
        }
        $data->save();

        return redirect('pg_ticket')->with(['success' => 'Tiket Berhasil dibuat, Tiket anda akan segera di tangani oleh tim kami. Terima kasih.']);
    }

     public function ticket_detail($code, $uuid){

        $ticket = DB::table('ticket')
                ->select(
                    'ticket.*',
                    'members.first_name',
                    'members.last_name',
                    'members.email',
                    'members.phone',
                    'category_ticket.name as category_name',
                    'c.first_name as cs_first_name',
                    'c.last_name as cs_last_name'
                    )
                ->leftJoin('members', 'members.id', '=', 'ticket.members_id')
                ->leftJoin('category_ticket', 'category_ticket.id', '=', 'ticket.category_id')
                ->leftJoin('members as c', 'c.id', '=', 'ticket.cs_id')
                ->where('ticket.status_id',1)
                ->where('ticket.ticket_code',$code)
                ->where('ticket.uuid',$uuid)
                ->where('ticket.members_id',Session('members_id'))
                ->first();

                $data['ticket'] = $ticket;


                $data['ticket_detail'] = DB::table('ticket_detail')
                ->select(
                    'ticket_detail.*',
                    'members.first_name',
                    'members.last_name',
                    'members.email',
                    'members.phone',
                    'users.roles_id'
                    )
                ->leftJoin('ticket', 'ticket.id', '=', 'ticket_detail.ticket_id')
                ->leftJoin('members', 'members.id', '=', 'ticket_detail.members_id')
                ->leftJoin('users', 'users.members_id', '=', 'members.id')
                ->where('ticket_detail.status_id',1)
                ->where('ticket_detail.ticket_id',$ticket->id)
                ->orderBy('ticket_detail.id','ASC')
                ->get();

         return view('member-area.ticket.detail')->with($data);
    }

     public function ticket_detail_action(Request $request){
        $file                = $request->file('file');

        $data                =  new Ticketdetail();
        $data->ticket_id       = $request->ticket_id;
        $data->desc          = $request->desc;
        $data->members_id    = Session::get('members_id');

        if(!empty($request->file)){
            $nama_file     = $file->getClientOriginalName();
            $tujuan_upload = 'dist/img/ticket';
            $file->move($tujuan_upload,$nama_file);
            $data->filename =  $nama_file;
        }
        $data->save();

         $ticket = DB::table('ticket')->where('id',$request->ticket_id)->first();

        return redirect('pg_ticket_detail/'.$ticket->ticket_code.'/'.$ticket->uuid)->with(['success' => 'Comment anda berhasil dicreate.']);
    }

     public function upgrade_account(){

          $data['premium'] = DB::table('level')->where('status_id',1)->where('id','2')->first();
             
          $check_orders = DB::table('orders')
                        ->select(
                                'orders.*',
                                )
                        ->leftJoin('members', 'members.id', '=', 'orders.members_id')
                        ->where('orders.status_id',1)
                        ->where('orders.expiry',0)
                        ->where('orders.orders_type','Upgrade')
                        ->where('orders.members_id',Session::get('members_id'))
                        ->first();
        $data['check_orders'] = $check_orders;
        $data['members'] = DB::table('members')->where('id',Session::get('members_id'))->where('status_id',1)->first();

        return view('member-area.upgrade_account.index')->with($data);
    }   


     public function confirm_upgrade_action(Request $request){

        $characters = '0123456789';
        $pin        = mt_rand(100000, 999999).$characters[rand(0, strlen($characters) - 1)];
        $string     = 'UPG'.str_shuffle($pin);
        $uuid = Str::uuid()->toString();

        $premium = DB::table('level')->where('status_id',1)->where('id','2')->first();

            $data                   =  new Orders();
            $data->uuid             = $uuid;
            $data->invoice          = $string;
            $data->application_id   = 0;
            $data->members_id       = Session::get('members_id');
            $data->checkout_date    = date('Y-m-d H:i:s');
            $data->expired_date     = date('Y-m-d H:i:s');
            $data->price            = $premium->price-$premium->price_discount;
            $data->paid_date        = $request->paid_date;
            $data->desc             = 'Upgrade Premium';
            $data->payment_status   = 'Verify';
            $data->orders_type       = 'Upgrade';
            $data->save();

            $file                = $request->file('file');

            $data2                =  new Comfirmpayment();
            $data2->uuid          = Str::uuid()->toString();
            $data2->invoice_id    = $data->uuid;
            $data2->invoice       = $data->invoice;
            $data2->name          = $request->name;
            $data2->email         = $request->email;
            $data2->norek_tujuan  = $request->norek_tujuan;
            $data2->rek_name      = $request->rek_name;
            $data2->price         = $data->price;
            $data2->paid_date     = $request->paid_date;
            $data2->desc          = $request->desc;
            $data2->members_id    = $request->members_id;
            if(!empty($request->file)){
                $nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
                $tujuan_upload = 'dist/payment/confirm';
                $file->move($tujuan_upload,$nama_file);
                $data2->filename =  $nama_file;
            }
            $data2->save();

            return redirect('pg_upgrade_account')->with(['success' => 'Konfirmasi pembayaran berhasil']);
            
            
    }

  
}