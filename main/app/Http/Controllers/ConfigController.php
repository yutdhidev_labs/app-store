<?php

 

namespace App\Http\Controllers;

 

use Illuminate\Http\Request;

use DB;

 

class ConfigController extends Controller

{
	public function clearRoute()
    {
        \Artisan::call('route:clear');
    }

    public function clearConfig()
    {
        \Artisan::call('config:clear');
    }

    public function cacheConfig()
    {
        \Artisan::call('route:cache');
    }

    public function cacheRoute()
    {
        \Artisan::call('route:cache');
    }

    public function clearView()
    {
        \Artisan::call('view:clear');
    }
 

}