<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class DownloadfileController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
		        	return $next($request);
	        }
	    });
	}

	public function hit_unduh(Request $request){
		$application = DB::table('application')->where('id',$request->file_id)->first();
		DB::table('application')->where('id',$request->file_id)->update([
						'hit_download'          => $application->hit_download + 1,
				]);

		$member = DB::table('members')->where('id',Session::get('members_id'))->first();
		 DB::table('logs')->insert([
            'name'   => 'Download',
            'desc'   => '<code>'.$member->name.'</code> telah berhasil mengunduh file <code>'.$request->name.'</code>',
            'created_by'   => Session::get('members_id'),
        ]);
	} 
}

