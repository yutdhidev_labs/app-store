<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
class ProfileController extends Controller
{

	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	 	return $next($request);
	        }
	    });
	}



	public function index(){
		return view('admin.profile.index');
	}

	public function change_password_action(Request $request){
			DB::table('users')->where('members_id',$request->id_user)->update([
			'password'          => bcrypt($request->password),
			]);
			return redirect('change_password')->with(['success' => 'Password Berhasil di perbaharui']);
	}

	public function profile(){

		$data['members'] = DB::table('members')
							->select(
								'members.*',
								'users.username'

								)
							->leftJoin('users', 'users.members_id', '=', 'members.id')
							->where('members.status_id',1)
							->where('members.id',session('members_id'))
							->first();

            return view('admin.profile.profile')->with($data);
        }

    public function update_profile(Request $request){
            
            DB::table('members')->where('id',$request->members_id)->update([
                'telp'          => $request->telp,
                'name'          => $request->name,
                'email'         => $request->email,
                'jenis_kelamin' => $request->jenis_kelamin,
                'alamat'        => $request->alamat,
            ]);

            DB::table('users')->where('members_id',$request->members_id)->update([
                'fullname'          => $request->name,
            ]);
            return redirect('profile')->with(['success' => 'Profile Berhasil di perbaharui']);
    }
}