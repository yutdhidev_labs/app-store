<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Application;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
class ApplicationController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){

		$data['app_premium'] = DB::table('application')
								->select(
								'application.*',
								'application_category.name as category_name'

								)
								->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
								->where('application.status_id',1)
								->where('application.app_type','Premium')
								->orderby('application.id','desc')
								->get();

		$data['app_free'] = DB::table('application')
								->select(
								'application.*',
								'application_category.name as category_name'

								)
								->leftJoin('application_category', 'application.category_id', '=', 'application_category.id')
								->where('application.status_id',1)
								->where('application.app_type','Free')
								->orderby('application.id','desc')
								->get();
		return view('admin.application.index')->with($data);
	}  

	function application_json(){
		
		$application = DB::table('application')
					->select(
						'id',
						'first_name as customer_name',
						'email',
						'phone',
						'join_date as date',
						'status_id'
					)
					->where('status_id',1)
					->orderby('id','desc')
					->get();
		echo json_encode($application);

	} 

	public function add(){
		$data['application_category'] = DB::table('application_category')->where('status_id',1)->get();
		return view('admin.application.add')->with($data);
	} 


	public function add_action(Request $request){


		$file       = $request->file('file');
		$file2      = $request->file('file2');

		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTU';
	    $pin        = mt_rand(100000, 999999).$characters[rand(0, strlen($characters) - 1)];
	    $string     = str_shuffle($pin);
	    $uuid = Str::uuid()->toString();

		$data                	=  new Application();
		$data->uuid          	= $uuid;
		$data->appcode          = $string;
		$data->name 			= $request->name;
		$data->category_id 		= $request->category_id;
		$data->desc 			= $request->desc;
		$data->level 			= $request->level;
		$data->app_type 		= $request->app_type;
		$data->status_publish 	= $request->status_publish;
		$data->price 			= $request->price;
		$data->link_video 		= $request->link_video;
		$data->link_web 		= $request->link_web;
		$data->label 			= $request->label;
		$data->created_by 		= Session::get('members_id');
		$data->created_at 		= date('Y-m-d H:i:s');

		if(!empty($request->file)){
			$nama_file     = $file->getClientOriginalName();
			$tujuan_upload = 'dist/app/'.$string.'/'.$uuid;
			$file->move($tujuan_upload,$nama_file);
			$data->filename =  $nama_file;
		}

		if(!empty($request->file2)){
			$nama_file2     = $file2->getClientOriginalName();
			$tujuan_upload2 = 'dist/app/'.$string.'/'.$uuid;
			$file2->move($tujuan_upload2,$nama_file2);
			$data->thumbnail =  $nama_file2;
		}

		$data->save();

		return redirect('application')->with(['success' => 'Application berhasil di create']);
	}


	public function edit($uuid){

		$data['application_category'] = DB::table('application_category')->where('status_id',1)->get();
		$application = DB::table('application')->where('status_id',1)->where('uuid',$uuid)->first();
		$data['application'] = $application;
		return view('admin.application.edit')->with($data);
	}  

	public function edit_action(Request $request){

		$file                = $request->file('file');
		$file2                = $request->file('file2');

		DB::table('application')->where('uuid',$request->code_id)->update([
			'name'  	  		=> $request->name,
			'category_id' 		=> $request->category_id,
			'desc'  	  		=> $request->desc,
			'requirement' 		=> $request->requirement,
			'status_publish' 	=> $request->status_publish,
			'level' 	  		=> $request->level,
			'app_type' 	  		=> $request->app_type,
			'price'  	  		=> $request->price,
			'link_video'  		=> $request->link_video,
			'link_web'    		=> $request->link_web,
			'updated_at'    	=> date('Y-m-d H:i:s'),
		]);

		if(!empty($request->file)){
			$nama_file     = $file->getClientOriginalName();
			$tujuan_upload = 'dist/app/'.$request->appcode.'/'.$request->code_id;
			$file->move($tujuan_upload,$nama_file);
			DB::table('application')->where('uuid',$request->code_id)->update([
				'filename'      => $nama_file,
			]);
		}

		if(!empty($request->file2)){
			$nama_file2     = $file2->getClientOriginalName();
			$tujuan_upload3 = 'dist/app/'.$request->appcode.'/'.$request->code_id;
			$file2->move($tujuan_upload3,$nama_file2);
			DB::table('application')->where('uuid',$request->code_id)->update([
				'thumbnail'      => $nama_file2,
			]);
		}
		return redirect('application')->with(['success' => 'Data Berhasil Diperbaharui']);
		
	}

	public function show($uuid){

		$application = DB::table('application')
						->select(
								'application.*',
								'application_category.name as category_name'

								)
						->leftJoin('application_category', 'application_category.id', '=', 'application.category_id')
						->where('application.status_id',1)
						->where('application.uuid',$uuid)->first();
		$data['application'] = $application;

		$data['orders'] = DB::table('orders')
						->select(
								'orders.*',
								'members.first_name',
								'members.last_name',
								'members.phone',
								'members.email'
								)
						->leftJoin('members', 'members.id', '=', 'orders.members_id')
						->where('orders.status_id',1)
						->where('orders.application_id',$application->id)
						->get();

		$data['history_download'] = DB::table('history_download')
						->select(
								'history_download.*',
								'members.first_name',
								'members.last_name',
								'members.phone',
								'members.email'
								)
						->leftJoin('members', 'members.id', '=', 'history_download.members_id')
						->where('history_download.status_id',1)
						->where('history_download.application_id',$application->id)
						->get();


		$data['logs_activity'] = DB::table('logs_activity')
						->select(
								'logs_activity.*',
								'members.first_name',
								'members.last_name',
								'members.avatar',
								'members.avatar_color',
								'members.phone',
								'members.email',
								'report.report_code',
								'report.subject',
								'report.category',
								'report.desc as report_desc',
								'report.filename as report_filename'
								)
						->leftJoin('members', 'members.id', '=', 'logs_activity.members_id')
						->leftJoin('report', 'report.id', '=', 'logs_activity.report_id')
						->where('logs_activity.status_id',1)
						->where('logs_activity.application_id',$application->id)
						->orderBy('logs_activity.id','desc')
						->get();

		$data['members'] = DB::table('history_download')
						->select(
								'members.first_name',
								'members.last_name',
								'members.avatar_color',
								'members.phone',
								'members.email'
								)
						->leftJoin('members', 'members.id', '=', 'history_download.members_id')
						->where('history_download.status_id',1)
						->where('history_download.application_id',$application->id)
						->groupBy('history_download.members_id')
						->get();

		return view('admin.application.show')->with($data);
	}  


	public function delete(Request $request){
		DB::table('application')->where('uuid',$request->id)->update([
			'status_id'      => 0,
		]);
		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}
}

