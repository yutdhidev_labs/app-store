<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class ApplicationcategoryController extends Controller
{
	public function __construct()
	{
	   $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['application_category'] = DB::table('application_category')->where('status_id',1)->get();
		return view('admin.application_category.index')->with($data);
	}     
	public function action(Request $request){

		if(!empty($request->id_application_category)){
			DB::table('application_category')->where('id',$request->id_application_category)->update([
				'name'   => $request->name,
				'desc' => $request->desc,
			]);
			
			$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Diperbaharui !'
				);
		}else{
			$check = DB::table('application_category')->where('name',$request->name)->count();
			if($check > 0){
				return redirect('application_category')->with(['failed' => 'Data Sudah Ada ! Data Gagal di Simpan.']);
			}else{

				DB::table('application_category')->insert([
					'name'   => $request->name,
					'desc' => $request->desc,
				]);

				$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Disimpan !'
				);
			}
		}
			echo json_encode($result);
	}


	public function delete(Request $request){
		DB::table('application_category')->where('id',$request->id)->update([
				'status_id'   => 0,
			]);
		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}
}

