<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Models\Ticket;
use App\Models\Ticketdetail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
class TicketController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
		        	return $next($request);
	        }
	    });
	}


	public function testing_mail(){
					$data = [
						'status_act'    		=> 'Waiting',
						'no_ticket'      		=> 'TIC-018083',
						'name'          	 	=> 'Yudhi',
						'email'          		=> 'yxlcreative@gmail.com',
		            ];

		            return view('emails.layout_mail')->with($data);

		    //         Mail::send('emails.layout_mail', $data, function ($mail) use($data)
		    //         {
						// $email_send = $data['email'];
						// $name_send  = $data['name'];
						// $subject    = 'Testing';
						// $mail->to($email_send, $name_send);
						// $mail->subject($subject);
		    //         });
	}

	public function index(){

		$query = DB::table('ticket')
							->select(
								'ticket.*',
								'members.name',
								'category.name as category_name',
								'c.name as teknisi_name',
								'a.name as report_by_name',
								'b.name as assign_by_name'
								)
							->leftJoin('members', 'members.id', '=', 'ticket.created_by')
							->leftJoin('category', 'category.id', '=', 'ticket.category_id')
							->leftJoin('department as a', 'a.id', '=', 'ticket.report_by')
							->leftJoin('department as b', 'b.id', '=', 'ticket.assign_by')
							->leftJoin('members as c', 'c.id', '=', 'ticket.teknisi_id')
							->where('ticket.status_id',1);

			if(Session::get('roles_id') == 1){
			  $ticket = $query->where('ticket.created_by',Session::get('members_id'));
			}
			
			$ticket = $query->get();


			$query1 = DB::table('ticket')
                            ->where('ticket.status_id',1)
                            ->where('ticket.ticket_status','Waiting');

            if(Session::get('roles_id') == 1){
              $total_waiting = $query1->where('ticket.created_by',Session::get('members_id'));
            }
            
            $total_waiting = $query1->count();


            $query2 = DB::table('ticket')
                            ->where('ticket.status_id',1)
                            ->where('ticket.ticket_status','Progress');

            if(Session::get('roles_id') == 1){
              $total_open = $query2->where('ticket.created_by',Session::get('members_id'));
            }
            
            $total_open = $query2->count();


             $query3 = DB::table('ticket')
                            ->where('ticket.status_id',1)
                            ->where('ticket.ticket_status','Close');

            if(Session::get('roles_id') == 1){
              $total_close = $query3->where('ticket.created_by',Session::get('members_id'));
            }
            
            $total_close = $query3->count();

             $query4 = DB::table('ticket')
                            ->where('ticket.status_id',1)
                            ->where('ticket.ticket_status','Pending');

            if(Session::get('roles_id') == 1){
              $total_hold = $query4->where('ticket.created_by',Session::get('members_id'));
            }
            
            $total_hold = $query4->count();

        $data['total_waiting'] = $total_waiting;
        $data['total_progress'] = $total_open;
        $data['total_close'] = $total_close;
        $data['total_hold'] = $total_hold;
        $data['total_all'] = $total_waiting + $total_open + $total_close + $total_hold;

		$data['ticket'] = $ticket;
		$data['department'] = DB::table('department')->where('status_id',1)->get();
		$data['category'] = DB::table('category')->where('status_id',1)->get();
		return view('ticket.index')->with($data);
	}     
	public function action(Request $request){
		$file                = $request->file('file');

		if(!empty($request->id_ticket)){
			DB::table('ticket')->where('id',$request->id_ticket)->update([
				'subject'   => $request->subject,
				'desc'   => $request->desc,
			]);

			if(!empty($request->file)){
				$nama_file     = $file->getClientOriginalName();
				$tujuan_upload = 'dist/img/ticket';
				$file->move($tujuan_upload,$nama_file);
				DB::table('ticket')->where('id',$request->id_ticket)->update([
					'filename'      => $nama_file,
				]);
			}
			
			return redirect('ticket')->with(['success' => 'Data Berhasil Diperbaharui']);
		}else{

			 $characters = '0123456789';
            $pin        = mt_rand(0001, 9999).$characters[rand(0, strlen($characters) - 1)];
            $string     = 'TIC-'.str_shuffle($pin);

			
				$data                =  new Ticket();
				$data->uuid          = Str::uuid()->toString();
				$data->ticket_code   = $string;
				$data->subject       = $request->subject;
				$data->desc          = $request->desc;
				$data->report_by     = $request->report_by;
				$data->assign_by     = $request->assign_by;
				$data->ticket_date   = $request->ticket_date;
				$data->category_id   = $request->category_id;
				$data->ticket_status   = 'Waiting';
				$data->created_by    = Session::get('members_id');

				if(!empty($request->file)){
					$nama_file     = $file->getClientOriginalName();
					$tujuan_upload = 'dist/img/ticket';
					$file->move($tujuan_upload,$nama_file);
					$data->filename =  $nama_file;
				}
				$data->save();


				$datEmail = [
						'status_act'    		=> 'Waiting',
						'no_ticket'      		=> $data->ticket_code,
						'name'          	 	=> Session::get('name'),
						'email'          		=> Session::get('email'),
		            ];

		            Mail::send('emails.layout_mail', $datEmail, function ($mail) use($datEmail)
		            {
						$email_send = $datEmail['email'];
						$name_send  = $datEmail['name'];
						$subject    = 'Open Tiket '.$datEmail['no_ticket'];
						$mail->to($email_send, $name_send);
						$mail->subject($subject);
		            });

		            $datEmail2 = [
						'status_act'    		=> 'Accept',
						'no_ticket'      		=> $data->ticket_code,
						'email'          	 	=> 'it@mmnatures.com',
						'name'          		=> 'Teknisi',
		            ];

		            Mail::send('emails.layout_mail', $datEmail2, function ($mail) use($datEmail2)
		            {
						$email_send = $datEmail2['email'];
						$name_send  = $datEmail2['name'];
						$subject    = 'Informasi Open Tiket '.$datEmail2['no_ticket'];
						$mail->to($email_send, $name_send);
						$mail->subject($subject);
		            });

				return redirect('ticket')->with(['success' => 'Data Berhasil Disimpan']);
		}
	}


	public function delete(Request $request){
		DB::table('ticket')->where('id',$request->id)->update([
				'status_id'   => 0,
		]);
		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}

	public function pending(Request $request){

		DB::table('ticket')->where('id',$request->id)->update([
				'ticket_status'   => 'Pending',
		]);


		$getticket = DB::table('ticket')->where('id',$request->id)->first();
		$getmember = DB::table('members')->where('id',$getticket->created_by)->first();

		$datEmail = [
						'status_act'    		=> 'Pending',
						'no_ticket'      		=> $getticket->ticket_code,
						'name'          	 	=> $getmember->name,
						'email'          		=> $getmember->email,
		            ];

        Mail::send('emails.layout_mail', $datEmail, function ($mail) use($datEmail)
        {
			$email_send = $datEmail['email'];
			$name_send  = $datEmail['name'];
			$subject    = 'Pending / Hold Tiket '.$datEmail['no_ticket'];
			$mail->to($email_send, $name_send);
			$mail->subject($subject);
        });



		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}

	public function open(Request $request){

		DB::table('ticket')->where('id',$request->id)->update([
				'ticket_status'   => 'Progress',
				'open_date'   => date('Y-m-d H:i:s'),
				'teknisi_id'   => Session::get('members_id'),
		]);


		$getticket = DB::table('ticket')->where('id',$request->id)->first();
		$getmember = DB::table('members')->where('id',$getticket->created_by)->first();

		$datEmail = [
						'status_act'    		=> 'Progress',
						'no_ticket'      		=> $getticket->ticket_code,
						'name'          	 	=> $getmember->name,
						'email'          		=> $getmember->email,
		            ];

        Mail::send('emails.layout_mail', $datEmail, function ($mail) use($datEmail)
        {
			$email_send = $datEmail['email'];
			$name_send  = $datEmail['name'];
			$subject    = 'Progress Tiket '.$datEmail['no_ticket'];
			$mail->to($email_send, $name_send);
			$mail->subject($subject);
        });

		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}

	public function close(Request $request){
 

		DB::table('ticket')->where('id',$request->id)->update([
				'ticket_status'   => 'Close',
				'close_date'   => date('Y-m-d H:i:s'),
		]);


		$getticket = DB::table('ticket')->where('id',$request->id)->first();
		$getmember = DB::table('members')->where('id',$getticket->created_by)->first();

		$datEmail = [
						'status_act'    		=> 'Close',
						'no_ticket'      		=> $getticket->ticket_code,
						'name'          	 	=> $getmember->name,
						'email'          		=> $getmember->email,
		            ];

        Mail::send('emails.layout_mail', $datEmail, function ($mail) use($datEmail)
        {
			$email_send = $datEmail['email'];
			$name_send  = $datEmail['name'];
			$subject    = 'Close Tiket '.$datEmail['no_ticket'];
			$mail->to($email_send, $name_send);
			$mail->subject($subject);
        });


		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}

	public function detail($uuid){
		$ticket = DB::table('ticket')
							->select(
								'ticket.*',
								'members.name',
								'members.department_id as department_user',
								'category.name as category_name',
								'c.department_id as department_teknisi',
								'c.name as teknisi_name',
								'a.name as report_by_name',
								'b.name as assign_by_name'
								)
							->leftJoin('members', 'members.id', '=', 'ticket.created_by')
							->leftJoin('category', 'category.id', '=', 'ticket.category_id')
							->leftJoin('department as a', 'a.id', '=', 'ticket.report_by')
							->leftJoin('department as b', 'b.id', '=', 'ticket.assign_by')
							->leftJoin('members as c', 'c.id', '=', 'ticket.teknisi_id')
							->where('ticket.status_id',1)
							->where('ticket.uuid',$uuid)
							->first();



		$ticket_detail = DB::table('ticket_detail')
							->select(
								'ticket_detail.*',
								'members.name',
								'department.name as department_name',
								)
							->leftJoin('members', 'members.id', '=', 'ticket_detail.created_by')
							->leftJoin('department', 'department.id', '=', 'members.department_id')
							->where('ticket_detail.status_id',1)
							->where('ticket_detail.ticket_id',$ticket->id)
							->orderby('ticket_detail.id','asc')
							->get();

		$data['ticket'] = $ticket;
		$data['ticket_detail'] = $ticket_detail;
		return view('ticket.detail')->with($data);
	}

	public function ticket_detail_action(Request $request){
		$file                = $request->file('file');

				$data                =  new Ticketdetail();
				$data->ticket_id     = $request->id_ticket;
				$data->desc          = $request->desc;
				$data->created_by    = Session::get('members_id');

				if(!empty($request->file)){
					$nama_file     = $file->getClientOriginalName();
					$tujuan_upload = 'dist/img/ticket';
					$file->move($tujuan_upload,$nama_file);
					$data->filename =  $nama_file;
				}
				$data->save();

				return redirect('detail/'.$request->ticket_uuid)->with(['success' => 'Data Berhasil Disimpan']);
	}
}

