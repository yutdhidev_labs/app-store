<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class DashboardController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {
            if(empty(Session::get('members_id')))
            {
                return redirect('logout');
            }else{
                if(Session::get('roles_id') > 2)
                {
                    return redirect('logout');
                }else{
                    return $next($request);
                }
            }
        });
	}
	
	public function index(){

		date_default_timezone_set("Asia/Jakarta");
			$jam=date("G");
            if($jam>=0&&$jam<=11){
                $salam="PAGI";
            }else if($jam>=12&&$jam<=15){
                $salam="SIANG";
            }else if($jam>=16&&$jam<=18){
                $salam="SORE";
            }else if($jam>=19&&$jam<=23){
                $salam="MALAM";
            }

			$data['salam'] = 'SELAMAT '.$salam;
            $data['members'] = DB::table('members')->where('status_id',1)->where('id',Session('members_id'))->first();

            $data['app_verifiy_count'] = DB::table('orders')->where('status_id',1)->where('payment_status','Verify')->where('orders_type','App')->count();
            $data['app_done_count'] = DB::table('orders')->where('status_id',1)->where('payment_status','Verify')->where('orders_type','App')->count();

            $data['up_verifiy_count'] = DB::table('orders')->where('status_id',1)->where('payment_status','Verify')->where('orders_type','Upgrade')->count();
            $data['up_done_count'] = DB::table('orders')->where('status_id',1)->where('payment_status','Verify')->where('orders_type','Upgrade')->count();
		
		return view('admin.dashboard.index')->with($data);
	}

    public function getchart(Request $request){


        $total_day = array('Days');
        $total_progress = array('Progress');
        $total_close = array('Close');
        $total_waiting = array('Waiting');
        $total_pending = array('Pending');

        $getday = date('d');
        $getday_start = date('d')-5;
        $getday_end = date('d')+1;

        for ($i=$getday_start; $i < $getday_end ; $i++) { 

            if($i < 9){
                $day = '0'.$i;
            }else{
                $day = $i;
            }

            $tanggal = date('Y-m-'.$day);

            $query2 = DB::table('ticket')
                          
                            ->where('ticket.status_id',1)
                            ->where('ticket.ticket_status','Progress')
                            ->whereDate('ticket.ticket_date',$tanggal);

            if(Session::get('roles_id') == 1){
              $total_open = $query2->where('ticket.created_by',Session::get('members_id'));
            }
            
            $total_open = $query2->count();

            $query3 = DB::table('ticket')
                          
                            ->where('ticket.status_id',1)
                            ->where('ticket.ticket_status','Close')
                            ->whereDate('ticket.ticket_date',$tanggal);

            if(Session::get('roles_id') == 1){
              $total_done = $query3->where('ticket.created_by',Session::get('members_id'));
            }
            
            $total_done = $query3->count();

            $query4 = DB::table('ticket')
                          
                            ->where('ticket.status_id',1)
                            ->where('ticket.ticket_status','Pending')
                            ->whereDate('ticket.ticket_date',$tanggal);

            if(Session::get('roles_id') == 1){
              $total_hold = $query4->where('ticket.created_by',Session::get('members_id'));
            }
            
            $total_hold = $query4->count();


            $query5 = DB::table('ticket')
                          
                            ->where('ticket.status_id',1)
                            ->where('ticket.ticket_status','Waiting')
                            ->whereDate('ticket.ticket_date',$tanggal);

            if(Session::get('roles_id') == 1){
              $total_wait = $query5->where('ticket.created_by',Session::get('members_id'));
            }
            
            $total_wait = $query5->count();

            if($total_open > 0){
                $total_progress[] =  $total_open;
            }else{
                $total_progress[] =  0;
            }

            if($total_done > 0){
                $total_close[] =  $total_done;
            }else{
                $total_close[] =  0;
            }

            if($total_wait > 0){
                $total_waiting[] =  $total_wait;
            }else{
                $total_waiting[] =  0;
            }

            if($total_hold > 0){
                $total_pending[] =  $total_hold;
            }else{
                $total_pending[] =  0;
            }

            $total_day[] =  date('d', strtotime($tanggal));

        }

        $result = array(
            'total_waiting' => $total_waiting,
            'total_pending' => $total_pending,
            'total_progress' => $total_progress,
            'total_close'   => $total_close,
            'total_day'     => $total_day,
        );

        echo json_encode($result);
    }

}