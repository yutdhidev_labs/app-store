<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class AboutsController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}


	public function index(){
		$data['abouts'] = DB::table('abouts')->where('id',1)->first();
		return view('admin.abouts.index')->with($data);
	}     
	public function action(Request $request){

			DB::table('abouts')->where('id',1)->update([
				'name'   		=> $request->name,
				'tagline'   	=> $request->tagline,
				'short_desc'   	=> $request->short_desc,
				'long_desc'   	=> $request->long_desc,
			]);
			
			$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Diperbaharui !'
				);
		
			echo json_encode($result);
	}
}

