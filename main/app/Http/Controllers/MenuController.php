<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class menuController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 1)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['menu'] 	   = DB::table('menu')->where('status_active','1')->get();
		$data['main_menu'] = DB::table('menu')->where('current',1)->where('status_active','1')->get();
		$data['parent_menu']    = DB::table('menu')->where('current',2)->where('status_active','1')->get();
		return view('admin.master.menu.index')->with($data);
	}     
	public function action(Request $request){

		if(!empty($request->id_menu)){
			DB::table('menu')->where('id',$request->id_menu)->update([
				'name'   => $request->name,
				'desc' => $request->desc,
			]);
			
			$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Diperbaharui !'
				);
		}else{
				if($request->type_menu == 'main'){
					DB::table('menu')->insert([
						'display_name'	=> $request->display_name,
						'sort_menu' 	=> $request->sort_menu,
						'url_func' 		=> '#',
						'fa_icon' 		=> '',
						'parent_id' 	=> '0',
						'current' 		=> '1',
					]);

				}else if($request->type_menu == 'parent'){
					DB::table('menu')->insert([
						'display_name'	=> $request->display_name,
						'sort_menu' 	=> $request->sort_menu,
						'url_func' 		=> '#',
						'fa_icon' 		=> $request->fa_icon,
						'parent_id' 	=> $request->main_menu,
						'current' 		=> '2',
					]);

				}else if($request->type_menu == 'child'){

					if($request->parent == 0){
						DB::table('menu')->insert([
							'display_name'	=> $request->display_name,
							'sort_menu' 	=> $request->sort_menu,
							'url_func' 		=> $request->url_func,
							'fa_icon' 		=> $request->fa_icon,
							'parent_id' 	=> $request->main_menu,
							'current' 		=> '0',
						]);
					}else{
						DB::table('menu')->insert([
							'display_name'	=> $request->display_name,
							'sort_menu' 	=> $request->sort_menu,
							'url_func' 		=> $request->url_func,
							'fa_icon' 		=> $request->fa_icon,
							'parent_id' 	=> $request->parent,
							'current' 		=> '0',
						]);
					}
					
				}

				

				$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Disimpan !'
				);
		}
			echo json_encode($result);
	}


	public function delete(Request $request){
		DB::table('menu')->where('id', $request->id)->delete();
		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}
}

