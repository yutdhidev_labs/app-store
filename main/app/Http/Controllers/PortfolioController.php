<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Portfolio;
use DB;
use Illuminate\Support\Facades\Session;
class PortfolioController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['portfolio'] = DB::table('portfolio')->where('status_id',1)->get();
		return view('admin.master.portfolio.index')->with($data);
	}     
	public function action(Request $request){

		$file                = $request->file('file');

		if(!empty($request->id_portfolio)){
			DB::table('portfolio')->where('id',$request->id_portfolio)->update([
				'name'   => $request->name,
				'desc'   => $request->desc,
			]);

			if(!empty($request->file)){
				$nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
				$tujuan_upload = 'dist/img/portfolio';
				$file->move($tujuan_upload,$nama_file);
				DB::table('portfolio')->where('id',$request->id_portfolio)->update([
					'filename'      => $nama_file,
				]);
			}
			return redirect('portfolio')->with(['success' => 'Data Berhasil Diperbaharui']);
		}else{
			
			$data                =  new Portfolio();
			$data->name          = $request->name;
			$data->desc          = $request->desc;
			if(!empty($request->file)){
				$nama_file     = date('YmdHis').'_'.$file->getClientOriginalName();
				$tujuan_upload = 'dist/img/portfolio';
				$file->move($tujuan_upload,$nama_file);
				$data->filename =  $nama_file;
			}
			$data->save();
			
			return redirect('portfolio')->with(['success' => 'Data Berhasil Disimpan']);
			
		}
	}


	public function delete(Request $request){
		DB::table('portfolio')->where('id',$request->id)->update([
			'status_id'      => 0,
		]);

		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}
}

