<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class FaqsController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['faqs'] = DB::table('faqs')->where('status_id',1)->get();
		return view('admin.master.faqs.index')->with($data);
	}     
	public function action(Request $request){

		if(!empty($request->id_faqs)){
			DB::table('faqs')->where('id',$request->id_faqs)->update([
				'name'   => $request->name,
				'desc' => $request->desc,
			]);
			
			$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Diperbaharui !'
				);
		}else{
			$check = DB::table('faqs')->where('name',$request->name)->count();
			if($check > 0){
				return redirect('faqs')->with(['failed' => 'Data Sudah Ada ! Data Gagal di Simpan.']);
			}else{

				DB::table('faqs')->insert([
					'name'   => $request->name,
					'desc' => $request->desc,
				]);

				$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Disimpan !'
				);
			}
		}
			echo json_encode($result);
	}


	public function delete(Request $request){
		DB::table('faqs')->where('id',$request->id)->update([
				'status_id'   => 0,
			]);
		$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Dihapus !'
				);
		echo json_encode($result);
	}
}

