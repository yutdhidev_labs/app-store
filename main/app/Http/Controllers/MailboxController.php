<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class MailboxController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {
            if(empty(Session::get('members_id')))
            {
                return redirect('logout');
            }else{
                if(Session::get('roles_id') > 2)
                {
                    return redirect('logout');
                }else{
                    return $next($request);
                }
            }
        });
	}
	
	public function index(){
            $data['members'] = DB::table('members')->where('status_id',1)->where('id',Session('members_id'))->first();
		return view('admin.mailbox.index')->with($data);
	}

    function mailbox_json(){
        
        // $mailbox = DB::table('mailbox')
        //             ->select(

        //                     'id',
        //                     DB::Raw('true as starred'),
        //                     DB::Raw('true as readed'),
        //                     'name',
        //                     'subject as title',
        //                     'desc as description',
        //                     'created_at as date',
        //                     DB::Raw('Inbox as tabtype'),
        //                     DB::Raw('# as userImg'),
        //                     DB::Raw('Support as labeltype')
        //             )
        //             ->where('status_id',1)
        //             ->get();

        //             dd($mailbox);
        // echo json_encode($application);

    } 

}