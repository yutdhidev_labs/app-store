<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class LogsController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {
			if(empty(Session::get('members_id')))
			{
				return redirect('logout');
			}else{
				return $next($request);
			}
		});
	}
	
	public function index(){

		$data['logs'] = DB::table('logs')
						->select('logs.*','members.name')
						->leftJoin('members', 'members.id', '=', 'logs.created_by')
						->where('logs.status_id',1)
						->where('logs.created_by',Session::get('members_id'))
						->get();

		return view('admin.logs.index')->with($data);
	}

}