<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
class ContactsController extends Controller
{
	public function __construct()
	{
	    $this->middleware(function ($request, $next) {
	        if(empty(Session::get('members_id')))
	        {
	            return redirect('logout');
	        }else{
	        	if(Session::get('roles_id') > 2)
		        {
		            return redirect('logout');
		        }else{
		        	return $next($request);
		        }
	        }
	    });
	}

	public function index(){
		$data['contacts'] = DB::table('contacts')->where('id',1)->first();
		return view('admin.contacts.index')->with($data);
	}     
	public function action(Request $request){

			DB::table('contacts')->where('id',1)->update([
				'name'   		=> $request->name,
				'building'   	=> $request->building,
				'phone'   		=> $request->phone,
				'address'   	=> $request->address,
				'city'   		=> $request->city,
				'postal_code'   => $request->postal_code,
				'country'   	=> $request->country,
				'facebook'   	=> $request->facebook,
				'instagram'   	=> $request->instagram,
				'linkedin'   	=> $request->linkedin,
				'youtube'   	=> $request->youtube,
				'telegram'   	=> $request->telegram,
			]);
			
			$result = array(
					'status' => 'OK',
					'title'  => 'Berhasil',
					'msg'    => 'Data Berhasil Diperbaharui !'
				);
		
			echo json_encode($result);
	}
}

