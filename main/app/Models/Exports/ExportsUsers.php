<?php

 namespace App\Models\Exports;
  
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
  
class ExportsUsers implements FromCollection, WithHeadings, ShouldAutoSize
{

protected $jenis;
public function __construct($value =null)
{

             $data = array_merge(compact('value'));
             $this->getdata =  $data['value'];
}

   public function collection()
    {       

             $data = DB::table('members')
                            ->select(
                                'members.name',
                                'members.email',
                                'members.jenis_kelamin',
                                'members.position'
                                )
                            ->leftJoin('users', 'users.members_id', '=', 'members.id')
                            ->where('members.status_id',1)
                            ->where('users.status_id',1)
                            ->get();

           return $data;
    }

    public function headings(): array
    {
       
        return [
                'Nama',
                'Email',
                'Gender',
                'Jabatan'
              ];
    }

}