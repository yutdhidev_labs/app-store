<?php

 namespace App\Models\Exports;
  
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
  
class ExportsTiket implements FromCollection, WithHeadings, ShouldAutoSize
{

protected $start_date;
protected $end_date;
protected $ticket_status;
public function __construct($value =null)
{

             $data = array_merge(compact('value'));
             $this->getdata =  $data['value'];
}

   public function collection()
    {       

           $query = DB::table('ticket')
                            ->select(
                                'ticket.ticket_code',
                                'ticket.ticket_date',
                                'ticket.subject',
                                'category.name as category_name',
                                'members.name',
                                'a.name as report_by_name',
                                'b.name as assign_by_name',
                                'c.name as teknisi_name',
                                'ticket.ticket_status',
                                )
                            ->leftJoin('members', 'members.id', '=', 'ticket.created_by')
                            ->leftJoin('category', 'category.id', '=', 'ticket.category_id')
                            ->leftJoin('department as a', 'a.id', '=', 'ticket.report_by')
                            ->leftJoin('department as b', 'b.id', '=', 'ticket.assign_by')
                            ->leftJoin('members as c', 'c.id', '=', 'ticket.teknisi_id')
                            ->where('ticket.status_id',1)
                            ->whereBetween('ticket.ticket_date', [$this->getdata['start_date'], $this->getdata['end_date']]);

            if($this->getdata['ticket_status'] != 'All'){
              $data = $query->where('ticket.ticket_status',$this->getdata['ticket_status']);
            }
            
            $data = $query->get();

           return $data;
    }

    public function headings(): array
    {
       
        return [
                'Kode Tiket',
                'Tanggal',
                'Subject',
                'Kategori',
                'Report By',
                'Report Department',
                'Assign Department',
                'Teknisi',
                'Status'
              ];
    }

}