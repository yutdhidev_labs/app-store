<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Application extends Model
{
    public $table = "application";
}
