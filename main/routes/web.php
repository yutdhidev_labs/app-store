<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear/route', 'ConfigController@clearRoute');
Route::get('/clear/config', 'ConfigController@clearConfig');
Route::get('/cache/config', 'ConfigController@cacheConfig');
Route::get('/cache/route', 'ConfigController@cacheRoute');
Route::get('/clear/view', 'ConfigController@clearView');

Route::get('/', 'HomeController@index');
Route::post('pg_contacts/action', 'HomeController@contacts_action');
Route::get('deviceip/{id}', 'LoginController@deviceip');
Route::get('testmail', 'LoginController@testmail');
Route::get('login', 'LoginController@index');
Route::get('register', 'LoginController@register');
Route::post('register/action', 'LoginController@registerPost');
Route::post('auth/log/access', 'LoginController@loginPost');
Route::get('logout', 'LoginController@logout');
Route::get('forgot-password', 'LoginController@forgot');
Route::post('auth/forgot/access', 'LoginController@forgotPost');


Route::get('icon', 'MemberareaController@icon');
Route::get('pg_dashboard', 'MemberareaController@dashboard');
Route::get('pg_myapp', 'MemberareaController@myapp');
Route::get('pg_application', 'MemberareaController@application');
Route::get('pg_application/{id}', 'MemberareaController@application_detail');
Route::get('pg_application/cart/{id}/{appcode}', 'MemberareaController@cart');
Route::get('pg_application/checkout/{id}/{appcode}', 'MemberareaController@checkout');
Route::post('pg_application/cart/action', 'MemberareaController@cart_action');
Route::post('pg_application_json', 'MemberareaController@application_json');
Route::post('pg_myapp_json', 'MemberareaController@myapp_json');
Route::get('pg_application/me', 'MemberareaController@application_me');
Route::get('pg_profile', 'MemberareaController@profile');
Route::post('pg_profile/update', 'MemberareaController@update_profile');
Route::post('pg_password/update', 'MemberareaController@update_password');
Route::post('pg_wilayah_json', 'MemberareaController@wilayah_json');
Route::post('confirm/action', 'MemberareaController@confirm_action');
Route::get('pg_transaction', 'MemberareaController@transaction');
Route::get('pg_faqs', 'MemberareaController@faqs');
Route::get('pg_pricing', 'MemberareaController@pricing');
Route::post('testimoni/action', 'MemberareaController@form_review');
Route::post('report_testimoni/action', 'MemberareaController@form_report');
Route::post('transaction/delete', 'MemberareaController@transaction_delete');

Route::get('pg_ticket', 'MemberareaController@ticket');
Route::get('pg_ticket/add', 'MemberareaController@ticket_add');
Route::get('pg_ticket_detail/{code}/{id}', 'MemberareaController@ticket_detail');
Route::post('pg_ticket/action', 'MemberareaController@ticket_action');
Route::post('pg_ticket_detail/action', 'MemberareaController@ticket_detail_action');
Route::post('pg_ticket/delete', 'MemberareaController@ticket_delete');


Route::get('pg_upgrade_account', 'MemberareaController@upgrade_account');
Route::post('confirm_upgrade/action', 'MemberareaController@confirm_upgrade_action');



Route::get('change_password', 'ProfileController@index');
Route::post('change_password/action', 'ProfileController@change_password_action');

 // ADMIN
Route::get('logs', 'LogsController@index');
Route::get('dashboard', 'DashboardController@index');
Route::post('getchart', 'DashboardController@getchart');
Route::post('getchart2', 'DashboardController@getchart2');
Route::get('profile', 'ProfileController@profile');
Route::post('change_profile/action', 'ProfileController@update_profile');

Route::get('mailbox', 'MailboxController@index');
Route::get('mailbox_json', 'MailboxController@mailbox_json');

Route::get('transaction', 'TransactionController@index');
Route::get('transaction_upgrade', 'TransactionController@upgrade');
Route::get('verify_payment', 'TransactionController@verifiy');
Route::get('verify_payment_upgrade', 'TransactionController@verify_upgrade');
Route::post('verify/action', 'TransactionController@verify_action');

Route::get('users', 'UsersController@index');
Route::post('users/action', 'UsersController@action');
Route::post('users/delete', 'UsersController@delete');
Route::get('users/pdf', 'UsersController@print_pdf');
Route::get('users/excel', 'UsersController@unduh_excel');

Route::get('testing_mail', 'TicketController@testing_mail');
Route::get('ticket', 'TicketController@index');
Route::get('detail/{id}', 'TicketController@detail');
Route::post('ticket_detail/action', 'TicketController@ticket_detail_action');
Route::post('ticket/delete', 'TicketController@delete');
Route::post('ticket/open', 'TicketController@open');
Route::post('ticket/close', 'TicketController@close');
Route::post('ticket/pending', 'TicketController@pending');

Route::get('category', 'CategoryController@index');
Route::post('category/action', 'CategoryController@action');
Route::post('category/delete', 'CategoryController@delete');

Route::get('services', 'ServicesController@index');
Route::post('services/action', 'ServicesController@action');
Route::post('services/delete', 'ServicesController@delete');

Route::get('sliders', 'SlidersController@index');
Route::post('sliders/action', 'SlidersController@action');
Route::post('sliders/delete', 'SlidersController@delete');

Route::get('gallery', 'GalleryController@index');
Route::post('gallery/action', 'GalleryController@action');
Route::post('gallery/delete', 'GalleryController@delete');

Route::get('portfolio', 'PortfolioController@index');
Route::post('portfolio/action', 'PortfolioController@action');
Route::post('portfolio/delete', 'PortfolioController@delete');

Route::get('faqs', 'FaqsController@index');
Route::post('faqs/action', 'FaqsController@action');
Route::post('faqs/delete', 'FaqsController@delete');

Route::get('client', 'ClientController@index');
Route::post('client/action', 'ClientController@action');
Route::post('client/delete', 'ClientController@delete');

Route::get('membership/members', 'MembersController@index');
Route::get('membership/members/add', 'MembersController@add');
Route::get('membership/members/edit/{id}', 'MembersController@edit');
Route::post('membership/members/action', 'MembersController@action');
Route::post('membership/members/delete', 'MembersController@delete');
Route::post('membership/members/update', 'MembersController@edit_action');
Route::post('membership/wilayah_json', 'MembersController@wilayah_json');
Route::get('membership/members_json', 'MembersController@members_json');

Route::get('application_category', 'ApplicationcategoryController@index');
Route::post('application_category/action', 'ApplicationcategoryController@action');
Route::post('application_category/delete', 'ApplicationcategoryController@delete');

Route::get('application', 'ApplicationController@index');
Route::get('application/add', 'ApplicationController@add');
Route::get('application/edit/{id}', 'ApplicationController@edit');
Route::get('application/show/{id}', 'ApplicationController@show');
Route::post('application/action', 'ApplicationController@add_action');
Route::post('application/delete', 'ApplicationController@delete');
Route::post('application/update', 'ApplicationController@edit_action');

Route::get('membership/level', 'LevelController@index');
Route::post('membership/level/action', 'LevelController@action');
Route::post('membership/level/delete', 'LevelController@delete');

Route::get('contacts', 'ContactsController@index');
Route::post('contacts/action', 'ContactsController@action');

Route::get('department', 'DepartmentController@index');
Route::post('department/action', 'DepartmentController@action');

Route::get('abouts', 'AboutsController@index');
Route::post('abouts/action', 'AboutsController@action');


Route::get('report', 'ReportController@index');
Route::get('report_excel/{start}/{end}/{status}', 'ReportController@report_excel');
Route::get('report_pdf/{start}/{end}/{status}', 'ReportController@report_pdf');
Route::post('report/action', 'ReportController@action');

Route::get('roles', 'RolesController@index');
Route::post('roles/action', 'RolesController@action');
Route::post('roles/delete', 'RolesController@delete');

Route::get('menu', 'MenuController@index');
Route::post('menu/action', 'MenuController@action');
Route::post('menu/delete', 'MenuController@delete');

Route::get('file_manager', 'UploadfileController@index');
Route::post('upload_file/action', 'UploadfileController@action');
Route::post('upload_file/delete', 'UploadfileController@delete');

Route::get('file_manager/{category}', 'DownloadfileController@file_manager');

Route::get('download_file', 'DownloadfileController@index');
Route::post('hit_unduh', 'DownloadfileController@hit_unduh');


