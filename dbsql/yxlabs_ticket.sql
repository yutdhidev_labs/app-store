/*
 Navicat Premium Data Transfer

 Source Server         : Mamp
 Source Server Type    : MySQL
 Source Server Version : 50734 (5.7.34)
 Source Host           : localhost:8889
 Source Schema         : yxlabs_ticket

 Target Server Type    : MySQL
 Target Server Version : 50734 (5.7.34)
 File Encoding         : 65001

 Date: 07/07/2023 13:56:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for abouts
-- ----------------------------
DROP TABLE IF EXISTS `abouts`;
CREATE TABLE `abouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `short_desc` text,
  `long_desc` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of abouts
-- ----------------------------
BEGIN;
INSERT INTO `abouts` (`id`, `name`, `tagline`, `short_desc`, `long_desc`, `created_at`, `updated_at`) VALUES (1, 'Guestbook System', 'Guestbook System', 'Guestbook System', 'Guestbook System', '2023-06-19 01:20:21', '2023-06-19 01:20:21');
COMMIT;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of category
-- ----------------------------
BEGIN;
INSERT INTO `category` (`id`, `name`, `desc`, `status_id`, `created_at`, `updated_at`) VALUES (1, 'Mengantar Barang / Paket', 'Mengantar Barang / Paket', 1, '2023-06-19 01:19:04', '2023-06-19 01:19:04');
INSERT INTO `category` (`id`, `name`, `desc`, `status_id`, `created_at`, `updated_at`) VALUES (2, 'Mengurus Perizinan', 'Mengurus Perizinan', 1, '2023-06-19 01:19:16', '2023-06-19 01:19:16');
INSERT INTO `category` (`id`, `name`, `desc`, `status_id`, `created_at`, `updated_at`) VALUES (3, 'Rapat', 'Rapat', 1, '2023-06-19 01:19:25', '2023-06-19 01:19:25');
INSERT INTO `category` (`id`, `name`, `desc`, `status_id`, `created_at`, `updated_at`) VALUES (4, 'Penawaran', 'Penawaran', 1, '2023-06-19 01:19:36', '2023-06-19 01:19:36');
INSERT INTO `category` (`id`, `name`, `desc`, `status_id`, `created_at`, `updated_at`) VALUES (5, 'Konsultasi', 'Konsultasi', 1, '2023-06-19 01:19:44', '2023-06-19 01:19:44');
COMMIT;

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `building` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `postal_code` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `linkedin` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `telegram` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contacts
-- ----------------------------
BEGIN;
INSERT INTO `contacts` (`id`, `name`, `phone`, `building`, `address`, `city`, `postal_code`, `country`, `facebook`, `instagram`, `linkedin`, `twitter`, `youtube`, `telegram`, `created_at`, `updated_at`) VALUES (1, 'PT. Merpati Mahardika', '+62 21 000000', 'Gedung ABC', 'Jl. Palmerah', 'Jakarta Barat', '12000', 'Indonesia', '#', '#', NULL, 'https://twitter.com/tribunnetwork', '#', '#', '2023-06-19 01:20:08', NULL);
COMMIT;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of department
-- ----------------------------
BEGIN;
INSERT INTO `department` (`id`, `name`, `desc`, `status_id`, `created_at`, `updated_at`) VALUES (1, 'Divisi Keuangan', 'Divisi Keuangan', 1, '2023-06-11 16:21:37', '2023-06-11 16:21:37');
INSERT INTO `department` (`id`, `name`, `desc`, `status_id`, `created_at`, `updated_at`) VALUES (2, 'HR Unit', 'HR Unit', 1, '2023-06-11 16:22:00', '2023-06-11 16:22:00');
INSERT INTO `department` (`id`, `name`, `desc`, `status_id`, `created_at`, `updated_at`) VALUES (3, 'Divisi IT', 'Divisi IT', 1, '2023-06-11 16:22:29', '2023-06-11 16:22:29');
COMMIT;

-- ----------------------------
-- Table structure for file_manager
-- ----------------------------
DROP TABLE IF EXISTS `file_manager`;
CREATE TABLE `file_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `upload_date` datetime DEFAULT NULL,
  `hit` int(11) DEFAULT '0',
  `status_id` int(11) DEFAULT '1',
  `created_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of file_manager
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for history_download
-- ----------------------------
DROP TABLE IF EXISTS `history_download`;
CREATE TABLE `history_download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `file_manager_id` int(11) DEFAULT '0',
  `download_date` datetime DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of history_download
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `desc` text,
  `created_by` varchar(255) DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logs
-- ----------------------------
BEGIN;
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (1, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-11 16:06:08', '2023-06-11 16:06:08');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (2, 'Login', 'Anda berhasil <code>Logout</code>', '2', 1, '2023-06-11 17:20:28', '2023-06-11 17:20:28');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (3, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>YUDHI</code>', '4', 1, '2023-06-11 17:21:25', '2023-06-11 17:21:25');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (4, 'Login', 'Anda berhasil <code>Logout</code>', '4', 1, '2023-06-11 17:56:15', '2023-06-11 17:56:15');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (5, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>YUDHI</code>', '4', 1, '2023-06-11 17:56:36', '2023-06-11 17:56:36');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (6, 'Login', 'Anda berhasil <code>Logout</code>', '4', 1, '2023-06-11 18:00:13', '2023-06-11 18:00:13');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (7, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Andi</code>', '8', 1, '2023-06-11 18:00:37', '2023-06-11 18:00:37');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (8, 'Login', 'Anda berhasil <code>Logout</code>', '8', 1, '2023-06-11 18:20:42', '2023-06-11 18:20:42');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (9, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>YUDHI</code>', '4', 1, '2023-06-11 18:20:47', '2023-06-11 18:20:47');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (10, 'Login', 'Anda berhasil <code>Logout</code>', '4', 1, '2023-06-11 19:42:36', '2023-06-11 19:42:36');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (11, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-11 19:42:43', '2023-06-11 19:42:43');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (12, 'Login', 'Anda berhasil <code>Logout</code>', NULL, 1, '2023-06-12 08:24:46', '2023-06-12 08:24:46');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (13, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>YUDHI</code>', '4', 1, '2023-06-12 08:39:19', '2023-06-12 08:39:19');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (14, 'Login', 'Anda berhasil <code>Logout</code>', '4', 1, '2023-06-12 10:24:19', '2023-06-12 10:24:19');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (15, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-12 10:24:26', '2023-06-12 10:24:26');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (16, 'Login', 'Anda berhasil <code>Logout</code>', '2', 1, '2023-06-12 10:57:29', '2023-06-12 10:57:29');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (17, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>YUDHI</code>', '4', 1, '2023-06-12 10:57:35', '2023-06-12 10:57:35');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (18, 'Login', 'Anda berhasil <code>Logout</code>', '4', 1, '2023-06-12 11:41:56', '2023-06-12 11:41:56');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (19, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-12 11:42:01', '2023-06-12 11:42:01');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (20, 'Login', 'Anda berhasil <code>Logout</code>', NULL, 1, '2023-06-12 13:48:39', '2023-06-12 13:48:39');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (21, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-12 13:52:02', '2023-06-12 13:52:02');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (22, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-12 14:19:25', '2023-06-12 14:19:25');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (23, 'Login', 'Anda berhasil <code>Logout</code>', '2', 1, '2023-06-12 14:23:09', '2023-06-12 14:23:09');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (24, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Andi</code>', '8', 1, '2023-06-12 14:23:27', '2023-06-12 14:23:27');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (25, 'Login', 'Anda berhasil <code>Logout</code>', NULL, 1, '2023-06-13 15:30:31', '2023-06-13 15:30:31');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (26, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-13 15:30:38', '2023-06-13 15:30:38');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (27, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Andi</code>', '8', 1, '2023-06-13 21:13:25', '2023-06-13 21:13:25');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (28, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-15 08:31:49', '2023-06-15 08:31:49');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (29, 'Login', 'Anda berhasil <code>Logout</code>', '2', 1, '2023-06-15 08:32:04', '2023-06-15 08:32:04');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (30, 'Login', 'Anda berhasil <code>Logout</code>', '10', 1, '2023-06-15 09:14:47', '2023-06-15 09:14:47');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (31, 'Login', 'Anda berhasil <code>Logout</code>', '11', 1, '2023-06-15 09:15:48', '2023-06-15 09:15:48');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (32, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>ELEARNING</code>', '11', 1, '2023-06-15 09:15:57', '2023-06-15 09:15:57');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (33, 'Login', 'Anda berhasil <code>Logout</code>', '11', 1, '2023-06-15 09:15:59', '2023-06-15 09:15:59');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (34, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-15 09:38:58', '2023-06-15 09:38:58');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (35, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-15 09:51:46', '2023-06-15 09:51:46');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (36, 'Login', 'Anda berhasil <code>Logout</code>', '2', 1, '2023-06-15 09:53:32', '2023-06-15 09:53:32');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (37, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Andi</code>', '8', 1, '2023-06-15 09:53:40', '2023-06-15 09:53:40');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (38, 'Login', 'Anda berhasil <code>Logout</code>', '8', 1, '2023-06-15 09:53:57', '2023-06-15 09:53:57');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (39, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>YUDHI</code>', '4', 1, '2023-06-15 09:54:34', '2023-06-15 09:54:34');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (40, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-19 01:09:19', '2023-06-19 01:09:19');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (41, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-06-19 02:26:13', '2023-06-19 02:26:13');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (42, 'Login', 'Anda berhasil <code>Logout</code>', '2', 1, '2023-06-19 02:26:25', '2023-06-19 02:26:25');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (43, 'Login', 'Anda berhasil <code>Login</code> dengan akun <code>Admin</code>', '2', 1, '2023-07-03 13:26:55', '2023-07-03 13:26:55');
INSERT INTO `logs` (`id`, `name`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (44, 'Login', 'Anda berhasil <code>Logout</code>', '2', 1, '2023-07-03 13:27:03', '2023-07-03 13:27:03');
COMMIT;

-- ----------------------------
-- Table structure for members
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(50) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `position` varchar(100) DEFAULT NULL,
  `alamat` text,
  `kota_id` int(11) DEFAULT NULL,
  `kecamatan_id` int(11) DEFAULT NULL,
  `kelurahan_id` int(11) DEFAULT NULL,
  `register_date` datetime DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of members
-- ----------------------------
BEGIN;
INSERT INTO `members` (`id`, `uuid`, `name`, `telp`, `email`, `jenis_kelamin`, `department_id`, `position`, `alamat`, `kota_id`, `kecamatan_id`, `kelurahan_id`, `register_date`, `status_id`, `created_at`, `updated_at`) VALUES (2, '730e13ed-7197-4a59-845e-cb12f20aa9fb', 'Admin', '087724434193', 'admin@admin.com', 'Laki-Laki', 2, NULL, NULL, NULL, NULL, NULL, '2023-03-01 09:24:27', 1, '2023-06-11 17:16:40', '2023-06-11 17:16:40');
INSERT INTO `members` (`id`, `uuid`, `name`, `telp`, `email`, `jenis_kelamin`, `department_id`, `position`, `alamat`, `kota_id`, `kecamatan_id`, `kelurahan_id`, `register_date`, `status_id`, `created_at`, `updated_at`) VALUES (4, '18200703-4da6-4012-919f-6fa2ac0e3bc1', 'YUDHI', '087724434193', 'yxlcreative@gmail.com2', 'Laki-Laki', 1, NULL, 'Jakarta', NULL, NULL, NULL, '2023-03-29 02:23:46', 1, '2023-06-12 11:42:16', '2023-06-12 11:42:16');
INSERT INTO `members` (`id`, `uuid`, `name`, `telp`, `email`, `jenis_kelamin`, `department_id`, `position`, `alamat`, `kota_id`, `kecamatan_id`, `kelurahan_id`, `register_date`, `status_id`, `created_at`, `updated_at`) VALUES (5, '8665e5e3-6fca-426d-9fdd-5cf56a76ce6b', 'YUDHI', '085647817992', 'yutdhidev@gmail.com', 'Laki-Laki', NULL, NULL, NULL, NULL, NULL, NULL, '2023-04-03 02:36:47', 1, '2023-04-03 02:36:47', '2023-04-03 02:36:47');
INSERT INTO `members` (`id`, `uuid`, `name`, `telp`, `email`, `jenis_kelamin`, `department_id`, `position`, `alamat`, `kota_id`, `kecamatan_id`, `kelurahan_id`, `register_date`, `status_id`, `created_at`, `updated_at`) VALUES (6, '45812a69-7fef-4b72-8213-ffea8c448c31', 'MAHAD', '0856478179921', 'admin@admin.com', 'Perempuan', NULL, NULL, NULL, NULL, NULL, NULL, '2023-04-03 02:39:08', 1, '2023-04-03 02:39:08', '2023-04-03 02:39:08');
INSERT INTO `members` (`id`, `uuid`, `name`, `telp`, `email`, `jenis_kelamin`, `department_id`, `position`, `alamat`, `kota_id`, `kecamatan_id`, `kelurahan_id`, `register_date`, `status_id`, `created_at`, `updated_at`) VALUES (7, 'd1829f66-4898-4fd7-934a-ee2c5b4d75b9', 'KAJIMU', '08564781799211', 'yutdhidev@gmail.com', 'Laki-Laki', NULL, NULL, NULL, NULL, NULL, NULL, '2023-04-03 02:40:32', 1, '2023-04-03 02:40:32', '2023-04-03 02:40:32');
INSERT INTO `members` (`id`, `uuid`, `name`, `telp`, `email`, `jenis_kelamin`, `department_id`, `position`, `alamat`, `kota_id`, `kecamatan_id`, `kelurahan_id`, `register_date`, `status_id`, `created_at`, `updated_at`) VALUES (8, 'f030eb14-5fea-4224-b771-a3b17301bcf5', 'Andi', '131313131', 'a@hjahd.com', 'Laki-Laki', 3, NULL, 'test', NULL, NULL, NULL, NULL, 1, '2023-06-11 17:17:18', '2023-06-11 17:17:18');
INSERT INTO `members` (`id`, `uuid`, `name`, `telp`, `email`, `jenis_kelamin`, `department_id`, `position`, `alamat`, `kota_id`, `kecamatan_id`, `kelurahan_id`, `register_date`, `status_id`, `created_at`, `updated_at`) VALUES (10, 'ecb2960e-33d9-4c97-8cc3-ab524e386376', 'YUDHI', '085647817992', 'yutdhidev2@gmail.com', 'Laki-Laki', 1, NULL, 'Loream Ipsum', NULL, NULL, NULL, NULL, 1, '2023-06-15 09:14:20', '2023-06-15 09:14:20');
INSERT INTO `members` (`id`, `uuid`, `name`, `telp`, `email`, `jenis_kelamin`, `department_id`, `position`, `alamat`, `kota_id`, `kecamatan_id`, `kelurahan_id`, `register_date`, `status_id`, `created_at`, `updated_at`) VALUES (11, 'cac24ab8-a899-4abd-8657-f0d4f775636a', 'ELEARNING', '08127970084', 'yutdhidev22@gmail.com', 'Perempuan', 1, NULL, 'Loream Ipsum', NULL, NULL, NULL, NULL, 1, '2023-06-15 09:15:07', '2023-06-15 09:15:07');
COMMIT;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_func` varchar(50) NOT NULL,
  `fa_icon` varchar(50) DEFAULT NULL,
  `display_name` varchar(100) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sort_menu` int(11) DEFAULT NULL,
  `current` int(11) DEFAULT NULL,
  `status_active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (1, '#', 'bx bx-home-circle', 'Menu', NULL, 0, 1, 1, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (2, '#', 'bx bx-home-circle', 'Data Master', NULL, 0, 2, 1, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (3, '#', 'bx bx-home-circle', 'Transaksi', NULL, 0, 3, 1, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (4, 'dashboard', 'bx bx-home-circle', 'Dashboard', NULL, 1, 1, 0, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (5, 'category', 'bx bx-menu', 'Kategori', NULL, 2, 1, 0, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (6, 'users', 'bx bx-user-plus', 'Users', NULL, 2, 1, 0, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (7, 'client', 'bx bx-user', 'Client', NULL, 2, 2, 0, 0);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (8, 'contacts', 'bx bxs-contact', 'Contacts', NULL, 2, 3, 0, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (9, 'abouts', 'bx bx-info-circle', 'Abouts', NULL, 2, 4, 0, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (10, 'upload_file', 'bx bx-menu', 'Upload File', NULL, 3, 1, 0, 1);
INSERT INTO `menu` (`id`, `url_func`, `fa_icon`, `display_name`, `desc`, `parent_id`, `sort_menu`, `current`, `status_active`) VALUES (11, 'download_file', 'bx bx-menu', 'Download File', NULL, 3, 2, 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `desc` text,
  `status_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
BEGIN;
INSERT INTO `roles` (`id`, `name`, `desc`, `status_id`) VALUES (1, 'User', 'User', 1);
INSERT INTO `roles` (`id`, `name`, `desc`, `status_id`) VALUES (2, 'Administrator', 'Administrator', 1);
INSERT INTO `roles` (`id`, `name`, `desc`, `status_id`) VALUES (3, 'Teknisi', 'Teknisi', 1);
COMMIT;

-- ----------------------------
-- Table structure for roles_access
-- ----------------------------
DROP TABLE IF EXISTS `roles_access`;
CREATE TABLE `roles_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles_access
-- ----------------------------
BEGIN;
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (1, 2, 1, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (2, 2, 2, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (3, 2, 3, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (4, 2, 4, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (5, 2, 5, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (6, 2, 6, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (7, 2, 7, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (8, 2, 8, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (9, 2, 9, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (10, 2, 10, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (11, 2, 11, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (12, 2, 12, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (14, 2, 14, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (15, 2, 15, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (16, 2, 16, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (17, 2, 17, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (18, 2, 18, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (19, 2, 19, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (20, 2, 20, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (21, 2, 21, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (22, 2, 22, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (23, 1, 1, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (24, 1, 4, 1);
INSERT INTO `roles_access` (`id`, `roles_id`, `menu_id`, `status_id`) VALUES (25, 1, 17, 1);
COMMIT;

-- ----------------------------
-- Table structure for ticket
-- ----------------------------
DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) DEFAULT NULL,
  `ticket_code` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `report_by` int(11) NOT NULL,
  `assign_by` int(11) NOT NULL,
  `desc` text,
  `filename` varchar(255) DEFAULT NULL,
  `ticket_date` date DEFAULT NULL,
  `open_date` datetime DEFAULT NULL,
  `close_date` datetime DEFAULT NULL,
  `ticket_status` varchar(100) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `teknisi_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket
-- ----------------------------
BEGIN;
INSERT INTO `ticket` (`id`, `uuid`, `ticket_code`, `category_id`, `subject`, `report_by`, `assign_by`, `desc`, `filename`, `ticket_date`, `open_date`, `close_date`, `ticket_status`, `status_id`, `created_by`, `teknisi_id`, `created_at`, `updated_at`) VALUES (1, 'e9a3d91a-9eea-477d-9187-dadd1a31bccx', 'TIC-98195', 1, 'Instalasi Network', 1, 3, 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 'fav.png', '2023-06-11', '2023-06-12 14:48:52', '2023-06-11 18:11:50', 'Pending', 1, 4, 8, '2023-06-15 09:56:54', '2023-06-15 09:56:54');
INSERT INTO `ticket` (`id`, `uuid`, `ticket_code`, `category_id`, `subject`, `report_by`, `assign_by`, `desc`, `filename`, `ticket_date`, `open_date`, `close_date`, `ticket_status`, `status_id`, `created_by`, `teknisi_id`, `created_at`, `updated_at`) VALUES (3, 'a1aa3f58-d2ed-452d-beea-3ec6f6b1814c', 'TIC-98195', 2, 'Instalasi Perangkat Lunak', 1, 3, 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 'fav.png', '2023-06-11', '2023-06-12 14:52:24', NULL, 'Pending', 1, 4, 8, '2023-06-15 09:56:54', '2023-06-15 09:56:54');
INSERT INTO `ticket` (`id`, `uuid`, `ticket_code`, `category_id`, `subject`, `report_by`, `assign_by`, `desc`, `filename`, `ticket_date`, `open_date`, `close_date`, `ticket_status`, `status_id`, `created_by`, `teknisi_id`, `created_at`, `updated_at`) VALUES (4, '0e5fa8d3-ddf5-4c48-8f52-6a87678852c2', 'TIC-98195', 4, 'Aplikasi Error', 1, 3, 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 'fav.png', '2023-06-11', '2023-06-12 14:57:07', '2023-06-12 14:57:10', 'Close', 1, 4, 8, '2023-06-15 09:56:54', '2023-06-15 09:56:54');
INSERT INTO `ticket` (`id`, `uuid`, `ticket_code`, `category_id`, `subject`, `report_by`, `assign_by`, `desc`, `filename`, `ticket_date`, `open_date`, `close_date`, `ticket_status`, `status_id`, `created_by`, `teknisi_id`, `created_at`, `updated_at`) VALUES (5, '963746f0-a8b0-46a1-b62c-3fce5f0cca04', 'TIC-98195', 3, 'Pemasangan Kabel LAN', 1, 2, 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 'fav.png', '2023-06-11', '2023-06-13 21:13:46', '2023-06-13 21:16:11', 'Close', 1, 4, 8, '2023-06-15 09:56:54', '2023-06-15 09:56:54');
INSERT INTO `ticket` (`id`, `uuid`, `ticket_code`, `category_id`, `subject`, `report_by`, `assign_by`, `desc`, `filename`, `ticket_date`, `open_date`, `close_date`, `ticket_status`, `status_id`, `created_by`, `teknisi_id`, `created_at`, `updated_at`) VALUES (6, 'a50ee717-61a0-499f-b634-56effb7835b4', 'TIC-98195', 2, 'Testing PC', 1, 3, 'test', 'fav (1).png', '2023-06-11', '2023-06-13 21:14:36', '2023-06-13 21:15:28', 'Close', 1, 4, 8, '2023-06-15 09:56:03', '2023-06-15 09:56:03');
INSERT INTO `ticket` (`id`, `uuid`, `ticket_code`, `category_id`, `subject`, `report_by`, `assign_by`, `desc`, `filename`, `ticket_date`, `open_date`, `close_date`, `ticket_status`, `status_id`, `created_by`, `teknisi_id`, `created_at`, `updated_at`) VALUES (7, '72fb338e-a96e-49e7-81db-9d1b7171e8bd', 'TIC-98195', 1, 'Error Aplikasi', 1, 3, 'Error Aplikasi', NULL, '2023-06-15', NULL, NULL, 'Waiting', 1, 4, NULL, '2023-06-15 09:56:54', '2023-06-15 09:56:54');
COMMIT;

-- ----------------------------
-- Table structure for ticket_detail
-- ----------------------------
DROP TABLE IF EXISTS `ticket_detail`;
CREATE TABLE `ticket_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` varchar(100) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `desc` text,
  `created_by` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket_detail
-- ----------------------------
BEGIN;
INSERT INTO `ticket_detail` (`id`, `ticket_id`, `filename`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (1, '1', 'fav.png', 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 4, 1, '2023-06-11 01:13:43', '2023-06-11 19:26:42');
INSERT INTO `ticket_detail` (`id`, `ticket_id`, `filename`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (2, '1', 'test.pdf', 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 8, 1, '2023-06-11 01:22:45', '2023-06-11 19:26:39');
INSERT INTO `ticket_detail` (`id`, `ticket_id`, `filename`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (3, '1', 'test.docx', 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 4, 1, '2023-06-11 12:22:51', '2023-06-11 19:26:45');
INSERT INTO `ticket_detail` (`id`, `ticket_id`, `filename`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (4, '1', 'fav.mp3', 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 8, 1, '2023-06-11 15:22:56', '2023-06-11 19:26:47');
INSERT INTO `ticket_detail` (`id`, `ticket_id`, `filename`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (5, '1', 'fav.png', 'There are many variations of passages of available, but the majority alteration in some form. As a highly skilled and successfull product development and design specialist with more than 4 Years of My experience.', 8, 1, '2023-06-11 19:13:43', '2023-06-11 19:13:43');
INSERT INTO `ticket_detail` (`id`, `ticket_id`, `filename`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (6, NULL, 'imgee.png', 'test', 4, 1, '2023-06-11 19:35:09', '2023-06-11 19:35:09');
INSERT INTO `ticket_detail` (`id`, `ticket_id`, `filename`, `desc`, `created_by`, `status_id`, `created_at`, `updated_at`) VALUES (7, '1', 'imgee.png', 'test', 4, 1, '2023-06-11 19:35:43', '2023-06-11 19:35:43');
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(50) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `roles_id` varchar(50) DEFAULT NULL,
  `members_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` (`id`, `uuid`, `username`, `fullname`, `password`, `roles_id`, `members_id`, `status_id`, `created_at`, `updated_at`) VALUES (1, 'e9a3d91a-9eea-477d-9187-dadd1a31bcca', 'admin', 'Admin', '$2y$10$POR1MqTAmVrV2/JpR2nJquH3NvXfpBxLpsrQ9wkDmXGAENVjzmo2W', '2', 2, 1, '2023-06-11 17:21:15', '2023-06-11 17:21:15');
INSERT INTO `users` (`id`, `uuid`, `username`, `fullname`, `password`, `roles_id`, `members_id`, `status_id`, `created_at`, `updated_at`) VALUES (2, 'a01388a3-fe4e-4668-afd9-a822d03aad54', 'yudhi', 'YUDHI', '$2y$10$POR1MqTAmVrV2/JpR2nJquH3NvXfpBxLpsrQ9wkDmXGAENVjzmo2W', '1', 4, 1, '2023-06-15 09:54:27', '2023-06-15 09:54:27');
INSERT INTO `users` (`id`, `uuid`, `username`, `fullname`, `password`, `roles_id`, `members_id`, `status_id`, `created_at`, `updated_at`) VALUES (3, '3bc9878e-aeee-4058-89ee-a70a1eb44a69', 'teknisi', 'Andi', '$2y$10$POR1MqTAmVrV2/JpR2nJquH3NvXfpBxLpsrQ9wkDmXGAENVjzmo2W', '3', 8, 1, '2023-06-11 17:17:19', '2023-06-11 17:17:19');
INSERT INTO `users` (`id`, `uuid`, `username`, `fullname`, `password`, `roles_id`, `members_id`, `status_id`, `created_at`, `updated_at`) VALUES (4, '34fb8533-e241-4cbf-af29-46a973945efc', NULL, 'YUDHI', '$2y$10$PYJgkhOCgbEK4F.yh4s8heMb9NsL7bE6AipxIik6h8okTy48x7kPq', '1', 10, 1, '2023-06-15 09:14:20', '2023-06-15 09:14:20');
INSERT INTO `users` (`id`, `uuid`, `username`, `fullname`, `password`, `roles_id`, `members_id`, `status_id`, `created_at`, `updated_at`) VALUES (5, '08e68272-7875-4ea4-abc9-f2f2e602b33f', 'yutdhidev22@gmail.com', 'ELEARNING', '$2y$10$.HDquvLe4M87XaRL.sN2q.qPH73DzcqD5GeM4xEpVl25cYfiYoI4G', '1', 11, 1, '2023-06-15 09:15:08', '2023-06-15 09:15:08');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
